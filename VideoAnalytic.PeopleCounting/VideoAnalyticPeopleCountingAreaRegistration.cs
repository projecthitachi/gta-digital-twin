﻿// MVCPluggableDemo.BacnetRaspberryAreaRegistration
using System;
using System.Web.Mvc;
using uPLibrary.Networking.M2Mqtt;
using VideoAnalytic.PeopleCounting;
using VideoAnalytic.PeopleCounting.Controllers;
using Common;
using CVCoreLib;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using VideoAnalytic.PeopleCounting.Models.System;
using System.Threading;
using System.Configuration;

namespace MVCPluggableDemo
{
    public class VideoAnalyticPeopleCountingAreaRegistration : AreaRegistration
    {
        public static string _MQTTTopic = "VideoAnalytic/PeopleCounting/#";
        public override string AreaName => "VideoAnalyticPeopleCounting";
        public Thread RunningThread;
        
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute("VideoAnalyticPeopleCounting_default", "VideoAnalyticPeopleCounting/{controller}/{action}/{id}", new
            {
                controller = "VideoAnalyticPeopleCounting_default",
                action = "Index",
                id = UrlParameter.Optional
            }, new string[1]
            {
            "VideoAnalyticPeopleCounting.Controllers"
            });

            CreateMqtt();
            BindHub();
        }

        public void BindHub()
        {
            RunningThread = new Thread(BindHubReal);
            RunningThread.Start();
        }

        public void BindHubReal()
        {
            while(Constants._Library == null || Constants._Library.MyHubObject == null)
            {
                Thread.Sleep(100);
            }

            //Constants._Library.OnCountPeople += CountPeople;
            //Constants._Library.OnPeopleAmountChangeMax += PeopleAmountChangeMax;
            //Constants._Library.OnPeopleAmountChangeMin += PeopleAmountChangeMin;
            //Constants._Library.OnSetImage += SetImage;
            //Constants._Library.OnSetImageBackground += SetImageBackground;
        }

        public void CreateMqtt()
        {
            try
            {
                Constants.MqttClient = new MqttClient(ConfigurationManager.AppSettings["server_mqtt"].ToString());
                Constants.MqttClient.Subscribe(new string[1]
                {
                    _MQTTTopic
                }, new byte[1]
                {
                    2
                });
                string clientId = Guid.NewGuid().ToString();
                //if (Constants.MqttClient != null) Constants.MqttClient.Connect(clientId);
            }
            catch (Exception ex)
            {
                LibraryTools.WriteName("CreateMqtt", ex);
            }
        }

        public static void CountPeople(string cameraid, int people)
        {
            try
            {
                if (Constants._Library == null) return;

                string msg = Newtonsoft.Json.JsonConvert.SerializeObject(Constants._Library.MyHubObject);
                byte[] message = System.Text.Encoding.ASCII.GetBytes(msg);
                if (Constants.MqttClient != null) Constants.MqttClient.Publish(_MQTTTopic, message);
            }
            catch (Exception ex)
            {
                LibraryTools.WriteName("CountPeople", ex);
            }
        }

        public static void SetImage(string cameraid, TagObject to)
        {
            try
            {
                if (Constants._Library == null) return;

                int maxItem = 10;
                if (Constants._Library.MyHubObject.DetectedImage == null)
                    Constants._Library.MyHubObject.DetectedImage = new List<string>();
                
                string fileContent = Convert.ToBase64String(System.IO.File.ReadAllBytes(to.ImageName));
                string str = "data:image/jpeg;base64," + fileContent;

                Constants._Library.MyHubObject.DetectedImage.Insert(0, str);

                if (Constants._Library.MyHubObject.DetectedImage.Count > maxItem)
                    Constants._Library.MyHubObject.DetectedImage.RemoveRange(maxItem, Constants._Library.MyHubObject.DetectedImage.Count - maxItem);
                
                string msg = Newtonsoft.Json.JsonConvert.SerializeObject(Constants._Library.MyHubObject);
                byte[] message = System.Text.Encoding.ASCII.GetBytes(msg);
                if (Constants.MqttClient != null) Constants.MqttClient.Publish(_MQTTTopic, message);
            }
            catch (Exception ex)
            {
                LibraryTools.WriteName("SetImage", ex);
            }
        }

        public static void SetImageBackground(string cameraid, TagObject to)
        {
            try
            {
                if (Constants._Library == null) return;

                string fileContent = Convert.ToBase64String(System.IO.File.ReadAllBytes(to.ImageBackground));
                string str = "data:image/jpeg;base64," + fileContent;

                Constants._Library.MyHubObject.CVCoreImage = str;

                string msg = Newtonsoft.Json.JsonConvert.SerializeObject(Constants._Library.MyHubObject);
                byte[] message = System.Text.Encoding.ASCII.GetBytes(msg);
                if (Constants.MqttClient != null) Constants.MqttClient.Publish(_MQTTTopic, message);
            }
            catch (Exception ex)
            {
                LibraryTools.WriteName("SetImage", ex);
            }
        }


        public static void PeopleAmountChangeMax(CVCoreObject obj, string cameraid, int count, int max)
        {
            if (Constants._Library == null) return;

            SqlVA_ALERTDAL dal = new SqlVA_ALERTDAL();
            
            foreach (string camid in Constants._Library._Config.GetCameraID())
            {
                if (camid != cameraid.ToString()) continue;

                VA_ALERT al = new VA_ALERT();
                al.ZoneID = camid.ToString();
                al.FrameUrl = obj.frame_url;
                al.CREATED_DT = DateTime.Now;
                al.CreateDate = al.CREATED_DT;
                al.CreateOfficer = "SYSTEM";
                al.UpdateDate = al.CREATED_DT;
                al.UpdateOfficer = "SYSTEM";
                al.ViewDate = DateTime.MinValue;
                al.AlertDate = al.CreateDate;
                al.Epc = count + "/" + max;
                al.Description = "CameraID " + cameraid + " has " + count + " person. Exceed maximum person " + max.ToString();
                dal.Insert(al);

                try
                {
                    Constants._Library.MyHubObject.AlertCount = dal.GetVAAlertCount();

                    string msg = Newtonsoft.Json.JsonConvert.SerializeObject(Constants._Library.MyHubObject);
                    byte[] message = System.Text.Encoding.ASCII.GetBytes(msg);
                    Constants.MqttClient.Publish(_MQTTTopic, message);
                }
                catch (Exception ex) { LibraryTools.WriteName("PeopleAmountChangeMax", ex); }
                break;
            }
        }

        public static void PeopleAmountChangeMin(CVCoreObject obj, string cameraid, int count, int min)
        {
            if (Constants._Library == null) return;

            SqlVA_ALERTDAL dal = new SqlVA_ALERTDAL();

            foreach (string camid in Constants._Library._Config.GetCameraID())
            {
                if (camid != cameraid.ToString()) continue;

                VA_ALERT al = new VA_ALERT();
                al.ZoneID = camid.ToString();
                al.CREATED_DT = DateTime.Now;
                al.CreateDate = al.CREATED_DT;
                al.CreateOfficer = "SYSTEM";
                al.UpdateDate = al.CREATED_DT;
                al.UpdateOfficer = "SYSTEM";
                al.ViewDate = DateTime.MinValue;
                al.AlertDate = al.CreateDate;
                al.Epc = count + "/" + min;
                al.Description = "CameraID " + cameraid + " has " + count + " person. Minimum person " + min.ToString();
                dal.Insert(al);

                try
                {
                    Constants._Library.MyHubObject.AlertCount = dal.GetVAAlertCount();

                    string msg = Newtonsoft.Json.JsonConvert.SerializeObject(Constants._Library.MyHubObject);
                    byte[] message = System.Text.Encoding.ASCII.GetBytes(msg);
                    Constants.MqttClient.Publish(_MQTTTopic, message);
                }
                catch (Exception ex) { LibraryTools.WriteName("PeopleAmountChangeMax", ex); }
                break;
            }
        }
    }
}