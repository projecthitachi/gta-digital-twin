﻿// Bacnet.Raspberry.Models.Raspberry_REC
using System;
using System.ComponentModel.DataAnnotations;

namespace VideoAnalytic.PeopleCounting.Models
{
    public class VAPeopleCounting_REC
    {
        public long RecordID
        {
            get;
            set;
        }

        public DateTime RecordTimestamp
        {
            get;
            set;
        }

        public int RecordStatus
        {
            get;
            set;
        }

        [Key]
        public string DeviceID
        {
            get;
            set;
        }

        public string DeviceName
        {
            get;
            set;
        }

        public string CategoryID
        {
            get;
            set;
        }

        public string IPAddress
        {
            get;
            set;
        }

        public string MacAddress
        {
            get;
            set;
        }

        public string SSID
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string MQTTHost
        {
            get;
            set;
        }

        public string MQTTUsername
        {
            get;
            set;
        }

        public string MQTTPassword
        {
            get;
            set;
        }

        public string StatusDevice
        {
            get;
            set;
        }

        public string StatusSwitch
        {
            get;
            set;
        }

        public int Live
        {
            get;
            set;
        }
    }
}