﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using MvcPaging;
using Common;

namespace CVCoreLib
{
    [Serializable]
    [Table(Name = "Lanes")]
    public class Lanes : CommonModel
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public override string Id{ get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int LaneID { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int CameraID { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int XTop { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int YTop { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int XBottom { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int YBottom { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Manager { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string MobileNo { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Email { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string CreateOfficer { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime CreateDate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string UpdateOfficer { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime UpdateDate { get; set; }

    }
}
