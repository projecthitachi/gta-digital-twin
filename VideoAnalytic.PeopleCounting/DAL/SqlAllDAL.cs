﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using MvcPaging;
using System.Data.SqlClient;
using System.Data;

namespace CVCoreLib
{
    public class SqlSettingDAL : SqlCommonDALFree<Setting>
    {
        public SqlSettingDAL() : base(new string[] { "Id" })
        {
            SortColumns = "CreateDate DESC ";
        }
    }

    public class SqlLanesStatusDAL : SqlCommonDALFree<LanesStatus>
    {
        public SqlLanesStatusDAL() : base(new string[] { "Id" })
        {
            SortColumns = "CreateDate DESC ";
        }
    }

    public class SqlLanesDAL : SqlCommonDALFree<Lanes>
    {
        public SqlLanesDAL() : base(new string[] { "Id" })
        {
            SortColumns = "CreateDate DESC ";
        }
    }

    public class SqlCVCoreObjectDAL : SqlCommonDALFree<CVCoreObjectDB>
    {
        public SqlCVCoreObjectDAL() : base(new string[] { "Id" })
        {
            SortColumns = "ID DESC ";
        }
    }

    public class SqlCVCoreShowPathCameraObjectDAL : SqlCommonDALFree<CVCoreShowPathCameraObject>
    {
        public SqlCVCoreShowPathCameraObjectDAL() : base(new string[] { "Id" })
        {
            SortColumns = "camera_id DESC ";
        }
    }

    public class SqlCVCorePeopleExitDAL : SqlCommonDALFree<CVCorePeopleExit>
    {
        public SqlCVCorePeopleExitDAL() : base(new string[] { "Id" })
        {
            SortColumns = "CreateDate DESC ";
        }
    }
}
