﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace Common
{
    public class SqlVA_ALERTDAL : SqlCommonDALFree<VA_ALERT>
    {
        public SqlVA_ALERTDAL() : base(new string[] { "Id" })
        {
            SortColumns = "AlertDate DESC";
        }

        public int GetVAAlertCount()
        {
            string sql = "SELECT COUNT(*) FROM VA_Alert a WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000) AND ZoneID IS NOT NULL";
            DataSet ds = Query.Builder.ExecuteDataSet(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) return 0;
            int c = 0;
            int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out c);
            return c;
        }

        public int GetEnergyAlertCount()
        {
            string sql = "SELECT COUNT(*) AS CNT FROM Alerts WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000)";
            DataSet ds = Query.Builder.ExecuteDataSet(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) return 0;
            int c = 0;
            int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out c);
            return c;
        }

        public int GetPeopleCount(string id)
        {
            string sql = "SELECT * FROM VA_ALERT WHERE Id=@Id";
            List<System.Data.IDataParameter> prms = new List<IDataParameter>();
            prms.Add(new SqlParameter("@Id", id));
            VA_ALERT al = Query.Builder.ExecuteSingle(sql, typeof(VA_ALERT), prms);
            if (al == null) return 0;
            return al.PeopleCount;
        }

        public List<string> GetAlertId()
        {
            List<VA_ALERT> list = GetAlertList();
            List<string> result = new List<string>();
            foreach(VA_ALERT va in list)
            {
                string p = va.VideoPath;
                int o = p.LastIndexOf("\\");
                if (o >= 0) p = p.Substring(o + 1);

                string str = va.Id + "_" + va.Description + "_" + p;
                result.Add(str);
            }
            return result;
        }

        public List<VA_ALERT> GetAlertList()
        {
            string sql = "SELECT TOP 4 * FROM VA_ALERT WHERE VideoPath IS NOT NULL AND Year(RecordStart) > 2000 ORDER BY CreateDate DESC";
            List<VA_ALERT> result = Query.Builder.ExecuteList(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());

            sql = "SELECT * FROM VA_ALERT WHERE Id='12ZHCLXUUO6R41GNYC3CLQV6'";
            VA_ALERT result2 = Query.Builder.ExecuteSingle(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());

            if (result2 != null) result.Insert(0, result2);
            return result;
        }

        public void AcknowledgeAll()
        {
            string sql = "UPDATE VA_Alert SET ViewDate=GETDATE() WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000)";
            Query.Builder.ExecuteNonQuery(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
        }

        public void Acknowledge(string al)
        {
            List<System.Data.IDataParameter> prms = new List<System.Data.IDataParameter>();
            prms.Add(new System.Data.SqlClient.SqlParameter("@Id", al ?? ""));
            string sql = "UPDATE VA_Alert SET ViewDate=GETDATE() WHERE Id=@Id";
            Query.Builder.ExecuteNonQuery(sql, typeof(VA_ALERT), prms);
        }
    }

    public class SqlVA_ALERTDETAILDAL : SqlCommonDALFree<VA_ALERTDETAIL>
    {
        public SqlVA_ALERTDETAILDAL() : base(new string[] { "Id" })
        {
            SortColumns = "CreateDate ASC";
        }
    }

    public class SqlVA_ALERTDETAIL2DAL : SqlCommonDALFree<VA_ALERTDETAIL2>
    {
        public SqlVA_ALERTDETAIL2DAL() : base(new string[] { "Id" })
        {
            SortColumns = "CreateDate ASC";
        }

    }
}