﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;

namespace Common
{
    public class SqlQueryBuilder<T> : QueryBuilderBase<T>, IQueryBuilder<T> where T:class, new()
    {
        public SqlQueryBuilder()
        {
            DBConnString = ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString;
        }

        public SqlQueryBuilder(IDbConnection connection, IDbTransaction transaction) : base(connection, transaction)
        {

        }

        public override IDbConnection GetConnection(string str)
        {
            return new SqlConnection(str);
        }

        public override IDbCommand GetCommand(string sql, IDbConnection conn)
        {
            return new SqlCommand(sql, (SqlConnection) conn);
        }

        public override IDataAdapter GetAdapter(IDbCommand com)
        {
            return new SqlDataAdapter((SqlCommand)com);
        }

        public override string[] GetColumnsType(Type type, string prefix, bool includedbgen = true)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);
                if (ts == null) continue;
                if (((ColumnAttribute)ts).IsDbGenerated && includedbgen == false) continue;
                string tt = "NVARCHAR(MAX)";
                if (t.PropertyType == typeof(int))
                    tt = "INT";
                if (t.PropertyType == typeof(byte[]))
                    tt = "VARBINARY(MAX)";
                if (t.PropertyType == typeof(long))
                    tt = "BIGINT";
                if (t.PropertyType == typeof(float))
                    tt = "DOUBLE";
                if (t.PropertyType == typeof(decimal))
                    tt = "DECIMAL";
                if (t.PropertyType == typeof(bool))
                    tt = "BIT";
                if (t.PropertyType == typeof(DateTime))
                    tt = "DATETIME";
                if (t.PropertyType == typeof(char))
                    tt = "NCHAR(1)";
                columns.Add(prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name) + "] " + tt);
            }
            if (columns.Count == 0)
            {
            }
            return columns.ToArray();
        }

        public override IDataParameter GetSqlParam(string propertyname, T obj)
        {
            IDataParameter param = new SqlParameter();
            param.Value = Convert.DBNull;
            param.ParameterName = "@" + propertyname;
            param.DbType = DbType.String;
            ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.NVarChar;
            
            PropertyInfo pi = obj.GetType().GetProperty(propertyname);
            if (pi == null) return param;

            Type t = pi.PropertyType;
            if (t == typeof(int))
            {
                param.DbType = DbType.Int32;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Int;
                param.Value = MyReflection.GetValue<int>(obj, propertyname);
            }
			else if (t == typeof(byte[]))
            {
                param.DbType = DbType.Binary;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.VarBinary;
                param.Value = MyReflection.GetValue<byte[]>(obj, propertyname);
            }
            else if (t == typeof(double))
            {
                param.DbType = DbType.Double;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Float;
                param.Value = MyReflection.GetValue<float>(obj, propertyname);
            }
            else if (t == typeof(float))
            {
                param.DbType = DbType.Decimal;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Decimal;
                param.Value = MyReflection.GetValue<decimal>(obj, propertyname);
            }
            else if (t == typeof(decimal))
            {
                param.DbType = DbType.Decimal;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Decimal;
                param.Value = MyReflection.GetValue<decimal>(obj, propertyname);
            }
            else if (t == typeof(long))
            {
                param.DbType = DbType.Int64;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.BigInt;
                param.Value = MyReflection.GetValue<long>(obj, propertyname);
            }
            else if (t == typeof(string))
            {
                param.DbType = DbType.String;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.NVarChar;
                param.Value = MyReflection.GetValue<string>(obj, propertyname) ?? Convert.DBNull;
            }
            else if (t == typeof(bool))
            {
                param.DbType = DbType.Byte;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Bit;
                param.Value = MyReflection.GetValue<bool>(obj, propertyname);
            }
            else if (t == typeof(DateTime))
            {
                param.DbType = DbType.DateTime;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.DateTime;
                param.Value = MyReflection.GetValue<DateTime>(obj, propertyname);
            }
            else if (t == typeof(bool?))
            {
                param.DbType = DbType.Byte;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Bit;
                param.Value = MyReflection.GetValue<object>(obj, propertyname) ?? Convert.DBNull;
            }
            else if (t == typeof(DateTime?))
            {
                param.DbType = DbType.DateTime;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.DateTime;
                param.Value = MyReflection.GetValue<object>(obj, propertyname) ?? Convert.DBNull;
            }

            return param;
        }

        public List<T> GetAll()
        {
            Type type = typeof(T);
            string col = ListColumn(type, "", false, true, true);
            if (string.IsNullOrEmpty(col)) col = "*";
            string sql = "SELECT " + col + " FROM [" + GetTableName(type) + "]";

            return ExecuteList(sql, type, new List<IDataParameter>());
        }

        public T Get(string Id)
        {
            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = typeof(T);
            string col = ListColumn(type, "", false, true, true);
            if (string.IsNullOrEmpty(col)) col = "*";
            string[] primaryKeys = GetPKColumnsArray(typeof(T), "", false, false, false);
            if (primaryKeys == null || primaryKeys.Length == 0)
                prms.Add(new SqlParameter("@Id", Id ?? Convert.DBNull));
            else
                prms.Add(new SqlParameter("@" + primaryKeys[0], Id ?? Convert.DBNull));
            string sql = "SELECT " + col + " FROM [" + GetTableName(type) + "] WHERE " + primaryKeys[0] + "=@" + primaryKeys[0];

            return ExecuteSingle(sql, type, prms);
        }

        public long Count()
        {
            Type type = typeof(T).GetType();
            string sql = "SELECT COUNT(*) AS RowCount FROM [" + GetTableName(type) + "]";

            SqlQueryBuilder<CountRow> dal = new SqlQueryBuilder<CountRow>();
            CountRow obj = dal.ExecuteSingle(sql, type, new List<IDataParameter>());
            return obj.RowCount;
        }

        public T Get(T obj)
        {
            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = obj.GetType();
            string pk = FillPKColumn(type, obj, prms, " AND ");
            string col = ListColumn(type, "", false, true, true);
            if (string.IsNullOrEmpty(col)) col = "*";
            string sql = "SELECT " + col + " FROM [" + GetTableName(type) + "] WHERE " + pk;

            return ExecuteSingle(sql, type, prms);
        }

        public T GetByMany(string type, string[] column, object[] values)
        {
            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                columndouble.Add(col + " = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "SELECT " + ListColumn(typeof(T), "", false, true, true) + " FROM [" + GetTableName(typeof(T)) + "] WHERE " + pk;

            return ExecuteSingle(sql, typeof(T), prms);
        }

        public List<T> GetManyByMany(string type, string[] column, object[] values) 
        {
            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                columndouble.Add(col + " = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "SELECT " + ListColumn(typeof(T), "", false, true, true) + " FROM [" + GetTableName(typeof(T)) + "] WHERE " + pk;

            return ExecuteList(sql, typeof(T), prms);
        }

        public int DeleteByMany(string type, string[] column, object[] values) 
        {
            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                columndouble.Add(col + " = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "DELETE FROM [" + GetTableName(typeof(T)) + "] WHERE " + pk;

            return ExecuteNonQuery(sql, typeof(T), prms);
        }

        public int DeleteByManyTransaction(string type, string[] column, object[] values)
        {
            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                columndouble.Add("[" + col + "] = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "DELETE FROM [" + GetTableName(typeof(T)) + "] WHERE " + pk;

            return ExecuteNonQueryTransaction(sql, typeof(T), prms);
        }

        public List<T> GetPage(T obj, int page, string sort, string otherCriteria = "") 
        {
            string where = string.Empty;

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;
            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = obj.GetType();
            string sql2 = @"SELECT * FROM
(
SELECT ROW_NUMBER() OVER (ORDER BY " + sort + @") ROWID, a.* FROM [" + GetTableName(type) + @"] a
" + where + @"
) as tb
WHERE tb.ROWID BETWEEN (((@PageIndex - 1) * @PageSize) + 1) AND @PageIndex * @PageSize
ORDER BY " + sort;

            prms.Add(new SqlParameter("@PageIndex", page));
            prms.Add(new SqlParameter("@PageSize", 30));

            return ExecuteList(sql2, type, prms);
        }

        public long CountPage(T obj, string sort, string otherCriteria = "")
        {
            string where = string.Empty;

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;

            List<IDataParameter> prms = new List<IDataParameter>();

            string sql2 = @"SELECT COUNT(*) AS [RowCount] FROM
(
SELECT ROW_NUMBER() OVER (ORDER BY " + sort + @") ROWID, a.* FROM [" + GetTableName(typeof(T)) + @"] a
" + where + @"
) as tb";

            SqlQueryBuilder<CountRow> dal = new SqlQueryBuilder<CountRow>();
            CountRow obj2 = dal.ExecuteSingle(sql2, typeof(CountRow), prms);
            return obj2.RowCount;
        }

        public string GetSPSearchA(Type t)
        {
            PropertyInfo[] cols = t.GetProperties();
            List<PKColumn> pkcols = GetPKColumns(t, "", false, false, false).ToList();
            string result = string.Empty;
            foreach (var pi in cols)
            {
                if (pi.GetCustomAttribute(typeof(ColumnAttribute)) == null) continue;
                ColumnAttribute ca = pi.GetCustomAttribute(typeof(ColumnAttribute)) as ColumnAttribute;
                Type tp = pi.PropertyType;
                if (tp == typeof(int) || tp == typeof(double) || tp == typeof(decimal) || tp == typeof(long))
                {
                    result += "CAST(a.[" + (ca.Name ?? pi.Name) + "] AS VARCHAR) LIKE b.val OR ";
                }
                else if (tp == typeof(string))
                {
                    result += "a.[" + (ca.Name ?? pi.Name) + "] LIKE b.val OR ";
                }
                else if (tp == typeof(DateTime))
                {
                    result += "(CONVERT(CHAR(50), a.[" + (ca.Name ?? pi.Name) + "], 113) LIKE b.val) OR ";
                }
            }
            if (result.Length > 0) result = result.Substring(0, result.Length - 4);
            if (result.Length > 0) return " JOIN dbo.Split(@MakeSearchTextLike,',') b ON " + result + "";
            return string.Empty;
        }

        public string GetSPSearchB(Type t)
        {
            PropertyInfo[] cols = t.GetProperties();
            string result = string.Empty;
            foreach (var pi in cols)
            {
                if (pi.GetCustomAttribute(typeof(ColumnAttribute)) == null) continue;
                ColumnAttribute ca = pi.GetCustomAttribute(typeof(ColumnAttribute)) as ColumnAttribute;
                var nettype = pi.PropertyType;

                if (nettype == typeof(int) || nettype == typeof(double) || nettype == typeof(decimal) || nettype == typeof(long))
                {
                    result += "CAST(a.[" + (ca.Name ?? pi.Name) + "] AS VARCHAR) LIKE b.val OR ";
                }
                else if (nettype == typeof(string))
                {
                    result += "a.[" + (ca.Name ?? pi.Name) + "] LIKE b.val OR ";
                }
                else if (nettype == typeof(DateTime))
                {
                    result += "(CONVERT(CHAR(11), a.[" + (ca.Name ?? pi.Name) + "], 106) LIKE b.val) OR ";
                }
            }
            if (result.Length > 0) result = result.Substring(0, result.Length - 4);
            if (result.Length > 0) return " JOIN dbo.Split(@MakeSearchTextLike,',') b ON " + result + "";
            return string.Empty;
        }

        public List<T> GetPageSearch(T obj, string searchtext, int page, string sort, string otherCriteria = "")
        {
            string where = string.Empty;

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;

            string over = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) over = ", " + MakeOver("b.", sort);
            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = obj.GetType();
            string sql2 = @" 
        DECLARE @MakeSearchTextLike AS NVARCHAR(1000)
        SET @MakeSearchTextLike = dbo.MakeSearchTextLike(@SearchText)
        DECLARE @RESULT TABLE (ROWID BIGINT, CNT BIGINT, " + ListColumnType(type, "", true) + @")
        INSERT INTO @RESULT
        SELECT * FROM
        (
			SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC " + over + @") ROWID, a.CNT, b.* FROM 
			(
				SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT FROM dbo.[" + GetTableName(type) + @"] a " + GetSPSearchA(type) + @"
                " + where + @"
				GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
			) a LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
        ) tb
		WHERE tb.ROWID BETWEEN (((@PageIndex - 1) * @PageSize) + 1) AND @PageIndex * @PageSize
		ORDER BY tb.CNT DESC
        
		DECLARE @TotalRow BIGINT
		SELECT @TotalRow=COUNT(*) FROM @RESULT
		IF ISNULL(@TotalRow, 0) = 0
		BEGIN
            INSERT INTO @RESULT
            SELECT * FROM
            (
			    SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC" + over + @") ROWID, a.CNT, b.* FROM 
			    (
				    SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT FROM dbo.[" + GetTableName(type) + @"] a " + GetSPSearchB(type) + @"
                    " + where + @"
				    GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
			    ) a LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
            ) tb
		    WHERE tb.ROWID BETWEEN (((@PageIndex - 1) * @PageSize) + 1) AND @PageIndex * @PageSize
		ORDER BY tb.CNT DESC
        END
        SELECT * FROM @RESULT";

            if (string.IsNullOrEmpty(sort) == false)
                sql2 += " ORDER BY " + sort;

            prms.Add(new SqlParameter("@SearchText", searchtext));
            prms.Add(new SqlParameter("@PageIndex", page));
            prms.Add(new SqlParameter("@PageSize", 30));

            return ExecuteList(sql2, type, prms);
        }

        public long CountPageSearch(T obj, string searchtext, string sort, string otherCriteria = "") 
        {
            string where = string.Empty;

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;

            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = obj.GetType();
            string sql2 = @" 
        DECLARE @MakeSearchTextLike AS NVARCHAR(1000)
        SET @MakeSearchTextLike = dbo.MakeSearchTextLike(@SearchText)
        DECLARE @RESULT TABLE (ROWID BIGINT, CNT BIGINT, " + ListColumnType(type, "", true) + @")
        INSERT INTO @RESULT
        SELECT * FROM
        (
			SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC) ROWID, a.CNT, b.* FROM 
			(
				SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT FROM dbo.[" + GetTableName(type) + @"] a " + GetSPSearchA(type) + @"
				" + where + @"
                GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
			) a LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
        ) tb
		ORDER BY tb.CNT DESC
        
		DECLARE @TotalRow BIGINT
		SELECT @TotalRow=COUNT(*) FROM @RESULT
		IF ISNULL(@TotalRow, 0) = 0
		BEGIN
            INSERT INTO @RESULT
            SELECT * FROM
            (
			    SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC) ROWID, a.CNT, b.* FROM 
			    (
				    SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT FROM dbo.[" + GetTableName(type) + @"] a " + GetSPSearchB(type) + @"
                    " + where + @"
				    GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
			    ) a LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
            ) tb
		ORDER BY tb.CNT DESC
        END
        SELECT COUNT(*) AS [RowCount] FROM @RESULT";

            prms.Add(new SqlParameter("@SearchText", searchtext));

            DataSet obj2 = ExecuteDataSet(sql2, typeof(T), prms);
            if (obj2.Tables == null || obj2.Tables.Count <= 0 || obj2.Tables[0].Rows.Count == 0 || obj2.Tables[0].Columns.Count == 0)
                return 0;
            int temp = 0;
            int.TryParse(obj2.Tables[0].Rows[0][0].ToString(), out temp);
            return temp;
        }

        public int Delete(T obj)
        {
            QueryTransaction<T> build = new QueryTransaction<T>();
            build.BeginTransaction();
            try
            {
                List<IDataParameter> prms = new List<IDataParameter>();

                Type type = obj.GetType();
                string pk = FillPKColumn(type, obj, prms, " AND ");
                string sql = "DELETE FROM [" + GetTableName(type) + "] WHERE " + pk;

                int ret = build.Builder.ExecuteNonQueryTransaction(sql, type, prms);
                InsertAudit(build.Builder, "", "Delete", null, obj, "");
                build.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                build.RollbackTransaction();
                throw;
            }
        }

        public int DeleteTransaction(T obj)
        {
            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = obj.GetType();
            string pk = FillPKColumn(type, obj, prms, " AND ");
            string sql = "DELETE FROM [" + GetTableName(type) + "] WHERE " + pk;
            InsertAudit(this, "", "Delete", obj, null, "");
            return ExecuteNonQueryTransaction(sql, type, prms);
        }

        public int Insert(T obj)
        {
            object[] ids = PrimaryKeyValues(obj);

            List<PKColumn> PrimaryKeyColumns = GetPKColumns(typeof(T), "", false, false, false);
            if ((ids == null || (ids.Length == 1 && (ids[0] == null || ids[0].ToString() == ""))) & PrimaryKeyColumns != null && PrimaryKeyColumns.Count == 1 && PrimaryKeyColumns[0].PropertyName == "Id")
            {
                ids = new object[] { MyRandom.RandomString(24) };
                MyReflection.SetValue(obj, "Id", ids[0]);
            }
            FormatObject(obj);

            QueryTransaction<T> build = new QueryTransaction<T>();
            build.BeginTransaction();
            try
            {
                Type type = obj.GetType();

                List<IDataParameter> prms = FillColumnSingle(type, obj, false);
                string sql = "INSERT INTO [" + GetTableName(type) + "] (" + ListColumn(type, "", false, false, true) + ") VALUES (" + ListColumn(type, "@", false, false, false) + ")";

                List<IDataParameter> pks = new List<IDataParameter>();
                FillPKColumn(type, obj, pks, "");

                List<string> pk = new List<string>();
                foreach (var sq in pks)
                {
                    pk.Add(sq.ParameterName + "=" + sq.Value.ToString());
                }

                int ret = build.Builder.ExecuteNonQueryTransaction(sql, type, prms);
                InsertAudit(build.Builder, "", "Insert " + string.Join(",", pk.ToArray()), null, obj, "");
                build.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                build.RollbackTransaction();
                throw;
            }
        }

        public int InsertTransaction(T obj)
        {
            object[] ids = PrimaryKeyValues(obj);
            List<PKColumn> PrimaryKeyColumns = GetPKColumns(typeof(T), "", false, false, false);
            if ((ids == null || (ids.Length == 1 && (ids[0] == null || ids[0].ToString() == ""))) & PrimaryKeyColumns != null && PrimaryKeyColumns.Count == 1 && PrimaryKeyColumns[0].PropertyName == "Id")
            {
                ids = new object[] { MyRandom.RandomString(24) };
                MyReflection.SetValue(obj, "Id", ids[0]);
            }
            FormatObject(obj);

            Type type = obj.GetType();
            List<IDataParameter> prms = FillColumnSingle(type, obj, false);
            string sql = "INSERT INTO [" + GetTableName(type) + "] (" + ListColumn(type, "", false, false, true) + ") VALUES (" + ListColumn(type, "@", false, false, false) + ")";

            List<IDataParameter> pks = new List<IDataParameter>();
            FillPKColumn(type, obj, pks, "");

            List<string> pk = new List<string>();
            foreach (var sq in pks)
            {
                pk.Add(sq.ParameterName + "=" + sq.Value.ToString());
            }

            int ret = ExecuteNonQueryTransaction(sql, type, prms);
            InsertAudit(this, "", "Insert", null, obj, "");
            return ret;
        }

        public int Update(T obj)
        {
            FormatObject(obj);
            QueryTransaction<T> build = new QueryTransaction<T>();
            build.BeginTransaction();
            try
            {
                T oldObj = Get(obj);
                Type type = typeof(T);

                List<IDataParameter> prms = new List<IDataParameter>();
                string where = FillPKColumn(type, obj, prms, " AND ");
                string sql = "UPDATE [" + GetTableName(type) + "] SET " + FillColumn(type, obj, prms, false) + " WHERE " + FillPKColumn(type, obj, prms, " AND ");
                int ret = build.Builder.ExecuteNonQueryTransaction(sql, type, prms);

                InsertAudit(build.Builder, "", "Update", oldObj, obj, "");
                build.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                build.RollbackTransaction();
                throw;
            }
        }

        public int UpdateTransaction(T obj)
        {
            FormatObject(obj);
            T oldObj = Get(obj);
            Type type = typeof(T);

            List<IDataParameter> prms = new List<IDataParameter>();
            string where = FillPKColumn(type, obj, prms, " AND ");
            string sql = "UPDATE [" + GetTableName(type) + "] SET " + FillColumn(type, obj, prms, false) + " WHERE " + FillPKColumn(type, obj, prms, " AND ");
            int ret = ExecuteNonQueryTransaction(sql, type, prms);

            InsertAudit(this, "", "Update", oldObj, obj, "");

            return ret;
        }

        public void InsertAudit(IQueryBuilder<T> qb, string sentence, string commandtype, object old, object newobj, string officername)
        {
			if (ConfigurationManager.AppSettings["UseAudit"] == "N") return;
        }
    }
}
