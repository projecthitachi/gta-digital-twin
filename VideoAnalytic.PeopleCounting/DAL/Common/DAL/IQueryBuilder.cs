﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Common
{
    public interface IQueryBuilder<T> where T : class
    {
        string[] GetColumnsType(Type type, string prefix, bool includedbgen = true);
        IDataParameter GetSqlParam(string propertyname, T obj);
        string FillPKColumn(Type type, T obj, List<IDataParameter> prms, string prefix);
        List<IDataParameter> FillColumnSingle(Type type, T obj, bool includedbgen = true);
        string FillColumn(Type type, T obj, List<IDataParameter> prms, bool includeDBGen);
        List<T> GetAll();
        T Get(string Id);
        long Count();
        T Get(T obj);
        T GetByMany(string type, string[] column, object[] values);
        List<T> GetManyByMany(string type, string[] column, object[] values);
        int DeleteByMany(string type, string[] column, object[] values);
        int DeleteByManyTransaction(string type, string[] column, object[] values);
        List<T> GetPage(T obj, int page, string sort, string otherCriteria = "");
        long CountPage(T obj, string sort, string otherCriteria = "");
        string GetSPSearchA(Type t);
        string GetSPSearchB(Type t);
        List<T> GetPageSearch(T obj, string searchtext, int page, string sort, string otherCriteria = "");
        long CountPageSearch(T obj, string searchtext, string sort, string otherCriteria = "");
        int Delete(T obj);
        int DeleteTransaction(T obj);
        int Insert(T obj);
        int InsertTransaction(T obj);
        int Update(T obj);
        int UpdateTransaction(T obj);
        void InsertAudit(IQueryBuilder<T> qb, string sentence, string commandtype, object old, object newobj, string officername);
        T ExecuteSingle(string sql, Type t, List<IDataParameter> prms, bool includeReadonly = false);
        T ExecuteSingleTransaction(string sql, Type t, List<IDataParameter> prms);
        int ExecuteCommandNonQuery(string query, List<IDataParameter> prms);
        T ExecuteCommandSingle(string query, List<IDataParameter> prms);
        List<T> ExecuteCommandList(string query, List<IDataParameter> prms);
        int ExecuteNonQuery(string sql, Type t, List<IDataParameter> prms);
        DataSet ExecuteDataSet(string sql, Type t, List<IDataParameter> prms);
        int ExecuteNonQueryTransaction(string sql, Type t, List<IDataParameter> prms);
        List<T> ExecuteList(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false);
        List<T> ExecuteListSP(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false);
        List<T> ExecuteListTransaction(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false);
    }
}
