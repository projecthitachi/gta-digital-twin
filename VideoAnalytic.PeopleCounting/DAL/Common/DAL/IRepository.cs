﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcPaging;

namespace Common
{
    public interface IRepository<T>
    {
        long ClearDB();
        
        List<T> GetAll();

        long Count();

        long CountSearch(string searchText);

        List<T> Aggregate(int id, string[] query, string sortcolumn, string ascdesc);
        
        List<T> FindOr(int id, string match, string[] column, object[] value, string order, string sortby);

        List<T> FindOr(string match, string[] column, object[] value, string sort, string orderby);

        List<T> FindAnd(int id, string match, string[] column, object[] value, string order, string sortby);

        List<T> FindAnd(string match, string[] column, object[] value, string sort, string orderby);

        List<T> Find(int id, string match, string type, string[] columns, object[] values, string order = "", string sortby = "");

        PagedList<T> GetPage(int page);

        PagedList<T> GetPageSearch(string searchText, int page);

        T Get(string objectid);

        T Get(object objectid);

        long Insert(T d);

        string InsertReturnID(T d);

        long Delete(T d);

        long Delete(string objectid);

        long Update(T d);

        T GetBy(string column, object value);

        T GetByIn(string column, object value, string column2, object value2);

        T GetByMany(string[] column, object[] value);

        List<T> GetManyByMany(string[] column, object[] value);

        List<T> GetManyBy(string column, object value);

        long DeleteBy(string column, object value);

        long DeleteByMany(string[] column, object[] value);
    }
}
