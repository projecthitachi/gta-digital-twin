﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Data.Linq.Mapping;
using System.Data;
using System.Linq;
using System.Configuration;

namespace Common
{
    public class PKColumn
    {
        public string PropertyName { get; set; }
        public string ColumnName { get; set; }
    }

    public class QueryBuilderBase<T> where T : class, new()
    {
        public string DBConnString = string.Empty;
        public IDbConnection Connection { get; set; }
        public IDbTransaction Transaction { get; set; }

        public QueryBuilderBase()
        {

        }

        public QueryBuilderBase(IDbConnection conn, IDbTransaction tran)
        {
            DBConnString = ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString;
            Connection = conn;
            Transaction = tran;
        }

        public string GetTableName(Type type)
        {
            object[] temp = type.GetCustomAttributes(typeof(System.Data.Linq.Mapping.TableAttribute), true);

            if (temp.Length == 0) return string.Empty;
            var name = (temp[0] as System.Data.Linq.Mapping.TableAttribute).Name;
            if (string.IsNullOrEmpty(name))
                return type.Name;
            return name;
        }

        public string[] GetColumns(Type type, string prefix, bool includeReadColumn, bool includedbgen, bool includeBracket)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);
                Attribute readOnlyColumn = t.GetCustomAttribute(typeof(ReadOnlyColumnAttribute), true);
                if (ts == null && includeReadColumn == false) continue;
                if (includeReadColumn && readOnlyColumn != null)
                {
                    columns.Add(prefix + (((ReadOnlyColumnAttribute)readOnlyColumn).Name ?? t.Name));
                }
                else if (ts != null)
                {
                    if (((ColumnAttribute)ts).IsDbGenerated && includedbgen == false) continue;
                    if (includeBracket)
                        columns.Add(prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name + "]"));
                    else
                        columns.Add(prefix + (((ColumnAttribute)ts).Name ?? t.Name));
                }
            }
            if (columns.Count == 0)
            {
            }
            return columns.ToArray();
        }

        public List<PKColumn> GetPKColumns(Type type, string prefix, bool includedbgen, bool includeBracket, bool usePropertyName)
        {
            List<PKColumn> pkcolumns = new List<PKColumn>();
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);

                if (ts == null) continue;
                ColumnAttribute cs = (ColumnAttribute)ts;
                if (cs == null) continue;
                if (cs.IsPrimaryKey == false && cs.IsDbGenerated == true && includedbgen == false) continue;

                PKColumn pk = new PKColumn();
                pk.PropertyName = prefix + t.Name;
                pk.ColumnName = prefix + ((ColumnAttribute)ts).Name;
                if (string.IsNullOrEmpty(pk.ColumnName)) pk.ColumnName = pk.PropertyName;
                if (cs.IsPrimaryKey)
                {
                    if (includeBracket)
                    {
                        pkcolumns.Add(pk);
                        if (usePropertyName)
                        {
                            columns.Add(prefix + "[" + t.Name + "]");
                            pk.ColumnName = prefix + "[" + t.Name + "]";
                        }
                        else
                        {
                            columns.Add(prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name) + "]");
                            pk.ColumnName = prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name) + "]";
                        }
                    }
                    else
                    {
                        pkcolumns.Add(pk);
                        if (usePropertyName)
                        {
                            columns.Add(prefix + t.Name);
                        }
                        else
                            columns.Add(prefix + (((ColumnAttribute)ts).Name ?? t.Name));
                    }
                }
            }
            return pkcolumns;
        }

        public string[] GetPKColumnsArray(Type type, string prefix, bool includedbgen, bool includeBracket, bool usePropertyName)
        {
            List<PKColumn> pkcolumns = GetPKColumns(type, prefix, includedbgen, includeBracket, usePropertyName);
            List<string> result = new List<string>();
            foreach (PKColumn pk in pkcolumns)
            {
                result.Add(pk.ColumnName);
            }
            return result.ToArray();
        }

        public string ListColumn(Type type, string prefix, bool includeReadOnly, bool includedbgen, bool includeBracket)
        {
            return string.Join(", ", GetColumns(type, prefix, includeReadOnly, includedbgen, includeBracket));
        }

        public string ListColumnType(Type type, string prefix, bool includedbgen)
        {
            return string.Join(", ", GetColumnsType(type, prefix, includedbgen));
        }

        public string ListPKColumn(Type type, string prefix, bool includedbgen, bool includeBracket, bool usePropertyName)
        {
            List<PKColumn> pks = GetPKColumns(type, prefix, includedbgen, includeBracket, usePropertyName);
            string result = string.Empty;
            foreach (PKColumn pk in pks)
            {
                result += pk.ColumnName + ",";
            }
            if (result.Length > 1) result = result.Substring(0, result.Length - 1);
            return result;
        }

        public string JoinPKAB(Type type, string prefix)
        {
            List<string> result = new List<string>();
            List<PKColumn> pks = GetPKColumns(type, "", false, true, false);
            foreach (PKColumn pk in pks)
            {
                result.Add("a." + pk.ColumnName + " = " + "b." + pk.ColumnName);
            }
            return string.Join(prefix, result.ToArray());
        }

        public string FillPKColumn(Type type, T obj, List<IDataParameter> prms, string prefix)
        {
            string sql = string.Empty;
            List<PKColumn> pks = GetPKColumns(type, "", false, false, false);
            List<string> rs = new List<string>();
            for (int i = 0; i < pks.Count; i++)
            {
                rs.Add("[" + pks[i].ColumnName + "] = @" + pks[i].PropertyName);
                var prm = GetSqlParam(pks[i].PropertyName, obj);
                if (prm.ParameterName.StartsWith("@[") && prm.ParameterName.EndsWith("]"))
                    prm.ParameterName = "@" + prm.ParameterName.Substring(2, prm.ParameterName.Length - 3);
                IDataParameter exist = prms.Where(m => m.ParameterName == prm.ParameterName).FirstOrDefault();
                if (exist == null) prms.Add(prm);
            }
            return string.Join(prefix, rs.ToArray());
        }

        public List<IDataParameter> FillColumnSingle(Type type, T obj, bool includedbgen = true)
        {
            string sql = string.Empty;
            List<IDataParameter> prms = new List<IDataParameter>();
            string[] pks = GetColumns(type, "", false, includedbgen, false);
            for (int i = 0; i < pks.Length; i++)
            {
                var prm = GetSqlParam(pks[i], obj);
                if (prm.ParameterName.StartsWith("@[") && prm.ParameterName.EndsWith("]"))
                    prm.ParameterName = "@" + prm.ParameterName.Substring(2, prm.ParameterName.Length - 3);
                IDataParameter exist = prms.Where(m => m.ParameterName == prm.ParameterName).FirstOrDefault();
                if (exist == null) prms.Add(prm);
            }
            return prms;
        }

        public string FillColumn(Type type, T obj, List<IDataParameter> prms, bool includeDBGen)
        {
            string sql = string.Empty;
            string[] pks = null;
            if (includeDBGen == false) pks = GetColumns(type, "", false, false, false);
            else pks = GetColumns(type, "", false, true, false);
            List<string> rs = new List<string>();
            for (int i = 0; i < pks.Length; i++)
            {
                string pksi = pks[i];
                if (pksi.StartsWith("[") && pksi.EndsWith("]")) pksi = pksi.Substring(1, pksi.Length - 2);
                rs.Add("[" + pksi + "] = @" + pksi);
                var prm = GetSqlParam(pksi, obj);
                if (prm.ParameterName.StartsWith("@[") && prm.ParameterName.EndsWith("]"))
                    prm.ParameterName = "@" + prm.ParameterName.Substring(2, prm.ParameterName.Length - 3);
                IDataParameter exist = prms.Where(m => m.ParameterName == prm.ParameterName).FirstOrDefault();
                if (exist == null) prms.Add(prm);
            }
            return string.Join(", ", rs.ToArray());
        }

        public string MakeOver(string prefix, string search)
        {
            string result = string.Empty;
            string[] searches = search.Split(new char[] { ',' });
            foreach (string s in searches)
            {
                if (s.IndexOf(".") >= 0) continue;
                if (string.IsNullOrEmpty(result))
                    result += prefix + s;
                else
                    result += ", " + prefix + s;
            }
            return result;
        }

        public object[] PrimaryKeyValues(T obj)
        {
            List<object> result = new List<object>();
            List<PKColumn> PrimaryKeyColumns = GetPKColumns(typeof(T), "", false, false, false);
            for (int i = 0; i < PrimaryKeyColumns.Count; i++)
            {
                object item = MyReflection.GetValue<object>(obj, PrimaryKeyColumns[i].PropertyName);
                result.Add(item);
            }
            return result.ToArray();
        }

        public static DateTime SetDateTime(DateTime dt)
        {
            if (dt == DateTime.MinValue || dt.Year == 1) dt = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            return dt;
        }

        public static DateTime GetDateTime(DateTime dt)
        {
            if (dt == System.Data.SqlTypes.SqlDateTime.MinValue.Value || dt.Year == 1) dt = DateTime.MinValue;
            return dt;
        }

        public virtual T FormatObject(T obj)
        {
            List<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            foreach (PropertyInfo info in properties)
            {
                if (info.PropertyType == typeof(DateTime))
                {
                    try
                    {
                        DateTime val = (DateTime)info.GetValue(obj);
                        val = SetDateTime(val);
                        info.SetValue(obj, val);
                    }
                    catch (IndexOutOfRangeException ex)
                    {

                    }
                    catch (Exception ex)
                    {
                        LibraryTools.WriteName("FormatObject", ex);
                    }
                }
                if (info.PropertyType == typeof(DateTime?))
                {
                    try
                    {
                        DateTime? val = (DateTime?)info.GetValue(obj);
                        if (val.HasValue == false) val = null;
                        else val = SetDateTime(val.Value);
                        info.SetValue(obj, val);
                    }
                    catch (IndexOutOfRangeException ex)
                    {

                    }
                    catch (Exception ex)
                    {
                        LibraryTools.WriteName("FormatObject", ex);
                    }
                }
            }
            return obj;
        }

        public virtual string[] GetColumnsType(Type type, string prefix, bool includedbgen = true)
        {
            return null;
        }

        public virtual IDataParameter GetSqlParam(string propertyname, T obj)
        {
            return null;
        }

        public virtual IDbConnection GetConnection(string str)
        {
            return null;
        }

        public virtual IDbCommand GetCommand(string sql, IDbConnection conn)
        {
            return null;
        }

        public virtual IDataAdapter GetAdapter(IDbCommand com)
        {
            return null;
        }

        public void ParameterAddRange(IDbCommand com, IDataParameter[] param)
        {
            foreach (IDbDataParameter p in param) com.Parameters.Add(p);
        }

        public T ExecuteSingle(string sql, Type t, List<IDataParameter> prms, bool includeReadonly = false)
        {
            T obj = new T();
            string[] cols = GetColumns(t, "", includeReadonly, true, false);
            bool hasRead = false;

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    ParameterAddRange(cmd, prms.ToArray());
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            hasRead = true;
                            foreach (string col in cols)
                            {
                                try
                                {
                                    string col2 = col;
                                    if (col.StartsWith("[") && col.EndsWith("]"))
                                        col2 = col.Substring(1, col.Length - 2);
                                    MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                                }
                                catch (IndexOutOfRangeException ex)
                                {

                                }
                                catch (Exception ex)
                                {
                                    LibraryTools.WriteName("ExecuteSingle", ex);
                                }
                            }
                        }
                        reader.Close();
                    }
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            if (hasRead == false) obj = default(T);
            return obj;
        }

        public T ExecuteSingleTransaction(string sql, Type t, List<IDataParameter> prms)
        {
            if (Connection == null) throw new Exception("Must initialised QueryBuilder with SqlConnection");

            T obj = new T();
            string[] cols = GetColumns(t, "", false, true, false);

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    ParameterAddRange(cmd, prms.ToArray());
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            foreach (string col in cols)
                            {
                                try
                                {
                                    string col2 = col;
                                    if (col.StartsWith("[") && col.EndsWith("]"))
                                        col2 = col.Substring(1, col.Length - 2);
                                    MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                                }
                                catch (IndexOutOfRangeException ex)
                                {

                                }
                                catch (Exception ex)
                                {
                                    LibraryTools.WriteName("ExecuteSingleTransaction", ex);
                                }
                            }
                        }
                        reader.Close();
                    }
                    cmd.Dispose();
                }
            }
            return obj;
        }

        public int ExecuteCommandNonQuery(string query, List<IDataParameter> prms)
        {
            if (prms == null) prms = new List<IDataParameter>();
            Type type = typeof(T);
            return ExecuteNonQuery(query, type, prms);
        }

        public T ExecuteCommandSingle(string query, List<IDataParameter> prms)
        {
            if (prms == null) prms = new List<IDataParameter>();
            Type type = typeof(T);
            return ExecuteSingle(query, type, prms);
        }

        public List<T> ExecuteCommandList(string query, List<IDataParameter> prms)
        {
            if (prms == null) prms = new List<IDataParameter>();
            Type type = typeof(T);
            return ExecuteList(query, type, prms);
        }

        public int ExecuteNonQuery(string sql, Type t, List<IDataParameter> prms)
        {
            string[] cols = GetColumns(t, "", false, true, false);
            int result = 0;

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());

                    result = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return result;
        }

        public DataSet ExecuteDataSet(string sql, Type t, List<IDataParameter> prms)
        {
            string[] cols = GetColumns(t, "", false, true, false);
            DataSet ds = new DataSet();

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());

                    IDataAdapter da = GetAdapter(cmd);
                    da.Fill(ds);
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return ds;
        }

        public int ExecuteNonQueryTransaction(string sql, Type t, List<IDataParameter> prms)
        {
            if (Connection == null) throw new Exception("Must initialised QueryBuilder with SqlConnection");

            string[] cols = GetColumns(t, "", false, true, false);
            int result = 0;

            IDbConnection conn = Connection;
            using (IDbCommand cmd = GetCommand(sql, conn))
            {
                cmd.Transaction = Transaction;

                cmd.CommandType = System.Data.CommandType.Text;
                if (prms != null) ParameterAddRange(cmd, prms.ToArray());

                result = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            return result;
        }

        public List<T> ExecuteList(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            List<T> result = new List<T>();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            T obj = new T();
                            foreach (string col in cols)
                            {
                                try
                                {
                                    string col2 = col;
                                    if (col.StartsWith("[") && col.EndsWith("]"))
                                        col2 = col.Substring(1, col.Length - 2);
                                    MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                                }
                                catch (IndexOutOfRangeException ex)
                                {

                                }
                                catch (Exception ex)
                                {
                                    LibraryTools.WriteName("ExecuteList", ex);
                                }
                            }
                            result.Add(obj);
                        }
                        reader.Close();
                    }
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return result;
        }

        public List<T> ExecuteListSP(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            List<T> result = new List<T>();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            T obj = new T();
                            foreach (string col in cols)
                            {
                                try
                                {
                                    string col2 = col;
                                    if (col.StartsWith("[") && col.EndsWith("]"))
                                        col2 = col.Substring(1, col.Length - 2);
                                    MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                                }
                                catch (IndexOutOfRangeException ex)
                                {

                                }
                                catch (Exception ex)
                                {
                                    LibraryTools.WriteName("ExecuteListSP", ex);
                                }
                            }
                            result.Add(obj);
                        }
                        reader.Close();
                    }
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return result;
        }

        public List<T> ExecuteListTransaction(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            if (Connection == null) throw new Exception("Must initialised QueryBuilder with SqlConnection");

            List<T> result = new List<T>();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            IDbConnection conn = Connection;
            using (IDbCommand cmd = GetCommand(sql, conn))
            {
                cmd.Transaction = Transaction;
                cmd.CommandType = System.Data.CommandType.Text;
                if (prms != null) ParameterAddRange(cmd, prms.ToArray());
                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        T obj = new T();
                        foreach (string col in cols)
                        {
                            try
                            {
                                string col2 = col;
                                if (col.StartsWith("[") && col.EndsWith("]"))
                                    col2 = col.Substring(1, col.Length - 2);
                                MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                            }
                            catch (IndexOutOfRangeException ex)
                            {

                            }
                            catch (Exception ex)
                            {
                                LibraryTools.WriteName("ExecuteListTransaction", ex);
                            }
                        }
                        result.Add(obj);
                    }
                    reader.Close();
                }
                cmd.Dispose();
            }
            return result;
        }
    }
}
