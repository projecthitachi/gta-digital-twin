﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace Common
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ReadOnlyColumnAttribute : Attribute
    {
        private string _Name = string.Empty;
        public ReadOnlyColumnAttribute([CallerMemberName] string propertyName = null)
        {
            _Name = propertyName;
        }

        public string Name { get { return _Name; } set { _Name = value; } }
    }
}
