﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Configuration;

namespace Common
{
    /// <summary>
    /// Summary description for Tools.
    /// </summary>
    public static class LibraryTools
    {
        static object obj = new object();
        static object m_syncWriteFile = new object();

        public static void WriteName(string name, string errMsg)
        {
            try
            {
                WriteFile(GetLogFilePath(name), errMsg + "\r\n");
                WriteFile(GetLogFilePath("AllLogs"), errMsg + "\r\n");
            }
            catch { }
        }

        public static void WriteName(string name, Exception ex)
        {
            try
            {
                WriteFile(GetLogFilePath(name), GetText(ex) + "\r\n");
                WriteFile(GetLogFilePath("AllLogs"), GetText(ex) + "\r\n");
            }
            catch { }
        }

        public static void WriteName(string name, string prefix, Exception ex)
        {
            try
            {
                WriteFile(GetLogFilePath(name), GetText(ex) + "\r\n");
                WriteFile(GetLogFilePath("AllLogs"), GetText(ex) + "\r\n");
            }
            catch { }
        }

        public static string GetText(Exception ex)
        {
            if (ex == null) return string.Empty;
            return ex.Message + " Stack Trace " + ex.StackTrace + " Inner Exception : " + GetText(ex.InnerException);
        }

        public static void WriteFile(string fileName, string text)
        {
            lock (m_syncWriteFile)
            {
                if (File.Exists(fileName) == false)
                {
                    File.Create(fileName).Close();
                }

                using (FileStream fs = new FileStream(fileName, FileMode.Append, FileAccess.Write, FileShare.Read))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(GetDateTimeString());
                        sw.Write(" - ");
                        sw.Write(text);

                        if (!text.EndsWith("\n"))
                        {
                            sw.Write("\r\n");
                        }
                        sw.Close();
                        sw.Dispose();
                    }
                    fs.Close();
                    fs.Dispose();
                }
            }
        }

        #region Get Filename/Path
        public static string GetLogFilePath(string filenameonly)
        {
            string nameonly = Path.GetFileNameWithoutExtension(filenameonly);
            string ext = Path.GetExtension(filenameonly);

            nameonly += "_" + GetDateString();
            if (string.IsNullOrEmpty(ext)) ext = ".log";
            filenameonly = nameonly + ext;

            string path = GetPathByKeyOrDefault("LogPath", "Logs");
            if (Directory.Exists(path) == false) Directory.CreateDirectory(path);
            return Path.Combine(path, filenameonly);
        }

        public static string GetPathByKeyOrDefault(string subpathConfigKey, string defaultRelativeFolder)
        {
            string path = string.Empty;
            if (string.IsNullOrEmpty(subpathConfigKey) == false) path = ConfigurationManager.AppSettings[subpathConfigKey];
            if (path != null && path.Length > 2 && path[1] == ':') return path;
            if (path != null && path.Length > 2 && path.StartsWith("\\\\")) return path;
            if (defaultRelativeFolder != null && defaultRelativeFolder.Length > 2 && defaultRelativeFolder[1] == ':') return defaultRelativeFolder;
            if (defaultRelativeFolder != null && defaultRelativeFolder.Length > 2 && defaultRelativeFolder.StartsWith("\\\\")) return defaultRelativeFolder;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Server != null)
            {
                if (string.IsNullOrEmpty(path) == false)
                {
                    if (path.StartsWith("~/")) path = System.Web.HttpContext.Current.Server.MapPath(path);
                    else path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/bin/"), path.TrimStart('/'));
                }
                else
                    path = System.Web.HttpContext.Current.Server.MapPath("~/bin/" + defaultRelativeFolder);
            }
            else if (string.IsNullOrEmpty(path) == false)
            {
                if (path.StartsWith("~/") && string.IsNullOrEmpty(RootPath) == false)
                {
                    path = path.TrimStart('~', '/');
                    path = Path.Combine(RootPath, path);
                }
                else
                    path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
            }
            else if (string.IsNullOrEmpty(path)) path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, defaultRelativeFolder);

            return path;
        }

        public static string RootPath
        {
            get
            {
                return ConfigurationManager.AppSettings["RootPath"];
            }
        }
        #endregion

        #region Get Date/Time in string
        //return like "20090824"
        public static string GetDateString()
        {
            DateTime dt = DateTime.Now;
            string date = string.Format("{0:0000}{1:00}{2:00}", dt.Year, dt.Month, dt.Day);
            return date;
        }

        //return like "17:05:23.104"
        public static string GetTimeString()
        {
            DateTime dt = DateTime.Now;
            string ret = string.Format("{0:00}:{1:00}:{2:00}.{3:000}", dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
            return ret;
        }

        //return like "17:05:23"
        public static string GetTimeStringShort()
        {
            DateTime dt = DateTime.Now;
            string ret = string.Format("{0:00}:{1:00}:{2:00}", dt.Hour, dt.Minute, dt.Second);
            return ret;
        }

        //return like "20090824 17:05:23.104"
        public static string GetDateTimeString()
        {
            string ret = GetDateString() + " " + GetTimeString();
            return ret;
        }
        #endregion

    }
}
