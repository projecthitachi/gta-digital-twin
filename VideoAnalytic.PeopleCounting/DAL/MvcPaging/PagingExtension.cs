﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Linq;
using Common;

namespace MvcPaging
{
    public static class PagingExtension
    {
        public static string GetPageLink(int i)
        {
            //<li><a href="/Admin/AccessRequestNew/New/2/">2</a></li>
            Uri uri = System.Web.HttpContext.Current.Request.Url;
            string path = uri.PathAndQuery;
            if (path == "/")
            {
                if (i <= 0) return "#";
                return path + i.ToString();
            }
            string[] strs = path.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
            if (strs == null || strs.Length == 0) return "#";
            string[] nos = strs[0].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (nos == null || nos.Length == 0) return "#";
            string no = nos[nos.Length - 1];
            List<string> result = new List<string>();
            result.AddRange(nos);
            int t = 0;
            if (int.TryParse(no, out t)) result.RemoveAt(result.Count - 1);
            result.Add(i.ToString());

            string query = "";
            if (strs.Length > 1)
            {
                List<string> rs = new List<string>();
                rs.AddRange(strs);
                rs.RemoveAt(0);
                query = "?" + string.Join("?", rs);
            }
            return "/" + string.Join("/", result) + query;
        }

        //<ul class="pagination"><li class="disabled"><a href="#">«</a></li><li class="active"><a href="#">1</a></li><li><a href="/Admin/AccessRequestNew/New/2/">2</a></li><li><a href="/Admin/AccessRequestNew/New/3/">3</a></li><li><a href="/Admin/AccessRequestNew/New/4/">4</a></li><li><a href="/Admin/AccessRequestNew/New/5/">5</a></li><li><a href="/Admin/AccessRequestNew/New/6/">6</a></li><li><a href="/Admin/AccessRequestNew/New/7/">7</a></li><li><a href="/Admin/AccessRequestNew/New/8/">8</a></li><li><a href="/Admin/AccessRequestNew/New/2/">»</a></li></ul>
        public static HtmlString Pager(object obj, int pageSize, int pageNumber, int totalItemCount)
        {
            string result = "";
            int totalPages = totalItemCount / pageSize;
            if (totalItemCount % pageSize != 0 || (totalItemCount < pageSize && totalItemCount != 0)) totalPages++;
            pageNumber++;

            Dictionary<int, string> strs = new Dictionary<int, string>();
            int take = 3;
            for (int i=1; i<=Math.Min(take, totalPages); i++)
            {
                if (i <= 0) continue;
                if (strs.ContainsKey(i) == false)
                {
                    if (i != pageNumber)
                        strs.Add(i, "<li><a href=\"" + GetPageLink(i) + "\">" + i.ToString() + "</a></li>");
                    else
                        strs.Add(i, "<li class=\"active\"><a href=\"#\">" + i.ToString() + "</a></li>");
                }
            }
            take = 5;
            for (int i = pageNumber - (take / 2); i <= Math.Min(totalPages, pageNumber - (take / 2) + take); i++)
            {
                if (i <= 0) continue;
                if (strs.ContainsKey(i) == false)
                {
                    if (i != pageNumber)
                        strs.Add(i, "<li><a href=\"" + GetPageLink(i) + "\">" + i.ToString() + "</a></li>");
                    else
                        strs.Add(i, "<li class=\"active\"><a href=\"#\">" + i.ToString() + "</a></li>");
                }
            }
            take = 3;
            for (int i = (totalPages / 2) - (take / 2); i <= Math.Min(totalPages, (totalPages / 2) - (take / 2) + take); i++)
            {
                if (i <= 0) continue;
                if (strs.ContainsKey(i) == false)
                {
                    if (i != pageNumber)
                        strs.Add(i, "<li><a href=\"" + GetPageLink(i) + "\">" + i.ToString() + "</a></li>");
                    else
                        strs.Add(i, "<li class=\"active\"><a href=\"#\">" + i.ToString() + "</a></li>");
                }
            }
            take = 3;
            for (int i = totalPages; i >= Math.Max(1, totalPages - take); i--)
            {
                if (i <= 0) continue;
                if (strs.ContainsKey(i) == false)
                {
                    if (i != pageNumber)
                        strs.Add(i, "<li><a href=\"" + GetPageLink(i) + "\">" + i.ToString() + "</a></li>");
                    else
                        strs.Add(i, "<li class=\"active\"><a href=\"#\">" + i.ToString() + "</a></li>");
                }
            }
            string previous = "<li class=\"disabled\"><a href=\"#\">Previous</a></li>";
            string next = "<li class=\"disabled\"><a href=\"#\">Next</a></li>";

            if (pageNumber > 1)
                previous = "<li><a href=\"" + GetPageLink(pageNumber - 1) + "\">Previous</a></li>";
            if (pageNumber < totalPages)
                next = "<li><a href=\"" + GetPageLink(pageNumber + 1) + "\">Next</a></li>";

            if (strs.Keys.Count == 0)
            {
                strs.Add(1, "<li><a href=\"" + GetPageLink(1) + "\">Next</a></li>");
            }

            var keys = strs.Keys.OrderBy(m => m).ToList();

            result = previous;
            int last = keys[0];
            for (int i = 0; i<keys.Count;i++)
            {
                int diff = keys[i] - last;
                if (diff > 1)
                    result += "<li><a href=\"#\">...</a></li>";
                result += strs[keys[i]];
                last = keys[i];
            }
            result += next;

            return new HtmlString("<ul class=\"pagination\">" + result + "</ul>");
        }
    }
}
