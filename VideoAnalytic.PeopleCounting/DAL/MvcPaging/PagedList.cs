﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;

namespace MvcPaging
{
    public class PagedList<T> : List<T>, IEnumerable<T>, IEnumerable
    {
        public int index = 0;
        public int size = 0;
        public int count = 0;

        public PagedList(IEnumerable<T> source, int index, int pageSize, int count)
        {
            this.AddRange(source);
            Init(index, pageSize, count);
        }

        public PagedList(IQueryable<T> source, int index, int pageSize, int count)
        {
            this.AddRange(source);
            Init(index, pageSize, count);
        }

        public void Init(int index, int size, int count)
        {
            this.index = index;
            this.size = size;
            this.count = count;
        }

        public int PageCount { get { return (count / size) + ((count % size == 0 || (count < size && count != 0)) ? 1 : 0); } }
        public int TotalItemCount { get { return count; } }
        public int PageIndex { get { return index; } }
        public int PageNumber { get { return index; } }
        public int PageSize { get { return size;  } }
        public bool HasPreviousPage { get { return index > 1; } }
        public bool HasNextPage { get { return index < PageCount; } }
        public bool IsFirstPage { get { return index == 1; } }
        public bool IsLastPage { get { return index == PageCount; } }
    }
}
