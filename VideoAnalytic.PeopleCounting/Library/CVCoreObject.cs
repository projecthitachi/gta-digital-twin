﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using Common;

namespace CVCoreLib
{
    [Serializable]
    public class TagObject
    {
        public string ImageName { get; set; }
        public string ImageBackground { get; set; }
        public CVCoreObject Object { get; set; }
        public CVCoreItemObject Item { get; set; }
    }

    [Serializable]
    public class CVCoreItemObject
    {
        public int x { get; set; }
        public int y { get; set; }
        public int h { get; set; }
        public int w { get; set; }
        public int object_type { get; set; }
        public long object_id { get; set; }

        public string GetImage(CVCoreObject obj)
        {
            return GetAPIBase() + "/cv/search/showImage?frame_id=" + GetFrameID(obj) + "&frame_size=" + GetFrameSize(obj) + "&rectangle=" + x + "," + y + "," + w + "," + h + "&crop_result=true";
        }

        public string GetAllImage(CVCoreObject obj)
        {
            return GetAPIBase() + "/cv/search/showObjectsInImage?frame_id=" + GetFrameID(obj) + "&frame_size=" + GetFrameSize(obj) + "&rectangle=" + x + "," + y + "," + w + "," + h + "&crop_result=true";
        }

        public string GetFrameImage(CVCoreObject obj)
        {
            return GetAPIBase() + "/cv/search/showImage?frame_id=" + GetFrameID(obj) + "&frame_size=" + GetFrameSize(obj) + "&rectangle=" + x + "," + y + "," + w + "," + h + "&crop_result=false";
        }

        public static string GetAPIBase()
        {
            string APIUrl = ConfigurationManager.AppSettings["APIUrl"];
            string APIBase = APIUrl.ToLower().Replace("https://", "").Replace("http://", "");
            if (APIBase.IndexOf("/") > 0) APIBase = APIBase.Substring(0, APIBase.IndexOf("/"));
            if (APIUrl.ToLower().Contains("https://")) APIBase = "https://" + APIBase;
            if (APIUrl.ToLower().Contains("http://")) APIBase = "http://" + APIBase;
            return APIBase;
        }

        public static string GetFrameSize(CVCoreObject obj)
        {
            if (obj == null) return string.Empty;
            return obj.frame_width + "," + obj.frame_height;
        }

        public string GetFrameID(CVCoreObject obj)
        {
            return GetFrameIDStatic(obj);
        }

        public static string GetFrameIDStatic(CVCoreObject obj)
        {
            if (obj == null || string.IsNullOrEmpty(obj.frame_url)) return string.Empty;
            int id = obj.frame_url.LastIndexOf("=");
            if (id == -1) return string.Empty;
            if (id == obj.frame_url.Length) return string.Empty;
            return obj.frame_url.Substring(id + 1);
        }

        public static string GetFrameID(string frameurl)
        {
            if (string.IsNullOrEmpty(frameurl)) return string.Empty;

            int id = frameurl.LastIndexOf("=");
            if (id == -1) return string.Empty;
            if (id == frameurl.Length) return string.Empty;
            return frameurl.Substring(id + 1);
        }
    }

    [Serializable]
    public class CVCoreServiceObject
    {
        public int face_detecting { get; set; }
        public int detector_mode { get; set; }
        public int poi_tracking { get; set; }
        public int voi_tracking { get; set; }
    }

    [Serializable]
    public class CVCoreObject
    {
        public string frame_url { get; set; }
        public int frame_width { get; set; }
        public int fps { get; set; }
        public double ingesting_time { get; set; }
        public double frame_read_time { get; set; }
        public double scale_factor { get; set; }
        public int status { get; set; }
        public int frame_time_stamp { get; set; }
        public double frame_ingesting_time { get; set; }
        public string url { get; set; }
        public int camera_type { get; set; }
        public int camera_id { get; set; }
        public int frame_height { get; set; }
        public double storing_time { get; set; }
        public CVCoreServiceObject services { get; set; }
        public double read_time { get; set; }
        public double frame_storing_time { get; set; }
        public List<CVCoreItemObject> @objects { get; set; }
        public List<CVCoreItemObject> @object { get; set; }

        public string GetShowPathUrl()
        {
            return CVCoreItemObject.GetAPIBase() + "/cv/inter/showPath";
        }
    }

    [Serializable]
    [Table(Name = "CVCoreObject")]
    public class CVCoreItemObjectDB
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public string ID { get; set;  }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string ParentID { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int x { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int y { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int h { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int w { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int object_type { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public long object_id { get; set; }
    }

    [Serializable]
    [Table(Name = "CVCoreObject")]
    public class CVCoreObjectDB
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public string ID { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string frame_url { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_width { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int fps { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public double ingesting_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public double frame_read_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public double scale_factor { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int status { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_time_stamp { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public double frame_ingesting_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string url { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int camera_type { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int camera_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_height { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public double storing_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int face_detecting { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int detector_mode { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int poi_tracking { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int voi_tracking { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public double read_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public double frame_storing_time { get; set; }
    }

    [Serializable]
    [Table(Name = "CVCoreShowPathCamera")]
    public class CVCoreShowPathCamera
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public string Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int camera_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public decimal latitude { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public decimal longitude { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int object_total { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int predicted_path_total { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime time { get; set; }
    }

    [Serializable]
    [Table(Name = "CVCoreShowPathCameraObject")]
    public class CVCoreShowPathCameraObject
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public string Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string parent_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public decimal avg_velocity { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public decimal confidence { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int entry_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int exit_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_height { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_width { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int object_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectangleh { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectanglew { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectanglex { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectangley { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int track_total { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int type { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public decimal velocity_deviation { get; set; }
    }

    [Serializable]
    [Table(Name = "CVCoreShowPathCameraObjectItem")]
    public class CVCoreShowPathCameraObjectItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public string Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string parent_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string grandparent_id { get; set; }
        
        public int frame_height { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int frame_width { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public decimal latitude { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public decimal longitude { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectangleh { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectanglew { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectanglex { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int rectangley { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int time { get; set; }
    }
}

//public string GetAPIBase()
//{
//    string APIUrl = ConfigurationManager.AppSettings["APIUrl"];
//    string APIBase = APIUrl.ToLower().Replace("https://", "").Replace("http://", "");
//    if (APIBase.IndexOf("/") > 0) APIBase = APIBase.Substring(0, APIBase.IndexOf("/"));
//    if (APIUrl.ToLower().Contains("https://")) APIBase = "https://" + APIBase;
//    if (APIUrl.ToLower().Contains("http://")) APIBase = "http://" + APIBase;
//    return APIBase;
//}