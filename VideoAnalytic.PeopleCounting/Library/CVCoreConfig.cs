﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CVCoreLib;
using System.IO;
using System.Drawing;
using Common;
using System.Configuration;

namespace CVCoreLib
{
    public class CVCoreConfig
    {
        private string APIUrl = string.Empty;
        private string TempFolder = string.Empty;
        private List<string> EntryCameraID = null;
        private List<string> ExitCameraID = null;
        private List<string> CameraID = new List<string>();
        private double Confidence = 0.5;
        private int MaxExitTime = 1000000;
        private int MaxPeopleCountTime = 60;
        private double FactorPeopleCount = 0.2;
        private int ReportPeriod = 30;
        private double TargetQueueTime = 5;
        private bool UseCache = false;
        private int MaxCrowdPerson = 15;
        private SettingDAL _settingDAL = new SettingDAL();
        private string RecordPath = string.Empty;
        private string RtspUrl = string.Empty;

        public CVCoreConfig()
        {
            APIUrl = GetAPIUrl();
            
            TempFolder = GetTempFolder();
            EntryCameraID = GetEntryCameraID();
            CameraID = GetCameraID();
            ExitCameraID = GetExitCameraID();
            Confidence = GetConfidence();
            MaxExitTime = GetMaxExitTime();
            MaxPeopleCountTime = GetMaxPeopleCountTime();
            FactorPeopleCount = GetFactorPeopleCount();
            ReportPeriod = GetReportPeriod();
            TargetQueueTime = GetTargetQueueTime();
            UseCache = GetUseCache();
            MaxCrowdPerson = GetMaxCrowdPerson();
            RecordPath = GetRecordPath();
            RtspUrl = GetRtspUrl("4");
        }

        public string GetRtspUrl(string cameraid)
        {
            Setting s = _settingDAL.GetBy("SettingName", "RtspUrl_" + cameraid);
            if (s != null)
            {
                return s.SettingValue;
            }

            string rtspurl = ConfigurationManager.AppSettings["RtspUrl_" + cameraid] ?? "rtsp://admin:Hitachi1@10.0.1.182";
            return rtspurl;
        }

        public string GetRecordPath()
        {
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content\\Record");
            if (Directory.Exists(folder) == false) Directory.CreateDirectory(folder);
            return folder;
        }

        public string GetTempFolder()
        {
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp");
            if (Directory.Exists(folder) == false) Directory.CreateDirectory(folder);
            return folder;
        }

        public DateTime FromUnixEpoch(int timestamp)
        {
            DateTime str = Calculation.FromUnixEpochTime(timestamp);
            str = str.AddSeconds(GetEpochAdjustmentSec());
            return str;
        }

        public int ToUnixEpoch(DateTime str)
        {
            str = str.AddSeconds(-1 * GetEpochAdjustmentSec());
            return (int) Calculation.ToUnixEpochTime(str);
        }

        public int GetEpochAdjustmentSec()
        {
            Setting s = _settingDAL.GetBy("SettingName", "EpochAdjustmentSec");
            if (s != null)
            {
                int se = 0;
                int.TryParse(s.SettingValue, out se);
                if (se == 0) se = 0;
                return se;
            }
            int sec = 0;
            string seconds = ConfigurationManager.AppSettings["EpochAdjustmentSec"] ?? "29000";
            int.TryParse(seconds, out sec);
            if (sec == 0) sec = 0;
            return sec;
        }

        public bool GetUseCache()
        {
            Setting s = _settingDAL.GetBy("SettingName", "UseCache");
            if (s != null)
            {
                if (s.SettingValue == "Y" || s.SettingValue == "1") return true;
            }
            
            string seconds = ConfigurationManager.AppSettings["UseCache"] ?? "Y";
            if (seconds == "Y" || seconds == "1") return true;
            return false;
        }

        public int GetMaxExitTime()
        {
            Setting s = _settingDAL.GetBy("SettingName", "MaxExitTime");
            if (s != null)
            {
                int se = 0;
                int.TryParse(s.SettingValue, out se);
                if (se == 0) se = 100000000;
                return se;
            }
            int sec = 100000000;
            string seconds = ConfigurationManager.AppSettings["MaxExitTime"] ?? "100000000"; // ConfigurationManager.AppSettings["MaxExitTime"] ?? "";
            int.TryParse(seconds, out sec);
            if (sec == 0) sec = 100000000;
            return sec;
        }

        public int GetMaxCrowdPerson()
        {
            Setting s = _settingDAL.GetBy("SettingName", "MaxCrowdPerson");
            if (s != null)
            {
                int se = 0;
                int.TryParse(s.SettingValue, out se);
                if (se == 0) se = 15;
                return se;
            }
            int sec = 100000000;
            string seconds = ConfigurationManager.AppSettings["MaxCrowdPerson"] ?? "15";// ConfigurationManager.AppSettings["MaxCrowdPerson"] ?? "";
            int.TryParse(seconds, out sec);
            if (sec == 0) sec = 15;
            return sec;
        }

        public int GetReportPeriod()
        {
            Setting s = _settingDAL.GetBy("SettingName", "ReportPeriod");
            if (s != null)
            {
                int se = 0;
                int.TryParse(s.SettingValue, out se);
                if (se == 0) se = 30;
                return se;
            }
            int mins = 30;
            string minstr = ConfigurationManager.AppSettings["ReportPeriod"] ?? "30"; //ConfigurationManager.AppSettings["ReportPeriod"] ?? "";
            int.TryParse(minstr, out mins);
            if (mins == 0) mins = 30;
            return mins;
        }

        public double GetFactorPeopleCount()
        {
            Setting s = _settingDAL.GetBy("SettingName", "FactorPeopleCount");
            if (s != null)
            {
                double se = 0;
                double.TryParse(s.SettingValue, out se);
                if (se == 0) se = 0.5;
                return se;
            }
            double sec = 0.5;
            string seconds = ConfigurationManager.AppSettings["FactorPeopleCount"] ?? "1";// ConfigurationManager.AppSettings["FactorPeopleCount"] ?? "";
            double.TryParse(seconds, out sec);
            if (sec == 0) sec = 0.5;
            return sec;
        }

        public int GetMaxPeopleCountTime()
        {
            Setting s = _settingDAL.GetBy("SettingName", "MaxPeopleCountTime");
            if (s != null)
            {
                int se = 0;
                int.TryParse(s.SettingValue, out se);
                if (se == 0) se = 60;
                return se;
            }
            int sec = 100000000;
            string seconds = ConfigurationManager.AppSettings["MaxPeopleCountTime"] ?? "2"; //ConfigurationManager.AppSettings["MaxPeopleCountTime"] ?? "";
            int.TryParse(seconds, out sec);
            if (sec == 0) sec = 60;
            return sec;
        }

        public double GetTargetQueueTime()
        {
            Setting s = _settingDAL.GetBy("SettingName", "TargetQueueTime");
            if (s != null)
            {
                double se = 0;
                double.TryParse(s.SettingValue, out se);
                if (se == 0) se = 5;
                return se;
            }
            double sec = 5;
            string seconds = ConfigurationManager.AppSettings["TargetQueueTime"] ?? "5"; //ConfigurationManager.AppSettings["TargetQueueTime"] ?? "";
            double.TryParse(seconds, out sec);
            if (sec == 0) sec = 5;
            return sec;
        }

        public List<string> GetEntryCameraID()
        {
            Setting s = _settingDAL.GetBy("SettingName", "EntryCamera");
            if (s != null)
            {
                List<string> result2 = new List<string>();
                string[] ss2 = s.SettingValue.Split(new char[] { ',' });
                foreach (string s2 in ss2) result2.Add(s2.Trim());
                return result2;
            }

            string entryCamera = ConfigurationManager.AppSettings["EntryCamera"] ?? "1"; //ConfigurationManager.AppSettings["EntryCamera"] ?? "";

            List<string> result = new List<string>();
            string[] ss = entryCamera.Split(new char[] { ',' });
            foreach (string xs in ss) result.Add(xs.Trim());

            return result;
        }

        public void SetSetting(string key, string value)
        {
            Setting s = _settingDAL.GetBy("SettingName", key);
            if (s != null)
            {
                s.SettingValue = value;
                _settingDAL.Update(s);
                return;
            }
            else
            {
                s = new Setting();
                s.SettingName = key;
                s.SettingValue = value;

                _settingDAL.Update(s);
            }

        }

        public List<string> GetCameraID()
        {
            Setting s = _settingDAL.GetBy("SettingName", "CameraID");
            if (s != null)
            {
                List<string> result2 = new List<string>();
                string[] ss2 = s.SettingValue.Split(new char[] { ',' });
                foreach (string s2 in ss2) result2.Add(s2.Trim());
                return result2;
            }

            string entryCamera = ConfigurationManager.AppSettings["CameraID"] ?? "1"; //ConfigurationManager.AppSettings["EntryCamera"] ?? "";

            List<string> result = new List<string>();
            string[] ss = entryCamera.Split(new char[] { ',' });
            foreach (string xs in ss) result.Add(xs.Trim());

            return result;
        }

        public List<string> GetExitCameraID()
        {
            Setting s = _settingDAL.GetBy("SettingName", "ExitCamera");
            if (s != null)
            {
                List<string> result2 = new List<string>();
                string[] ss2 = s.SettingValue.Split(new char[] { ',' });
                foreach (string s2 in ss2) result2.Add(s2.Trim());
                return result2;
            }

            string exitCamera = ConfigurationManager.AppSettings["ExitCamera"] ?? "1"; //ConfigurationManager.AppSettings["ExitCamera"] ?? "";

            List<string> result = new List<string>();
            string[] ss = exitCamera.Split(new char[] { ',' });
            foreach (string xs in ss) result.Add(xs.Trim());

            return result;
        }

        public double GetConfidence()
        {
            Setting s = _settingDAL.GetBy("SettingName", "Confidence");
            if (s != null)
            {
                double se = 0;
                double.TryParse(s.SettingValue, out se);
                if (se == 0) se = 0.5;
                return se;
            }
            string confidence = ConfigurationManager.AppSettings["Confidence"] ?? "0.5";//ConfigurationManager.AppSettings["Confidence"] ?? "";
            double result = 0;
            double.TryParse(confidence, out result);
            return result;
        }

        public string GetAPIUrl()
        {
            return GetAPIBase() + "/cv/camera/getFramePerCamera";
        }

        public string GetAPIBase()
        {
            return CVCoreItemObject.GetAPIBase();
        }
    }
}

// public string GetAPIBase()
// {
//    string str = ConfigurationManager.AppSettings["APIUrl"] ?? "";
//    string APIBase = str.ToLower().Replace("https://", "").Replace("http://", "");
//    if (APIBase.IndexOf("/") > 0) APIBase = APIBase.Substring(0, APIBase.IndexOf("/"));
//    if (str.ToLower().Contains("https://")) APIBase = "https://" + APIBase;
//    if (str.ToLower().Contains("http://")) APIBase = "http://" + APIBase;
//    return APIBase;
// }

// public int GetLaneID()
// {
//    LanesDAL dal = new LanesDAL();
//    List<Lanes> lns = dal.GetManyBy("CameraID", cameraid);
//    if (lns == null || lns.Count <= 0) return 0;
//    if (lns.Count == 1) return lns[0].LaneID;
//    foreach (Lanes l in lns)
//    {
//        BaseBox cell = new BaseBox(0, 0, l.XTop, l.YTop, new CellMovement(l.XBottom / 2, l.YBottom / 2, l.XBottom / 2, l.YBottom / 2, 0, 0, 0, 0, 0, 0), false);
//        bool result = Polygon.PointInPolygon(cell, c.rectangle.x, c.rectangle.y);
//        if (result)
//        {
//            return l.LaneID;
//        }
//     }
// }
//return 0;