﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Configuration;

namespace CVCoreLib
{
    public class CVCoreCache
    {
        public string DBConnection = string.Empty;
        public CVCoreCache()
        {
            DBConnection = ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString;
        }

        public bool IsPastHour(DateTime end)
        {
            TimeSpan ts = DateTime.Now - end;
            if (ts.TotalHours > 1) return true;
            return false;
        }

        public bool IsPastDay(DateTime end)
        {
            TimeSpan ts = DateTime.Now - end;
            if (ts.TotalHours > 1) return true;
            return false;
        }

        public string MakeKey(List<object> objs)
        {
            List<string> keys = new List<string>();
            foreach(object obj in objs)
            {
                string str = obj.ToString().PadLeft(200, ' ');
                keys.Add(str);
            }
            return string.Join("", keys);
        }

        public void SaveObject(string key, object obj)
        {
            string sql = "INSERT INTO TempCache (Id, CacheKey, Data, CreateOfficer, CreateDate, UpdateOfficer, UpdateDate) VALUES (@Id, @Key, @Obj, 'SYSTEM', @DT, 'SYSTEM', @DT)";
            List<SqlParameter> prms = new List<SqlParameter>();
            SqlParameter p1 = new SqlParameter("@Id", Common.MyRandom.RandomString(24));
            SqlParameter p2 = new SqlParameter("@Key", key);
            SqlParameter p3 = new SqlParameter("@Obj", ToByteArray(obj));
            SqlParameter p4 = new SqlParameter("@DT", DateTime.Now);
            prms.Add(p1);
            prms.Add(p2);
            prms.Add(p3);
            prms.Add(p4);

            SqlConnection conn = null;
            SqlCommand com = null;
            try
            {
                conn = new SqlConnection(DBConnection);
                conn.Open();
                com = new SqlCommand(sql, conn);
                com.CommandType = System.Data.CommandType.Text;
                com.Parameters.AddRange(prms.ToArray());
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("Cache", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (com != null)
                {
                    com.Dispose();
                }
            }
        }

        public T GetObject<T>(string key)
        {
            string sql = "SELECT * FROM TempCache WHERE CacheKey=@Key";
            List<SqlParameter> prms = new List<SqlParameter>();
            SqlParameter p1 = new SqlParameter("@Key", key);
            prms.Add(p1);

            SqlConnection conn = null;
            SqlCommand com = null;
            SqlDataReader reader = null;
            try
            {
                conn = new SqlConnection(DBConnection);
                conn.Open();
                com = new SqlCommand(sql, conn);
                com.CommandType = System.Data.CommandType.Text;
                com.Parameters.AddRange(prms.ToArray());
                reader = com.ExecuteReader();

                byte[] data = null;
                while(reader.Read())
                {
                    data = (byte[])reader["Data"];
                }
                try
                {
                    T result = FromByteArray<T>(data);
                    return result;
                }
                catch(Exception ex)
                {
                    Common.LibraryTools.WriteName("Cache", ex);
                }
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("Cache", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (com != null)
                {
                    com.Dispose();
                }
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return default(T);
        }

        public byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public T FromByteArray<T>(byte[] data)
        {
            if (data == null)
                return default(T);
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(data))
            {
                object obj = bf.Deserialize(ms);
                return (T)obj;
            }
        }
    }
}
