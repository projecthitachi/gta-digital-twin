﻿// has.app.Components.RealTimeHub
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using CVCoreLib;
using System;
using Common;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using VideoAnalytic.PeopleCounting;
using System.Threading;
using System.Web.Mvc;
using System.Web;
using System.Net.WebSockets;
using Newtonsoft.Json;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace has.app.Components
{
    public class VABehavior : WebSocketBehavior
    {
        protected override void OnClose(CloseEventArgs e)
        {
            if (VAHub.PlaybackClients.ContainsKey(this)) VAHub.PlaybackClients.Remove(this);
        }

        protected override void OnError(WebSocketSharp.ErrorEventArgs e)
        {
            if (VAHub.PlaybackClients.ContainsKey(this)) VAHub.PlaybackClients.Remove(this);
        }

        protected override void OnOpen()
        {
            if (VAHub.PlaybackClients.ContainsKey(this) == false) VAHub.PlaybackClients.Add(this, "");
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (e.Data.StartsWith("ID:"))
            {
                if (VAHub.PlaybackClients.ContainsKey(this)) VAHub.PlaybackClients[this] = e.Data.Substring(3);
                else VAHub.PlaybackClients.Add(this, e.Data.Substring(3));
            }

            if (e.Data.StartsWith("MAXMIN:"))
            {
                string str = e.Data.Substring(7);
                string[] ss = str.Split(new char[] { '_' });
                if (ss.Length == 2)
                {
                    VAHub.Receive(ss[0], ss[1]);
                }
            }

            if (e.Data.StartsWith("CAMERA:"))
            {
                string str = e.Data.Substring(7);
                int cam = 0;
                int.TryParse(str, out cam);
                if (VAHub.CameraConn.ContainsKey(this) == false) VAHub.CameraConn.Add(this, cam);
                else VAHub.CameraConn[this] = cam;
            }
        }
    }
    public class VAHub
    {
        public static Action<object, bool> Broadcast;

        public static VAHubModel MyHubObject = new VAHubModel();
        public static CVCoreLibrary CVCoreLibrary = CVCoreLibrary.Instance;
        public static string LastBackgroundID = string.Empty;

        public static Dictionary<WebSocketBehavior, int> CameraConn = new Dictionary<WebSocketBehavior, int>();
        public static Dictionary<WebSocketBehavior, int> BackgroundIndex = new Dictionary<WebSocketBehavior, int>();
        public static Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL>> Background = new Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL>>();
        public static Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL2>> Thumb = new Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL2>>();
        public static Dictionary<WebSocketBehavior, string> PlaybackClients = new Dictionary<WebSocketBehavior, string>();
        public static WebSocketServer wss;

        public static void InitializeCVCore()
        {
            ThreadStart ts = new ThreadStart(InitializeCVCoreReal);
            Thread t = new Thread(ts);
            t.Start();
        }

        public static void InitializeCVCoreReal()
        {
            var wss = new WebSocketServer("ws://localhost:8181");
            wss.AddWebSocketService<VABehavior>("/service");
            wss.Start();

            VAHub.CVCoreLibrary.OnCountPeople += OnCountPeople;
            VAHub.CVCoreLibrary.OnSetImage += SetImage;
            VAHub.CVCoreLibrary.OnSetImageBackground += SetImageBackground;
            VAHub.CVCoreLibrary.OnPeopleAmountChangeMax += PeopleAmountChangeMax;
            VAHub.CVCoreLibrary.OnPeopleAmountChangeMin += PeopleAmountChangeMin;

            VAHub.CVCoreLibrary.Start();
            VAHub.CVCoreLibrary.MyHubObject = MyHubObject;

            Constants._Library = VAHub.CVCoreLibrary;

            //string id = "12ZHCLXUUO6R41GNYC3CLQV6";
            SqlVA_ALERTDAL dal = new SqlVA_ALERTDAL();

            while(VAHub.CVCoreLibrary.IsRunning)
            {
                try
                {
                    string obj = string.Empty;
                    foreach (WebSocketBehavior con in PlaybackClients.Keys)
                    {
                        MyHubObject.AlertList = dal.GetAlertId();
                        MyHubObject.AlertListObject = dal.GetAlertList();

                        if (PlaybackClients[con] == "")
                        {
                            obj = JsonConvert.SerializeObject(MyHubObject);
                            con.Context.WebSocket.Send(obj);
                            Thread.Sleep(100);
                            continue;
                        }

                        SqlVA_ALERTDETAILDAL dal1 = new SqlVA_ALERTDETAILDAL();
                        var back = dal1.GetManyBy("ParentId", PlaybackClients[con]);
                        if (Background.ContainsKey(con) == false) Background.Add(con, back);
                        else Background[con] = back;

                        SqlVA_ALERTDETAIL2DAL dal2 = new SqlVA_ALERTDETAIL2DAL();
                        var thumb = dal2.GetManyBy("ParentId", PlaybackClients[con]);
                        if (Thumb.ContainsKey(con) == false) Thumb.Add(con, thumb);
                        else Thumb[con] = thumb;

                        MyHubObject.PeopleCount = dal.GetPeopleCount(PlaybackClients[con]);
                        if (Background.ContainsKey(con) && Background[con].Count > 0)
                        {
                            List<string> image = new List<string>();
                            if (Thumb.ContainsKey(con) && Thumb[con].Count > 0)
                            {
                                foreach (VA_ALERTDETAIL2 t in Thumb[con])
                                {
                                    if (t == null) continue;
                                    image.Add(t.Thumb);
                                }
                            }

                            VA_ALERT va = dal.GetBy("Id", PlaybackClients[con]);
                            if (BackgroundIndex.ContainsKey(con) == false) BackgroundIndex.Add(con, 0);
                            BackgroundIndex[con] = BackgroundIndex[con] + 1;

                            if (BackgroundIndex[con] > Background[con].Count) BackgroundIndex[con] = 0;
                            if (Background[con].Count > 0 && BackgroundIndex[con] >= 0 && BackgroundIndex[con] < Background[con].Count)
                            {
                                try
                                {
                                    MyHubObject.CVCoreImage = Background[con][BackgroundIndex[con]].Image;
                                }
                                catch { }
                            }

                            if (image.Count > 0) MyHubObject.DetectedImage = image;
                            if (va != null) MyHubObject.PeopleCount = va.PeopleCount;
                        }

                        obj = JsonConvert.SerializeObject(MyHubObject);
                        con.Context.WebSocket.Send(obj);
                    }

                    Thread.Sleep(100);
                }
                catch(Exception ex)
                {
                    LibraryTools.WriteName("VAHub_InitializeCVCoreReal", ex);
                }
            }
        }

        private static Dictionary<string, int> CameraCount = new Dictionary<string, int>();
        private static void OnCountPeople(string cameraid, int str)
        {
            MyHubObject.PeopleCount = str;
            foreach(WebSocketBehavior b in CameraConn.Keys)
            {
                if (CameraConn[b].ToString() == cameraid)
                {
                    string obj = JsonConvert.SerializeObject(MyHubObject);
                    b.Context.WebSocket.Send(obj);
                }
            }
        }

        public static void SetImage(string cameraid, TagObject to)
        {
            try
            {
                SqlVA_ALERTDETAIL2DAL dal = new SqlVA_ALERTDETAIL2DAL();
                VA_ALERTDETAIL2 a = dal.GetBy("FrameUrl", to.Object.frame_url);
                if (a != null) return;

                string fileContent = Convert.ToBase64String(File.ReadAllBytes(to.ImageName));
                string str = "data:image/jpeg;base64," + fileContent;

                string lasteventid = string.Empty;
                if (VAHub.CVCoreLibrary.LastEventID.ContainsKey(cameraid)) lasteventid = VAHub.CVCoreLibrary.LastEventID[cameraid];

                if (lasteventid != "" && VAHub.CVCoreLibrary.RecordStart.ContainsKey(cameraid) && VAHub.CVCoreLibrary.RecordStart[cameraid] != DateTime.MinValue)
                {
                    TimeSpan ts = DateTime.Now - VAHub.CVCoreLibrary.RecordStart[cameraid];
                    if (ts.TotalSeconds < 10)
                    {
                        VA_ALERTDETAIL2 alert = new VA_ALERTDETAIL2();
                        alert.ParentId = lasteventid;
                        alert.FrameUrl = to.Object.frame_url;
                        alert.Thumb = str;
                        alert.CreateDate = DateTime.Now;
                        alert.CreateOfficer = "SYSTEM";
                        alert.UpdateDate = DateTime.Now;
                        alert.UpdateOfficer = "SYSTEM";
                        dal.Insert(alert);
                    }
                    else
                    {
                        if (VAHub.CVCoreLibrary.LastEventID.ContainsKey(cameraid)) VAHub.CVCoreLibrary.LastEventID[cameraid] = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LibraryTools.WriteName("VAHub_SetImage", ex);
            }
        }

        public static void SetImageBackground(string cameraid, TagObject to)
        {
            try
            {
                SqlVA_ALERTDETAILDAL dal = new SqlVA_ALERTDETAILDAL();
                VA_ALERTDETAIL a = dal.GetBy("FrameUrl", to.Object.frame_url);
                if (a != null) return;

                string fileContent = Convert.ToBase64String(File.ReadAllBytes(to.ImageBackground));
                string str = "data:image/jpeg;base64," + fileContent;

                string lasteventid = string.Empty;
                if (VAHub.CVCoreLibrary.LastEventID.ContainsKey(cameraid)) lasteventid = VAHub.CVCoreLibrary.LastEventID[cameraid];

                if (lasteventid != "" && VAHub.CVCoreLibrary.RecordStart.ContainsKey(cameraid) && VAHub.CVCoreLibrary.RecordStart[cameraid] != DateTime.MinValue )
                {
                    TimeSpan ts = DateTime.Now - VAHub.CVCoreLibrary.RecordStart[cameraid];
                    if (ts.TotalSeconds < 10)
                    {
                        VA_ALERTDETAIL alert = new VA_ALERTDETAIL();
                        alert.ParentId = lasteventid;
                        alert.FrameUrl = to.Object.frame_url;
                        alert.Image = str;
                        alert.CreateDate = DateTime.Now;
                        alert.CreateOfficer = "SYSTEM";
                        alert.UpdateDate = DateTime.Now;
                        alert.UpdateOfficer = "SYSTEM";
                        dal.Insert(alert);
                    }
                    else
                    {
                        if (VAHub.CVCoreLibrary.LastEventID.ContainsKey(cameraid)) VAHub.CVCoreLibrary.LastEventID[cameraid] = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LibraryTools.WriteName("VAHub_SetImageBackground", ex);
            }
        }

        public static string Receive(string maxo, string mino)
        {
            int max = 0;
            int min = 0;
            int.TryParse(maxo, out max);
            int.TryParse(mino, out min);
            VAHub.CVCoreLibrary.MaxPerson = max;
            VAHub.CVCoreLibrary.MinPerson = min;
            MyHubObject.MaxPerson = max;
            MyHubObject.MinPerson = min;

            return "success";
        }

        public static void PeopleAmountChangeMax(CVCoreObject obj, string cameraid, int count, int max)
        {
            SqlVA_ALERTDAL dal = new SqlVA_ALERTDAL();
            VA_ALERT aa = dal.GetBy("FrameUrl", obj.frame_url);
            if (aa != null) return;

            foreach (string camid in VAHub.CVCoreLibrary._Config.GetCameraID())
            {
                if (camid != cameraid.ToString()) continue;

                string path = string.Empty;
                DateTime vidstart = DateTime.Now;
                VAHub.CVCoreLibrary.StartTime = vidstart;
                VAHub.CVCoreLibrary.StartRecord(cameraid);

                while (VAHub.CVCoreLibrary.LastVideoPath.ContainsKey(cameraid) == false || string.IsNullOrEmpty(VAHub.CVCoreLibrary.LastVideoPath[cameraid]))
                {
                    TimeSpan ts = DateTime.Now - vidstart;
                    if (ts.TotalSeconds > 10) break;
                    Thread.Sleep(100);
                }

                path = VAHub.CVCoreLibrary.LastVideoPath[cameraid];
                if (string.IsNullOrEmpty(path)) break;

                VA_ALERT al = new VA_ALERT();
                al.FrameUrl = obj.frame_url;
                al.VideoPath = path;
                al.CameraID = cameraid;
                al.RecordStart = vidstart;
                al.MaxPerson = max;
                al.MinPerson = 0;
                al.IsMax = true;
                al.PeopleCount = count;
                al.ZoneID = camid.ToString();
                al.CREATED_DT = DateTime.Now;
                al.CreateDate = al.CREATED_DT;
                al.CreateOfficer = "SYSTEM";
                al.UpdateDate = al.CREATED_DT;
                al.UpdateOfficer = "SYSTEM";
                al.ViewDate = DateTime.MinValue;
                al.AlertDate = al.CreateDate;
                al.Epc = count + "/" + max;
                al.Description = "CameraID " + cameraid + " has " + count + " person. Exceed maximum person " + max.ToString();
                dal.Insert(al);

                if (VAHub.CVCoreLibrary.LastEventID.ContainsKey(cameraid)) VAHub.CVCoreLibrary.LastEventID[cameraid] = al.Id;
                else VAHub.CVCoreLibrary.LastEventID.Add(cameraid, al.Id);

                VAHub.CVCoreLibrary.StartTime = DateTime.Now;
                break;
            }
        }

        public static void PeopleAmountChangeMin(CVCoreObject obj, string cameraid, int count, int min)
        {
            SqlVA_ALERTDAL dal = new SqlVA_ALERTDAL();
            VA_ALERT aa = dal.GetBy("FrameUrl", obj.frame_url);
            if (aa != null) return;

            foreach (string camid in VAHub.CVCoreLibrary._Config.GetCameraID())
            {
                if (camid != cameraid.ToString()) continue;

                string path = string.Empty;
                DateTime vidstart = DateTime.Now;
                if (VAHub.CVCoreLibrary.StartTime == DateTime.MinValue) VAHub.CVCoreLibrary.StartTime = vidstart;
                VAHub.CVCoreLibrary.StartRecord(cameraid);
                
                while (VAHub.CVCoreLibrary.LastVideoPath.ContainsKey(cameraid) == false || string.IsNullOrEmpty(VAHub.CVCoreLibrary.LastVideoPath[cameraid]))
                {
                    TimeSpan ts = DateTime.Now - vidstart;
                    if (ts.TotalSeconds > 10) break;
                    Thread.Sleep(100);
                }

                path = VAHub.CVCoreLibrary.LastVideoPath[cameraid];
                if (string.IsNullOrEmpty(path)) break;

                VA_ALERT al = new VA_ALERT();
                al.FrameUrl = obj.frame_url;
                al.VideoPath = path;
                al.CameraID = cameraid;
                al.MaxPerson = 0;
                al.MinPerson = min;
                al.IsMax = false;
                al.PeopleCount = count;
                al.RecordStart = vidstart;
                al.RecordStop = new DateTime(2001, 1, 1);
                al.ZoneID = camid.ToString();
                al.CREATED_DT = DateTime.Now;
                al.CreateDate = al.CREATED_DT;
                al.CreateOfficer = "SYSTEM";
                al.UpdateDate = al.CREATED_DT;
                al.UpdateOfficer = "SYSTEM";
                al.ViewDate = DateTime.MinValue;
                al.AlertDate = al.CreateDate;
                al.Epc = count + "/" + min;
                al.Description = "CameraID " + cameraid + " has " + count + " person. Minimum person " + min.ToString();
                dal.Insert(al);

                if (VAHub.CVCoreLibrary.LastEventID.ContainsKey(cameraid)) VAHub.CVCoreLibrary.LastEventID[cameraid] = al.Id;
                else VAHub.CVCoreLibrary.LastEventID.Add(cameraid, al.Id);

                break;
            }
        }
    }
}