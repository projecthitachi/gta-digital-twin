﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Polygon
    {
        public static bool PointInPolygon(BaseBox cell, double X, double Y)
        {
            ScatterPoint[] Points = new ScatterPoint[4];

            Points[0] = new ScatterPoint(cell.TopLeftX, cell.TopLeftY, string.Empty);
            Points[1] = new ScatterPoint(cell.TopRightX, cell.TopRightY, string.Empty);
            Points[2] = new ScatterPoint(cell.BottomRightX, cell.BottomRightY, string.Empty);
            Points[3] = new ScatterPoint(cell.BottomLeftX, cell.BottomLeftY, string.Empty);

            // Get the angle between the point and the
            // first and last vertices.
            int max_point = Points.Length - 1;
            double total_angle = GetAngle(
                Points[max_point].UTMX, Points[max_point].UTMY, X, Y, Points[0].UTMX, Points[0].UTMY);

            // Add the angles from the point
            // to each other pair of vertices.
            for (int i = 0; i < max_point; i++)
            {
                total_angle += GetAngle(Points[i].UTMX, Points[i].UTMY, X, Y, Points[i + 1].UTMX, Points[i + 1].UTMY);
            }

            // The total angle should be 2 * PI or -2 * PI if
            // the point is in the polygon and close to zero
            // if the point is outside the polygon.
            return (Math.Abs(total_angle) > 0.000001);
        }

        public static bool PointInBox(BaseBox cell, double X, double Y)
        {
            bool truex = false;
            if (cell.TopLeftX < cell.BottomRightX)
            {
                if (cell.TopLeftX <= X && X <= cell.BottomRightX) truex = true;
            }
            else
            {
                if (cell.BottomRightX <= X && X <= cell.TopLeftX) truex = true;
            }

            if (truex)
            {
                if (cell.TopLeftY < cell.BottomRightY)
                {
                    if (cell.TopLeftY <= Y && Y <= cell.BottomRightY) return true;
                }
                else
                {
                    if (cell.BottomRightY <= Y && Y <= cell.TopLeftY) return true;
                }
            }

            return false;
        }

        public static double GetAngle(double Ax, double Ay, double Bx, double By, double Cx, double Cy)
        {
            // Get the dot product.
            double dot_product = DotProduct(Ax, Ay, Bx, By, Cx, Cy);

            // Get the cross product.
            double cross_product = CrossProductLength(Ax, Ay, Bx, By, Cx, Cy);

            // Calculate the angle.
            return (float)Math.Atan2(cross_product, dot_product);
        }

        private static double DotProduct(double Ax, double Ay, double Bx, double By, double Cx, double Cy)
        {
            // Get the vectors' coordinates.
            double BAx = Ax - Bx;
            double BAy = Ay - By;
            double BCx = Cx - Bx;
            double BCy = Cy - By;

            // Calculate the dot product.
            return (BAx * BCx + BAy * BCy);
        }

        public static double CrossProductLength(double Ax, double Ay, double Bx, double By, double Cx, double Cy)
        {
            // Get the vectors' coordinates.
            double BAx = Ax - Bx;
            double BAy = Ay - By;
            double BCx = Cx - Bx;
            double BCy = Cy - By;

            // Calculate the Z coordinate of the cross product.
            return (BAx * BCy - BAy * BCx);
        }
    }
}
