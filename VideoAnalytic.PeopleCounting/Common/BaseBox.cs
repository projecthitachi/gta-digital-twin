﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class BaseBox
    {
        public double X;
        public double Y;
        public int IndexX = 0; // start from X=0, Y=0
        public int IndexY = 0;
        public List<List<BaseBox>> DirectionCells = new List<List<BaseBox>>();
        public Func<double, double, bool> IsWithin = null;

        public CellMovement Movement = null;
        public bool Movable = true;

        public BaseBox(int indexX, int indexY, double x, double y, CellMovement movement, bool initDirection = false)
        {
            X = x;
            Y = y;
            IndexX = indexX;
            IndexY = indexY;
            Movement = movement;

            if (initDirection) InitDirectionCell();
        }

        public void InitDirectionCell()
        {
            DirectionCells.Clear();
            for (int j = 0; j < 3; j++)
            {
                List<BaseBox> list = new List<BaseBox>();
                for (int i = 0; i < 3; i++)
                {
                    BaseBox item = new BaseBox(j, i, j, i, new CellMovement((Movement.RelativeRightX * 2) / 3 / 2, (Movement.RelativeRightY * 2) / 3 / 2, (Movement.RelativeBelowX * 2) / 3 / 2, (Movement.RelativeBelowY * 2) / 3 / 2, TopLeftX, TopLeftY, (i * Movement.RelativeRightX * 2 / 3), 0, 0, (j * Movement.RelativeBelowY * 2 / 3)), false);
                    item.X = item.TopLeftX;
                    item.Y = item.TopLeftY;
                    list.Add(item);
                }
                DirectionCells.Add(list);
            }
        }

        public bool WithinDirection(double x, double y)
        {
            return Polygon.PointInPolygon(this, x, y);
        }

        #region Corners
        public double TopLeftX
        {
            get
            {
                return Movement.X00 + (Movement.OffsetRightX) + (Movement.OffsetBottomX);
            }
        }

        public double TopLeftY
        {
            get
            {
                return Movement.Y00 + (Movement.OffsetRightY) + (Movement.OffsetBottomY);
            }
        }

        public double TopRightX
        {
            get
            {
                return TopLeftX + (Movement.RelativeRightX * 2);
            }
        }

        public double TopRightY
        {
            get
            {
                return TopLeftY + (Movement.RelativeRightY * 2);
            }
        }

        public double BottomLeftX
        {
            get
            {
                return TopLeftX + (Movement.RelativeBelowX * 2);
            }
        }

        public double BottomLeftY
        {
            get
            {
                return TopLeftY + (Movement.RelativeBelowY * 2);
            }
        }

        public double BottomRightX
        {
            get
            {
                return TopRightX + (Movement.RelativeBelowX * 2);
            }
        }

        public double BottomRightY
        {
            get
            {
                return TopRightY + (Movement.RelativeBelowY * 2);
            }
        }
        #endregion
    }
}
