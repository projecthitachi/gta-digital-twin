﻿// Bacnet.Raspberry.Controllers.RaspberryController
using Kendo.Mvc.Export;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Telerik.Documents.SpreadsheetStreaming;
using uPLibrary.Networking.M2Mqtt.Messages;
using VideoAnalytic.PeopleCounting;
using VideoAnalytic.PeopleCounting.Models.System;
using VideoAnalytic.PeopleCounting.Models;


namespace VideoAnalytic.PeopleCounting.Controllers
{
    public class VideoAnalyticPeopleCountingController : Controller
    {
        public VideoAnalyticPeopleCountingController()
        {
           
        }

        

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest poRequest)
        {
            VAPeopleCountingModel oClass = new VAPeopleCountingModel();
            List<VAPeopleCounting_REC> vResult = oClass.GetList().ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest poRequest, VAPeopleCounting_REC poRecord)
        {
            if (poRecord != null && base.ModelState.IsValid)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                DbContextTransaction oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    VAPeopleCountingModel oClass = new VAPeopleCountingModel();
                    if (!oClass.Insert(oRemoteDB, poRecord))
                    {
                        base.ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    base.ModelState.AddModelError("Error", ex.Message);
                }
            }
            return Json(new VAPeopleCounting_REC[1]
            {
            poRecord
            }.ToDataSourceResult(poRequest, base.ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest poRequest, VAPeopleCounting_REC poRecord)
        {
            if (poRecord != null && base.ModelState.IsValid)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                DbContextTransaction oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    VAPeopleCountingModel oClass = new VAPeopleCountingModel();
                    if (!oClass.Update(oRemoteDB, poRecord))
                    {
                        base.ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    base.ModelState.AddModelError("Error", ex.Message);
                }
            }
            return Json(new VAPeopleCounting_REC[1]
            {
            poRecord
            }.ToDataSourceResult(poRequest, base.ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest poRequest, VAPeopleCounting_REC poRecord)
        {
            if (poRecord != null)
            {
                DatabaseContext oRemoteDB = new DatabaseContext();
                DbContextTransaction oTransaction = oRemoteDB.Database.BeginTransaction();
                try
                {
                    VAPeopleCountingModel oClass = new VAPeopleCountingModel();
                    if (!oClass.Delete(oRemoteDB, poRecord))
                    {
                        base.ModelState.AddModelError("Error", oClass.ErrorMessage);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    base.ModelState.AddModelError("Error", ex.Message);
                }
            }
            return Json(new VAPeopleCounting_REC[1]
            {
            poRecord
            }.ToDataSourceResult(poRequest, base.ModelState));
        }

        public ActionResult ObjectRead([DataSourceRequest] DataSourceRequest poRequest, string DeviceID)
        {
            VAPeopleCountingModel oClass = new VAPeopleCountingModel();
            List<VAPeopleCounting_REC> vResult = oClass.GetListDataObject(DeviceID).ToList();
            return Json(vResult.ToDataSourceResult(poRequest));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateStatusObj(string[] RecordID, string[] voObject, string DeviceID)
        {
            Dictionary<string, string> vsLogMessage = new Dictionary<string, string>();
            DatabaseContext oRemoteDB = new DatabaseContext();
            DbContextTransaction oTransaction = oRemoteDB.Database.BeginTransaction();
            try
            {
                VAPeopleCountingModel oClass = new VAPeopleCountingModel();
                string voRecordID = (RecordID != null) ? string.Join(", ", RecordID) : null;
                if (!oClass.updateStatusObj(oRemoteDB, voRecordID, DeviceID))
                {
                    throw new Exception(oClass.ErrorMessage);
                }
                oTransaction.Commit();
                string targetPathView = base.Server.MapPath("~/Components/Plugins/Bacnet/");
                if (!Directory.Exists(targetPathView))
                {
                    Directory.CreateDirectory(targetPathView);
                }
                else
                {
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(voObject));
                    System.IO.File.WriteAllText(base.Server.MapPath("~/Components/Plugins/Bacnet/" + DeviceID + ".json"), strValue);
                }
                vsLogMessage.Add("errorcode", "0");
                vsLogMessage.Add("title", "Success");
                vsLogMessage.Add("msg", "Insert Data seccess");
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                vsLogMessage.Add("errorcode", "100");
                vsLogMessage.Add("title", "warning");
                vsLogMessage.Add("msg", ex.Message);
            }
            return Json(vsLogMessage, JsonRequestBehavior.DenyGet);
        }

        public ActionResult Command(string cmd, string value)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            if (value != "All")
            {
                string strValue2 = "{'deviceID':'" + value + "'}";
                Constants.MqttClient.Publish("Bacnet/Raspberry/Whois", Encoding.UTF8.GetBytes(strValue2), 2, retain: false);
            }
            else
            {
                VAPeopleCountingModel oClass = new VAPeopleCountingModel();
                List<VAPeopleCounting_REC> vResult = oClass.GetList().ToList();
                foreach (VAPeopleCounting_REC oData in vResult)
                {
                    string strValue = Convert.ToString(cmd);
                    Constants.MqttClient.Publish("Bacnet/Raspberry/" + oData.DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), 2, retain: false);
                }
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        //public static void Raspberry_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        //{
        //    string[] topic = e.Topic.Split('/');
        //    string Category = topic[0];
        //    if (topic.Length <= 3)
        //    {
        //        return;
        //    }
        //    string message = Encoding.Default.GetString(e.Message);
        //    if (!GlobalFunction.IsValidJson(message))
        //    {
        //        return;
        //    }
        //    IHubContext RT = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();
        //    object result = JsonConvert.DeserializeObject(message);
        //    JObject voobj = JObject.Parse(result.ToString());
        //    string Brand = topic[1];
        //    string hostDevice = topic[2];
        //    string cmd = topic[3];
        //    if (cmd == "WhoisDatas")
        //    {
        //        string DeviceID = voobj["0"].ToString().Split(':')[1];
        //        DatabaseContext oRemoteDB2 = new DatabaseContext();
        //        VAPeopleCountingModel oQuery2 = new VAPeopleCountingModel();
        //        VAPeopleCountingObject_REC voRecord = new VAPeopleCountingObject_REC();
        //        voRecord.DeviceID = DeviceID;
        //        oQuery2.ObjectDelete(oRemoteDB2, voRecord);
        //        int num;
        //        for (int i = 1; i < voobj.Count; i = num + 1)
        //        {
        //            VAPeopleCountingObject_REC poRecord2 = new VAPeopleCountingObject_REC();
        //            poRecord2.DeviceID = DeviceID;
        //            poRecord2.ObjectName = voobj[string.Concat(i) ?? ""].ToString().Split(':')[0];
        //            poRecord2.Instance = voobj[string.Concat(i) ?? ""].ToString().Split(':')[1];
        //            oQuery2.ObjectInsert(oRemoteDB2, poRecord2);
        //            num = i;
        //        }
        //        RT.Clients.All.Raspberry("Whois", DeviceID);
        //    }
        //    else if (cmd == "Error")
        //    {
        //        RT.Clients.All.Raspberry("Error", voobj["msg"]);
        //    }
        //    else if (cmd == "Datas")
        //    {
        //        DatabaseContext oRemoteDB = new DatabaseContext();
        //        VAPeopleCountingModel oQuery = new VAPeopleCountingModel();
        //        VAPeopleCountingDataLog_REC poRecord = new VAPeopleCountingDataLog_REC();
        //        poRecord.RecordTimestamp = DateTime.Now;
        //        poRecord.DeviceID = hostDevice;
        //        poRecord.ObjectID = hostDevice;
        //        poRecord.Payload = message;
        //        oQuery.DataLogInsert(oRemoteDB, poRecord);
        //        RT.Clients.All.Raspberry("DataLog", message);
        //    }
        //}

        public ActionResult DataLog()
        {
            return View();
        }

        public ActionResult GetListDataLog([DataSourceRequest] DataSourceRequest poRequest, string DeviceID)
        {
            VAPeopleCountingModel oModel = new VAPeopleCountingModel();
            IList<VAPeopleCountingDataLog_REC> oTable = oModel.GetListDataLog(DeviceID).ToList();
            JsonResult jsonResult = Json(oTable.ToDataSourceResult(poRequest));
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public IEnumerable<VAPeopleCountingDataLog_REC> GetListDataLogReport()
        {
            VAPeopleCountingModel oClass = new VAPeopleCountingModel();
            return oClass.GetListDataLog().ToList();
        }

        [HttpPost]
        public FileStreamResult ExportServer(string model, string data)
        {
            IList<ExportColumnSettings> columnsData2 = JsonConvert.DeserializeObject<IList<ExportColumnSettings>>(HttpUtility.UrlDecode(model));
            columnsData2 = columnsData2.Skip(1).ToArray();
            dynamic options = JsonConvert.DeserializeObject(HttpUtility.UrlDecode(data));
            SpreadDocumentFormat exportFormat3 = (!((options.format.ToString() == "csv") ? true : false)) ? (exportFormat3 = SpreadDocumentFormat.Xlsx) : (exportFormat3 = SpreadDocumentFormat.Csv);
            Action<ExportCellStyle> cellStyle = ChangeCellStyle;
            Action<ExportRowStyle> rowStyle = ChangeRowStyle;
            Action<ExportColumnStyle> columnStyle = ChangeColumnStyle;
            string fileName = string.Format("{0}.{1}", options.title, options.format);
            string mimeType = Helpers.GetMimeType(exportFormat3);
            Stream exportStream = (exportFormat3 == SpreadDocumentFormat.Xlsx) ? GetListDataLogReport().ToXlsxStream(columnsData2) : GetListDataLogReport().ToCsvStream(columnsData2);
            FileStreamResult fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0L, SeekOrigin.Begin);
            return fileStreamResult;
        }

        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            SpreadBorder border = new SpreadBorder(SpreadBorderStyle.Thick, new SpreadThemableColor(new SpreadColor(0, 0, 0)));
            SpreadCellFormat spreadCellFormat = new SpreadCellFormat();
            spreadCellFormat.FontSize = 11.0;
            spreadCellFormat.ForeColor = new SpreadThemableColor(new SpreadColor(0, 0, 0));
            spreadCellFormat.WrapText = false;
            SpreadCellFormat format = spreadCellFormat;
            e.Cell.SetFormat(format);
        }

        private void ChangeRowStyle(ExportRowStyle e)
        {
            e.Row.SetHeightInPixels((e.Index == 0) ? 80 : 30);
        }

        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            double width = (e.Name == "Item ID" || e.Name == "Barcode") ? 200 : 100;
            e.Column.SetWidthInPixels(width);
        }
    }

}
