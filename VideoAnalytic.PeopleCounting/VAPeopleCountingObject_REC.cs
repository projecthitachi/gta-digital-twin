﻿// Bacnet.Raspberry.Models.RaspberryObject_REC
using System;
using System.ComponentModel.DataAnnotations;

namespace VideoAnalytic.PeopleCounting.Models
{
    public class VAPeopleCountingObject_REC
    {
        [Display(Name = "RecordID")]
        public long RecordID
        {
            get;
            set;
        }

        [Display(Name = "RecordTimestamp")]
        public DateTime RecordTimestamp
        {
            get;
            set;
        }

        [Display(Name = "Record Status")]
        public int RecordStatus
        {
            get;
            set;
        }

        [Display(Name = "Device ID")]
        public string DeviceID
        {
            get;
            set;
        }

        [Display(Name = "ObjectName")]
        public string ObjectName
        {
            get;
            set;
        }

        [Display(Name = "Instance")]
        public string Instance
        {
            get;
            set;
        }
    }
}