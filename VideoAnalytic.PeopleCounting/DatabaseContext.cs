﻿// Bacnet.Raspberry.Models.System.DatabaseContext
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

namespace VideoAnalytic.PeopleCounting.Models.System
{ 
    public class DatabaseContext : DbContext
    {
        private int ErrorNo
        {
            get;
            set;
        }

        private string ErrorMessage
        {
            get;
            set;
        }

        public DatabaseContext()
            : base("name=DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e2)
            {
                ErrorMessage = e2.Message;
                return 0;
            }
            catch (DbUpdateException e3)
            {
                ErrorMessage = e3.Message;
                return 0;
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                return 0;
            }
        }
    }
}