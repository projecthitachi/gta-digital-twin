﻿// Bacnet.Raspberry.Components.Constants
using uPLibrary.Networking.M2Mqtt;
using CVCoreLib;

namespace VideoAnalytic.PeopleCounting
{
    public class Constants
    {
        public static CVCoreLibrary _Library = CVCoreLibrary.Instance;
        public static MqttClient MqttClient = null;
    }
}
