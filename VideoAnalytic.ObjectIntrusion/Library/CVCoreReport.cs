﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace CVCoreLib
{
    [Serializable]
    public class CVCoreReport
    {
        public DateTime StartTime { get; set; }
        public double Rate { get; set; }
        public PeopleReport CurrentPeopleCount { get; set; }

        public List<PeopleReport> TodayPeopleCount { get; set; }
        public QueueTime CurrentQueueTime { get; set; }
        public List<QueueTime> TodayQueueTime { get; set; }
        public int TodayQueueTimePerLaneIndex { get; set; }
        public List<QueueTimePerLane> TodayQueueTimePerLane { get; set; }
        public List<QueueTime> Worst3Lanes { get; set; }
        public QueueTime CurrentQueueTimePerOpenLane { get; set; }
        public QueueTime CurrentQueueTimePerAllLane { get; set; }
        public QueueTime CurrentQueueTimeLongest { get; set; }
        public QueueTime CurrentCompliance { get; set; }
        public int LanesOpen { get; set; }

        public List<DateTime> GetDatePart(DateTime day, int ReportPeriod)
        {
            List<DateTime> DatePart = new List<DateTime>();
            DateTime that = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0);
            DatePart.Add(that);
            while(that.Day == day.Day)
            {
                that = that.AddMinutes(ReportPeriod);
                DatePart.Add(that);
            }
            return DatePart;
        }
    }

    [Serializable]
    public class PeopleReport
    {
        public DateTime Date { get; set; }
        public string DateStr { get; set; }
        public int Count { get; set; }
        public double CrowdIndex
        {
            get
            {
                CVCoreConfig config = new CVCoreConfig();
                return (double)Count / (double)config.GetMaxCrowdPerson() * (double)100;
            }
        }
    }

    [Serializable]
    public class QueueTime
    {
        public Random rnd = new Random();
        public static CVCoreConfig Config = new CVCoreConfig();
        public DateTime Date { get; set; }
        public int LaneID { get; set; }
        public string DateStr { get; set; }
        public int QL = 0;
        public int QueueLength
        {
            get
            {
                return QL;
            }
            set
            {
                QL = value;
                //QL = rnd.Next(60) + 60;
            }
        }
        public double Rate = 0f;
        public double LanesRate
        {
            get
            {
                if (double.IsNaN(Rate)) return 0f;
                return Rate;
            }
            set
            {
                if (double.IsNaN(value)) Rate = 0;
                else Rate = value;
            }
        }
        private double Dbl = 0f;
        [ReadOnlyColumn]
        public double QueueLengthDbl
        {
            get
            {
                if (double.IsNaN(Dbl)) return 0f;
                return Dbl;
            }
            set {
                if (double.IsNaN(value)) Dbl = 0;
                else Dbl = value;
            }
        }

        public double QueuePercent
        {
            get
            {
                return ((double)QueueLength / ((double)Config.GetTargetQueueTime() * (double)60)) * (double)100;
            }
        }

        public string GetPercent(double percent)
        {
            string color = "#b6ff00";
            percent = Math.Round(percent);
            if (percent <= 50) color = "#b6ff00";
            else if (percent > 50) color = "#ff6a00";
            string str = "text-align: center; height: 160px; background: -webkit-linear-gradient(top, " + color + " " + percent + "%, #ffffff " + percent + "%); background:-moz-linear-gradient(top, " + color + " " + percent + "%, #ffffff " + percent + "%);background: -ms-linear-gradient(top, " + color + " " + percent + "%, #ffffff " + percent + "%); background: -o-linear-gradient(top, " + color + " " + percent + "%, #ffffff " + percent + "%); background: linear-gradient(to top, " + color + " " + percent + "%, #ffffff " + percent + "%); border: 1px solid black;";
            return str;
        }
    }

    [Serializable]
    public class QueueTimePerLane
    {
        public int LaneID { get; set; }
        public List<QueueTime> QueueLengthList { get; set; }

        public QueueTime GetQueueTime(DateTime epoch)
        {
            QueueTime qt = new QueueTime();
            qt.Date = epoch;
            qt.DateStr = epoch.ToString("hh:mm tt");

            double sumql = 0f;
            double sumqldbl = 0f;
            double sumrate = 0f;
            foreach (QueueTime q in QueueLengthList)
            {
                sumql += q.QueueLength;
                sumqldbl += q.QueueLength;
                sumrate += q.LanesRate;
            }

            qt.LanesRate = sumrate;
            qt.QueueLengthDbl = sumqldbl;
            qt.QueueLength = (int)sumql;
            return qt;
        }
    }
}
