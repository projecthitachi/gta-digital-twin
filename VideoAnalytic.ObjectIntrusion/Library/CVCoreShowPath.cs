﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVCoreLib
{
    [Serializable]
    public class CVCoreShowPathRectangle
    {
        public int h { get; set; }
        public int y { get; set; }
        public int w { get; set; }
        public int x { get; set; }
    }


    [Serializable]
    public class CVCoreShowPathTrackList
    {
        public int frame_height { get; set; }
        public int frame_id { get; set; }
        public CVCoreShowPathRectangle rectangle { get; set; }
        public int frame_width { get; set; }
        public int time { get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }
    }

    [Serializable]
    public class CVCoreShowPathObjectList
    {
        public int frame_id { get; set; }
        public int entry_time { get; set; }
        public double confidence { get; set; }
        public double velocity_deviation { get; set; }
        public int frame_width { get; set; }
        public int type { get; set; }
        public int exit_time { get; set; }
        public int frame_height { get; set; }
        public int track_total { get; set; }
        public CVCoreShowPathRectangle rectangle { get; set; }
        public int object_id { get; set; }
        public List<CVCoreShowPathTrackList> track_list { get; set; }
        public double avg_velocity { get; set; }
    }

    [Serializable]
    public class CVCoreShowPathCameraList
    {
        public int object_total { get; set; }
        public List<object> predicted_path_list { get; set; }
        public int predicted_path_total { get; set; }
        public List<CVCoreShowPathObjectList> object_list { get; set; }
        public int camera_id { get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }
    }

    [Serializable]
    public class CVCoreShowPathObject
    {
        public int camera_total { get; set; }
        public List<CVCoreShowPathCameraList> camera_list { get; set; }
    }
}
