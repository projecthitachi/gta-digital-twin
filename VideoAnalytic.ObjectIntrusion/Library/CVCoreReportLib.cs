﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace CVCoreLib
{
    public class CVCoreReportLib
    {
        public CVCoreDB _DB = new CVCoreDB();
        public CVCoreCache Cache = new CVCoreCache();
        public CVCoreConfig _Config = new CVCoreConfig();
        public string format = "hh:mm tt";


        public int CountPeopleReal(int timestamp, Action<string, int> CountPeople, int cameraid = 0, bool equal = false)
        {
            if (timestamp != 0)
            {
                int count = _DB.GetPeopleCount(timestamp, _Config.GetMaxPeopleCountTime(), cameraid, equal);
                if (CountPeople != null) CountPeople(cameraid.ToString(), count);
                return count;
            }
            return 0;
        }

        public void GetTodayQueueTimePerLane(double rate, DateTime epoch, CVCoreReport report)
        {
            string key = Cache.MakeKey(new List<object> { "TodayQueueTimePerLane", rate, epoch, report });
            CVCoreReport test = Cache.GetObject<CVCoreReport>(key);
            if (_Config.GetUseCache() && Cache.IsPastHour(epoch) && test != null)
            {
                report.TodayQueueTimePerLane = test.TodayQueueTimePerLane;
                report.CurrentQueueTimePerAllLane = test.CurrentQueueTimePerAllLane;
                report.CurrentQueueTimePerOpenLane = test.CurrentQueueTimePerOpenLane;
                report.CurrentQueueTimeLongest = test.CurrentQueueTimeLongest;
                report.CurrentCompliance = test.CurrentCompliance;
                report.LanesOpen = test.LanesOpen;
                report.Worst3Lanes = test.Worst3Lanes;
                return;
            }

            List<DateTime> dts = report.GetDatePart(epoch, _Config.GetReportPeriod());
            report.TodayQueueTimePerLane = new List<QueueTimePerLane>();
            LanesDAL dal = new LanesDAL();
            List<Lanes> lanes = dal.GetAll();
            foreach (Lanes l in lanes)
            {
                QueueTimePerLane qtpl = new QueueTimePerLane();
                qtpl.LaneID = l.LaneID;
                qtpl.QueueLengthList = new List<QueueTime>();

                report.TodayQueueTimePerLane.Add(qtpl);
            }

            double sumQT = 0f;
            double longest = 0f;
            double max = _Config.GetTargetQueueTime() * 60;
            int notcomply = 0;
            int lastdate = 0;
            for (int i = 0; i < dts.Count; i++)
            {
                var dt = dts[i];
                int d = (int)Calculation.ToUnixEpochTime(dt);
                double ratex = _DB.GetOpenLanesRate(dt);

                foreach (Lanes l in lanes)
                {
                    QueueTimePerLane qtpl = report.TodayQueueTimePerLane.Where(m => m.LaneID == l.LaneID).FirstOrDefault();
                    if (qtpl != null)
                    {
                        QueueTime qt = new QueueTime();
                        qt.Date = dt;
                        qt.DateStr = dt.ToString("HH:mm");
                        qtpl.QueueLengthList.Add(qt);
                    }
                }

                List<CVCorePeopleExit> ppl3 = _DB.GetQueueTimePerLane(1, dt, dt.AddMinutes(_Config.GetReportPeriod()));
                foreach (CVCorePeopleExit pp in ppl3)
                {
                    QueueTime qt = new QueueTime { LaneID = pp.LaneID, QueueLength = pp.ExitTime, LanesRate = pp.ExitTime * ratex, Date = dt, DateStr = dt.ToString("HH:mm") };
                    QueueTimePerLane qtpl = report.TodayQueueTimePerLane.Where(m => m.LaneID == pp.LaneID).FirstOrDefault();
                    if (qtpl != null)
                    {
                        qtpl.QueueLengthList.RemoveAt(qtpl.QueueLengthList.Count - 1);
                        qtpl.QueueLengthList.Add(qt);

                        DateTime n2 = DateTime.Now;
                        if (dt <= n2 && n2 <= dt.AddMinutes(_Config.GetReportPeriod()))
                        {
                            lastdate = i;
                            sumQT += pp.ExitTime;
                            if (longest < pp.ExitTime) longest = pp.ExitTime;
                            if (pp.ExitTime > max)
                            {
                                notcomply++;
                            }
                        }
                    }
                }
            }

            report.TodayQueueTimePerLaneIndex = lastdate;
            //DateTime n = DateTime.Now;
            //if (epoch.Day != n.Day || epoch.Month != n.Month || epoch.Year != n.Year)
            //{
            //    //for (int i = lastdate + 1; i < report.TodayQueueTime.Count - 1; i++)
            //    //    report.TodayQueueTime.RemoveAt(i);
            //}            

            //WORST 3 LANES
            report.Worst3Lanes = new List<QueueTime>();
            Dictionary<int, double> sum = new Dictionary<int, double>();

            foreach (QueueTimePerLane lane in report.TodayQueueTimePerLane)
            {
                foreach (QueueTime qt in lane.QueueLengthList)
                {
                    if (sum.ContainsKey(lane.LaneID))
                        sum[lane.LaneID] += qt.QueueLength;
                    else
                        sum.Add(lane.LaneID, qt.QueueLength);
                }
            }
            List<double> values = sum.Values.ToList().OrderByDescending(m => m).Take(3).ToList();
            List<int> kys = sum.Keys.ToList();
            List<QueueTime> worst3 = new List<QueueTime>();
            foreach (int ky in kys)
            {
                if (values.Contains(sum[ky]) && sum[ky] != 0)
                {
                    QueueTimePerLane ql = report.TodayQueueTimePerLane.Where(m => m.LaneID == ky).FirstOrDefault();
                    if (ql == null) continue;
                    QueueTime qt = ql.GetQueueTime(dts[report.TodayQueueTimePerLaneIndex]);
                    qt.LaneID = ky;
                    worst3.Add(qt);
                }
            }
            report.Worst3Lanes = worst3;
            CalculatePer(report, epoch);
            if (_Config.GetUseCache() && Cache.IsPastHour(epoch)) Cache.SaveObject(key, report);
        }

        public void GetCurrentPeopleCount(int timestamp, CVCoreReport report)
        {
            DateTime epoch = _Config.FromUnixEpoch(timestamp);
            int count = CountPeopleReal(timestamp, null);
            report.CurrentPeopleCount = new PeopleReport { Count = count, Date = epoch, DateStr = epoch.ToString(format) };
        }

        public DateTime GetHistoryPeopleCount(DateTime dt, CVCoreReport report)
        {
            int timestamp = (int)_Config.ToUnixEpoch(dt);
            DateTime epoch = Calculation.FromUnixEpochTime(timestamp);
            int count = CountPeopleReal(timestamp, null);
            report.CurrentPeopleCount = new PeopleReport { Count = count, Date = epoch, DateStr = epoch.ToString(format) };
            return epoch;
        }

        public double GetCurrentQueueTime(DateTime epoch, CVCoreReport report)
        {
            int totalmins = epoch.Hour * 60 + epoch.Minute;
            int min = totalmins % _Config.GetReportPeriod();
            DateTime start = new DateTime(epoch.Year, epoch.Month, epoch.Day, epoch.AddMinutes(-1 * min).Hour, epoch.AddMinutes(-1 * min).Minute, 0);
            DateTime end = new DateTime(epoch.Year, epoch.Month, epoch.Day, epoch.AddMinutes(-1 * min + _Config.GetReportPeriod()).Hour, epoch.AddMinutes(-1 * min + _Config.GetReportPeriod()).Minute, 0);

            double rate = _DB.GetOpenLanesRate(start);

            CVCorePeopleExit ppl = _DB.GetQueueTime(1, start, end);
            int temp = 0;
            if (ppl != null) temp = ppl.ExitTime;
            report.CurrentQueueTime = new QueueTime { LaneID = -1, QueueLength = temp, LanesRate = temp * rate, Date = start, DateStr = start.ToString(format) };
            return rate;
        }

        public void GetTodayPeopleCount(DateTime epoch, CVCoreReport report)
        {
            int count;
            List<DateTime> dts = report.GetDatePart(epoch, _Config.GetReportPeriod());
            report.TodayPeopleCount = new List<PeopleReport>();
            foreach (DateTime dt in dts)
            {
                int d = (int)Calculation.ToUnixEpochTime(dt);
                count = CountPeopleReal(d, null);
                PeopleReport pr = new PeopleReport { Count = count, Date = epoch, DateStr = epoch.ToString(format) };
                report.TodayPeopleCount.Add(pr);
            }
        }

        public void GetTodayQueueTime(DateTime epoch, CVCoreReport report)
        {
            List<DateTime> dts = report.GetDatePart(epoch, _Config.GetReportPeriod());
            report.TodayQueueTime = new List<QueueTime>();
            foreach (DateTime dt in dts)
            {
                int d = (int)Calculation.ToUnixEpochTime(dt);
                CVCorePeopleExit ppl2 = _DB.GetQueueTime(1, dt, dt.AddMinutes(_Config.GetReportPeriod()));
                int temp2 = 0;
                if (ppl2 != null) temp2 = ppl2.ExitTime;

                double ratex = _DB.GetOpenLanesRate(dt);
                QueueTime qt = new QueueTime { LaneID = -1, QueueLength = temp2, LanesRate = temp2 * ratex, Date = dt, DateStr = dt.ToString(format) };
                report.TodayQueueTime.Add(qt);
            }
        }

        public void CalculatePer(CVCoreReport report, DateTime epoch)
        {
            if (report == null || report.TodayQueueTimePerLane == null) return;
            int notcomply = 0;
            double sumQT = 0;
            double longest = 0;
            double rate = report.Rate;
            for (int i=0; i<report.TodayQueueTimePerLane.Count; i++)
            {
                for (int j=0;j<report.TodayQueueTimePerLane[i].QueueLengthList.Count; j++)
                {
                    QueueTime qt = report.TodayQueueTimePerLane[i].QueueLengthList[j];
                    DateTime end = qt.Date.AddMinutes(_Config.GetReportPeriod());
                    DateTime now = DateTime.Now;
                    if (qt.Date <= now && now <= end)
                    {
                        sumQT += qt.QueueLength;
                        if (longest < qt.QueueLength) longest = qt.QueueLength;
                        if (qt.QueueLength > _Config.GetTargetQueueTime() * 60) notcomply++;
                    }
                }
            }
            List<LanesStatus> all = _DB.GetOpenLanes(epoch.AddSeconds(_Config.GetEpochAdjustmentSec()));
            int open = 0;
            if (all != null && all.Count > 0) open = all.Count(m => m.OpenClose == true);

            double comply = (all.Count - notcomply) / (double)all.Count * (double)100;

            QueueTime perOpen = new QueueTime { LaneID = -1, QueueLengthDbl = (sumQT / (double)open / (double)60), LanesRate = (sumQT / (double)open / (double)60) * rate };
            QueueTime perAll = new QueueTime { LaneID = -1, QueueLengthDbl = (sumQT / (double)all.Count / (double)60), LanesRate = (sumQT / (double)all.Count / (double)60) * rate };
            QueueTime longestx = new QueueTime { LaneID = -1, QueueLengthDbl = (longest / (double)60), LanesRate = (longest / (double)60) * rate };
            QueueTime compliance = new QueueTime { LaneID = -1, QueueLengthDbl = comply, LanesRate = (comply / (double)60) * rate };

            report.CurrentQueueTimePerAllLane = perAll;
            report.CurrentQueueTimePerOpenLane = perOpen;
            report.CurrentQueueTimeLongest = longestx;
            report.CurrentCompliance = compliance;
            report.LanesOpen = open;
        }
    }
}
