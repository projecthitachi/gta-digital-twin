﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Net;
using System.Data;
using System.Net.Security;
using Newtonsoft.Json;
using System.IO;
using Common;
using VideoAnalytic.ObjectIntrusion;

namespace CVCoreLib
{
    public class CVCoreLibrary
    {
        public int MaxAlert = 2;
        public int SubCameraTotal = 10;

        public VAHubModel MyHubObject = null;
        public CVCoreDB _DB = new CVCoreDB();
        public CVCoreCache Cache = new CVCoreCache();
        public CVCoreConfig _Config = new CVCoreConfig();
        public CVCoreReport _Report = new CVCoreReport();
        public CVCoreReport _ReportReal = new CVCoreReport();
        public CVCoreReportLib _ReportLib = new CVCoreReportLib();


        public Action<string, int> CountPeople;
        public Action<string> QueueTimeRefresh;
        public Action<string, TagObject> SetImage;
        public Action<string, TagObject> SetImageBackground;
        public Action<CVCoreObject, string, int, int> PeopleAmountChangeMax;
        public Action<CVCoreObject, string, int, int> PeopleAmountChangeMin;

        public delegate void DelCountPeople(string cameraid, int str);
        public delegate void DelQueueTimeRefresh(string cameraid);
        public delegate void DelSetImage(string cameraid, TagObject obj);
        public delegate void DelSetImageBackground(string cameraid, TagObject obj);
        public delegate void DelPeopleAmountChangeMax(CVCoreObject obj, string cameraid, int b, int c);
        public delegate void DelPeopleAmountChangeMin(CVCoreObject obj, string cameraid, int b, int c);

        public event DelCountPeople OnCountPeople;
        public event DelQueueTimeRefresh OnQueueTimeRefresh;
        public event DelSetImage OnSetImage;
        public event DelSetImageBackground OnSetImageBackground;
        public event DelPeopleAmountChangeMax OnPeopleAmountChangeMax;
        public event DelPeopleAmountChangeMin OnPeopleAmountChangeMin;

        public int MaxPerson { get; set; }
        public int MinPerson { get; set; }
        public bool IsRunning = false;
        public Thread RunningThread = null;
        public Thread PictureThread = null;
        public Thread PathThread = null;
        public Thread CleanUpThread = null;
        public Thread StopRecordThread = null;
        public long ItemInserted = 0;
        public long PersonInserted = 0;
        public object PictureLock = new object();
        public object CameraLock = new object();
        public List<string> _CameraID = new List<string>();
        public int LastFrameTimeStamp = 0;
        public int CameraCount = 0;
        public List<CVCoreObject> Pictures = new List<CVCoreObject>();
        public Dictionary<string, string> PicturesBackground = new Dictionary<string, string>();
        public List<TagObject> CameraObject = new List<TagObject>();
        public Dictionary<string, string> PictureUrl = new Dictionary<string, string>();
        public Dictionary<string, int> ObjectTime = new Dictionary<string, int>();
        public Dictionary<string, int> ObjectTime2 = new Dictionary<string, int>();
        public Dictionary<string, DateTime> ObjectTimeCreated = new Dictionary<string, DateTime>();
        public Dictionary<string, DateTime> ObjectTime2Created = new Dictionary<string, DateTime>();
        public Dictionary<string, DateTime> ProcessedFrames = new Dictionary<string, DateTime>();
        public object ProcessedLock = new object();
        public Dictionary<string, string> LastEventID = new Dictionary<string, string>();
        public DateTime StartTime = DateTime.MinValue;
        public string SingleCameraID = string.Empty;
       
        public List<string> CameraID
        {
            get
            {
                if (string.IsNullOrEmpty(SingleCameraID) == false) return new List<string> { SingleCameraID };
                return _Config.GetCameraID();
            }
            set
            {
                _CameraID = value;
                Pictures.Clear();
                PictureUrl.Clear();
            }
        }

        private CVCoreLibrary()
        {
            
        }

        private static object _Object = new object();
        private static CVCoreLibrary _Instance;
        public static CVCoreLibrary Instance
        {
            get
            {
                if (_Instance != null) return _Instance;
                lock (_Object)
                {
                    _Instance = new CVCoreLibrary();
                }
                return _Instance;
            }
        }

        public void StopThread(Thread t)
        {
            if (t == null) return;
            try
            {
                t.Abort();
            }
            catch { }
        }

        public void Stop()
        {
            IsRunning = false;
            StopThread(RunningThread);
            StopThread(PictureThread);
            StopThread(PathThread);
            StopThread(CleanUpThread);
            StopThread(StopRecordThread);
            RunningThread = null;
            PictureThread = null;
            PathThread = null;
            CleanUpThread = null;
            ItemInserted = 0;
            PersonInserted = 0;
        }

        public void StartSingle(string cameraid)
        {
            SingleCameraID = cameraid;
            Start();
        }

        public void Start()
        {
            IsRunning = true;
            StopThread(RunningThread);
            StopThread(PictureThread);
            StopThread(PathThread);
            StopThread(CleanUpThread);
            StopThread(StopRecordThread);

            ThreadStart ts = new ThreadStart(StartReal);
            RunningThread = new Thread(ts);
            RunningThread.Start();

            ThreadStart ts2 = new ThreadStart(PictureReal);
            PictureThread = new Thread(ts2);
            PictureThread.Start();

            ThreadStart ts5 = new ThreadStart(CleanupReal);
            CleanUpThread = new Thread(ts5);
            CleanUpThread.Start();

            ThreadStart ts6 = new ThreadStart(RecordThreadReal);
            StopRecordThread = new Thread(ts6);
            StopRecordThread.Start();
        }

        public void AddCount(DataTable dt)
        {
            try
            {
                ItemInserted++;
                PersonInserted += dt.Rows.Count;
            }
            catch
            {
                ItemInserted = 0;
                PersonInserted = 0;
            }
        }

        public CVCoreReport GetHistoryReport(DateTime start)
        {
            start = new DateTime(start.Year, start.Month, start.Day, 23, 59, 59);
            string key = Cache.MakeKey(new List<object> { "GetHistoryReport", start });
            CVCoreReport test = Cache.GetObject<CVCoreReport>(key);
            CVCoreReport _report = new CVCoreReport();

            if (_Config.GetUseCache() && Cache.IsPastHour(start) && test != null)
            {
                _report.CurrentPeopleCount = test.CurrentPeopleCount;
                _report.CurrentQueueTime = test.CurrentQueueTime;
                _report.TodayPeopleCount = test.TodayPeopleCount;
                _report.TodayQueueTime = test.TodayQueueTime;
                _report.TodayQueueTimePerLane = test.TodayQueueTimePerLane;
                _report.CurrentQueueTimePerAllLane = test.CurrentQueueTimePerAllLane;
                _report.CurrentQueueTimePerOpenLane = test.CurrentQueueTimePerOpenLane;
                _report.CurrentQueueTimeLongest = test.CurrentQueueTimeLongest;
                _report.CurrentCompliance = test.CurrentCompliance;
                _report.LanesOpen = test.LanesOpen;
                _report.Worst3Lanes = test.Worst3Lanes;
                return _report;
            }
            
            // CURRENT PEOPLE COUNT
            _ReportLib.GetHistoryPeopleCount(start, _report);

            // CURRENT QUEUE LENGTH
            double rate = _ReportLib.GetCurrentQueueTime(start, _report);
            _report.Rate = rate;

            // TODAY PEOPLE COUNT
            _ReportLib.GetTodayPeopleCount(start, _report);

            // TODAY QUEUE LENGTH
            _ReportLib.GetTodayQueueTime(start, _report);

            // TODAY QUEUE LENGTH PER LANE
            _ReportLib.GetTodayQueueTimePerLane(rate, start, _report);

            if (_Config.GetUseCache() && Cache.IsPastHour(start)) Cache.SaveObject(key, _report);
            return _report;
        }

        private void ReportReal()
        {
            while(IsRunning)
            {
                try
                {
                    // CURRENT PEOPLE COUNT
                    int timestampdb = _DB.GetLastTimeStamp();
                    DateTime epoch = DateTime.Now;
                    int timestamp = _Config.ToUnixEpoch(epoch);
                    DateTime epoch2 = Common.Calculation.FromUnixEpochTime(timestampdb);
                    if ((epoch - epoch2).TotalHours < 1)
                        epoch = epoch2;
                    if (epoch2 > epoch)
                        epoch = epoch2;

                    _ReportLib.GetCurrentPeopleCount((int)Common.Calculation.ToUnixEpochTime(epoch), _Report);
                    _Report.StartTime = epoch;
                    _ReportReal = Common.HexOps.DeepClone<CVCoreReport>(_Report);

                    // CURRENT QUEUE LENGTH
                    double rate = _ReportLib.GetCurrentQueueTime(epoch, _Report);
                    _Report.Rate = rate;
                    _ReportReal = Common.HexOps.DeepClone<CVCoreReport>(_Report);

                    // TODAY PEOPLE COUNT
                    _ReportLib.GetTodayPeopleCount(epoch, _Report);
                    _ReportReal = Common.HexOps.DeepClone<CVCoreReport>(_Report);

                    // TODAY QUEUE LENGTH
                    _ReportLib.GetTodayQueueTime(epoch, _Report);
                    _ReportReal = Common.HexOps.DeepClone<CVCoreReport>(_Report);

                    // TODAY QUEUE LENGTH PER LANE
                    _ReportLib.GetTodayQueueTimePerLane(rate, epoch, _Report);
                    _ReportReal = Common.HexOps.DeepClone<CVCoreReport>(_Report);
                }
                catch (Exception ex)
                {
                    Common.LibraryTools.WriteName("CVCoreLib", ex);
                }

                Thread.Sleep(2000);
            }
        }

        private void CleanupReal()
        {
            while (IsRunning)
            {
                try
                {
                    string[] files = Directory.GetFiles(_Config.GetTempFolder());
                    for (int i=0;i<files.Length; i++)
                    {
                        string file = files[i];
                        FileInfo fi = new FileInfo(file);
                        TimeSpan span = DateTime.Now - fi.CreationTime;
                        if (span.TotalDays >= 1)
                        {
                            try
                            {
                                File.Delete(files[i]);
                            }
                            catch { }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Common.LibraryTools.WriteName("CVCoreLib", ex);
                    Thread.Sleep(1000);
                    continue;
                }
                Thread.Sleep(1000 * 60 * 60 * 1);
            }
        }

        public Dictionary<string, int> CameraPeopleCount = new Dictionary<string, int>();
        public Dictionary<int, int> CameraPplCount = new Dictionary<int, int>();
        public Dictionary<int, int> CameraPplReachMaxMin = new Dictionary<int, int>(); // 1 Max, -1 Min, 0
        public Dictionary<int, string> CameraPplReachMax = new Dictionary<int, string>(); // M
        public Dictionary<int, string> CameraPplReachMin = new Dictionary<int, string>(); // M
        public Dictionary<string, Vlc.DotNet.Core.VlcMediaPlayer> MediaPlayer = new Dictionary<string, Vlc.DotNet.Core.VlcMediaPlayer>();
        public Dictionary<string, string> LastVideoPath = new Dictionary<string, string>();
        public Dictionary<string, DateTime> RecordStart = new Dictionary<string, DateTime>();
        public Dictionary<string, DateTime> RecordStartStop = new Dictionary<string, DateTime>();
        public Dictionary<string, DateTime> RecordStop = new Dictionary<string, DateTime>();

        public bool StartRecord(string cameraid)
        {
            if (MediaPlayer.ContainsKey(cameraid))
            {
                if (RecordStart.ContainsKey(cameraid) == false) RecordStart.Add(cameraid, DateTime.Now);
                else RecordStart[cameraid] = DateTime.Now;

                return false;
            }

            try
            {
                var libDirectory = new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", "libvlc", IntPtr.Size == 4 ? "win-x86" : "win-x64"));
                var destination = Path.Combine(_Config.GetRecordPath(), Common.MyRandom.RandomString(10) + ".mp4");

                if (LastVideoPath.ContainsKey(cameraid)) LastVideoPath[cameraid] = destination;
                else LastVideoPath.Add(cameraid, destination);

                var x = new Vlc.DotNet.Core.VlcMediaPlayer(libDirectory);
                MediaPlayer.Add(cameraid, x);

                var mediaOptions = new[]
                {
                ":sout=#file{dst=" + destination + "}",
                ":sout-keep"
            };

                x.SetMedia(new Uri(_Config.GetRtspUrl(cameraid)), mediaOptions);
                x.Play();

                if (RecordStart.ContainsKey(cameraid) == false || RecordStart[cameraid] == DateTime.MinValue)
                {
                    if (RecordStart.ContainsKey(cameraid) == false) RecordStart.Add(cameraid, DateTime.Now);
                    else RecordStart[cameraid] = DateTime.Now;

                    if (RecordStartStop.ContainsKey(cameraid) == false) RecordStartStop.Add(cameraid, DateTime.Now);
                    else RecordStartStop[cameraid] = DateTime.Now;
                }
                return true;
            }
            catch(Exception ex)
            {
                LibraryTools.WriteName("CVCoreLibrary_StartRecord", ex);
                return false;
            }
        }

        public void RecordThreadReal()
        {
            SqlVA_ALERTDAL dalal = new SqlVA_ALERTDAL();

            while(IsRunning)
            {
                try
                {
                    var keys = RecordStartStop.Keys.ToList();
                    foreach(string key in keys)
                    {
                        DateTime dt = RecordStartStop[key];
                        TimeSpan ts = DateTime.Now - dt;

                        if (ts.TotalSeconds > 10)
                        {
                            if (StopRecord(key))
                            {
                                if (LastVideoPath.ContainsKey(key.ToString()))
                                {
                                    string path = LastVideoPath[key.ToString()];

                                    var stopTime = DateTime.Now;
                                    VA_ALERT va = dalal.GetBy("VideoPath", path);

                                    if (va != null)
                                    {
                                        va.RecordStop = stopTime;
                                        dalal.Update(va);
                                    }

                                    if (RecordStop.ContainsKey(key)) RecordStop[key] = stopTime;
                                    else RecordStop.Add(key, stopTime);
                                }
                            }
                        }
                    }
                }
                catch { }
                Thread.Sleep(100);
            }
        }

        public bool StopRecord(string cameraid)
        {
            if (MediaPlayer.ContainsKey(cameraid) == false) return false;

            var x = MediaPlayer[cameraid];
            x.Stop();
            x.Dispose();

            MediaPlayer.Remove(cameraid);

            if (RecordStop.ContainsKey(cameraid) == false) RecordStop.Add(cameraid, DateTime.Now);
            else RecordStop[cameraid] = DateTime.Now;

            if (RecordStart.ContainsKey(cameraid) == false) RecordStart.Add(cameraid, DateTime.MinValue);
            else RecordStart[cameraid] = DateTime.MinValue;
            return true;
        }

        private void StartReal()
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback( delegate { return true; });
            WebClient wc = new WebClient();

            int count = 0;
            int cleanupper = 1000;
            while (IsRunning)
            {
                try
                {
                    string str = wc.DownloadString(_Config.GetAPIUrl());
                    CVCoreObject[] items = JsonConvert.DeserializeObject<CVCoreObject[]>(str);
                    count++;

                    foreach(CVCoreObject c in items)
                    {
                        if (LastFrameTimeStamp < c.frame_time_stamp) LastFrameTimeStamp = c.frame_time_stamp;
                        string frameid = CVCoreItemObject.GetFrameID(c.frame_url);
                        int cameraid = c.camera_id;

                        if (CameraPplCount.ContainsKey(cameraid) == false) CameraPplCount.Add(cameraid, 0);
                        if (CameraPplReachMaxMin.ContainsKey(cameraid) == false) CameraPplReachMaxMin.Add(cameraid, 0);
                        if (CameraPplReachMax.ContainsKey(cameraid) == false) CameraPplReachMax.Add(cameraid, "");
                        if (CameraPplReachMin.ContainsKey(cameraid) == false) CameraPplReachMin.Add(cameraid, "");

                        foreach (string id in CameraID)
                        {
                            if (c.camera_id.ToString() == id.Trim())
                            {
                                if (c.objects != null)
                                {
                                    if (CountPeople != null) CountPeople(c.camera_id.ToString(), c.objects.Count);
                                    if (OnCountPeople != null) OnCountPeople(c.camera_id.ToString(), c.objects.Count);

                                    if (CameraPplCount[cameraid] != c.objects.Count)
                                    {
                                        CameraPplCount[cameraid] = c.objects.Count;

                                        if ((CameraPplReachMaxMin[cameraid] == 0 || CameraPplReachMaxMin[cameraid] != 1) && c.objects.Count > MaxPerson)
                                        {
                                            if (CameraPplReachMax[cameraid].IndexOf("," + c.objects.Count) < 0)
                                            {
                                                StartRecord(cameraid.ToString());
                                                if (PeopleAmountChangeMax != null) PeopleAmountChangeMax(c, cameraid.ToString(), c.objects.Count, MaxPerson);
                                                if (OnPeopleAmountChangeMax != null) OnPeopleAmountChangeMax(c, cameraid.ToString(), c.objects.Count, MaxPerson);
                                                CameraPplReachMaxMin[cameraid] = 1;
                                            }
                                            else
                                            {
                                                CameraPplReachMax[cameraid] += "," + c.objects.Count;
                                            }
                                        }
                                    }
                                }
                                else if (c.@object != null)
                                {
                                    if (CountPeople != null) CountPeople(cameraid.ToString(), c.@object.Count);
                                    if (OnCountPeople != null) OnCountPeople(cameraid.ToString(), c.@object.Count);

                                    if (CameraPplCount[cameraid] != c.objects.Count)
                                    {
                                        CameraPplCount[cameraid] = c.@object.Count;
                                        if ((CameraPplReachMaxMin[cameraid] == 0 || CameraPplReachMaxMin[cameraid] != -1) && c.objects.Count < MinPerson)
                                        {
                                            if (CameraPplReachMin[cameraid].IndexOf("," + c.objects.Count) < 0)
                                            {
                                                StartRecord(cameraid.ToString());
                                                if (PeopleAmountChangeMin != null) PeopleAmountChangeMin(c, cameraid.ToString(), c.@object.Count, MinPerson);
                                                if (OnPeopleAmountChangeMin != null) OnPeopleAmountChangeMin(c, cameraid.ToString(), c.@object.Count, MinPerson);
                                                CameraPplReachMaxMin[cameraid] = -1;
                                            }
                                            else
                                            {
                                                CameraPplReachMin[cameraid] += "," + c.objects.Count;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (c.objects != null)
                                {
                                    if (CameraPplCount[cameraid] != c.objects.Count)
                                    {
                                        CameraPplCount[cameraid] = c.objects.Count;
                                    }
                                }
                                else if (c.@object != null)
                                {
                                    if (CameraPplCount[cameraid] != c.objects.Count)
                                    {
                                        CameraPplCount[cameraid] = c.@object.Count;
                                    }
                                }
                            }
                        }

                        lock (ProcessedLock)
                        {
                            if (ProcessedFrames.ContainsKey(frameid)) continue;
                            ProcessedFrames.Add(frameid, DateTime.Now);
                        }

                        if (count % cleanupper == 0)
                        {
                            List<string> keys = ProcessedFrames.Keys.ToList();
                            for (int i = 0; i < keys.Count; i++)
                            {
                                DateTime last = ProcessedFrames[keys[i]];
                                TimeSpan ts = DateTime.Now - last;
                                if (ts.TotalMinutes > 5)
                                {
                                    ProcessedFrames.Remove(keys[i]);
                                }
                            }
                        }

                        _DB.InsertItem(c, AddCount);
                        foreach(string id in CameraID)
                        {
                            if (c.camera_id.ToString() == id.Trim())
                            {
                                if (CountPeople != null) CountPeople(cameraid.ToString(), CameraPplCount[c.camera_id]);
                                if (OnCountPeople != null) OnCountPeople(cameraid.ToString(), CameraPplCount[c.camera_id]);

                                lock(PictureLock)
                                {
                                    Pictures.Add(c);
                                }
                            }
                        }

                        int idx = c.camera_id;
                        int people2 = _ReportLib.CountPeopleReal(_DB.GetLastTimeStamp(), CountPeople, idx, true);

                        if (CameraPeopleCount.ContainsKey(idx.ToString()))
                            CameraPeopleCount[idx.ToString()] = CameraPeopleCount[idx.ToString()] + people2;
                        else
                            CameraPeopleCount.Add(idx.ToString(), people2);
                    }
                    count = count % cleanupper;

                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    Common.LibraryTools.WriteName("CVCoreLib", ex);
                }
            }
        }

        public void PictureReal()
        {
            WebClient wc = new WebClient();
            while (IsRunning)
            {
                try
                {
                    CVCoreObject obj = null;
                    if (Pictures.Count > 0)
                    {
                        obj = Pictures[0];
                        lock(PictureLock)
                        {
                            Pictures.RemoveAt(0);
                        }
                    }
                    if (obj == null)
                    {
                        Thread.Sleep(100);
                        continue;
                    }

                    string TempFilename = string.Empty;

                    if (obj.objects != null)
                    {
                        foreach (CVCoreItemObject item in obj.@objects)
                        {
                            TempFilename = Common.MyRandom.RandomString(10) + ".jpg";

                            string url = item.GetImage(obj);
                            
                            try
                            {
                                string imagename = string.Empty;
                                if (PictureUrl.ContainsKey(url) == false)
                                {
                                    imagename = Path.Combine(_Config.GetTempFolder(), TempFilename);
                                    wc.DownloadFile(url, imagename);
                                    PictureUrl.Add(url, imagename);
                                }
                                else
                                {
                                    imagename = PictureUrl[url];
                                }

                                TagObject to = new TagObject();
                                to.Item = item;
                                to.Object = obj;
                                to.ImageName = imagename;
                                
                                if (SetImage != null) SetImage(obj.camera_id.ToString(), to);
                                if (OnSetImage != null) OnSetImage(obj.camera_id.ToString(), to);

                                if (PicturesBackground.ContainsKey(url) == false)
                                {
                                    try
                                    {
                                        string frame = Path.Combine(Path.GetDirectoryName(to.ImageName), to.Item.GetFrameID(to.Object) + "-" + to.Item.object_id + ".jpg");
                                        string urlx = to.Item.GetFrameImage(to.Object);
                                        wc.DownloadFile(urlx, frame);
                                        to.ImageBackground = frame;

                                        if (SetImageBackground != null) SetImageBackground(obj.camera_id.ToString(), to);
                                        if (OnSetImageBackground != null) OnSetImageBackground(obj.camera_id.ToString(), to);

                                        PicturesBackground.Add(url, frame);
                                    }
                                    catch (Exception ex) { }
                                }
                                else
                                {
                                    to.ImageBackground = PicturesBackground[url];

                                    if (SetImageBackground != null) SetImageBackground(obj.camera_id.ToString(), to);
                                    if (OnSetImageBackground != null) OnSetImageBackground(obj.camera_id.ToString(), to);
                                }

                                lock (CameraLock)
                                {
                                    CameraObject.Add(to);
                                }
                            }
                            catch { }
                        }
                    }

                    if (obj.@object != null)
                    {
                        foreach (CVCoreItemObject item in obj.@object)
                        {
                            TempFilename = Common.MyRandom.RandomString(10) + ".jpg";

                            string url = item.GetImage(obj);
                            try
                            {
                                string imagename = string.Empty;
                                if (PictureUrl.ContainsKey(url) == false)
                                {
                                    imagename = Path.Combine(_Config.GetTempFolder(), TempFilename);
                                    wc.DownloadFile(url, imagename);
                                    PictureUrl.Add(url, imagename);
                                }
                                else
                                {
                                    imagename = PictureUrl[url];
                                }

                                TagObject to = new TagObject();
                                to.Item = item;
                                to.Object = obj;
                                to.ImageName = imagename;

                                if (SetImage != null) SetImage(obj.camera_id.ToString(), to);
                                if (OnSetImage != null) OnSetImage(obj.camera_id.ToString(), to);

                                if (PicturesBackground.ContainsKey(url) == false)
                                {
                                    try
                                    {
                                        string frame = Path.Combine(Path.GetDirectoryName(to.ImageName), to.Item.GetFrameID(to.Object) + "-" + to.Item.object_id + ".jpg");
                                        string urlx = to.Item.GetFrameImage(to.Object);
                                        wc.DownloadFile(urlx, frame);

                                        if (SetImageBackground != null) SetImageBackground(obj.camera_id.ToString(), to);
                                        if (OnSetImageBackground != null) OnSetImageBackground(obj.camera_id.ToString(), to);

                                        PicturesBackground.Add(url, frame);
                                    }
                                    catch (Exception ex) { }
                                }
                                else
                                {
                                    to.ImageBackground = PicturesBackground[url];

                                    if (SetImageBackground != null) SetImageBackground(obj.camera_id.ToString(), to);
                                    if (OnSetImageBackground != null) OnSetImageBackground(obj.camera_id.ToString(), to);
                                }

                                lock (CameraLock)
                                {
                                    CameraObject.Add(to);
                                }
                            }
                            catch { }
                        }
                    }
                    Thread.Sleep(30);
                }
                catch (Exception ex)
                {
                    Common.LibraryTools.WriteName("CVCoreLib", ex);
                }
            }
        }

        public void PathReal()
        {
            while(IsRunning)
            {
                try
                {
                    if (CameraObject != null && CameraObject.Count > 0)
                    {
                        TagObject to = null;
                        lock(CameraLock)
                        {
                            int count = CameraObject.Count;
                            if (CameraCount < SubCameraTotal)
                            {
                                CameraCount++;
                                to = CameraObject[0];
                                CameraObject.Remove(to);
                            }
                        }

                        if (to != null)
                        {
                            ParameterizedThreadStart ts = new ParameterizedThreadStart(ProcessPath);
                            Thread t = new Thread(ts);
                            t.Start(to);
                        }
                    }
                }
                catch { }
                Thread.Sleep(100);
            }
        }

        public void ProcessPath(object obj)
        {
            TagObject to = (TagObject)obj;
            PersistTagObject(to);
            lock (CameraLock) CameraCount--;
        }

        public void GetPeopleCount()
        {
            ThreadStart ts = new ThreadStart(delegate { _ReportLib.CountPeopleReal(_DB.GetLastTimeStamp(), CountPeople); });
            Thread t = new Thread(ts);
            t.Start();
        }

        public int GetQueueTime()
        {
            DateTime time = _Config.FromUnixEpoch(_DB.GetLastTimeStamp());
            CVCorePeopleExit exit = _DB.GetQueueTime(1, time, time.AddMinutes(-1 * _Config.GetReportPeriod()));
            return exit.ExitTime;
        }

        public int GetQueueTime2()
        {
            CVCorePeopleExit exit = _DB.GetQueueTime(2, DateTime.Now, DateTime.Now.AddMinutes(-1 * _Config.GetReportPeriod()));
            return exit.ExitTime;
        }

        public void DetermineTime(CVCoreShowPathObjectList c, CVCoreShowPathCameraList camera, CVCoreShowPathObject obj)
        {
            CVCoreShowPathCameraList exitCamera = null;
            List<string> exits = _Config.GetExitCameraID();
            foreach (var y in obj.camera_list)
            {
                if (exits.Contains(y.camera_id.ToString()) && y.camera_id != camera.camera_id)
                {
                    exitCamera = y;
                    break;
                }
            }
            DetermineTimeMethod1(c.object_id, camera.camera_id, c.rectangle.x, c.rectangle.y);
            DetermineTimeMethod2(c.object_id, camera.camera_id, c.rectangle.x, c.rectangle.y, exitCamera);
        }

        public void DetermineTimeMethod1(int object_id, int camera_id, int x, int y)
        {
            // ONE CAMERA METHOD
            if (/*ObjectTime.ContainsKey(object_id.ToString()) == false && */object_id != -1)
            {
                int entry = _DB.GetTime("entry_time", _Config.GetMaxExitTime(), object_id.ToString());
                int exit = _DB.GetTimeMax("exit_time", object_id.ToString());

                CVCorePeoplExitDAL peopleDal = new CVCorePeoplExitDAL();
                CVCorePeopleExit ppl = new CVCorePeopleExit { ObjectID = object_id, CameraID = camera_id, LaneID = 0, CameraMethod = 1, EntryDate = DateTime.Now, EntryTime = entry, ExitTime = exit };
                peopleDal.InsertOrUpdate(ppl);

               
                if (ObjectTime2.ContainsKey(object_id.ToString()))
                    ObjectTime2[object_id.ToString()] = exit - entry;
                else
                    ObjectTime2.Add(object_id.ToString(), exit - entry);

                if (ObjectTime2Created.ContainsKey(object_id.ToString()) == false)
                    ObjectTime2Created.Add(object_id.ToString(), DateTime.Now);

                if (QueueTimeRefresh != null) QueueTimeRefresh(camera_id.ToString());
                if (OnQueueTimeRefresh != null) OnQueueTimeRefresh(camera_id.ToString());
            }
        }

        public void DetermineTimeMethod2(int object_id, int camera_id, int x, int y, CVCoreShowPathCameraList exitCamera)
        {
            // TWO CAMERA METHOD
            if (object_id != -1)
            {
                int entry_time = _DB.GetTime("entry_time", _Config.GetMaxExitTime(), object_id.ToString());
                List<string> exits = _Config.GetExitCameraID();

                if (exitCamera != null)
                {
                    int exit_time = 0;
                    try
                    {
                        // exit_time = exitCamera.object_list.Where(m => m.object_id == c.object_id).Max(m => m.exit_time);
                        // DIFFERENT CAMERA USE MAX CONFIDENCE LEVEL
                        IEnumerable<CVCoreShowPathObjectList> persons = null;
                        try
                        {
                            persons = exitCamera.object_list.Where(m => m.confidence == exitCamera.object_list.Max(n => n.confidence));
                        }
                        catch { }
                        CVCoreShowPathObjectList person = null;
                        try
                        {
                            person = persons.FirstOrDefault();
                        }
                        catch { }
                        if (person != null)
                        {
                            try
                            {
                                exit_time = exitCamera.object_list.Where(m => m.object_id == person.object_id).Max(m => m.exit_time);
                            }
                            catch { }
                        }
                    }
                    catch { }

                    if (exit_time != 0)
                    {
                        CVCorePeoplExitDAL peopleDal = new CVCorePeoplExitDAL();
                        CVCorePeopleExit ppl = new CVCorePeopleExit { ObjectID = object_id, CameraID = camera_id, LaneID = 0, CameraMethod = 2, EntryDate = DateTime.Now, EntryTime = entry_time, ExitTime = exit_time };
                        peopleDal.InsertOrUpdate(ppl);

                        int seconddifferences = exit_time - entry_time;
                        if (ObjectTime.ContainsKey(object_id.ToString()) == false)
                        {
                            ObjectTime.Add(object_id.ToString(), seconddifferences);
                            if (ObjectTimeCreated.ContainsKey(object_id.ToString()) == false)
                                ObjectTimeCreated.Add(object_id.ToString(), DateTime.Now);

                            if (QueueTimeRefresh != null) QueueTimeRefresh(camera_id.ToString());
                            if (OnQueueTimeRefresh != null) OnQueueTimeRefresh(camera_id.ToString());
                        }
                    }
                }
            }
            RemoveObjectTime();
        }

        public void RemoveObjectTime()
        {
            DateTime minTime = DateTime.Now.AddSeconds(-1 * _Config.GetMaxExitTime());
            List<string> keys = ObjectTime.Keys.ToList();
            for (int i=ObjectTime.Count - 1; i>= 0; i--)
            {
                if (ObjectTimeCreated.ContainsKey(keys[i]) && ObjectTimeCreated[keys[i]] < minTime)
                {
                    ObjectTime.Remove(keys[i]);
                }
            }

            keys = ObjectTime2.Keys.ToList();
            for (int i = ObjectTime2.Count - 1; i >= 0; i--)
            {
                if (ObjectTime2Created.ContainsKey(keys[i]) && ObjectTime2Created[keys[i]] < minTime)
                {
                    ObjectTime2.Remove(keys[i]);
                }
            }
        }

        public void PersistTagObject(TagObject to)
        {
            try
            {
                string time = "[" + (to.Object.frame_time_stamp - _Config.GetMaxExitTime()).ToString() + "," + (to.Object.frame_time_stamp + _Config.GetMaxExitTime()).ToString() + "]";
                string camera = "[" + to.Object.camera_id.ToString() + "," + string.Join(",", _Config.GetExitCameraID()) + "]";
                if (_Config.GetExitCameraID().Contains(to.Object.camera_id.ToString()))
                    camera = "[" + to.Object.camera_id.ToString() + "]";
                double confidence = _Config.GetConfidence();

                WebClient cwc = new WebClient();

                string poststring = "type=" + to.Item.object_type.ToString() + "&time_range=" + time + "&camera_id_list=" + camera + "&isIntraTracking=false&confidence=" + confidence;

                string url = to.Object.GetShowPathUrl();
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(poststring);
                byte[] result = cwc.UploadData(new Uri(url), "POST", bytes);

                string json = System.Text.Encoding.UTF8.GetString(result);

                CVCoreShowPathObject obj = JsonConvert.DeserializeObject<CVCoreShowPathObject>(json);
                if (obj == null) return;
                for (int i= obj.camera_list.Count - 1; i>=0;i--)
                {
                    CVCoreShowPathCameraList c = obj.camera_list[i];
                    
                    try
                    {
                        if (c.object_list != null && c.object_list.Count > 0)
                        {
                            CVCoreShowPathObjectList dbo = c.object_list.OrderByDescending(m => m.confidence).FirstOrDefault();
                            c.object_list.Clear();
                            c.object_list.Add(dbo);
                        }
                    }
                    catch { }
                    _DB.InsertCamera(c, obj, _Config.GetEntryCameraID(), DetermineTime);
                }
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("CVCoreLib", ex);
            }
        }
    }
}
