﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CVCoreLib;
using System.Configuration;
using Common;

namespace CVCoreLib
{
    public class CVCoreDB
    {
        public CVCoreCache Cache = new CVCoreCache();
        public CVCoreConfig Config = new CVCoreConfig();
        public string DBConnection = string.Empty;


        public CVCoreDB()
        {
            DBConnection = ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString;
        }

        public List<CVCorePeopleExit> GetQueueTimePerLane(int cameramethod, DateTime start, DateTime end)
        {
            string key = Cache.MakeKey(new List<object> { "GetQueueTimePerLane", cameramethod, start, end });
            var test = Cache.GetObject<List<CVCorePeopleExit>>(key);
            if (Config.GetUseCache() && Cache.IsPastHour(end) && test != null) return test;

            string sql = @"SELECT LaneID, AVG(ExitTime-EntryTime) AS ExitTime FROM CVCorePeopleExit
WHERE CameraMethod=@method AND EntryDate BETWEEN @start AND @end
GROUP BY LaneID
";
            SqlCVCorePeopleExitDAL dal = new SqlCVCorePeopleExitDAL();
            List<IDataParameter> prms = new List<IDataParameter>();
            SqlParameter p1 = new SqlParameter("@start", start);
            SqlParameter p2 = new SqlParameter("@end", end);
            SqlParameter p3 = new SqlParameter("@method", cameramethod);
            prms.Add(p1);
            prms.Add(p2);
            prms.Add(p3);
            List<CVCorePeopleExit> result = dal.Query.Builder.ExecuteList(sql, typeof(CVCorePeopleExit), prms, false);
            if (Cache.IsPastHour(end)) Cache.SaveObject(key, result);
            return result;
        }

        public CVCorePeopleExit GetQueueTimePerLane(int cameraMethod, int laneid, DateTime start, DateTime end)
        {
            string key = Cache.MakeKey(new List<object> { "GetQueueTimePerLane", cameraMethod, laneid, start, end });
            var test = Cache.GetObject<CVCorePeopleExit>(key);
            if (Config.GetUseCache() && Cache.IsPastHour(end) && test != null) return test;

            string sql = @"SELECT LaneID, AVG(ExitTime-EntryTime) AS ExitTime FROM CVCorePeopleExit
WHERE CameraMethod=@method AND LaneID=@LaneID AND EntryDate BETWEEN @start AND @end
GROUP BY LaneID
";
            SqlCVCorePeopleExitDAL dal = new SqlCVCorePeopleExitDAL();
            List<IDataParameter> prms = new List<IDataParameter>();
            SqlParameter p1 = new SqlParameter("@start", start);
            SqlParameter p2 = new SqlParameter("@end", end);
            SqlParameter p3 = new SqlParameter("@method", cameraMethod);
            SqlParameter p4 = new SqlParameter("@LaneID", laneid);
            prms.Add(p1);
            prms.Add(p2);
            prms.Add(p3);
            prms.Add(p4);
            CVCorePeopleExit result = dal.Query.Builder.ExecuteSingle(sql, typeof(CVCorePeopleExit), prms);
            if (Cache.IsPastHour(end)) Cache.SaveObject(key, result);
            return result;
        }

        public Dictionary<DateTime, double> LanesRate = new Dictionary<DateTime, double>();
        public double GetOpenLanesRate(DateTime endDate)
        {
            if (LanesRate.ContainsKey(endDate)) return LanesRate[endDate];
            if (endDate.Year < 2000)
                return 0;

            string key = Cache.MakeKey(new List<object> { "GetOpenLanesRate", endDate });
            var test = Cache.GetObject<double>(key);
            if (Config.GetUseCache() && Cache.IsPastHour(endDate) && test != 0) return test;

            string sql = @"SELECT SUM(CASE WHEN d.OpenClose = 1 THEN 1 ELSE 0 END) / COUNT(*) AS QueueLengthDbl FROM
(
SELECT b.Id, b.StatusTime, b.OperatorName, c.LaneID, ISNULL(b.OpenClose, 0) AS OpenClose,  b.CreateOfficer, b.CreateDate, b.UpdateOfficer, b.UpdateDate FROM 
Lanes c LEFT JOIN
(SELECT LaneID, MAX(StatusTime) AS StatusTime FROM LanesStatus
WHERE StatusTime < @EndDate
GROUP BY LaneID) a ON c.LaneID = a.LaneID LEFT JOIN LanesStatus b ON a.LaneID = b.LaneID AND a.StatusTime = b.StatusTime
) d
";
            SqlLanesStatusDAL dal = new SqlLanesStatusDAL();
            List<IDataParameter> prms = new List<IDataParameter>();
            SqlParameter p1 = new SqlParameter("@EndDate", endDate);
            prms.Add(p1);

            SqlQueryBuilder<QueueTime> mydal = new SqlQueryBuilder<QueueTime>();
            QueueTime result = mydal.ExecuteSingle(sql, typeof(QueueTime), prms, true);
            try
            {
                LanesRate.Add(endDate, result.QueueLengthDbl);
            }
            catch { }
            if (Cache.IsPastHour(endDate)) Cache.SaveObject(key, result.QueueLengthDbl);
            return result.QueueLengthDbl;
        }

        public List<LanesStatus> GetOpenLanes(DateTime endDate)
        {
            string key = Cache.MakeKey(new List<object> { "GetOpenLanes", endDate });
            var test = Cache.GetObject<List<LanesStatus>>(key);
            if (Config.GetUseCache() && Cache.IsPastHour(endDate) && test != null) return test;

            string sql = @"SELECT b.Id, b.StatusTime, b.OperatorName, c.LaneID, ISNULL(b.OpenClose, 0) AS OpenClose,  b.CreateOfficer, b.CreateDate, b.UpdateOfficer, b.UpdateDate FROM 
Lanes c LEFT JOIN
(SELECT LaneID, MAX(StatusTime) AS StatusTime FROM LanesStatus
WHERE StatusTime < @EndDate
GROUP BY LaneID) a ON c.LaneID = a.LaneID LEFT JOIN LanesStatus b ON a.LaneID = b.LaneID AND a.StatusTime = b.StatusTime
";
            SqlLanesStatusDAL dal = new SqlLanesStatusDAL();
            List<IDataParameter> prms = new List<IDataParameter>();
            SqlParameter p1 = new SqlParameter("@EndDate", endDate);
            prms.Add(p1);
            List<LanesStatus> result = dal.Query.Builder.ExecuteList(sql, typeof(LanesStatus), prms, false);
            if (Cache.IsPastHour(endDate)) Cache.SaveObject(key, result);
            return result;
        }

        public CVCorePeopleExit GetQueueTime(int cameraMethod, DateTime start, DateTime end)
        {
            string key = Cache.MakeKey(new List<object> { "GetQueueTime", cameraMethod, start, end });
            var test = Cache.GetObject<CVCorePeopleExit>(key);
            if (Config.GetUseCache() && Cache.IsPastHour(end) && test != null) return test;

            string sql = @"SELECT AVG(ExitTime-EntryTime) AS ExitTime FROM CVCorePeopleExit
WHERE CameraMethod=@Method AND EntryDate BETWEEN @start AND @end
";
            SqlCVCorePeopleExitDAL dal = new SqlCVCorePeopleExitDAL();
            List<IDataParameter> prms = new List<IDataParameter>();
            SqlParameter p1 = new SqlParameter("@start", start);
            SqlParameter p2 = new SqlParameter("@end", end);
            SqlParameter p3 = new SqlParameter("@Method", cameraMethod);
            prms.Add(p1);
            prms.Add(p2);
            prms.Add(p3);
            CVCorePeopleExit result = dal.Query.Builder.ExecuteSingle(sql, typeof(CVCorePeopleExit), prms);
            if (Cache.IsPastHour(end)) Cache.SaveObject(key, result);
            return result;
        }

        public List<CVCorePeopleExit> GetLanesReport(DateTime start, int stepMins)
        {
            string key = Cache.MakeKey(new List<object> { "GetLanesReport", start, stepMins });
            var test = Cache.GetObject<List<CVCorePeopleExit>>(key);
            if (Config.GetUseCache() && Cache.IsPastHour(start.AddMinutes(stepMins)) && test != null) return test;

            string sql = @"
SELECT B.*
FROM
(
SELECT LaneID, MAX(EntryDate) AS EntryDate FROM CVCorePeopleExit
WHERE DAY(EntryDate) = DAY(@D) AND MONTH(EntryDate) = MONTH(@D) AND YEAR(EntryDate) = YEAR(@D) AND DATEPART(HOUR, EntryDate) = DATEPART(HOUR, @D) AND (DATEPART(minute, EntryDate) >= DATEPART(minute, @D) OR DATEPART(minute, EntryDate) <= DATEPART(minute, EntryDate) + @StepMins)
GROUP BY LaneID
) A
LEFT JOIN CVCorePeopleExit B ON A.LaneID = B.LaneID AND A.EntryDate = B.EntryDate
WHERE DAY(A.EntryDate) = DAY(@D) AND MONTH(A.EntryDate) = MONTH(@D) AND YEAR(A.EntryDate) = YEAR(@D) AND DATEPART(HOUR, A.EntryDate) = DATEPART(HOUR, @D) AND (DATEPART(minute, A.EntryDate) >= DATEPART(minute, @D) OR DATEPART(minute, A.EntryDate) <= DATEPART(minute, A.EntryDate) + @StepMins)
ORDER BY A.LaneID";
            SqlCVCorePeopleExitDAL dal = new SqlCVCorePeopleExitDAL();
            List<IDataParameter> prms = new List<IDataParameter>();
            SqlParameter p1 = new SqlParameter("@D", start);
            SqlParameter p2 = new SqlParameter("@StepMins", stepMins);
            prms.Add(p1);
            prms.Add(p2);
            List<CVCorePeopleExit> result = dal.Query.Builder.ExecuteList(sql, typeof(CVCorePeopleExit), prms, false);
            if (Cache.IsPastHour(start.AddMinutes(stepMins))) Cache.SaveObject(key, result);
            return result;
        }

        public void InsertItem(CVCoreObject item, Action<DataTable> AddCount)
        {
            SqlConnection conn = null;
            SqlCommand comm = null;
            List<SqlParameter> para = new List<SqlParameter>();
            try
            {
                conn = new SqlConnection(DBConnection);
                conn.Open();
                comm = new SqlCommand("InsertCVCoreObject", conn);

                DataTable dt = new DataTable();
                dt.Columns.Add("x", typeof(int));
                dt.Columns.Add("y", typeof(int));
                dt.Columns.Add("h", typeof(int));
                dt.Columns.Add("w", typeof(int));
                dt.Columns.Add("object_type", typeof(int));
                dt.Columns.Add("object_id", typeof(long));
                dt.AcceptChanges();
                if (item.@objects != null)
                {
                    foreach (CVCoreItemObject obj in item.@objects)
                    {
                        DataRow dr = dt.NewRow();
                        dr["x"] = obj.x;
                        dr["y"] = obj.y;
                        dr["h"] = obj.h;
                        dr["w"] = obj.w;
                        dr["object_type"] = obj.object_type;
                        dr["object_id"] = obj.object_id;
                        dt.Rows.Add(dr);
                    }
                }
                if (item.@object != null)
                {
                    foreach (CVCoreItemObject obj in item.@object)
                    {
                        DataRow dr = dt.NewRow();
                        dr["x"] = obj.x;
                        dr["y"] = obj.y;
                        dr["h"] = obj.h;
                        dr["w"] = obj.w;
                        dr["object_type"] = obj.object_type;
                        dt.Rows.Add(dr);
                    }
                }
                dt.AcceptChanges();

                SqlParameter para1 = new SqlParameter("@frame_url", item.frame_url);
                SqlParameter para2 = new SqlParameter("@frame_width", item.frame_width);
                SqlParameter para3 = new SqlParameter("@fps", item.fps);
                SqlParameter para4 = new SqlParameter("@ingesting_time", item.ingesting_time);
                SqlParameter para5 = new SqlParameter("@frame_read_time", item.frame_read_time);
                SqlParameter para6 = new SqlParameter("@scale_factor", item.scale_factor);
                SqlParameter para7 = new SqlParameter("@status", item.status);
                SqlParameter para8 = new SqlParameter("@frame_time_stamp", item.frame_time_stamp);
                SqlParameter para9 = new SqlParameter("@frame_ingesting_time", item.frame_ingesting_time);
                SqlParameter para10 = new SqlParameter("@url", item.url);
                SqlParameter para11 = new SqlParameter("@camera_type", item.camera_type);
                SqlParameter para12 = new SqlParameter("@camera_id", item.camera_id);
                SqlParameter para13 = new SqlParameter("@frame_height", item.frame_height);
                SqlParameter para14 = new SqlParameter("@storing_time", item.storing_time);
                SqlParameter para15 = new SqlParameter("@face_detecting", item.services.face_detecting);
                SqlParameter para16 = new SqlParameter("@detector_mode", item.services.detector_mode);
                SqlParameter para17 = new SqlParameter("@poi_tracking", item.services.poi_tracking);
                SqlParameter para18 = new SqlParameter("@voi_tracking", item.services.voi_tracking);
                SqlParameter para19 = new SqlParameter("@read_time", item.read_time);
                SqlParameter para20 = new SqlParameter("@frame_storing_time", item.frame_storing_time);
                SqlParameter para21 = new SqlParameter("@items", dt);
                //SqlParameter para22 = new SqlParameter("@items", dt);
                para21.TypeName = "dbo.CVCoreItemObject";
                comm.Parameters.Add(para1);
                comm.Parameters.Add(para2);
                comm.Parameters.Add(para3);
                comm.Parameters.Add(para4);
                comm.Parameters.Add(para5);
                comm.Parameters.Add(para6);
                comm.Parameters.Add(para7);
                comm.Parameters.Add(para8);
                comm.Parameters.Add(para9);
                comm.Parameters.Add(para10);
                comm.Parameters.Add(para11);
                comm.Parameters.Add(para12);
                comm.Parameters.Add(para13);
                comm.Parameters.Add(para14);
                comm.Parameters.Add(para15);
                comm.Parameters.Add(para16);
                comm.Parameters.Add(para17);
                comm.Parameters.Add(para18);
                comm.Parameters.Add(para19);
                comm.Parameters.Add(para20);
                comm.Parameters.Add(para21);
                comm.CommandType = CommandType.StoredProcedure;
                int result = comm.ExecuteNonQuery();

                AddCount(dt);                
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("CVCoreLib", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (comm != null)
                {
                    comm.Clone();
                    comm.Dispose();
                }
            }
        }

        public List<CVCoreShowPathCameraObject> GetLastBoxes()
        {
            CVCoreShowPathCameraObjectDAL dal = new CVCoreShowPathCameraObjectDAL();
            SqlCVCoreShowPathCameraObjectDAL dal2 = (SqlCVCoreShowPathCameraObjectDAL) dal.Repository;
            int frameid = 0;
            try
            {
                frameid = dal2.Collection.Max(m => m.frame_id);
            }
            catch { };
            if (frameid == 0) return new List<CVCoreShowPathCameraObject>();

            List< CVCoreShowPathCameraObject> result = dal2.Collection.Where(m => m.frame_id == frameid).ToList();
            if (result == null) return new List<CVCoreShowPathCameraObject>();
            return result;
        }

        public int GetLastTimeStamp()
        {
            CVCoreObjectDAL dal = new CVCoreObjectDAL();
            SqlCVCoreObjectDAL dal2 = (SqlCVCoreObjectDAL)dal.Repository;
            int timestamp = 0;
            try
            {
                timestamp = dal2.Collection.Max(m => m.frame_time_stamp);
            }
            catch { };
            return timestamp;
        }

        public void InsertCamera(CVCoreShowPathCameraList item, CVCoreShowPathObject obj, List<string> EntryCameraID, Action<CVCoreShowPathObjectList,CVCoreShowPathCameraList,CVCoreShowPathObject> DetermineTime)
        {
            SqlConnection conn = null;
            SqlCommand comm = null;
            SqlCommand comm2 = null;
            SqlCommand comm3 = null;
            List<SqlParameter> para = new List<SqlParameter>();
            try
            {
                conn = new SqlConnection(DBConnection);
                conn.Open();
                comm = new SqlCommand("INSERT INTO CvCoreShowPathCamera SELECT @id, @camera_id, @latitude, @longitude, @object_total, @predicted_path_total, GETDATE()", conn);

                string id = Common.MyRandom.RandomString(24);
                SqlParameter para1 = new SqlParameter("@id", id);
                SqlParameter para2 = new SqlParameter("@camera_id", item.camera_id);
                SqlParameter para3 = new SqlParameter("@latitude", item.latitude);
                SqlParameter para4 = new SqlParameter("@longitude", item.longitude);
                SqlParameter para5 = new SqlParameter("@object_total", item.object_total);
                SqlParameter para6 = new SqlParameter("@predicted_path_total", item.predicted_path_total);
                comm.Parameters.Add(para1);
                comm.Parameters.Add(para2);
                comm.Parameters.Add(para3);
                comm.Parameters.Add(para4);
                comm.Parameters.Add(para5);
                comm.Parameters.Add(para6);
                comm.CommandType = CommandType.Text;
                int result = comm.ExecuteNonQuery();

                foreach (var a in item.object_list)
                {
                    try
                    {
                        comm2 = new SqlCommand("INSERT INTO CvCoreShowPathCameraObject " +
                        "SELECT @id, @parent_id, @avg_velocity, @confidence, @entry_time, @exit_time, @frame_height, @frame_id, @frame_width, @object_id, @rectangleh, @rectanglew, @rectanglex, @rectangley, @track_total, @type, @velocity_deviation"
                        , conn);

                        string itemid = Common.MyRandom.RandomString(24);

                        SqlParameter apara1 = new SqlParameter("@id", itemid);
                        SqlParameter apara2 = new SqlParameter("@parent_id", id);
                        SqlParameter apara3 = new SqlParameter("@avg_velocity", a.avg_velocity);
                        SqlParameter apara4 = new SqlParameter("@confidence", a.confidence);
                        SqlParameter apara5 = new SqlParameter("@entry_time", a.entry_time);
                        SqlParameter apara6 = new SqlParameter("@exit_time", a.exit_time);
                        SqlParameter apara7 = new SqlParameter("@frame_height", a.frame_height);
                        SqlParameter apara8 = new SqlParameter("@frame_id", a.frame_id);
                        SqlParameter apara9 = new SqlParameter("@frame_width", a.frame_width);
                        SqlParameter apara10 = new SqlParameter("@object_id", a.object_id);

                        if (a.rectangle == null) a.rectangle = new CVCoreShowPathRectangle();
                        SqlParameter apara11 = new SqlParameter("@rectangleh", a.rectangle.h);
                        SqlParameter apara12 = new SqlParameter("@rectanglew", a.rectangle.w);
                        SqlParameter apara13 = new SqlParameter("@rectanglex", a.rectangle.x);
                        SqlParameter apara14 = new SqlParameter("@rectangley", a.rectangle.y);
                        SqlParameter apara15 = new SqlParameter("@track_total", a.track_total);
                        SqlParameter apara16 = new SqlParameter("@type", a.type);
                        SqlParameter apara17 = new SqlParameter("@velocity_deviation", a.velocity_deviation);

                        comm2.Parameters.Add(apara1);
                        comm2.Parameters.Add(apara2);
                        comm2.Parameters.Add(apara3);
                        comm2.Parameters.Add(apara4);
                        comm2.Parameters.Add(apara5);
                        comm2.Parameters.Add(apara6);
                        comm2.Parameters.Add(apara7);
                        comm2.Parameters.Add(apara8);
                        comm2.Parameters.Add(apara9);
                        comm2.Parameters.Add(apara10);
                        comm2.Parameters.Add(apara11);
                        comm2.Parameters.Add(apara12);
                        comm2.Parameters.Add(apara13);
                        comm2.Parameters.Add(apara14);
                        comm2.Parameters.Add(apara15);
                        comm2.Parameters.Add(apara16);
                        comm2.Parameters.Add(apara17);

                        comm2.CommandType = CommandType.Text;
                        result = comm2.ExecuteNonQuery();

                        foreach (var b in a.track_list)
                        {
                            try
                            {
                                comm3 = new SqlCommand("INSERT INTO CVCoreShowPathCameraObjectItem SELECT " +
                                "@id, @parent_id, @grandparent_id, @frame_height, @frame_id, @frame_width, @latitude, @longitude, @rectangleh, @rectanglew, @rectanglex, @rectangley, @time"
                                , conn);
                                string myid = Common.MyRandom.RandomString(24);

                                SqlParameter bpara1 = new SqlParameter("@id", myid);
                                SqlParameter bpara2 = new SqlParameter("@parent_id", itemid);
                                SqlParameter bpara3 = new SqlParameter("@grandparent_id", id);
                                SqlParameter bpara4 = new SqlParameter("@frame_height", b.frame_height);
                                SqlParameter bpara5 = new SqlParameter("@frame_id", b.frame_id);
                                SqlParameter bpara6 = new SqlParameter("@frame_width", b.frame_width);
                                SqlParameter bpara7 = new SqlParameter("@latitude", b.latitude);
                                SqlParameter bpara8 = new SqlParameter("@longitude", b.longitude);
                                SqlParameter bpara9 = new SqlParameter("@rectangleh", b.rectangle.h);
                                SqlParameter bpara10 = new SqlParameter("@rectanglew", b.rectangle.w);
                                SqlParameter bpara11 = new SqlParameter("@rectanglex", b.rectangle.x);
                                SqlParameter bpara12 = new SqlParameter("@rectangley", b.rectangle.y);
                                SqlParameter bpara13 = new SqlParameter("@time", b.time);

                                comm3.Parameters.Add(bpara1);
                                comm3.Parameters.Add(bpara2);
                                comm3.Parameters.Add(bpara3);
                                comm3.Parameters.Add(bpara4);
                                comm3.Parameters.Add(bpara5);
                                comm3.Parameters.Add(bpara6);
                                comm3.Parameters.Add(bpara7);
                                comm3.Parameters.Add(bpara8);
                                comm3.Parameters.Add(bpara9);
                                comm3.Parameters.Add(bpara10);
                                comm3.Parameters.Add(bpara11);
                                comm3.Parameters.Add(bpara12);
                                comm3.Parameters.Add(bpara13);
                                comm3.CommandType = CommandType.Text;
                                result = comm3.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                Common.LibraryTools.WriteName("CVCoreLib", ex);
                            }
                            finally
                            {
                                if (comm3 != null)
                                {
                                    comm3.Dispose();
                                }
                            }
                        }

                        if (EntryCameraID.Contains(item.camera_id.ToString()))
                            DetermineTime(a, item, obj);
                    }
                    catch (Exception ex)
                    {
                        Common.LibraryTools.WriteName("CVCoreLib", ex);
                    }
                    finally
                    {
                        if (comm2 != null)
                        {
                            comm2.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("CVCoreLib", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (comm != null)
                {
                    comm.Dispose();
                }
                if (comm2 != null)
                {
                    comm2.Dispose();
                }
            }
        }

        public int GetTime(string entryexit, int fromcurrent, string objectid)
        {
            int time = (int)Config.ToUnixEpoch(DateTime.Now);
            time = time - Math.Abs(fromcurrent);

            SqlConnection conn = null;
            SqlCommand comm = null;
            List<SqlParameter> para = new List<SqlParameter>();
            try
            {
                conn = new SqlConnection(DBConnection);
                conn.Open();
                comm = new SqlCommand("SELECT MIN(" + entryexit + ") FROM [CVCoreShowPathCameraObject] WHERE " + entryexit + " >= @time AND [object_id]=@objectid", conn);
                SqlParameter para1 = new SqlParameter("@objectid", objectid);
                SqlParameter para2 = new SqlParameter("@time", time);
                comm.Parameters.Add(para1);
                comm.Parameters.Add(para2);

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(ds);

                int result = 0;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Count > 0)
                {
                    result = (int)ds.Tables[0].Rows[0][0];
                }
                return result;
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("CVCoreLib", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (comm != null)
                {
                    comm.Dispose();
                }
            }
            return 0;
        }

        public int GetPeopleCount(int now, int maxpeoplecounttime, int cameraid = 0, bool equal = false)
        {
            int time = now;
            if (equal == false) time = time - Math.Abs(maxpeoplecounttime);
            string key = Cache.MakeKey(new List<object> { "GetPeopleCount", now, maxpeoplecounttime, cameraid });
            var test = Cache.GetObject<int>(key);
            if (Config.GetUseCache() && Cache.IsPastHour(Config.FromUnixEpoch(time)) && test != 0) return test;

            string andcamera = "";
            if (cameraid != 0)
            {
                andcamera = " AND b.camera_id = " + cameraid;
            }

            string str = ">=";
            if (equal) str = "=";
            SqlConnection conn = null;
            SqlCommand comm = null;
            List<SqlParameter> para = new List<SqlParameter>();
            try
            {
                conn = new SqlConnection(DBConnection);
                conn.Open();
                comm = new SqlCommand("SELECT count(*) FROM ( SELECT object_id FROM[CVCoreItemObject] a LEFT JOIN[CVCoreObject] b on a.ParentID = b.ID WHERE b.frame_time_stamp " + str + " @time " + andcamera + " Group by object_id ) aa", conn);
                SqlParameter para2 = new SqlParameter("@time", time);
                comm.Parameters.Add(para2);

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(ds);

                int result = 0;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Count > 0)
                {
                    result = (int)ds.Tables[0].Rows[0][0];
                }
                CVCoreConfig _Config = new CVCoreConfig();
                result = (int)Math.Round((double)result * _Config.GetFactorPeopleCount());
                if (Cache.IsPastHour(Config.FromUnixEpoch(time))) Cache.SaveObject(key, result);
                return result;
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("CVCoreLib", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (comm != null)
                {
                    comm.Dispose();
                }
            }
            return 0;
        }

        public int GetTimeMax(string entryexit, string objectid)
        {
            SqlConnection conn = null;
            SqlCommand comm = null;
            List<SqlParameter> para = new List<SqlParameter>();
            try
            {
                conn = new SqlConnection(DBConnection);
                conn.Open();
                comm = new SqlCommand("SELECT MAX(" + entryexit + ") FROM [CVCoreShowPathCameraObject] WHERE [object_id]=@objectid", conn);
                SqlParameter para1 = new SqlParameter("@objectid", objectid);
                comm.Parameters.Add(para1);

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(ds);

                int result = 0;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Count > 0)
                {
                    result = (int)ds.Tables[0].Rows[0][0];
                }
                return result;
            }
            catch (Exception ex)
            {
                Common.LibraryTools.WriteName("CVCoreLib", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (comm != null)
                {
                    comm.Dispose();
                }
            }
            return 0;
        }
    }
}
