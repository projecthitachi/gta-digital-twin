/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 18 Dec 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Data;
using MvcPaging;
using System.Data.SqlClient;

namespace Common
{
    public class SqlRFID_ALERTDAL : SqlCommonDALBase<RFID_ALERT>
    {
        public SqlRFID_ALERTDAL() : base(new string[] { "Id" })
        {

            SortColumns = "AlertDate DESC";
            ExactColumns = new string[] { "AlertDate", "AssetName", "Description", "Epc", "ZoneName", "ViewDate" };
            SearchColumns = new string[] { "AlertDate", "AssetName", "Description", "Epc", "ZoneName", "ViewDate" };
            List<string> tables = new List<string> { "RFID_AssetOwn", "Rack" };
            List<string> ons = new List<string> { "[0].Epc=[1].Epc", "[0].ZoneID=[2].RackID" };
            List<string> columns = new List<string> { "AssetName", "RackName AS ZoneName" };
            JoinSearch js = new JoinSearch(typeof(VA_ALERT), tables, ons, columns);
            js.LeftJoin = new List<string> { "Rack", "RFID_AssetOwn" };
            JoinSearchConfig = js;
        }

        public void UpdateImage(string rackID, string image)
        {
            image = image.Substring(image.IndexOf("Content"));
            image = image.Replace("\\", "/");
            image = "/" + image;
            string sql = "UPDATE RFID_ALERT SET Image=@Image WHERE ZoneID=@ZoneID AND ISNULL(Image, '') = '' AND DATEDIFF(s, CreateDate, GETDATE()) < 60";
            List<IDataParameter> prms = new List<IDataParameter>();
            prms.Add(new SqlParameter("@Image", image));
            prms.Add(new SqlParameter("@ZoneID", rackID));
            base.Query.Builder.ExecuteNonQuery(sql, typeof(RFID_ALERT), prms);
        }

        public PagedList<RFID_ALERT> GetPage(int page, string rfid, string hva)
        {
            if (string.IsNullOrEmpty(rfid)) rfid = "";
            if (string.IsNullOrEmpty(hva)) hva = "";

            string OtherCriteria = "";
            if (rfid.ToUpper() == "Y") OtherCriteria = "ISNULL(a1.AssetName, '') <> ''";
            if (hva.ToUpper() == "Y") OtherCriteria = "ISNULL(a1.AssetName, '') = ''";
            long count = Query.Builder.CountPage(new RFID_ALERT(), SortColumns, JoinSearchConfig, OtherCriteria);

            List<RFID_ALERT> results = GetValue(Query.Builder.GetPage(new RFID_ALERT(), page, SortColumns, JoinSearchConfig, OtherCriteria));
            PagedList<RFID_ALERT> output = new PagedList<RFID_ALERT>(results, page - 1, PageSize, (int)count);
            return output;
        }

        public PagedList<RFID_ALERT> GetPageSearch(string text, int page, string rfid, string hva)
        {
            if (string.IsNullOrEmpty(rfid)) rfid = "";
            if (string.IsNullOrEmpty(hva)) hva = "";

            string OtherCriteria = "";
            if (rfid.ToUpper() == "Y") OtherCriteria = "ISNULL(a1.AssetName, '') <> ''";
            if (hva.ToUpper() == "Y") OtherCriteria = "ISNULL(a1.AssetName, '') = ''";
            long count = Query.Builder.CountPageSearch(new RFID_ALERT(), text, SortColumns, JoinSearchConfig, null, null, OtherCriteria);

            List<RFID_ALERT> results = GetValue(Query.Builder.GetPageSearch(new RFID_ALERT(), text, page, SortColumns, JoinSearchConfig, null, null, OtherCriteria));
            PagedList<RFID_ALERT> output = new PagedList<RFID_ALERT>(results, page - 1, PageSize, (int)count);
            return output;
        }


        public int GetAlertCount()
        {
            string sql = "SELECT COUNT(*) FROM RFID_Alert a LEFT JOIN Rack b ON a.ZoneID = b.RackID LEFT JOIN RFID_AssetOwn c ON a.Epc = c.Epc WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000) AND ZoneID IS NOT NULL";
            DataSet ds = Query.Builder.ExecuteDataSet(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) return 0;
            int c = 0;
            int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out c);
            return c;
        }

        public int GetAlerCount2(bool rfid)
        {
            string andwhere = "";
            if (rfid == true) andwhere = " AND ISNULL(c.AssetName, '') <> ''";
            else andwhere = " AND ISNULL(c.AssetName, '') = ''";
            string sql = "SELECT COUNT(*) FROM RFID_Alert a LEFT JOIN Rack b ON a.ZoneID = b.RackID LEFT JOIN RFID_AssetOwn c ON a.Epc = c.Epc WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000) AND ZoneID IS NOT NULL " + andwhere;
            DataSet ds = Query.Builder.ExecuteDataSet(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) return 0;
            int c = 0;
            int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out c);
            return c;
        }

        public int GetEnergyAlertCount()
        {
            string sql = "SELECT COUNT(*) AS CNT FROM Alerts WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000)";
            DataSet ds = Query.Builder.ExecuteDataSet(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) return 0;
            int c = 0;
            int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out c);
            return c;
        }

        public void AcknowledgeAll()
        {
            string sql = "UPDATE RFID_Alert SET ViewDate=GETDATE() WHERE ViewDate IS NULL OR YEAR(ViewDate) < 2000";
            Query.Builder.ExecuteNonQuery(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
        }

        public void Acknowledge(string al)
        {
            List<System.Data.IDataParameter> prms = new List<System.Data.IDataParameter>();
            prms.Add(new System.Data.SqlClient.SqlParameter("@Id", al ?? ""));
            string sql = "UPDATE RFID_Alert SET ViewDate=GETDATE() WHERE Id=@Id";
            Query.Builder.ExecuteNonQuery(sql, typeof(VA_ALERT), prms);
        }
    }
}