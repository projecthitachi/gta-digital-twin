﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using System.Linq;

namespace CVCoreLib
{
    public class CVCoreObjectDAL : BaseRepository<CVCoreObjectDB>
    {
        public CVCoreObjectDAL()
        {
            Repository = new SqlCVCoreObjectDAL();

        }
    }

    public class LanesDAL : BaseRepository<Lanes>
    {
        public LanesDAL()
        {
            Repository = new SqlLanesDAL();
        }
    }

    public class CVCoreShowPathCameraObjectDAL : BaseRepository<CVCoreShowPathCameraObject>
    {
        public CVCoreShowPathCameraObjectDAL()
        {
            Repository = new SqlCVCoreShowPathCameraObjectDAL();
        }
    }

    public class SettingDAL : BaseRepository<Setting>
    {
        public SettingDAL()
        {
            Repository = new SqlSettingDAL();
        }
    }

    public class CVCorePeoplExitDAL : BaseRepository<CVCorePeopleExit>
    {
        public CVCorePeoplExitDAL()
        {
            Repository = new SqlCVCorePeopleExitDAL();
        }

        public void InsertOrUpdate(CVCorePeopleExit pe)
        {
            if (pe == null) return;
            SqlCVCorePeopleExitDAL col = (SqlCVCorePeopleExitDAL)Repository;
            CVCorePeopleExit people = col.Collection.Where(m => m.CameraMethod == pe.CameraMethod && m.ObjectID == pe.ObjectID && m.EntryDate.Year == pe.EntryDate.Year && m.EntryDate.Month == pe.EntryDate.Month && m.EntryDate.Day == pe.EntryDate.Day).FirstOrDefault();
            if (people == null)
            {
                Insert(pe);
            }
            else
            {
                pe.Id = people.Id;
                Update(pe);
            }
        }
    }
}
