﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Common
{
    [Table]
    public class SleepCount : CommonModel
    {
        [Column(IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public override string Id { get; set; }

        [Column(IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int Count { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string CreateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime CreateDate { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string UpdateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime UpdateDate { get; set; }
    }
}
