﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace Common
{
    [Serializable]
    public class CommonModel
    {
        public CommonModel()
        {

        }

        [Column(IsPrimaryKey = true)]

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string CreateOfficer { get; set; }
        public virtual DateTime CreateDate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string UpdateOfficer { get; set; }
        public virtual DateTime UpdateDate { get; set; }

        public void Detach()
        {
            var mi = GetType().GetMethod("Initialize", BindingFlags.Instance | BindingFlags.NonPublic);
            if (mi != null)
                mi.Invoke(this, null);
        }
    }
}