/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 01 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
namespace Common
{
    public class JoinSearch
    {
        public Type TT;
        public List<string> Aliases = new List<string>();

        public List<string> Tables = new List<string>();
        public List<string> ColumnList = new List<string>();
        public List<string> Ons = new List<string>();
        public Dictionary<string, string> ColumnAlias = new Dictionary<string, string>();
        public List<string> LeftJoin = new List<string>();

        public JoinSearch(Type t, List<string> tables, List<string> ons, List<string> columns)
        {
            TT = t;
            if (tables.Count != columns.Count)
                throw new Exception("Tables count must be same with columns count\r\nEach column item is a list of column delimited with coma");
            if (tables.Count != ons.Count)
                throw new Exception("ONs count must be same with tables count");

            List<string> aliases = new List<string>();
            int i = 0;

            TableAttribute ta = t.GetCustomAttribute(typeof(TableAttribute)) as TableAttribute;
            if (ta != null) tables.Insert(0, ta.Name ?? t.Name);
            else tables.Insert(0, t.Name);
            Tables = tables;

            foreach (string table in tables)
            {
                if (i == 0) aliases.Add("a");
                else aliases.Add("a" + i.ToString());
                i++;
            }
            Aliases = aliases;
            i = 0;

            foreach (string column in columns)
            {
                List<string> cs = new List<string>();
                string[] cols = column.Split(new char[] { ',' });
                foreach (string col in cols)
                {
                    cs.Add(aliases[i+1] + "." + col.Trim());
                    ColumnAlias.Add(col.Trim(), aliases[i+1] + "." + col.Trim());
                }
                ColumnList.Add(string.Join(", ", cs));
                i++;
            }

            List<string> os = new List<string>();
            Ons.Add("");
            foreach (string o in ons)
            {
                string temp = SetAlias(o);
                Ons.Add(temp);
            }
        }

        public bool ContainsStartWith(List<string> keys, string name)
        {
            foreach(string key in keys)
            {
                string temp = name;
                string temp2 = key.Trim();
                if (temp2.IndexOf(" ") > 0)
                {
                    temp2 = temp2.Substring(temp2.LastIndexOf(" ") + 1);
                    if (temp2 == temp) return true;
                }
                if (key == temp) return true;
            }
            return false;
        }

        public string[] GetColumnsType(Type type)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(Common.ReadOnlyColumnAttribute), true);
                if (ts == null) continue;

                string name = (((Common.ReadOnlyColumnAttribute)ts).Name ?? t.Name);
                if (ContainsStartWith(ColumnAlias.Keys.ToList(), name) == false) continue;

                string tt = "NVARCHAR(MAX)";
                if (t.PropertyType == typeof(int))
                    tt = "INT";
                if (t.PropertyType == typeof(byte[]))
                    tt = "VARBINARY(MAX)";
                if (t.PropertyType == typeof(long))
                    tt = "BIGINT";
                if (t.PropertyType == typeof(float))
                    tt = "DOUBLE";
                if (t.PropertyType == typeof(decimal))
                    tt = "DECIMAL";
                if (t.PropertyType == typeof(bool))
                    tt = "BIT";
                if (t.PropertyType == typeof(DateTime))
                    tt = "DATETIME";
                if (t.PropertyType == typeof(char))
                    tt = "NCHAR(1)";
                columns.Add("[" + (((Common.ReadOnlyColumnAttribute)ts).Name ?? t.Name) + "] " + tt);
            }
            return columns.ToArray();
        }

        public string[] GetDeclareColumnsType(Type type, bool includePrefix)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(Common.ReadOnlyColumnAttribute), true);
                if (ts == null) continue;

                string name = (((Common.ReadOnlyColumnAttribute)ts).Name ?? t.Name);
                if (ContainsStartWith(ColumnAlias.Keys.ToList(), name) == false) continue;

                if (includePrefix == false) columns.Add(name);
                else columns.Add(ColumnAlias[name]);
            }
            return columns.ToArray();
        }

        public string GetSelectColumns()
        {
            return string.Join(", ", ColumnList);
        }

        public string GetSelectColumns2()
        {
            List<string> result = new List<string>();
            string[] test = GetDeclareColumnsType(TT, false);
            
            foreach(string tt in test)
            {
                result.Add("a." + tt);
            }
            return string.Join(", ", result.ToArray());
        }

        public string GetDeclareColumns(Type t)
        {
            return string.Join(", ", GetColumnsType(t).ToArray());
        }

        public string GetInner()
        {
            string str = "";
            for (int i=1; i<Tables.Count; i++)
            {
                if (LeftJoin.Contains(Tables[i]))
                    str += " LEFT JOIN " + Tables[i] + " " + Aliases[i] + " ON " + Ons[i];
                else
                    str += " INNER JOIN " + Tables[i] + " " + Aliases[i] + " ON " + Ons[i];
            }
            return str;
        }

        public string SetAlias(string o)
        {
            //[0].ServerID=[1].AssetID;([0].LocationID=[2].LocationName,[1].ImageID=[2].ImageNo)
            //translate to = "a0.ServerID=a1.AssetID AND (a0.LocationID=a2.LocationName OR a1.ImageID=a2.ImageNo)
            string temp = o;
            string[] matches = CommonStringOps.TagMatch(temp, "[", "]");
            foreach (string match in matches)
            {
                int j = 0;
                int.TryParse(match, out j);
                temp = temp.Replace("[" + j.ToString() + "].", Aliases[j] + ".");
            }
            
            if (temp.ToUpper().IndexOf("ISNULL") < 0)
            {
                temp = temp.Replace(";", " AND ");
                temp = temp.Replace(",", " OR ");
            }
            return temp;
        }
    }
}
