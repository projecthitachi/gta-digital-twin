/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 19 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Common
{
    [Table]
    public class RFID_ALERT : CommonModel
    {

        [Column(IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]

        [BsonRepresentation(BsonType.ObjectId)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public override string Id { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime AlertDate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime CREATED_DT { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Epc { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string ZoneID{ get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Description { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string DataID { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Image { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime ViewDate { get; set; }

        [ReadOnlyColumn]
        public string AssetName{ get; set; }

        [ReadOnlyColumn]
        public string ZoneName { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string CreateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime CreateDate { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string UpdateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime UpdateDate { get; set; }
    }
}