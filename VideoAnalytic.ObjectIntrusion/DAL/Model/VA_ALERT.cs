﻿/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 19 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Common
{
    [Table]
    public class VA_ALERT : CommonModel

    {

        [Column(IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public override string Id { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string VideoPath { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string FrameUrl { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime RecordStart { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime RecordStop { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string CameraID { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int MaxPerson { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int MinPerson { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public int PeopleCount { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public bool IsMax { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ImageList { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime AlertDate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime CREATED_DT { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Epc { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string ZoneID { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Description { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime ViewDate { get; set; }

        [ReadOnlyColumn]
        public string AssetName { get; set; }

        [ReadOnlyColumn]
        public string ZoneName { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string CreateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime CreateDate { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string UpdateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime UpdateDate { get; set; }
    }

    [Table]
    public class VA_ALERTDETAIL : CommonModel
    {

        [Column(IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public override string Id { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ParentId { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FrameUrl { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Image { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string CreateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime CreateDate { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string UpdateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime UpdateDate { get; set; }
    }

    [Table]
    public class VA_ALERTDETAIL2 : CommonModel
    {

        [Column(IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public override string Id { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FrameUrl { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ParentId { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Thumb { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string CreateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime CreateDate { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string UpdateOfficer { get; set; }
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime UpdateDate { get; set; }
    }
}