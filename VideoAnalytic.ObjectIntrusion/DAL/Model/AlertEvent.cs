﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common
{
    public class AlertEvent
    {
        public HVAAlert Alert { get; set; }
        public List<Callback> Callbacks { get; set; }

    }
}
