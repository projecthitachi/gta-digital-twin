﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using MvcPaging;
using Common;

namespace CVCoreLib
{
    [Serializable]
    [Table(Name = "Setting")]
    public class Setting : CommonModel
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        [Column(UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        [Key]
        public override string Id{ get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string SettingName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public string SettingValue { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string CreateOfficer { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime CreateDate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override string UpdateOfficer { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Column(UpdateCheck = UpdateCheck.Never)]
        public override DateTime UpdateDate { get; set; }

    }
}
