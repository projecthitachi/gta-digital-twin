﻿/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 18 Dec 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Data;

namespace Common
{
    public class SqlHVAAlertDAL : SqlCommonDALBase<HVAAlert>
    {
        public SqlHVAAlertDAL()
        {
            SortColumns = "CreateDate DESC";

        }

        public int GetVAAlertCount()
        {
            string sql = "SELECT COUNT(*) FROM HVAAlert a WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000)";
            DataSet ds = Query.Builder.ExecuteDataSet(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) return 0;
            int c = 0;
            int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out c);
            return c;
        }

        public void AcknowledgeAll()
        {
            string sql = "UPDATE HVAAlert SET ViewDate=GETDATE() WHERE (ViewDate IS NULL OR YEAR(ViewDate) < 2000)";
            Query.Builder.ExecuteNonQuery(sql, typeof(VA_ALERT), new List<System.Data.IDataParameter>());
        }

        public void Acknowledge(string al)
        {
            List<System.Data.IDataParameter> prms = new List<System.Data.IDataParameter>();
            prms.Add(new System.Data.SqlClient.SqlParameter("@Id", al ?? ""));
            string sql = "UPDATE HVAAlert SET ViewDate=GETDATE() WHERE Id=@Id";
            Query.Builder.ExecuteNonQuery(sql, typeof(VA_ALERT), prms);
        }
    }
}