﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;

namespace Common
{
    [Table]
    public class CountRow
    {
        [Column]
        public long RowCount { get; set; }
    }


    public class QueryTransaction<T> : IDisposable where T:class, new()
    {
        public IQueryBuilder<T> Builder { get; set; }
        private SqlConnection Connection { get; set; }
        private SqlTransaction Transaction { get; set; }
        private string[] PrimaryKeyColumns { get; set; }

        public QueryTransaction()
        {
            Builder = new SqlQueryBuilder<T>();
        }

        public string[] GetPKColumns(Type type, string prefix, bool includedbgen = true)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);

                if (ts == null) continue;
                ColumnAttribute cs = (ColumnAttribute)ts;
                if (cs == null) continue;
                if (cs.IsDbGenerated == true && includedbgen == false) continue;
                if (cs.IsPrimaryKey) columns.Add(prefix + (cs.Name ?? t.Name));
            }
            return columns.ToArray();
        }

        public void BeginTransaction()
        {
            Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString);
            Connection.Open();
            Transaction = Connection.BeginTransaction();
            Builder = new SqlQueryBuilder<T>(Connection, Transaction);
        }

        public void CommitTransaction()
        {
            Transaction.Commit();
            Connection.Close();
            Connection.Dispose();
        }

        public void Insert(T obj)
        {
            if (Transaction == null) Builder.Insert(obj);
            else Builder.InsertTransaction(obj);
        }

        public void Update(T obj)
        {
            if (Transaction == null) Builder.Update(obj);
            else Builder.UpdateTransaction(obj);
        }

        public void Delete(T obj)
        {
            if (Transaction == null) Builder.Delete(obj);
            else Builder.DeleteTransaction(obj);
        }

        public int DeleteByMany(string[] column, object[] value)
        {
            if (Transaction == null) return Builder.DeleteByMany("and", column, value);
            else return Builder.DeleteByManyTransaction("and", column, value);
        }

        public void RollbackTransaction()
        {
            Transaction.Rollback();
            Connection.Close();
            Connection.Dispose();
        }

        public void Dispose()
        {
            Transaction.Rollback();
            Connection.Close();
            Connection.Dispose();
        }
    }

    
}
