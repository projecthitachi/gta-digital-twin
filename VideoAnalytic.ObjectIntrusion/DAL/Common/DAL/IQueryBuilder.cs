/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 15 Oct 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Common
{
    public interface IQueryBuilder<T> where T : class
    {
        IDbConnection Connection { get; set; }

        IDbTransaction Transaction { get; set; }

        void BeginTransaction();
        void CopyTransactionTo<U>(IQueryBuilder<U> to) where U:class,new();
        void RollbackTransaction();
        void CommitTransaction();

        string[] GetColumnsType(Type type, string prefix, bool includedbgen = true);
        IDataParameter GetSqlParam(string propertyname, T obj);
        string FillPKColumn(Type type, T obj, List<IDataParameter> prms, string prefix, string joiner);
        List<IDataParameter> FillColumnSingle(Type type, T obj, bool includedbgen = true);
        string FillColumn(Type type, T obj, List<IDataParameter> prms, bool includeDBGen);
        List<T> GetAll(string sort = null, JoinSearch js = null);
        T Get(string Id, JoinSearch js = null);
        long Count();
        T Get(T obj, JoinSearch js = null);
        T GetByMany(string type, string[] column, object[] values, JoinSearch js = null);
        List<T> GetManyByMany(string type, string[] column, object[] values, JoinSearch js = null);
        long CountByMany(string type, string[] column, object[] values, JoinSearch js = null);
        int DeleteByMany(string type, string[] column, object[] values);
        int DeleteByManyTransaction(string type, string[] column, object[] values);
        List<T> GetPage(T obj, int page, string sort, JoinSearch js = null, string otherCriteria = "");
        long CountPage(T obj, string sort, JoinSearch js = null, string otherCriteria = "");
        string GetSPSearchA(Type t, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, bool isdate = false);
        string GetSPSearchB(Type t, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, bool isdate = false);
        List<T> GetPageSearch(T obj, string searchtext, int page, string sort, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, string otherCriteria = "");
        long CountPageSearch(T obj, string searchtext, string sort, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, string otherCriteria = "");
        int Delete(T obj);
        int DeleteTransaction(T obj);
        int Insert(T obj);
        int InsertTransaction(T obj);
        int Update(T obj);
        int UpdateTransaction(T obj);
        void InsertAudit(IQueryBuilder<T> qb, string sentence, string commandtype, object old, object newobj, string officername);
        T ExecuteSingle(string sql, Type t, List<IDataParameter> prms, bool includeReadonlyColumn = false);
        T ExecuteSingleTransaction(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false);
        int ExecuteNonQuery(string sql, Type t, List<IDataParameter> prms);
        DataSet ExecuteDataSet(string sql, Type t, List<IDataParameter> prms);
        int ExecuteNonQueryTransaction(string sql, Type t, List<IDataParameter> prms);
        List<T> ExecuteList(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false);
        List<T> ExecuteListSP(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false);
        List<T> ExecuteListTransaction(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false);
    }
}
