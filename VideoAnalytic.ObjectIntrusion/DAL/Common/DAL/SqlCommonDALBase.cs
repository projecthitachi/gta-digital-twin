/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 07 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using MvcPaging;
using System.Reflection;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;
using System.Configuration;

namespace Common
{
    public class SqlCommonDALBase<T> : IRepository<T> where T : class, new()
    {
        public QueryTransaction<T> Query = null;
        public DataContext Context = null;
        public Table<T> Collection = null;

        public int PageSize = 30;
        public string Name = string.Empty;
        public string SortColumns = "CreateDate DESC";
        public string[] SearchColumns = null;
        public string[] ExactColumns = null;
        public JoinSearch JoinSearchConfig = null;
        public string OtherCriteria = string.Empty;
        public string[] _PrimaryKeyColumns;

        public SqlCommonDALBase() : this("", null)
        {
        }

        public SqlCommonDALBase(string name) : this(name, null)
        {
        }

        public SqlCommonDALBase(string[] primaryKeyColumns) : this("", primaryKeyColumns)
        {
        }

        public SqlCommonDALBase(string name, string[] primaryKeyColumns)
        {
            Name = name;
            string[] cols = null;
			_PrimaryKeyColumns = (new SqlQueryBuilder<T>()).GetPKColumnsArray(typeof(T), "", false, false, false, out cols);
            Context = new DataContext(ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString);
            Collection = (Table<T>)Context.GetTable(typeof(T));

            if (string.IsNullOrEmpty(Name)) Name = GetTableName();

            PageSize = 30;
            Query = new QueryTransaction<T>();
        }

        public void SetPrimaryKeyColumns()
        {
            QueryBuilderBase<T> builder = new QueryBuilderBase<T>();
            string[] cols;
            _PrimaryKeyColumns = builder.GetPKColumnsArray(typeof(T), "", false, false, false, out cols);
        }

        public string[] PrimaryKeyColumns
        {
            get { return _PrimaryKeyColumns; }
        }

        public object[] PrimaryKeyValues(T obj) 
        {
            List<object> result = new List<object>();
            for (int i = 0; i < PrimaryKeyColumns.Length; i++)
            {
                object item = MyReflection.GetValue<object>(obj, PrimaryKeyColumns[i]);
                result.Add(item);
            }
            return result.ToArray();
        }

        public string GetTableName()
        {
            Type type = typeof(T);
            object[] temp = type.GetCustomAttributes(typeof(System.Data.Linq.Mapping.TableAttribute), true);

            if (temp.Length == 0) return null;
            var name = (temp[0] as System.Data.Linq.Mapping.TableAttribute).Name;
            if (string.IsNullOrEmpty(name)) return type.Name;
            return name;
        }

        public void RefreshContext()
        {
            DiscardPendingChanges(Context);
        }

        public void DiscardPendingChanges(DataContext context)
        {
            RefreshPendingChanges(context, RefreshMode.OverwriteCurrentValues);
            ChangeSet changeSet = context.GetChangeSet();
            if (changeSet != null)
            {
                foreach (object objToInsert in changeSet.Inserts)
                {
                    context.GetTable(objToInsert.GetType()).DeleteOnSubmit(objToInsert);
                }

                foreach (object objToDelete in changeSet.Deletes)
                {
                    context.GetTable(objToDelete.GetType()).InsertOnSubmit(objToDelete);
                }
            }
        }

        public void RefreshPendingChanges(DataContext context, RefreshMode refreshMode)
        {
            ChangeSet changeSet = context.GetChangeSet();
            if (changeSet != null)
            {
                context.Refresh(refreshMode, changeSet.Deletes);
                context.Refresh(refreshMode, changeSet.Updates);
            }
        }

        public void Detach(List<T> items)
        {
            if (items == null) return;
        }

        public void Detach(T item)
        {
            if (item == null) return;
        }

        public virtual List<T> GetAll()
        {
            return GetValue(Query.Builder.GetAll(SortColumns, JoinSearchConfig));
        }

        public virtual List<T> Aggregate(int id, string[] query, string sortcolumn, string ascdesc)
        {
            return null;
        }

		public virtual List<SleepCount> Aggregate(int id, string[] query)
        {
            return null;
        }

        public virtual long ClearDB()
        {
            return Query.Builder.ExecuteNonQuery("TRUNCATE TABLE " + typeof(T).Name, typeof(T), null);
        }

        public virtual long Count()
        {
            return Query.Builder.Count();
        }

        public virtual PagedList<T> GetPage(int page)
        {
            long count = Query.Builder.CountPage(new T(), SortColumns, JoinSearchConfig, OtherCriteria);

            List<T> results = GetValue(Query.Builder.GetPage(new T(), page, SortColumns, JoinSearchConfig, OtherCriteria));
            PagedList<T> output = new PagedList<T>(results, page - 1, PageSize, (int) count);
            return output;
        }

        public virtual long CountSearch(string searchText)
        {
            return Query.Builder.CountPageSearch(new T(), searchText, SortColumns, JoinSearchConfig, SearchColumns, ExactColumns, OtherCriteria);
        }

        public virtual PagedList<T> GetPageSearch(string searchText, int page)
        {
            long count = Query.Builder.CountPageSearch(new T(), searchText, SortColumns, JoinSearchConfig, SearchColumns, ExactColumns, OtherCriteria);

            List<T> results = GetValue(Query.Builder.GetPageSearch(new T(), searchText, page, SortColumns, JoinSearchConfig, SearchColumns, ExactColumns, OtherCriteria));
            PagedList<T> output = new PagedList<T>(results, page - 1, PageSize, (int)count);
            return output;

        }

        public U PrimaryKeyOneValue<U>(T obj)
        {
            if (PrimaryKeyColumns == null || PrimaryKeyColumns.Length != 1) return default(U);
            return MyReflection.GetValue<U>(obj, PrimaryKeyColumns[0].ToString());
        }

        public virtual T Get(object objectid)
        {
            if (objectid is T)
            {
                return GetValue(Query.Builder.GetByMany(" AND ", PrimaryKeyColumns, PrimaryKeyValues((T)objectid)));
            }
            else
            {
                return GetValue(Query.Builder.Get(objectid.ToString(), JoinSearchConfig));
            }
        }

        public virtual T Get(string objectid)
        {
            return GetValue(Query.Builder.Get(objectid, JoinSearchConfig));
        }

        //public static DateTime SetDateTime(DateTime dt)
        //{
        //    if (dt == DateTime.MinValue || dt.Year == 1) dt = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
        //    return dt;
        //}

        //public static DateTime GetDateTime(DateTime dt)
        //{
        //    if (dt == System.Data.SqlTypes.SqlDateTime.MinValue.Value || dt.Year == 1) dt = DateTime.MinValue;
        //    return dt;
        //}

        //public virtual T FormatObject(T obj)
        //{
        //    List<PropertyInfo> properties = typeof(T).GetProperties().ToList();
        //    foreach (PropertyInfo info in properties)
        //    {
        //        if (info.PropertyType == typeof(DateTime))
        //        {
        //            try
        //            {
        //                DateTime val = (DateTime)info.GetValue(obj);
        //                val = QueryBuilderBase<T>.SetDateTime(val);
        //                info.SetValue(obj, val);
        //            }
        //            catch(Exception ex)
        //            {
        //                Console.Write(ex.Message);
        //            }
        //        }

        //        if (info.PropertyType == typeof(DateTime?))
        //        {
        //            try
        //            {
        //                DateTime? val = (DateTime?)info.GetValue(obj);
        //                if (val.HasValue == false) val = null;
        //                else val = QueryBuilderBase<T>.SetDateTime(val.Value);
        //                info.SetValue(obj, val);
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.Write(ex.Message);
        //            }
        //        }
        //    }
        //    return obj;
        //}

        public virtual T FormatGetObject(T obj)
        {
            List<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            foreach (PropertyInfo info in properties)
            {
                if (info.PropertyType == typeof(DateTime))
                {
                    try
                    {
                        DateTime val = (DateTime)info.GetValue(obj);
                        val = QueryBuilderBase<T>.GetDateTime(val);
                        info.SetValue(obj, val);
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex.Message);
                    }
                }

                if (info.PropertyType == typeof(DateTime?))
                {
                    try
                    {
                        DateTime? val = (DateTime?)info.GetValue(obj);
                        if (val.HasValue) val = QueryBuilderBase<T>.GetDateTime(val.Value);
                        else val = null;
                        info.SetValue(obj, val);
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex.Message);
                    }
                }
            }
            return obj;
        }

        public virtual U FormatGetObject<U>(U obj)
        {
            List<PropertyInfo> properties = typeof(U).GetProperties().ToList();
            foreach (PropertyInfo info in properties)
            {
                if (info.PropertyType == typeof(DateTime))
                {
                    DateTime val = (DateTime)info.GetValue(obj);
                    val = QueryBuilderBase<T>.GetDateTime(val);
                    try
                    {
                        info.SetValue(obj, val);
                    }
                    catch (Exception)
                    {

                    }
                }

                if (info.PropertyType == typeof(DateTime?))
                {
                    DateTime? val = (DateTime?)info.GetValue(obj);
                    if (val.HasValue) val = QueryBuilderBase<T>.GetDateTime(val.Value);
                    else val = null;

                    try
                    {
                        info.SetValue(obj, val);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return obj;
        }

        public List<T> GetValue(List<T> objs)
        {
            if (objs == null) return null;
            foreach (T obj in objs)
            {
                FormatGetObject(obj);
            }
            return objs;
        }

        public PagedList<U> GetValueAnonymous<U>(PagedList<U> objs)
        {
            if (objs == null) return null;
            foreach (U obj in objs)
            {
                FormatGetObject(obj);
            }
            return objs;
        }

        public T GetValue(T objs)
        {
            if (objs == null) return null;
            FormatGetObject(objs);

            return objs;
        }

        public virtual long Insert(T d)
        {
            d = QueryBuilderBase<T>.FormatObject(d);

            object[] ids = PrimaryKeyValues(d);
            if ((ids == null || (ids.Length == 1 && (ids[0] == null || ids[0].ToString() == ""))) & PrimaryKeyColumns != null && PrimaryKeyColumns.Length == 1 && PrimaryKeyColumns[0] == "Id")
            {
                ids = new object[] { MongoDB.Bson.ObjectId.GenerateNewId().ToString() };
                MyReflection.SetValue(d, "Id", ids[0]);
            }
            QueryBuilderBase<T>.FormatObject(d);
            T obj = Query.Builder.GetByMany("and", PrimaryKeyColumns, ids);
            if (obj != null)
            {
                object[] vals = PrimaryKeyValues(obj);
                if (obj != null && vals != null && vals.Length > 0 && vals[0] != null) Delete(obj);
            }
            return Query.Builder.Insert(QueryBuilderBase<T>.FormatObject(d));
        }

        public virtual string InsertReturnID(T d)
        {
            object[] ids = PrimaryKeyValues(d);
            if ((ids == null || (ids.Length == 1 && (ids[0] == null || ids[0].ToString() == ""))) & PrimaryKeyColumns != null && PrimaryKeyColumns.Length == 1 && PrimaryKeyColumns[0] == "Id")
            {
                ids = new object[] { MongoDB.Bson.ObjectId.GenerateNewId().ToString() };
                MyReflection.SetValue(d, "Id", ids[0]);
            }
            QueryBuilderBase<T>.FormatObject(d);
            T objx = Query.Builder.GetByMany("and", PrimaryKeyColumns, ids);
            object[] vals = PrimaryKeyValues(d);
            if (d != null && vals != null && vals.Length > 0 && vals[0] != null) Delete(d);

            Query.Builder.Insert(d);

            string id = string.Empty;
            foreach (object obj in PrimaryKeyValues(d))
            {
                id += obj.ToString() + ",";
            }
            if (id.Length > 0) id = id.Substring(0, id.Length - 1);
            return id;
        }

        public virtual long Delete(T d)
        {
            return Query.Builder.Delete(QueryBuilderBase<T>.FormatObject(d));
        }

        public virtual long Max()
        {
            if (PrimaryKeyColumns == null || PrimaryKeyColumns.Length != 1) return 0;
            Type t = MyReflection.GetType<T>(PrimaryKeyColumns[0]);
            
            if (t == typeof(decimal) || t == typeof(long) || t == typeof(float) || t == typeof(double) || t == typeof(int))
            {
                SqlCommonDALBase<T> newRepo = new SqlCommonDALBase<T>(Name, PrimaryKeyColumns);
                T obj = newRepo.Collection.OrderBy(PrimaryKeyColumns[0] + " DESC").FirstOrDefault();
                if (obj == null) return 0;
                return PrimaryKeyOneValue<long>(obj);
            }
            return 0;
        }

        public virtual long Delete(string objectid)
        {
            T obj = GetValue(Get(objectid));
            return Query.Builder.Delete(obj);
        }

        public virtual DateTime LastModify()
        {
            string sql = @"SELECT MAX(last_user_update) AS LastModifyDate
		FROM sys.dm_db_index_usage_stats i JOIN sys.tables t ON (t.object_id = i.object_id)
		WHERE database_id = db_id() AND t.NAME='" + Name + @"'
		GROUP BY t.name";

            SqlCommonDALBase<T> newRepo = new SqlCommonDALBase<T>(Name, PrimaryKeyColumns);
            DateModel dm = newRepo.Context.ExecuteQuery<DateModel>(sql).FirstOrDefault();
            if (dm == null || dm.LastModifyDate.HasValue == false) return DateTime.Now;
            return dm.LastModifyDate.Value;
        }

        public virtual long Update(T d)
        {
            QueryBuilderBase<T>.FormatObject(d);
            return Query.Builder.Update(d);
        }

        public T GetBy(string column, object value)
        {
            
            return Query.Builder.GetByMany("and", new string[] { column }, new object[] { value }, JoinSearchConfig);
        }

        public T GetByIn(string column, object value, string column2, object value2)
        {
            return Query.Builder.GetByMany("and", new string[] { column, column2 }, new object[] { value, value2 }, JoinSearchConfig);
        }

        public virtual List<T> FindOr(string match, string[] column, object[] value, string order, string sortby)
        {
            return Find(0, match, "or", column, value, order, sortby);
        }

        public virtual List<T> FindOr(int id, string match, string[] column, object[] value, string order, string sortby)
        {
            return Find(id, match, "or", column, value, order, sortby);
        }

        public virtual List<T> FindAnd(string match, string[] column, object[] value, string order, string sortby)
        {
            return Find(0, match, "and", column, value, order, sortby);
        }

        public virtual List<T> FindAnd(int id, string match, string[] column, object[] value, string order, string sortby)
        {
            return Find(id, match, "and", column, value, order, sortby);
        }

        public virtual List<T> Find(int id, string match, string type, string[] column, object[] value, string order="", string sortby="")
        {
            SqlCommonDALBase<T> newRepo = new SqlCommonDALBase<T>(Name, PrimaryKeyColumns);

            RefreshContext();
            List<T> obj = null;
            if (id == 0)
            {
                if (string.IsNullOrEmpty(order) == false && string.IsNullOrEmpty(sortby) == false)
                {
                    if (match == "exact") obj = GetValue(newRepo.Collection.Where(GetQuery("exact", type, column, value)).OrderBy(sortby + " " + order).ToList());
                    else if (match == "like") obj = GetValue(newRepo.Collection.Where(GetQuery("like", type, column, value)).OrderBy(sortby + " " + order).ToList());
                    else if (match == "exactnot") obj = GetValue(newRepo.Collection.Where(GetQuery("exactnot", type, column, value)).OrderBy(sortby + " " + order).ToList());
                }
                else
                {
                    if (match == "exact") obj = GetValue(newRepo.Collection.Where(GetQuery("exact", type, column, value)).ToList());
                    else if (match == "like") obj = GetValue(newRepo.Collection.Where(GetQuery("like", type, column, value)).ToList());
                    else if (match == "exactnot") obj = GetValue(newRepo.Collection.Where(GetQuery("exactnot", type, column, value)).ToList());
                }
            }
            else
            {
                int limit = (id - 1) * 30;
                int take = 30;
                int count = 0;
                if (string.IsNullOrEmpty(order) == false && string.IsNullOrEmpty(sortby) == false)
                {
                    if (match == "exact")
                    {
                        obj = GetValue(newRepo.Collection.Where(GetQuery("exact", type, column, value)).Skip(limit).Take(take).OrderBy(sortby + " " + order).ToList());
                        count = newRepo.Collection.Where(GetQuery("exact", type, column, value)).Count();
                    }
                    else if (match == "like")
                    {
                        obj = GetValue(newRepo.Collection.Where(GetQuery("like", type, column, value)).Skip(limit).Take(take).OrderBy(sortby + " " + order).ToList());
                        count = newRepo.Collection.Where(GetQuery("like", type, column, value)).Count();
                    }
                    else if (match == "exactnot")
                    {
                        obj = GetValue(newRepo.Collection.Where(GetQuery("exactnot", type, column, value)).Skip(limit).Take(take).OrderBy(sortby + " " + order).ToList());
                        count = newRepo.Collection.Where(GetQuery("exactnot", type, column, value)).Count();
                    }
                }
                else
                {
                    if (match == "exact")
                    {
                        obj = GetValue(newRepo.Collection.Where(GetQuery("exact", type, column, value)).Skip(limit).Take(take).ToList());
                        count = newRepo.Collection.Where(GetQuery("exact", type, column, value)).Count();
                    }
                    else if (match == "like")
                    {
                        obj = GetValue(newRepo.Collection.Where(GetQuery("like", type, column, value)).Skip(limit).Take(take).ToList());
                        count = newRepo.Collection.Where(GetQuery("like", type, column, value)).Count();
                    }
                    else if (match == "exactnot")
                    {
                        obj = GetValue(newRepo.Collection.Where(GetQuery("exactnot", type, column, value)).Skip(limit).Take(take).ToList());
                        count = newRepo.Collection.Where(GetQuery("exactnot", type, column, value)).Count();
                    }
                }

                long pageCount = (int)(count / 30);
                if (pageCount % 30 > 0) pageCount++;

                PagedList<T> output = new PagedList<T>(obj, id - 1, 30, count);
                RefreshContext();
                return output;
            }
            RefreshContext();
            Detach(obj);
            return obj;
        }

        public T GetByMany(string[] column, object[] value)
        {
            return Query.Builder.GetByMany("and", column, value, JoinSearchConfig);
        }

        public long CountByMany(string[] column, object[] value)
        {
            return Query.Builder.CountByMany("and", column, value, JoinSearchConfig);
        }

        public List<T> GetManyByMany(string type, string[] column, object[] value)
        {
            return Query.Builder.GetManyByMany(type, column, value, JoinSearchConfig);
        }

        public List<T> GetManyByMany(string[] column, object[] value)
        {
            return Query.Builder.GetManyByMany("and", column, value, JoinSearchConfig);
        }

        public List<T> GetManyBy(string column, object value)
        {
            return Query.Builder.GetManyByMany("and", new string[] { column } , new object[] { value }, JoinSearchConfig);
        }

        public List<T> GetManyBySearch(string column, string value)
        {
            SqlCommonDALBase<T> newRepo = new SqlCommonDALBase<T>(Name, PrimaryKeyColumns);
            RefreshContext();
            List<T> obj = GetValue(newRepo.Collection.Where(GetQuery("exact", "and", new string[] { column }, new object[] { value })).ToList());
            RefreshContext();
            Detach(obj);
            return obj;
        }

        public virtual long DeleteBy(string column, object value)
        {
            return Query.Builder.DeleteByMany("and", new string[] { column }, new object[] { value });
        }

        public virtual long DeleteByMany(string[] column, object[] value)
        {
            return Query.Builder.DeleteByMany("and", column, value);
        }
        
        #region Critical
        private static readonly MethodInfo ContainsMethod = typeof(SqlMethods).GetMethod("Like", new Type[] { typeof(string), typeof(string) });
        private static readonly MethodInfo EqualsMethod = typeof(SqlMethods).GetMethod("Equals", new Type[] { typeof(object), typeof(object) });
        private static readonly MethodInfo ConvertMethod = typeof(Convert).GetMethod("ToString", new Type[] { typeof(object) });

        static SqlCommonDALBase()
        {
            if (EqualsMethod == null) EqualsMethod = typeof(SqlMethods).GetMethod("Equals", new Type[] { typeof(object) });
        }

        public Expression<Func<X, bool>> StringCompare<X>(MethodInfo mi, string propertyName, string queryText)
        {
            var parameter = Expression.Parameter(typeof(X));
            var getter = Expression.PropertyOrField(parameter, propertyName);
            if (getter.Type == typeof(string))
            {
                if (mi == ContainsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, getter, Expression.Constant("%" + queryText + "%", typeof(string)));
                    }
                    catch
                    {
                        //callEx = Expression.Call(getter, mi, Expression.Constant(queryText, typeof(string)));
                    }
                    return Expression.Lambda<Func<X, bool>>(callEx, parameter);
                }
                else if (mi == EqualsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, Expression.Constant(getter, typeof(object)), Expression.Constant(queryText, typeof(object)));
                    }
                    catch
                    {
                        // This is second type of EqualsMethod which is set in Static Constructor, where only 1 parameter with object type
                        // The caller is not EqualsMethod but it is an object

                        // No need to cast getter to object, because if do that it will compare using reference but we need
                        // to compare using value
                        callEx = Expression.Call(getter, mi, Expression.Constant(queryText, typeof(object)));
                    }
                    return Expression.Lambda<Func<X, bool>>(callEx, parameter);
                }
            }
            else
            {
                var callExConvert = Expression.Call(ConvertMethod, Expression.Convert(getter, typeof(object)));

                if (mi == ContainsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, callExConvert, Expression.Constant("%" + queryText + "%", typeof(string)));
                    }
                    catch
                    {
                        //callEx = Expression.Call(callExConvert, mi, Expression.Constant(queryText, typeof(string)));
                    }
                    return Expression.Lambda<Func<X, bool>>(callEx, parameter);
                }
                else if (mi == EqualsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, Expression.Constant(callExConvert, typeof(object)), Expression.Constant(queryText, typeof(object)));
                    }
                    catch
                    {
                        // This is second type of EqualsMethod which is set in Static Constructor, where only 1 parameter with object type
                        // The caller is not EqualsMethod but it is an object

                        // No need to cast getter to object, because if do that it will compare using reference but we need
                        // to compare using value
                        callEx = Expression.Call(callExConvert, mi, Expression.Constant(queryText, typeof(object)));
                    }
                    return Expression.Lambda<Func<X, bool>>(callEx, parameter);
                }
            }
            return null;
        }

        public Expression<Func<X, bool>> StringCompareNot<X>(MethodInfo mi, string propertyName, string queryText)
        {
            var parameter = Expression.Parameter(typeof(X));
            var getter = Expression.PropertyOrField(parameter, propertyName);
            if (getter.Type == typeof(string))
            {
                if (mi == ContainsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, getter, Expression.Constant("%" + queryText + "%", typeof(string)));
                    }
                    catch
                    {
                        //callEx = Expression.Call(getter, mi, Expression.Constant(queryText, typeof(string)));
                    }
                    return Expression.Lambda<Func<X, bool>>(callEx, parameter);
                }
                else if (mi == EqualsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, Expression.Constant(getter, typeof(object)), Expression.Constant(queryText, typeof(object)));
                    }
                    catch
                    {
                        // This is second type of EqualsMethod which is set in Static Constructor, where only 1 parameter with object type
                        // The caller is not EqualsMethod but it is an object

                        // No need to cast getter to object, because if do that it will compare using reference but we need
                        // to compare using value
                        callEx = Expression.Call(getter, mi, Expression.Constant(queryText, typeof(object)));
                    }
                    return Expression.Lambda<Func<X, bool>>(callEx, parameter);
                }
            }
            else
            {
                var callExConvert = Expression.Call(ConvertMethod, Expression.Convert(getter, typeof(object)));

                if (mi == ContainsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, callExConvert, Expression.Constant("%" + queryText + "%", typeof(string)));
                    }
                    catch
                    {
                        //callEx = Expression.Call(callExConvert, mi, Expression.Constant(queryText, typeof(string)));
                    }
                    return Expression.Lambda<Func<X, bool>>(Expression.Not(callEx), parameter);
                }
                else if (mi == EqualsMethod)
                {
                    MethodCallExpression callEx = null;
                    try
                    {
                        callEx = Expression.Call(mi, Expression.Constant(callExConvert, typeof(object)), Expression.Constant(queryText, typeof(object)));
                    }
                    catch
                    {
                        // This is second type of EqualsMethod which is set in Static Constructor, where only 1 parameter with object type
                        // The caller is not EqualsMethod but it is an object

                        // No need to cast getter to object, because if do that it will compare using reference but we need
                        // to compare using value
                        callEx = Expression.Call(callExConvert, mi, Expression.Constant(queryText, typeof(object)));
                    }
                    return Expression.Lambda<Func<X, bool>>(Expression.Not(callEx), parameter);
                }
            }
            return null;
        }

        public Expression<Func<T, bool>> GetQuery(string prefix, string type, string[] columnName, object[] columnValue)
        {
            var param = Expression.Parameter(typeof(T));
            Expression result = Expression.Constant(true);
            List<Expression<Func<T, bool>>> filters = new List<Expression<Func<T, bool>>>();
            IEnumerable<PropertyInfo> properties = typeof(T).GetProperties().ToArray();

            foreach (PropertyInfo pi in properties)
            {
                Expression<Func<T, bool>> single = null;
                if (Attribute.IsDefined(pi, typeof(ColumnAttribute)) == false) continue;

                bool exist = true;
                int i = 0;
                int valueIndex = 0;
                if (columnName != null && columnValue != null && columnName.Length == columnValue.Length) // check Values in Columns
                {
                    exist = false;
                    for (i = 0; i < columnName.Length; i++)
                    {
                        if (columnName[i] == pi.Name)
                        {
                            exist = true;
                            valueIndex = i;
                            break;
                        }
                    }
                }
                else if (columnValue != null && columnName == null) // check Value in all columns
                {
                    valueIndex = 0;
                    exist = true;
                }
                if (exist == false) continue;

                if (pi.PropertyType == typeof(string))
                {
                    if (prefix == "like")
                        single = StringCompare<T>(ContainsMethod, pi.Name, columnValue[valueIndex].ToString());
                    else if (prefix == "exact")
                        single = StringCompare<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                    else if (prefix == "exactnot")
                        single = StringCompareNot<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                }
                else if (pi.PropertyType == typeof(int) || pi.PropertyType == typeof(long) || pi.PropertyType == typeof(float) || pi.PropertyType == typeof(decimal) || pi.PropertyType == typeof(double))
                {
                    decimal dt = new decimal();
                    if (decimal.TryParse(columnValue[i].ToString(), out dt))
                    {
                        if (prefix == "exact")
                            single = StringCompare<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                        else if (prefix == "like")
                            single = StringCompare<T>(ContainsMethod, pi.Name, columnValue[valueIndex].ToString());
                        else if (prefix == "exactnot")
                            single = StringCompareNot<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                    }
                }
                else if (pi.PropertyType == typeof(DateTime))
                {
                    DateTime dt = new DateTime();
                    if (DateTime.TryParse(columnValue[i].ToString(), out dt))
                    {
                        if (prefix == "exact")
                            single = StringCompare<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                        else if (prefix == "like")
                            single = StringCompare<T>(ContainsMethod, pi.Name, columnValue[valueIndex].ToString());
                        else if (prefix == "exactnot")
                            single = StringCompareNot<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                    }
                }
                else if (pi.PropertyType == typeof(bool))
                {
                    bool dt = false;
                    if (bool.TryParse(columnValue[i].ToString(), out dt))
                    {
                        if (prefix == "exact")
                            single = StringCompare<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                        else if (prefix == "like")
                            single = StringCompare<T>(ContainsMethod, pi.Name, columnValue[valueIndex].ToString());
                        else if (prefix == "exactnot")
                            single = StringCompareNot<T>(EqualsMethod, pi.Name, columnValue[valueIndex].ToString());
                    }
                }
                if (single != null) filters.Add(single);
            }

            Expression<Func<T, bool>> result2 = null;
            if (filters.Count > 0)
            {
                if (type == "or")
                {
                    var combined = filters.Select(func => Replace(func.Body, func.Parameters[0], param))
                    .Aggregate((a, b) => Expression.OrElse(a, b));

                    result2 = Expression.Lambda<Func<T, bool>>(combined, param);
                }
                else if (type == "and")
                {
                    var combined = filters.Select(func => Replace(func.Body, func.Parameters[0], param))
                   .Aggregate((a, b) => Expression.AndAlso(a, b));

                    result2 = Expression.Lambda<Func<T, bool>>(combined, param);
                }
                return result2;
            }

            return Expression.Lambda<Func<T, bool>>(result, param);
        }

        public Expression<Func<U, bool>> GetQuery<U>(IQueryable<U> collection, string prefix, string type, string[] columnName, object[] columnValue)
        {
            var param = Expression.Parameter(typeof(U));
            Expression result = Expression.Constant(true);
            List<Expression<Func<U, bool>>> filters = new List<Expression<Func<U, bool>>>();
            IEnumerable<PropertyInfo> properties = typeof(U).GetProperties().ToArray();

            foreach (PropertyInfo pi in properties)
            {
                Expression<Func<U, bool>> single = null;
                //if (Attribute.IsDefined(pi, typeof(ColumnAttribute)) == false) continue;

                bool exist = true;
                int i = 0;
                int valueIndex = 0;
                if (columnName != null && columnValue != null && columnName.Length == columnValue.Length) // check Values in Columns
                {
                    exist = false;
                    for (i = 0; i < columnName.Length; i++)
                    {
                        if (columnName[i] == pi.Name)
                        {
                            exist = true;
                            valueIndex = i;
                            break;
                        }
                    }
                }
                else if (columnValue != null && columnName == null) // check Value in all columns
                {
                    valueIndex = 0;
                    exist = true;
                }
                if (exist == false) continue;

                if (pi.PropertyType == typeof(string))
                {
                    if (prefix == "like")
                        single = StringCompare<U>(ContainsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                    else if (prefix == "exact")
                        single = StringCompare<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                    else if (prefix == "exactnot")
                        single = StringCompareNot<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                }
                else if (pi.PropertyType == typeof(int) || pi.PropertyType == typeof(long) || pi.PropertyType == typeof(float) || pi.PropertyType == typeof(decimal) || pi.PropertyType == typeof(double))
                {
                    decimal dt = new decimal();
                    if (decimal.TryParse((columnValue[valueIndex] ?? "").ToString(), out dt))
                    {
                        if (prefix == "exact")
                            single = StringCompare<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                        else if (prefix == "like")
                            single = StringCompare<U>(ContainsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                        else if (prefix == "exactnot")
                            single = StringCompareNot<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                    }
                }
                else if (pi.PropertyType == typeof(DateTime))
                {
                    DateTime dt = new DateTime();
                    if (DateTime.TryParse((columnValue[valueIndex] ?? "").ToString(), out dt))
                    {
                        if (prefix == "exact")
                            single = StringCompare<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                        else if (prefix == "like")
                            single = StringCompare<U>(ContainsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                        else if (prefix == "exactnot")
                            single = StringCompareNot<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                    }
                }
                else if (pi.PropertyType == typeof(bool))
                {
                    bool dt = false;
                    if (bool.TryParse((columnValue[valueIndex] ?? "").ToString(), out dt))
                    {
                        if (prefix == "exact")
                            single = StringCompare<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                        else if (prefix == "like")
                            single = StringCompare<U>(ContainsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                        else if (prefix == "exactnot")
                            single = StringCompareNot<U>(EqualsMethod, pi.Name, (columnValue[valueIndex] ?? "").ToString());
                    }
                }
                if (single != null) filters.Add(single);
            }

            Expression<Func<U, bool>> result2 = null;
            if (filters.Count > 0)
            {
                if (type == "or")
                {
                    var combined = filters.Select(func => Replace(func.Body, func.Parameters[0], param))
                    .Aggregate((a, b) => Expression.OrElse(a, b));

                    result2 = Expression.Lambda<Func<U, bool>>(combined, param);
                }
                else if (type == "and")
                {
                    var combined = filters.Select(func => Replace(func.Body, func.Parameters[0], param))
                   .Aggregate((a, b) => Expression.AndAlso(a, b));

                    result2 = Expression.Lambda<Func<U, bool>>(combined, param);
                }
                return result2;
            }

            return Expression.Lambda<Func<U, bool>>(result, param);
        }

        public class ReplaceVisitor : ExpressionVisitor
        {
            private readonly Expression from, to;
            public ReplaceVisitor(Expression from, Expression to)
            {
                this.from = from;
                this.to = to;
            }
            public override Expression Visit(Expression node)
            {
                return node == from ? to : base.Visit(node);
            }
        }

        public static Expression Replace(Expression expression, Expression searchEx, Expression replaceEx)
        {
            return new ReplaceVisitor(searchEx, replaceEx).Visit(expression);
        }
        #endregion
    }
}