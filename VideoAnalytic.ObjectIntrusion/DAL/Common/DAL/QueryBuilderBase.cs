/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 08 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Data.Linq.Mapping;
using System.Data;
using System.Linq;

namespace Common
{
    public class PKColumn
    {
        public string PropertyName { get; set; }

        public string ColumnName { get; set; }
    }

    public class QueryBuilderBase<T> where T : class, new()
    {
        public string DBConnString = string.Empty;

        private IDbConnection _Connection;
        private IDbTransaction _Transaction;
        public static DateTime MyMinValue = new DateTime(1754, 1, 1);

        public IDbConnection Connection
        {
            get
            {
                return _Connection;
            }
            set
            {
                _Connection = value;
            }
        }

        public IDbTransaction Transaction
        {
            get
            {
                return _Transaction;
            }
            set
            {
                _Transaction = value;
            }
        }

        public QueryBuilderBase()
        {

        }

        public void BeginTransaction()
        {
            if (Connection != null || Transaction != null) throw new Exception("There is current Transaction which has not been committed/rollback");
            Connection = GetConnection(DBConnString);
            Connection.Open();

            Transaction = Connection.BeginTransaction();
        }

        public void RollbackTransaction()
        {
            if (Connection == null) return;
            if (Transaction == null) return;

            Transaction.Rollback();
            Transaction.Dispose();
            Connection.Close();
            Connection.Dispose();

            Transaction = null;
            Connection = null;
        }

        public void CommitTransaction()
        {
            if (Connection == null) return;
            if (Transaction == null) return;

            Transaction.Commit();
            Transaction.Dispose();
            Connection.Close();
            Connection.Dispose();

            Transaction = null;
            Connection = null;
        }

        public void CopyTransactionTo<U>(IQueryBuilder<U> to) where U : class,new()
        {
            to.Transaction = Transaction;
            to.Connection = Connection;
        }

        public QueryBuilderBase(IDbConnection conn, IDbTransaction tran)
        {
            DBConnString = System.Configuration.ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString;
            Connection = conn;
            Transaction = tran;
        }

        public bool IsNewPK(Type type, string prefix, bool includedbgen = true)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);

                if (ts == null) continue;
                ColumnAttribute cs = (ColumnAttribute)ts;
                if (cs == null) continue;
                if (cs.IsPrimaryKey) columns.Add(prefix + (((ColumnAttribute)ts).Name ?? t.Name));
            }

            if (columns.Count > 1 && columns.Where(m => m == prefix + "Id").Count() == 1)
            {
                columns.Remove(prefix + "Id");
                return true;
            }
            return false;
        }

        public string GetTableName(Type type)
        {
            object[] temp = type.GetCustomAttributes(typeof(System.Data.Linq.Mapping.TableAttribute), true);

            if (temp.Length == 0) return string.Empty;
            var name = (temp[0] as System.Data.Linq.Mapping.TableAttribute).Name;
            if (string.IsNullOrEmpty(name))
                return type.Name;
            return name;
        }

        public string[] GetColumns(Type type, string prefix, bool includeReadColumn, bool includedbgen, bool includeBracket)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);
                Attribute readOnlyColumn = t.GetCustomAttribute(typeof(ReadOnlyColumnAttribute), true);
                if (ts == null && includeReadColumn == false) continue;
                if (includeReadColumn && readOnlyColumn != null)
                {
                    columns.Add(prefix + (((ReadOnlyColumnAttribute)readOnlyColumn).Name ?? t.Name));
                }
                else if (ts != null)
                {
                    if (((ColumnAttribute)ts).IsDbGenerated && includedbgen == false) continue;
                    if (includeBracket)
                        columns.Add(prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name + "]"));
                    else
                        columns.Add(prefix + (((ColumnAttribute)ts).Name ?? t.Name));
                }
            }
            if (IsNewPK(type, prefix, includedbgen))
            {
                if (includeBracket)
                    columns.Remove(prefix + "[Id]");
                else
                    columns.Remove(prefix + "Id");
            }
            return columns.ToArray();
        }

        public List<PKColumn> GetPKColumns(Type type, string prefix, bool includedbgen, bool includeBracket, bool usePropertyName)
        {
            List<PKColumn> pkcolumns = new List<PKColumn>();
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);

                if (ts == null) continue;
                ColumnAttribute cs = (ColumnAttribute)ts;
                if (cs == null) continue;
                if (cs.IsPrimaryKey == false && cs.IsDbGenerated == true && includedbgen == false) continue;

                PKColumn pk = new PKColumn();
                pk.PropertyName = prefix + t.Name;
                pk.ColumnName = prefix + ((ColumnAttribute)ts).Name;
                if (string.IsNullOrEmpty(pk.ColumnName) || pk.ColumnName == prefix) pk.ColumnName = pk.PropertyName;
                if (cs.IsPrimaryKey)
                {
                    if (includeBracket)
                    {
                        pkcolumns.Add(pk);
                        if (usePropertyName)
                        {
                            columns.Add(prefix + "[" + t.Name + "]");
                            pk.ColumnName = prefix + "[" + t.Name + "]";
                        }
                        else
                        {
                            columns.Add(prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name) + "]");
                            pk.ColumnName = prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name) + "]";
                        }
                    }
                    else
                    {
                        pkcolumns.Add(pk);
                        if (usePropertyName)
                        {
                            columns.Add(prefix + t.Name);
                        }
                        else
                            columns.Add(prefix + (((ColumnAttribute)ts).Name ?? t.Name));
                    }
                }
            }
            return pkcolumns;
        }

        public string[] GetPKColumnsArray(Type type, string prefix, bool includedbgen, bool includeBracket, bool usePropertyName, out string[] cols)
        {
            List<PKColumn> pkcolumns = GetPKColumns(type, prefix, includedbgen, includeBracket, usePropertyName);
            List<string> result = new List<string>();
            foreach (PKColumn pk in pkcolumns)
            {
                result.Add(pk.ColumnName);
            }
            List<PKColumn> cols2 = GetPKColumns(type, "", includedbgen, includeBracket, usePropertyName);
            List<string> colsresult = new List<string>();
            foreach (PKColumn pk in cols2)
                colsresult.Add(pk.ColumnName);
            cols = colsresult.ToArray();
            return result.ToArray();
        }

        public string ListColumn(Type type, string prefix, bool includeReadOnly, bool includedbgen, bool includeBracket)
        {
            return string.Join(", ", GetColumns(type, prefix, includeReadOnly, includedbgen, includeBracket));
        }

        public string ListColumnType(Type type, string prefix, bool includedbgen)
        {
            return string.Join(", ", GetColumnsType(type, prefix, includedbgen));
        }

        public string ListPKColumn(Type type, string prefix, bool includedbgen, bool includeBracket, bool usePropertyName)
        {
            List<PKColumn> pks = GetPKColumns(type, prefix, includedbgen, includeBracket, usePropertyName);
            string result = string.Empty;
            foreach (PKColumn pk in pks)
            {
                result += pk.ColumnName + ",";
            }
            if (result.Length > 1) result = result.Substring(0, result.Length - 1);
            return result;
        }

        public string JoinPKAB(Type type, string prefix, string aaaa = "", string bbbb = "")
        {
            if (string.IsNullOrEmpty(aaaa)) aaaa = "a";
            if (string.IsNullOrEmpty(bbbb)) bbbb = "b";
            List<string> result = new List<string>();
            List<PKColumn> pks = GetPKColumns(type, "", false, true, false);
            foreach (PKColumn pk in pks)
            {
                result.Add(aaaa + "." + pk.ColumnName + " = " + bbbb+ "." + pk.ColumnName);
            }
            return string.Join(prefix, result.ToArray());
        }

        public string FillPKColumn(Type type, T obj, List<IDataParameter> prms, string prefix, string joiner)
        {
            string sql = string.Empty;
            List<PKColumn> pks = GetPKColumns(type, "", false, false, false);
            List<string> rs = new List<string>();
            for (int i = 0; i < pks.Count; i++)
            {
                rs.Add(prefix + "[" + pks[i].ColumnName + "] = @" + pks[i].PropertyName);
                var prm = GetSqlParam(pks[i].PropertyName, obj);
                if (prm.ParameterName.StartsWith("@[") && prm.ParameterName.EndsWith("]"))
                    prm.ParameterName = "@" + prm.ParameterName.Substring(2, prm.ParameterName.Length - 3);
                IDataParameter exist = prms.Where(m => m.ParameterName == prm.ParameterName).FirstOrDefault();
                if (exist == null) prms.Add(prm);
            }
            if (IsNewPK(type, prefix, true))
            {
                rs.Remove(prefix + "[Id] = @Id");
            }
            return string.Join(joiner, rs.ToArray());
        }

        public List<IDataParameter> FillColumnSingle(Type type, T obj, bool includedbgen = true)
        {
            string sql = string.Empty;
            List<IDataParameter> prms = new List<IDataParameter>();
            string[] pks = GetColumns(type, "", false, includedbgen, false);
            for (int i = 0; i < pks.Length; i++)
            {
                var prm = GetSqlParam(pks[i], obj);
                if (prm.ParameterName.StartsWith("@[") && prm.ParameterName.EndsWith("]"))
                    prm.ParameterName = "@" + prm.ParameterName.Substring(2, prm.ParameterName.Length - 3);
                IDataParameter exist = prms.Where(m => m.ParameterName == prm.ParameterName).FirstOrDefault();
                if (exist == null) prms.Add(prm);
            }
            return prms;
        }

        public string FillColumn(Type type, T obj, List<IDataParameter> prms, bool includeDBGen)
        {
            string sql = string.Empty;
            string[] pks = null;
            if (includeDBGen == false) pks = GetColumns(type, "", false, false, false);
            else pks = GetColumns(type, "", false, true, false);
            List<string> rs = new List<string>();
            for (int i = 0; i < pks.Length; i++)
            {
                string pksi = pks[i];
                if (pksi.StartsWith("[") && pksi.EndsWith("]")) pksi = pksi.Substring(1, pksi.Length - 2);
                rs.Add("[" + pksi + "] = @" + pksi);
                var prm = GetSqlParam(pksi, obj);
                if (prm.ParameterName.StartsWith("@[") && prm.ParameterName.EndsWith("]"))
                    prm.ParameterName = "@" + prm.ParameterName.Substring(2, prm.ParameterName.Length - 3);
                IDataParameter exist = prms.Where(m => m.ParameterName == prm.ParameterName).FirstOrDefault();
                if (exist == null) prms.Add(prm);
            }
            return string.Join(", ", rs.ToArray());
        }

        public string MakeOver(string prefix, string search, JoinSearch js)
        {
            string result = string.Empty;
            string[] searches = search.Split(new char[] { ',' });

            List<string> tmp = new List<string>();
            bool previous = false;
            foreach(string str in searches)
            {
                if (str.ToUpper().Trim().StartsWith("ISNULL"))
                {
                    tmp.Add(str);
                    previous = true;
                }
                else
                {
                    if (previous && tmp.Count > 0)
                    {
                        tmp[tmp.Count - 1] += "," + str;
                        previous = false;
                    }
                    else
                    {
                        tmp.Add(str);
                    }
                }
            }
            searches = tmp.ToArray();
            foreach (string s in searches)
            {
                string ss = s.Trim();
                if (ss.IndexOf(".") >= 0)
                {
                    result += ss;
                    continue;
                }
                if (ss.ToUpper().StartsWith("ISNULL("))
                {
                    //ISNULL(aa,bb)
                    string order = "ASC";
                    if (ss.ToUpper().Trim().EndsWith("DESC")) order = "DESC";

                    string sss = ss.Substring(0, ss.Length - order.Length).Trim();
                    int length = ss.Length - sss.Length;
                    ss = ss.Substring(7, ss.Length - (8 + length));
                    string[] twovars = ss.Split(new char[] { ',' });

                    string vara = string.Empty;
                    string varb = string.Empty;
                    if (twovars != null && twovars.Length == 2)
                    {
                        if (twovars[0].IndexOf(".") >= 0)
                            vara += twovars[0].Trim();
                        else
                            vara += prefix + twovars[0].Trim();

                        if (twovars[1].IndexOf(".") >= 0)
                            varb += twovars[1].Trim();
                        else
                            varb += prefix + twovars[1].Trim();

                        if (string.IsNullOrEmpty(result))
                            result += "ISNULL(" + vara + ", " + varb + ") " + order;
                        else
                            result += ", " + "ISNULL(" + vara + ", " + varb + ") " + order;

                        continue;
                    }
                }
                string[] sscol = ss.Split(new char[] { ' ' });

                if (js != null)
                {
                    if (js.ColumnAlias.ContainsKey(sscol[0]))
                    {
                        if (string.IsNullOrEmpty(result))
                            result += js.ColumnAlias[sscol[0]];
                        else
                            result += ", " + js.ColumnAlias[sscol[0]];
                        continue;
                    }
                }

                if (string.IsNullOrEmpty(result))
                    result += prefix + ss;
                else
                    result += ", " + prefix + ss;
            }
            return result;
        }

        public object[] PrimaryKeyValues(T obj)
        {
            List<object> result = new List<object>();
            List<PKColumn> PrimaryKeyColumns = GetPKColumns(typeof(T), "", false, false, false);
            for (int i = 0; i < PrimaryKeyColumns.Count; i++)
            {
                object item = MyReflection.GetValue<object>(obj, PrimaryKeyColumns[i].PropertyName);
                result.Add(item);
            }
            return result.ToArray();
        }

        public static DateTime SetDateTime(DateTime dt)
        {
            if (dt == DateTime.MinValue || dt.Year == 1 || dt == MyMinValue) dt = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            return dt;
        }

        public static DateTime GetDateTime(DateTime dt)
        {
            if (dt == System.Data.SqlTypes.SqlDateTime.MinValue.Value || dt.Year == 1) dt = MyMinValue;
            return dt;
        }

        public static T FormatObject(T obj)
        {
            List<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            foreach (PropertyInfo info in properties)
            {
                if (info.PropertyType == typeof(DateTime))
                {
                    try
                    {
                        DateTime val = (DateTime)info.GetValue(obj);
                        val = SetDateTime(val);
                        info.SetValue(obj, val);
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                    catch (Exception ex)
                    {
                        LibraryTools.WriteName("FormatObject", ex);
                    }
                }
                if (info.PropertyType == typeof(DateTime?))
                {
                    try
                    {
                        DateTime? val = (DateTime?)info.GetValue(obj);
                        if (val.HasValue == false) val = null;
                        else val = SetDateTime(val.Value);
                        info.SetValue(obj, val);
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                    catch (Exception ex)
                    {
                        LibraryTools.WriteName("FormatObject", ex);
                    }
                }
            }
            return obj;
        }

        public virtual string[] GetColumnsType(Type type, string prefix, bool includedbgen = true)
        {
            return null;
        }

        public virtual IDataParameter GetSqlParam(string propertyname, T obj)
        {
            return null;
        }

        public virtual IDbConnection GetConnection(string str)
        {
            return null;
        }

        public virtual IDbCommand GetCommand(string sql, IDbConnection conn)
        {
            return null;
        }

        public virtual IDataAdapter GetAdapter(IDbCommand com)
        {
            return null;
        }

        public void ParameterAddRange(IDbCommand com, IDataParameter[] param)
        {
            foreach (IDbDataParameter p in param) com.Parameters.Add(p);
        }

        public T ExecuteSingle(string sql, Type t, List<IDataParameter> prms, bool includeReadonlyColumn = false)
        {
            if (Connection != null && Transaction != null) return ExecuteSingleTransaction(sql, t, prms, includeReadonlyColumn);

            T obj = new T();
            string[] cols = GetColumns(t, "", includeReadonlyColumn, true, false);
            bool hasRead = false;

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    ParameterAddRange(cmd, prms.ToArray());
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            hasRead = true;
                            foreach (string col in cols)
                            {
                                try
                                {
                                    string col2 = col;
                                    if (col.StartsWith("[") && col.EndsWith("]"))
                                        col2 = col.Substring(1, col.Length - 2);
                                    MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                                }
                                catch (IndexOutOfRangeException)
                                {

                                }
                                catch (ArgumentException ex)
                                {
                                    LibraryTools.WriteName("Execute_WrongType", ex);
                                }
                                catch (Exception ex)
                                {
                                    LibraryTools.WriteName("ExecuteSingle", ex);
                                }
                            }
                        }
                        reader.Close();
                    }
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            if (hasRead == false) obj = default(T);
            return obj;
        }

        public T ExecuteSingleTransaction(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            if (Connection == null) throw new Exception("Must initialised QueryBuilder with Connection Object");
            if (Transaction == null) throw new Exception("Must initialised QueryBuilder with BeginTransaction");

            T obj = new T();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            if (Connection.State != ConnectionState.Open) Connection.Open();

            using (IDbCommand cmd = GetCommand(sql, Connection))
            {
                cmd.Transaction = Transaction;
                cmd.CommandType = System.Data.CommandType.Text;
                ParameterAddRange(cmd, prms.ToArray());
                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        foreach (string col in cols)
                        {
                            try
                            {
                                string col2 = col;
                                if (col.StartsWith("[") && col.EndsWith("]"))
                                    col2 = col.Substring(1, col.Length - 2);
                                MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                            }
                            catch (IndexOutOfRangeException)
                            {

                            }
                            catch (ArgumentException ex)
                            {
                                LibraryTools.WriteName("Execute_WrongType", ex);
                            }
                            catch (Exception ex)
                            {
                                LibraryTools.WriteName("ExecuteSingleTransaction", ex);
                            }
                        }
                    }
                    reader.Close();
                }
                cmd.Dispose();
            }
            return obj;
        }

        public int ExecuteNonQuery(string sql, Type t, List<IDataParameter> prms)
        {
            if (Connection != null && Transaction != null) return ExecuteNonQueryTransaction(sql, t, prms);

            string[] cols = GetColumns(t, "", false, true, false);
            int result = 0;

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());

                    result = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return result;
        }

        public int ExecuteNonQueryTransaction(string sql, Type t, List<IDataParameter> prms)
        {
            if (Connection == null) throw new Exception("Must initialised QueryBuilder with Connection Object");
            if (Transaction == null) throw new Exception("Must initialised QueryBuilder with BeginTransaction");

            string[] cols = GetColumns(t, "", false, true, false);
            int result = 0;

            if (Connection.State != ConnectionState.Open) Connection.Open();
            
            using (IDbCommand cmd = GetCommand(sql, Connection))
            {
                cmd.Transaction = Transaction;
                cmd.CommandType = System.Data.CommandType.Text;
                if (prms != null) ParameterAddRange(cmd, prms.ToArray());

                result = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            return result;
        }

        public DataSet ExecuteDataSet(string sql, Type t, List<IDataParameter> prms)
        {
            DataSet ds = new DataSet();

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());

                    IDataAdapter da = GetAdapter(cmd);
                    da.Fill(ds);
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return ds;
        }

        public List<T> ExecuteList(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            if (Connection != null && Transaction != null) return ExecuteListTransaction(sql, t, prms, includeReadOnlyColumn);

            List<T> result = new List<T>();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            T obj = new T();
                            foreach (string col in cols)
                            {
                                try
                                {
                                    string col2 = col;
                                    if (col.StartsWith("[") && col.EndsWith("]"))
                                        col2 = col.Substring(1, col.Length - 2);
                                    MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                                }
                                catch (IndexOutOfRangeException)
                                {

                                }
                                catch (ArgumentException ex)
                                {
                                    LibraryTools.WriteName("Execute_WrongType", ex);
                                }
                                catch (Exception ex)
                                {
                                    LibraryTools.WriteName("ExecuteList", ex);
                                }
                            }
                            result.Add(obj);
                        }
                        reader.Close();
                    }
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return result;
        }

        public List<T> ExecuteListSP(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            if (Connection != null && Transaction != null) return ExecuteListSPTransaction(sql, t, prms, includeReadOnlyColumn);

            List<T> result = new List<T>();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            using (IDbConnection conn = GetConnection(DBConnString))
            {
                conn.Open();
                using (IDbCommand cmd = GetCommand(sql, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (prms != null) ParameterAddRange(cmd, prms.ToArray());
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            T obj = new T();
                            foreach (string col in cols)
                            {
                                try
                                {
                                    string col2 = col;
                                    if (col.StartsWith("[") && col.EndsWith("]"))
                                        col2 = col.Substring(1, col.Length - 2);
                                    MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                                }
                                catch (IndexOutOfRangeException)
                                {

                                }
                                catch (ArgumentException ex)
                                {
                                    LibraryTools.WriteName("Execute_WrongType", ex);
                                }
                                catch (Exception ex)
                                {
                                    LibraryTools.WriteName("ExecuteListSP", ex);
                                }
                            }
                            result.Add(obj);
                        }
                        reader.Close();
                    }
                    cmd.Dispose();
                }
                conn.Close();
                conn.Dispose();
            }
            return result;
        }

        public List<T> ExecuteListSPTransaction(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            if (Connection == null) throw new Exception("Must initialised QueryBuilder with Connection Object");
            if (Transaction == null) throw new Exception("Must initialised QueryBuilder with BeginTransaction");

            List<T> result = new List<T>();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            if (Connection.State != ConnectionState.Open) Connection.Open();
            
            using (IDbCommand cmd = GetCommand(sql, Connection))
            {
                cmd.Transaction = Transaction;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                if (prms != null) ParameterAddRange(cmd, prms.ToArray());
                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        T obj = new T();
                        foreach (string col in cols)
                        {
                            try
                            {
                                string col2 = col;
                                if (col.StartsWith("[") && col.EndsWith("]"))
                                    col2 = col.Substring(1, col.Length - 2);
                                MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                            }
                            catch (IndexOutOfRangeException)
                            {

                            }
                            catch (ArgumentException ex)
                            {
                                LibraryTools.WriteName("Execute_WrongType", ex);
                            }
                            catch (Exception ex)
                            {
                                LibraryTools.WriteName("ExecuteListSP", ex);
                            }
                        }
                        result.Add(obj);
                    }
                    reader.Close();
                }
                cmd.Dispose();
            }
             
            return result;
        }

        public List<T> ExecuteListTransaction(string sql, Type t, List<IDataParameter> prms, bool includeReadOnlyColumn = false)
        {
            if (Connection == null) throw new Exception("Must initialised QueryBuilder with Connection Object");
            if (Transaction == null) throw new Exception("Must initialised QueryBuilder with BeginTransaction");

            List<T> result = new List<T>();
            string[] cols = GetColumns(t, "", includeReadOnlyColumn, true, false);

            if (Connection.State != ConnectionState.Open) Connection.Open();

            using (IDbCommand cmd = GetCommand(sql, Connection))
            {
                cmd.Transaction = Transaction;
                cmd.CommandType = System.Data.CommandType.Text;

                if (prms != null) ParameterAddRange(cmd, prms.ToArray());
                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        T obj = new T();
                        foreach (string col in cols)
                        {
                            try
                            {
                                string col2 = col;
                                if (col.StartsWith("[") && col.EndsWith("]"))
                                    col2 = col.Substring(1, col.Length - 2);
                                MyReflection.SetValue(obj, col2, reader[col2] == Convert.DBNull ? null : reader[col2]);
                            }
                            catch (IndexOutOfRangeException)
                            {

                            }
                            catch (ArgumentException ex)
                            {
                                LibraryTools.WriteName("Execute_WrongType", ex);
                            }
                            catch (Exception ex)
                            {
                                LibraryTools.WriteName("ExecuteListTransaction", ex);
                            }
                        }
                        result.Add(obj);
                    }
                    reader.Close();
                }
                cmd.Dispose();
            }
            return result;
        }
    }
}
