/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 01 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;

namespace Common
{
    public class SqlQueryBuilder<T> : QueryBuilderBase<T>, IQueryBuilder<T> where T:class, new()
    {
        

        public SqlQueryBuilder()
        {

            DBConnString = System.Configuration.ConfigurationManager.ConnectionStrings["SqlDB"].ConnectionString;
        }

        public SqlQueryBuilder(IDbConnection connection, IDbTransaction transaction) : base(connection, transaction)
        {

        }

        public override IDbConnection GetConnection(string str)
        {
            return new SqlConnection(str);
        }

        public override IDbCommand GetCommand(string sql, IDbConnection conn)
        {
            return new SqlCommand(sql, (SqlConnection) conn);
        }

        public override IDataAdapter GetAdapter(IDbCommand com)
        {
            return new SqlDataAdapter((SqlCommand)com);
        }

        public override string[] GetColumnsType(Type type, string prefix, bool includedbgen = true)
        {
            List<string> columns = new List<string>();
            PropertyInfo[] temp = type.GetProperties();

            foreach (var t in temp)
            {
                Attribute ts = t.GetCustomAttribute(typeof(System.Data.Linq.Mapping.ColumnAttribute), true);
                if (ts == null) continue;
                if (((ColumnAttribute)ts).IsDbGenerated && includedbgen == false) continue;
                string tt = "NVARCHAR(MAX)";
                if (t.PropertyType == typeof(int))
                    tt = "INT";
                if (t.PropertyType == typeof(byte[]))
                    tt = "VARBINARY(MAX)";
                if (t.PropertyType == typeof(long))
                    tt = "BIGINT";
                if (t.PropertyType == typeof(float))
                    tt = "DOUBLE";
                if (t.PropertyType == typeof(decimal))
                    tt = "DECIMAL";
                if (t.PropertyType == typeof(bool))
                    tt = "BIT";
                if (t.PropertyType == typeof(DateTime))
                    tt = "DATETIME";
                if (t.PropertyType == typeof(char))
                    tt = "NCHAR(1)";
                columns.Add(prefix + "[" + (((ColumnAttribute)ts).Name ?? t.Name) + "] " + tt);
            }
            if (IsNewPK(type, prefix, includedbgen))
            {
                columns.Remove(prefix + "[Id]");
            }
            return columns.ToArray();
        }

        public override IDataParameter GetSqlParam(string propertyname, T obj)
        {
            IDataParameter param = new SqlParameter();
            param.Value = Convert.DBNull;
            param.ParameterName = "@" + propertyname;
            param.DbType = DbType.String;
            ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.NVarChar;
            
            PropertyInfo pi = obj.GetType().GetProperty(propertyname);
            if (pi == null) return param;

            Type t = pi.PropertyType;
            if (t == typeof(int))
            {
                param.DbType = DbType.Int32;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Int;
                param.Value = MyReflection.GetValue<int>(obj, propertyname);
            }
			else if (t == typeof(byte[]))
            {
                param.DbType = DbType.Binary;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.VarBinary;
                param.Value = MyReflection.GetValue<byte[]>(obj, propertyname);
            }
            else if (t == typeof(double))
            {
                param.DbType = DbType.Double;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Float;
                param.Value = MyReflection.GetValue<float>(obj, propertyname);
            }
            else if (t == typeof(float))
            {
                param.DbType = DbType.Decimal;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Decimal;
                param.Value = MyReflection.GetValue<decimal>(obj, propertyname);
            }
            else if (t == typeof(decimal))
            {
                param.DbType = DbType.Decimal;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Decimal;
                param.Value = MyReflection.GetValue<decimal>(obj, propertyname);
            }
            else if (t == typeof(long))
            {
                param.DbType = DbType.Int64;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.BigInt;
                param.Value = MyReflection.GetValue<long>(obj, propertyname);
            }
            else if (t == typeof(string))
            {
                param.DbType = DbType.String;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.NVarChar;
                param.Value = MyReflection.GetValue<string>(obj, propertyname) ?? Convert.DBNull;
            }
            else if (t == typeof(bool))
            {
                param.DbType = DbType.Byte;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Bit;
                param.Value = MyReflection.GetValue<bool>(obj, propertyname);
            }
            else if (t == typeof(DateTime))
            {
                param.DbType = DbType.DateTime;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.DateTime;
                param.Value = MyReflection.GetValue<DateTime>(obj, propertyname);
            }
            else if (t == typeof(bool?))
            {
                param.DbType = DbType.Byte;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.Bit;
                param.Value = MyReflection.GetValue<object>(obj, propertyname) ?? Convert.DBNull;
            }
            else if (t == typeof(DateTime?))
            {
                param.DbType = DbType.DateTime;
                ((SqlParameter)param).SqlDbType = System.Data.SqlDbType.DateTime;
                param.Value = MyReflection.GetValue<object>(obj, propertyname) ?? Convert.DBNull;
            }

            return param;
        }

        public List<T> GetAll(string sort = null, JoinSearch js = null)
        {
            string select = string.Empty;
            string inner = string.Empty;
            if (js != null)
            {
                select = ", " + js.GetSelectColumns();
                inner = js.GetInner();
            }
            Type type = typeof(T);
            string col = ListColumn(type, "a.", false, true, true);
            if (string.IsNullOrEmpty(col)) col = "*";
            string sql = "SELECT " + col + " "  + select + " FROM [" + GetTableName(type) + "] a " + inner;

            if (string.IsNullOrEmpty(sort) == false)
                sql += " ORDER BY " + sort;
            return ExecuteList(sql, type, new List<IDataParameter>(), true);
        }

        public string GetColumnWithPrefix(string col, JoinSearch js)
        {
            if (js == null) return col;
            if (col.IndexOf(".") >= 0) return col;

            string prefix = "a";
            int i = 1;
            foreach (string str in js.ColumnList)
            {
                string[] cols = str.Split(new char[] { ',' });
                foreach(string c in cols)
                {
                    if (col.ToLower().Trim() == c.ToLower().Trim())
                        return "a" + i.ToString() + ".[" + col + "]";
                }
                i++;
            }
            return prefix + ".[" + col + "]";
        }

        public T Get(string Id, JoinSearch js = null)
        {
            List<IDataParameter> prms = new List<IDataParameter>();

            string inner = string.Empty;
            string select = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = "," + js.GetSelectColumns();
            }

            Type type = typeof(T);
            string col = ListColumn(type, "a.", false, true, true);
            if (string.IsNullOrEmpty(col)) col = "*";
            string[] cols = null;
            string[] primaryKeys = GetPKColumnsArray(typeof(T), "a.", false, false, false, out cols);

            if (primaryKeys == null || primaryKeys.Length == 0)
                prms.Add(new SqlParameter("@Id", Id ?? Convert.DBNull));
            else
                prms.Add(new SqlParameter("@" + cols[0], Id ?? Convert.DBNull));
            string sql = "SELECT " + col + " " + select + " FROM [" + GetTableName(type) + "] a " + inner + " WHERE " + primaryKeys[0] + "=@" + cols[0];

            return ExecuteSingle(sql, type, prms, true);
        }

        public long Count()
        {
            Type type = typeof(T).GetType();
            string sql = "SELECT COUNT(*) AS RowCount FROM [" + GetTableName(type) + "]";

            SqlQueryBuilder<CountRow> dal = new SqlQueryBuilder<CountRow>();
            CountRow obj = dal.ExecuteSingle(sql, type, new List<IDataParameter>());
            return obj.RowCount;
        }

        public T Get(T obj, JoinSearch js = null)
        {
            List<IDataParameter> prms = new List<IDataParameter>();

            string inner = string.Empty;
            string select = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = "," + js.GetSelectColumns();
            }

            Type type = obj.GetType();
            string pk = FillPKColumn(type, obj, prms, "a.", " AND ");
            string col = ListColumn(type, "a.", false, true, true);
            if (string.IsNullOrEmpty(col)) col = "*";
            string sql = "SELECT " + col + " " + select + " FROM [" + GetTableName(type) + "] a " + inner +  " WHERE " + pk;

            return ExecuteSingle(sql, type, prms, true);
        }

        public T GetByMany(string type, string[] column, object[] values, JoinSearch js = null)
        {
            string inner = string.Empty;
            string select = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = "," + js.GetSelectColumns();
            }

            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                if (values[i] == Convert.DBNull || values[i] == null)
                    columndouble.Add(GetColumnWithPrefix(col, js) + " IS NULL OR " + "a." + col + "=''");
                else
                    columndouble.Add(GetColumnWithPrefix(col, js) + " = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "SELECT " + ListColumn(typeof(T), "a.", false, true, true) + " " + select + " FROM [" + GetTableName(typeof(T)) + "] a " + inner + " WHERE " + pk;

            return ExecuteSingle(sql, typeof(T), prms, true);
        }

        public long CountByMany(string type, string[] column, object[] values, JoinSearch js = null)
        {
            string inner = string.Empty;
            string select = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = "," + js.GetSelectColumns();
            }

            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                if (values[i] == Convert.DBNull || values[i] == null)
                    columndouble.Add(GetColumnWithPrefix(col, js) + " IS NULL OR " + "a." + col + "=''");
                else
                    columndouble.Add(GetColumnWithPrefix(col, js) + " = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "SELECT COUNT(*) AS CNT FROM [" + GetTableName(typeof(T)) + "] a " + inner + " WHERE " + pk;

            DataSet ds = ExecuteDataSet(sql, typeof(T), prms);
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0 || ds.Tables[0].Columns.Count == 0 || ds.Tables[0].Rows[0][0] == Convert.DBNull) return 0;
            return (long)ds.Tables[0].Rows[0][0];
        }

        public List<T> GetManyByMany(string type, string[] column, object[] values, JoinSearch js = null) 
        {
            string inner = string.Empty;
            string select = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = "," + js.GetSelectColumns();
            }

            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                if (values[i] == Convert.DBNull || values[i] == null)
                    columndouble.Add(GetColumnWithPrefix(col, js) + " IS NULL OR " + col + "=''");
                else
                    columndouble.Add(GetColumnWithPrefix(col, js) + " = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "SELECT " + ListColumn(typeof(T), "a.", false, true, true) + " " + select + " FROM [" + GetTableName(typeof(T)) + "] a " + inner + " WHERE " + pk;

            return ExecuteList(sql, typeof(T), prms, true);
        }

        public int DeleteByMany(string type, string[] column, object[] values) 
        {
            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                if (values[i] == Convert.DBNull || values[i] == null)
                    columndouble.Add(col + " IS NULL OR " + col + "=''");
                else
                    columndouble.Add(col + " = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "DELETE FROM [" + GetTableName(typeof(T)) + "] WHERE " + pk;

            return ExecuteNonQuery(sql, typeof(T), prms);
        }

        public int DeleteByManyTransaction(string type, string[] column, object[] values)
        {
            List<IDataParameter> prms = new List<IDataParameter>();
            List<string> columndouble = new List<string>();
            for (int i = 0; i < column.Length; i++)
            {
                string col = column[i];
                if (values[i] == Convert.DBNull || values[i] == null)
                    columndouble.Add("[" + col + "] IS NULL");
                else
                    columndouble.Add("[" + col + "] = @" + col);
                prms.Add(new SqlParameter("@" + col, values[i]));
            }
            string pk = string.Join(" " + type + " ", columndouble.ToArray());
            string sql = "DELETE FROM [" + GetTableName(typeof(T)) + "] WHERE " + pk;

            return ExecuteNonQueryTransaction(sql, typeof(T), prms);
        }

        public List<T> GetPage(T obj, int page, string sort, JoinSearch js = null, string otherCriteria = "") 
        {
            string where = string.Empty;

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;
            List<IDataParameter> prms = new List<IDataParameter>();

            string inner = string.Empty;
            string select = string.Empty;
            string joincolumn = string.Empty;
            string select2 = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = ", " + js.GetSelectColumns();
                select2 = ", " + js.GetSelectColumns2();
                string declare = js.GetDeclareColumns(typeof(T));
                if (string.IsNullOrEmpty(declare) == false) joincolumn = ", " + declare;
            }

            Type type = obj.GetType();
            string[] sps = sort.Split(new char[] { ',' });
            List<string> ss = new List<string>();
            foreach(string s in sps)
            {
                if (s.IndexOf(".") >= 0 || s.IndexOf("ISNULL", StringComparison.CurrentCultureIgnoreCase) >= 0) ss.Add(s);
                else ss.Add("a." + s);
            }
            string sql2 = @"SELECT * FROM
(
SELECT ROW_NUMBER() OVER (ORDER BY " + string.Join(", ", ss.ToArray()) + @") ROWID, a.*" + select + @"
FROM [" + GetTableName(type) + @"] a
" + inner + @"
" + where + @"
) as tb
WHERE tb.ROWID BETWEEN (((@PageIndex - 1) * @PageSize) + 1) AND @PageIndex * @PageSize
ORDER BY " + sort;

            prms.Add(new SqlParameter("@PageIndex", page));
            prms.Add(new SqlParameter("@PageSize", 30));

            return ExecuteList(sql2, type, prms, true);
        }

        public long CountPage(T obj, string sort, JoinSearch js = null, string otherCriteria = "")
        {
            string where = string.Empty;

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;

            string inner = string.Empty;
            string select = string.Empty;
            string joincolumn = string.Empty;
            string select2 = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = ", " + js.GetSelectColumns();
                select2 = ", " + js.GetSelectColumns2();
                string declare = js.GetDeclareColumns(typeof(T));
                if (string.IsNullOrEmpty(declare) == false) joincolumn = ", " + declare;
            }

            string[] sps = sort.Split(new char[] { ',' });
            List<string> ss = new List<string>();
            foreach (string s in sps)
            {
                if (s.IndexOf(".") >= 0 || s.IndexOf("ISNULL", StringComparison.CurrentCultureIgnoreCase) >= 0) ss.Add(s);
                else ss.Add("a." + s);
            }

            List<IDataParameter> prms = new List<IDataParameter>();
            string sql2 = @"SELECT COUNT(*) AS [RowCount] FROM
(
SELECT ROW_NUMBER() OVER (ORDER BY " + string.Join(", ", ss.ToArray()) + @") ROWID, a.* " + select + @"
FROM [" + GetTableName(typeof(T)) + @"] a
" + inner + @"
" + where + @"
) as tb";

            SqlQueryBuilder<CountRow> dal = new SqlQueryBuilder<CountRow>();
            CountRow obj2 = dal.ExecuteSingle(sql2, typeof(CountRow), prms, true);
            return obj2.RowCount;
        }

        public bool ContainsAs(List<string> counts, string str)
        {
            foreach(string c in counts)
            {
                string temp = c;
                if (temp.IndexOf(" ") > 0)
                {
                    temp = temp.Substring(temp.LastIndexOf(" ") + 1);
                }
                if (temp == str) return true;
            }
            return false;
        }

        public string GetThisColumnAlias(Dictionary<string,string> counts, string str, bool withAs)
        {
            foreach (string c in counts.Keys.ToList())
            {
                string temp = c;
                if (temp.IndexOf(" ") > 0)
                {
                    temp = temp.Substring(temp.LastIndexOf(" ") + 1);
                }
                if (temp == str)
                {
                    if (counts[c].Contains(" AS "))
                    {
                        string strx = counts[c];
                        string[] strs = strx.Split(new string[] { " AS " }, StringSplitOptions.None);
                        return strs[0];
                    }
                    return counts[c];
                };
            }
            return string.Empty;
        }

        public string GetSPSearchA(Type t, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, bool isdate = false)
        {
            List<string> searches = new List<string>();
            if (searchColumns != null) searches = searchColumns.ToList();

            List<string> exactList = new List<string>();
            if (exactColumns != null) exactList = exactColumns.ToList();

            PropertyInfo[] cols = t.GetProperties();
            List<PKColumn> pkcols = GetPKColumns(t, "", false, false, false).ToList();
            string result = string.Empty;
            
            foreach (var pi in cols)
            {
                if (pi.GetCustomAttribute(typeof(ColumnAttribute)) == null && 
                    pi.GetCustomAttribute(typeof(ReadOnlyColumnAttribute)) == null) continue;

                Attribute cax = null;

                ColumnAttribute ca = pi.GetCustomAttribute(typeof(ColumnAttribute)) as ColumnAttribute;
                ReadOnlyColumnAttribute rca = pi.GetCustomAttribute(typeof(ReadOnlyColumnAttribute)) as ReadOnlyColumnAttribute;

                string name = ca != null ? ca.Name : "";
                if (string.IsNullOrEmpty(name)) name = rca != null ? rca.Name : "";
                if (string.IsNullOrEmpty(name)) name = pi.Name;

                if (js != null && js.ColumnAlias != null)
                {
                    if (rca != null && ContainsAs(js.ColumnAlias.Keys.ToList(), name) == false) continue;
                }
                else
                {
                    if (rca != null) continue;
                }
                if (ca != null && searches.Count > 0 && searches.Contains(name) == false) continue;
                if (ca != null && name == "Id") continue;

                cax = ca;
                if (cax == null) cax = rca;

                Dictionary<string, string> caa = new Dictionary<string, string>();
                if (js != null) caa = js.ColumnAlias;

                string col = "a.[" + name + "]";
                if (rca != null) col = string.IsNullOrEmpty(GetThisColumnAlias(caa, name, false)) ? col : GetThisColumnAlias(caa, name, false); 
                if (col.IndexOf(" AS ", StringComparison.CurrentCultureIgnoreCase) > 0)
                {
                    col = col.Substring(0, col.IndexOf(" AS ", StringComparison.CurrentCultureIgnoreCase));
                }

                Type tp = pi.PropertyType;
                if (isdate && tp != typeof(DateTime)) continue;
                if (exactList.Contains(name) || (tp == typeof(DateTime)))
                {
                    if (tp == typeof(int) || tp == typeof(double) || tp == typeof(decimal) || tp == typeof(long))
                    {
                        result += "CAST(" + col + " AS VARCHAR) LIKE '%' + @SearchText + '%' OR ";
                    }
                    else if (tp == typeof(string))
                    {
                        result += col + " LIKE '%' + @SearchText + '%' OR ";
                    }
                    else if (tp == typeof(DateTime))
                    {
                        result += "(CONVERT(CHAR(20), " + col + ", 113) LIKE '%' + @SearchText + '%') OR ";
                    }
                    continue;
                }

                if (tp == typeof(int) || tp == typeof(double) || tp == typeof(decimal) || tp == typeof(long))
                {
                    result += "CAST(" + col + " AS VARCHAR) LIKE b.val OR ";
                }
                else if (tp == typeof(string))
                {
                    result += "" + col + " LIKE b.val OR ";
                }
                else if (tp == typeof(DateTime))
                {
                    result += "(CONVERT(CHAR(50), " + col + ", 113) LIKE b.val) OR ";
                }
            }
            if (result.Length > 0) result = result.Substring(0, result.Length - 4);
            if (result.Length > 0) result = " JOIN dbo.Split(@MakeSearchTextLike,',') b ON " + result + "";
            return result;
        }

        public string GetSPSearchB(Type t, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, bool isdate = false)
        {
            List<string> searches = new List<string>();
            if (searchColumns != null) searches = searchColumns.ToList();

            List<string> exactList = new List<string>();
            if (exactColumns != null) exactList = exactColumns.ToList();

            PropertyInfo[] cols = t.GetProperties();
            string result = string.Empty;
            foreach (var pi in cols)
            {
                if (pi.GetCustomAttribute(typeof(ColumnAttribute)) == null &&
                    pi.GetCustomAttribute(typeof(ReadOnlyColumnAttribute)) == null) continue;

                Attribute cax = null;

                ColumnAttribute ca = pi.GetCustomAttribute(typeof(ColumnAttribute)) as ColumnAttribute;
                ReadOnlyColumnAttribute rca = pi.GetCustomAttribute(typeof(ReadOnlyColumnAttribute)) as ReadOnlyColumnAttribute;

                string name = ca != null ? ca.Name : "";
                if (string.IsNullOrEmpty(name)) name = rca != null ? rca.Name : "";
                if (string.IsNullOrEmpty(name)) name = pi.Name;

                if (js != null && js.ColumnAlias != null)
                {
                    if (rca != null && ContainsAs(js.ColumnAlias.Keys.ToList(), name) == false) continue;
                }
                else
                {
                    if (rca != null) continue;
                }
                if (ca != null && searches.Count > 0 && searches.Contains(name) == false) continue;
                if (ca != null && name == "Id") continue;

                cax = ca;
                if (cax == null) cax = rca;

                string col = "a.[" + name + "]";
                Dictionary<string, string> caa = new Dictionary<string, string>();
                if (js != null) caa = js.ColumnAlias;
                if (js != null)
                {
                    if (rca != null) col = string.IsNullOrEmpty(GetThisColumnAlias(caa, name, false)) ? col : GetThisColumnAlias(caa, name, false);
                }
                if (col.IndexOf(" AS ", StringComparison.CurrentCultureIgnoreCase) > 0)
                {
                    col = col.Substring(0, col.IndexOf(" AS ", StringComparison.CurrentCultureIgnoreCase));
                }

                var nettype = pi.PropertyType;
                if (isdate && nettype != typeof(DateTime)) continue;
                //if (exactList.Contains(name) || (nettype == typeof(DateTime)))
                //{
                //    if (nettype == typeof(int) || nettype == typeof(double) || nettype == typeof(decimal) || nettype == typeof(long))
                //    {
                //        result += "CAST(" + col + " AS VARCHAR) LIKE '%' + @SearchText + '%' OR ";
                //    }
                //    else if (nettype == typeof(string))
                //    {
                //        result += "" + col + " LIKE '%' + @SearchText + '%' OR ";
                //    }
                //    else if (nettype == typeof(DateTime))
                //    {
                //        result += "(CONVERT(CHAR(20), " + col + ", 113) LIKE '%' + @SearchText + '%') OR ";
                //    }
                //    continue;
                //}

                if (nettype == typeof(int) || nettype == typeof(double) || nettype == typeof(decimal) || nettype == typeof(long))
                {
                    result += "CAST(" + col + " AS VARCHAR) LIKE b.val OR ";
                }
                else if (nettype == typeof(string))
                {
                    result += "" + col + " LIKE b.val OR ";
                }
                else if (nettype == typeof(DateTime))
                {
                    result += "(CONVERT(CHAR(20), " + col + ", 113) LIKE b.val) OR ";
                }
            }
            if (result.Length > 0) result = result.Substring(0, result.Length - 4);
            if (result.Length > 0) result = " JOIN dbo.Split(@MakeSearchTextLike,',') b ON " + result + "";
            return result;
        }

        //      public List<T> GetPageSearch(T obj, string searchtext, int page, string sort, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, string otherCriteria = "")
        //      {
        //          string where = string.Empty;
        //          bool isdate = false;
        //          DateTime dt = DateTime.Now;
        //          if (DateTime.TryParse(searchtext, out dt))
        //          {
        //              isdate = true;
        //              searchtext = dt.ToString("dd MMM yyyy HH:mm:ss");
        //          }

        //          if (string.IsNullOrEmpty(otherCriteria) == false)
        //              where = "WHERE " + otherCriteria;

        //          string over = string.Empty;
        //          if (string.IsNullOrEmpty(sort) == false) over = ", " + MakeOver("b.", sort, js);
        //          List<IDataParameter> prms = new List<IDataParameter>();

        //          Type type = obj.GetType();

        //          string inner = string.Empty;
        //          string select = string.Empty;
        //          string joincolumn = string.Empty;
        //          string select2 = string.Empty;
        //          if (js != null)
        //          {
        //              inner = js.GetInner();
        //              select = ", " + js.GetSelectColumns();
        //              select2 =  ", " + js.GetSelectColumns2();
        //              string declare = js.GetDeclareColumns(typeof(T));
        //              if (string.IsNullOrEmpty(declare) == false) joincolumn = ", " + declare;
        //          }

        //          string sql2 = @" 
        //      DECLARE @MakeSearchTextLike AS NVARCHAR(1000)
        //      SET @MakeSearchTextLike = dbo.MakeSearchTextLike(@SearchText)
        //      DECLARE @RESULT TABLE (ROWID BIGINT, CNT BIGINT, " + ListColumnType(type, "", true) + joincolumn + @")
        //      INSERT INTO @RESULT
        //      SELECT * FROM
        //      (
        //	SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC " + over + @") ROWID, a.CNT, b.* " + select + @" FROM 
        //	(
        //		SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT 
        //              FROM dbo.[" + GetTableName(type) + @"] a 
        //              " + inner + @"
        //              " + GetSPSearchA(type, js, searchColumns, exactColumns, isdate) + @"
        //              " + where + @"
        //		GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
        //	) a 
        //          LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
        //          " + inner.Replace("a.", "b.") + @"
        //      ) tb
        //WHERE tb.ROWID BETWEEN (((@PageIndex - 1) * @PageSize) + 1) AND @PageIndex * @PageSize
        //ORDER BY tb.CNT DESC

        //DECLARE @TotalRow BIGINT
        //SELECT @TotalRow=COUNT(*) FROM @RESULT
        //IF ISNULL(@TotalRow, 0) = 0
        //BEGIN
        //          INSERT INTO @RESULT
        //          SELECT * FROM
        //          (
        //	    SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC" + over + @") ROWID, a.CNT, b.* " + select + @" FROM 
        //	    (
        //		    SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT 
        //                  FROM dbo.[" + GetTableName(type) + @"] a
        //                  " + inner + @"
        //                  " + GetSPSearchB(type, js, searchColumns, exactColumns, isdate) + @"
        //                  " + where + @"
        //		    GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
        //	    ) a 
        //              LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
        //              " + inner.Replace("a.", "b.") + @"
        //          ) tb
        //    WHERE tb.ROWID BETWEEN (((@PageIndex - 1) * @PageSize) + 1) AND @PageIndex * @PageSize
        //ORDER BY tb.CNT DESC
        //      END
        //      SELECT * FROM @RESULT";

        //          if (string.IsNullOrEmpty(sort) == false)
        //              sql2 += " ORDER BY " + sort;

        //          prms.Add(new SqlParameter("@SearchText", searchtext));
        //          prms.Add(new SqlParameter("@PageIndex", page));
        //          prms.Add(new SqlParameter("@PageSize", WebConfigure.PageSize));

        //          return ExecuteList(sql2, type, prms, true);
        //      }

        public List<T> GetPageSearch(T obj, string searchtext, int page, string sort, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, string otherCriteria = "")
        {
            string where = string.Empty;
            bool isdate = false;
            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(searchtext, out dt))
            {
                isdate = true;
                searchtext = dt.ToString("dd MMM yyyy HH:mm:ss");
            }

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;

            string over = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) over = ", " + MakeOver("b.", sort, js);
            string overaa = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) overaa = ", " + MakeOver("aa.", sort, js);
            string overbb = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) overbb = ", " + MakeOver("bb.", sort, js);
            string overtbb = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) overtbb = ", " + MakeOver("tbb.", sort, js);

            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = obj.GetType();

            string inner = string.Empty;
            string select = string.Empty;
            string joincolumn = string.Empty;
            string select2 = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = ", " + js.GetSelectColumns();
                select2 = ", " + js.GetSelectColumns2();
                string declare = js.GetDeclareColumns(typeof(T));
                if (string.IsNullOrEmpty(declare) == false) joincolumn = ", " + declare;
            }

            string sql2 = @" 
        DECLARE @MakeSearchTextLike AS NVARCHAR(1000)
        SET @MakeSearchTextLike = dbo.MakeSearchTextLike(@SearchText)

        SELECT * " + select + @" 
		FROM
		(
			SELECT " + ListColumn(type, "", false, true, true) + @", MAX(CNT) AS CNT
			FROM
			(
				SELECT ROW_NUMBER() OVER (ORDER BY aa.CNT DESC " + overaa + @") ROWID, aa.* FROM 
				(
					SELECT cccc.*, bbbb.CNT
					FROM
					(
						SELECT " + ListPKColumn(type, "aaa.", true, true, false) + @", MAX(CNT) AS CNT
						FROM
						(
							SELECT DISTINCT a.*, 999999999999 AS CNT
							FROM " + GetTableName(type) + @" a
                            " + inner + @"
							" + GetSPSearchA(type, js, searchColumns, exactColumns, isdate) + @"
                            " + where + @"
				
							UNION
				
							SELECT bb.*, aa.CNT FROM 
							(
								SELECT " + ListPKColumn(type, "a.", true, true, false) + @", SUM(LEN(b.val)) AS CNT 
								FROM " + GetTableName(type) + @" a 
                                " + inner + @"
								" + GetSPSearchB(type, js, searchColumns, exactColumns, isdate) + @"
                                " + where + @"
								GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
							) aa
							INNER JOIN " + GetTableName(type) + @" bb ON " + JoinPKAB(type, " AND ", "aa", "bb") + @"
						) aaa
						GROUP BY " + ListPKColumn(type, "aaa.", true, true, false) + @"
					) bbbb
					INNER JOIN " + GetTableName(type) + @" cccc  ON " + JoinPKAB(type, " AND ", "bbbb", "cccc") + @"
				) aa
			) tb
			WHERE tb.ROWID BETWEEN (((@PageIndex - 1) * @PageSize) + 1) AND @PageIndex * @PageSize
			GROUP BY ROWID, " + ListColumn(type, "", false, true, true) + @"
		) tbb
        " + inner.Replace("a.", "tbb.") + @"
		ORDER BY tbb.CNT DESC " + overtbb + @"";

            prms.Add(new SqlParameter("@SearchText", searchtext));
            prms.Add(new SqlParameter("@PageIndex", page));
            prms.Add(new SqlParameter("@PageSize", 30));

            return ExecuteList(sql2, type, prms, true);
        }

        //      public long CountPageSearch(T obj, string searchtext, string sort, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, string otherCriteria = "") 
        //      {
        //          string where = string.Empty;
        //          bool isdate = false;
        //          DateTime dt = DateTime.Now;
        //          if (DateTime.TryParse(searchtext, out dt)) isdate = true;

        //          if (string.IsNullOrEmpty(otherCriteria) == false)
        //              where = "WHERE " + otherCriteria;

        //          List<IDataParameter> prms = new List<IDataParameter>();

        //          string inner = string.Empty;
        //          string select = string.Empty;
        //          string joincolumn = string.Empty;
        //          string select2 = string.Empty;
        //          if (js != null)
        //          {
        //              inner = js.GetInner();
        //              select = ", " + js.GetSelectColumns();
        //              if (string.IsNullOrEmpty(js.GetSelectColumns2()) == false)
        //                  select2 = ", " + js.GetSelectColumns2();
        //              if (string.IsNullOrEmpty(js.GetDeclareColumns(typeof(T))) == false)
        //                  joincolumn = ", " + js.GetDeclareColumns(typeof(T));
        //          }

        //          Type type = obj.GetType();
        //          string sql2 = @" 
        //      DECLARE @MakeSearchTextLike AS NVARCHAR(1000)
        //      SET @MakeSearchTextLike = dbo.MakeSearchTextLike(@SearchText)
        //      DECLARE @RESULT TABLE (ROWID BIGINT, CNT BIGINT, " + ListColumnType(type, "", true) + joincolumn + @")
        //      INSERT INTO @RESULT
        //      SELECT * FROM
        //      (
        //	SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC) ROWID, a.CNT, b.* " + select + @" FROM 
        //	(
        //		SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT 
        //              FROM dbo.[" + GetTableName(type) + @"] a
        //              " + inner + @"
        //              " + GetSPSearchA(type, js, searchColumns, exactColumns, isdate) + @"
        //		" + where + @"
        //              GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
        //	) a 
        //          LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
        //          " + inner.Replace("a.", "b.") + @"
        //      ) tb
        //ORDER BY tb.CNT DESC

        //DECLARE @TotalRow BIGINT
        //SELECT @TotalRow=COUNT(*) FROM @RESULT
        //IF ISNULL(@TotalRow, 0) = 0
        //BEGIN
        //          INSERT INTO @RESULT
        //          SELECT * FROM
        //          (
        //	    SELECT ROW_NUMBER() OVER (ORDER BY a.CNT DESC) ROWID, a.CNT, b.* " + select + @" FROM 
        //	    (
        //		    SELECT " + ListPKColumn(type, "a.", true, true, false) + @", COUNT(*) AS CNT 
        //                  FROM dbo.[" + GetTableName(type) + @"] a 
        //                  " + inner + @"
        //                  " + GetSPSearchB(type, js, searchColumns, exactColumns, isdate) + @"
        //                  " + where + @"
        //		    GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
        //	    ) a LEFT JOIN dbo." + GetTableName(type) + @" b  ON " + JoinPKAB(type, " AND ") + @"
        //              " + inner.Replace("a.", "b.") + @"
        //          ) tb
        //ORDER BY tb.CNT DESC
        //      END
        //      SELECT COUNT(*) AS [RowCount] FROM @RESULT";

        //          prms.Add(new SqlParameter("@SearchText", searchtext));

        //          DataSet obj2 = ExecuteDataSet(sql2, typeof(T), prms);
        //          if (obj2.Tables == null || obj2.Tables.Count <= 0 || obj2.Tables[0].Rows.Count == 0 || obj2.Tables[0].Columns.Count == 0)
        //              return 0;
        //          int temp = 0;
        //          int.TryParse(obj2.Tables[0].Rows[0][0].ToString(), out temp);
        //          return temp;
        //      }

        public long CountPageSearch(T obj, string searchtext, string sort, JoinSearch js = null, string[] searchColumns = null, string[] exactColumns = null, string otherCriteria = "")
        {
            string where = string.Empty;
            bool isdate = false;
            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(searchtext, out dt)) isdate = true;

            if (string.IsNullOrEmpty(otherCriteria) == false)
                where = "WHERE " + otherCriteria;

            List<IDataParameter> prms = new List<IDataParameter>();

            string inner = string.Empty;
            string select = string.Empty;
            string joincolumn = string.Empty;
            string select2 = string.Empty;
            if (js != null)
            {
                inner = js.GetInner();
                select = ", " + js.GetSelectColumns();
                if (string.IsNullOrEmpty(js.GetSelectColumns2()) == false)
                    select2 = ", " + js.GetSelectColumns2();
                if (string.IsNullOrEmpty(js.GetDeclareColumns(typeof(T))) == false)
                    joincolumn = ", " + js.GetDeclareColumns(typeof(T));
            }

            string overaa = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) overaa = ", " + MakeOver("aa.", sort, js);
            string overbb = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) overbb = ", " + MakeOver("bb.", sort, js);
            string overtbb = string.Empty;
            if (string.IsNullOrEmpty(sort) == false) overtbb = ", " + MakeOver("tbb.", sort, js);


            Type type = obj.GetType();
            string sql2 = @" DECLARE @MakeSearchTextLike AS NVARCHAR(1000)
        SET @MakeSearchTextLike = dbo.MakeSearchTextLike(@SearchText)

        SELECT COUNT(*) AS CNT
		FROM
		(
			SELECT " + ListColumn(type, "", false, true, true) + @", MAX(CNT) AS CNT
			FROM
			(
				SELECT ROW_NUMBER() OVER (ORDER BY aa.CNT DESC " + overaa + @") ROWID, aa.* FROM 
				(
					SELECT cccc.*, bbbb.CNT
					FROM
					(
						SELECT " + ListPKColumn(type, "aaa.", true, true, false) + @", MAX(CNT) AS CNT
						FROM
						(
							SELECT DISTINCT a.*, 999999999999 AS CNT
							FROM " + GetTableName(type) + @" a
                            " + inner + @"
							" + GetSPSearchA(type, js, searchColumns, exactColumns, isdate) + @"
                            " + where + @"
				
							UNION
				
							SELECT bb.*, aa.CNT FROM 
							(
								SELECT " + ListPKColumn(type, "a.", true, true, false) + @", SUM(LEN(b.val)) AS CNT 
								FROM " + GetTableName(type) + @" a 
                                " + inner + @"
								" + GetSPSearchB(type, js, searchColumns, exactColumns, isdate) + @"
                                " + where + @"
								GROUP BY " + ListPKColumn(type, "a.", true, true, false) + @"
							) aa
							INNER JOIN " + GetTableName(type) + @" bb ON " + JoinPKAB(type, " AND ", "aa", "bb") + @"
						) aaa
						GROUP BY " + ListPKColumn(type, "aaa.", true, true, false) + @"
					) bbbb
					INNER JOIN " + GetTableName(type) + @" cccc  ON " + JoinPKAB(type, " AND ", "bbbb", "cccc") + @"
				) aa
			) tb
			GROUP BY ROWID, " + ListColumn(type, "", false, true, true) + @"
		) tbb
        " + inner.Replace("a.", "tbb.");

            prms.Add(new SqlParameter("@SearchText", searchtext));

            DataSet obj2 = ExecuteDataSet(sql2, typeof(T), prms);
            if (obj2.Tables == null || obj2.Tables.Count <= 0 || obj2.Tables[0].Rows.Count == 0 || obj2.Tables[0].Columns.Count == 0)
                return 0;
            int temp = 0;
            int.TryParse(obj2.Tables[0].Rows[0][0].ToString(), out temp);
            return temp;
        }

        public int Delete(T obj)
        {
            QueryTransaction<T> build = new QueryTransaction<T>();
            build.Builder.BeginTransaction();
            try
            {
                List<IDataParameter> prms = new List<IDataParameter>();

                Type type = obj.GetType();
                string pk = FillPKColumn(type, obj, prms, "", " AND ");
                string sql = "DELETE FROM [" + GetTableName(type) + "] WHERE " + pk;

                int ret = build.Builder.ExecuteNonQueryTransaction(sql, type, prms);
                InsertAudit(build.Builder, "", "Delete", null, obj, "");
                build.Builder.CommitTransaction();
                return ret;
            }
            catch (Exception)
            {
                build.Builder.RollbackTransaction();
                throw;
            }
        }

        public int DeleteTransaction(T obj)
        {
            List<IDataParameter> prms = new List<IDataParameter>();

            Type type = obj.GetType();
            string pk = FillPKColumn(type, obj, prms, "", " AND ");
            string sql = "DELETE FROM [" + GetTableName(type) + "] WHERE " + pk;
            InsertAudit(this, "", "Delete", obj, null, "");
            return ExecuteNonQueryTransaction(sql, type, prms);
        }

        public int Insert(T obj)
        {
            object[] ids = PrimaryKeyValues(obj);

            List<PKColumn> PrimaryKeyColumns = GetPKColumns(typeof(T), "", false, false, false);
            if ((ids == null || (ids.Length == 1 && (ids[0] == null || ids[0].ToString() == ""))) & PrimaryKeyColumns != null && PrimaryKeyColumns.Count == 1 && PrimaryKeyColumns[0].PropertyName == "Id")
            {
                ids = new object[] { MongoDB.Bson.ObjectId.GenerateNewId().ToString() };
                MyReflection.SetValue(obj, "Id", ids[0]);
            }

            if (MyReflection.GetValue<DateTime>(obj, "CreateDate").Year <= 2000) MyReflection.SetValue(obj, "CreateDate", DateTime.Now);
            if (string.IsNullOrEmpty(MyReflection.GetValue<string>(obj, "CreateOfficer"))) MyReflection.SetValue(obj, "CreateOfficer", "SYSTEM");

            FormatObject(obj);

            QueryTransaction<T> build = new QueryTransaction<T>();
            build.Builder.BeginTransaction();
            try
            {
                Type type = obj.GetType();

                List<IDataParameter> prms = FillColumnSingle(type, obj, false);
                string sql = "INSERT INTO [" + GetTableName(type) + "] (" + ListColumn(type, "", false, false, true) + ") VALUES (" + ListColumn(type, "@", false, false, false) + ")";

                List<IDataParameter> pks = new List<IDataParameter>();
                FillPKColumn(type, obj, pks, "", "");

                List<string> pk = new List<string>();
                foreach (var sq in pks)
                {
                    pk.Add(sq.ParameterName + "=" + sq.Value.ToString());
                }

                int ret = build.Builder.ExecuteNonQueryTransaction(sql, type, prms);
                InsertAudit(build.Builder, "", "Insert " + string.Join(",", pk.ToArray()), null, obj, "");
                build.Builder.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                build.Builder.RollbackTransaction();
                throw;
            }
        }

        public int InsertTransaction(T obj)
        {
            object[] ids = PrimaryKeyValues(obj);
            List<PKColumn> PrimaryKeyColumns = GetPKColumns(typeof(T), "", false, false, false);
            if ((ids == null || (ids.Length == 1 && (ids[0] == null || ids[0].ToString() == ""))) & PrimaryKeyColumns != null && PrimaryKeyColumns.Count == 1 && PrimaryKeyColumns[0].PropertyName == "Id")
            {
                ids = new object[] { MongoDB.Bson.ObjectId.GenerateNewId().ToString() };
                MyReflection.SetValue(obj, "Id", ids[0]);
            }

            if (MyReflection.GetValue<DateTime>(obj, "CreateDate").Year <= 2000) MyReflection.SetValue(obj, "CreateDate", DateTime.Now);
            if (string.IsNullOrEmpty(MyReflection.GetValue<string>(obj, "CreateOfficer"))) MyReflection.SetValue(obj, "CreateOfficer", "SYSTEM");

            FormatObject(obj);

            Type type = obj.GetType();
            List<IDataParameter> prms = FillColumnSingle(type, obj, false);
            string sql = "INSERT INTO [" + GetTableName(type) + "] (" + ListColumn(type, "", false, false, true) + ") VALUES (" + ListColumn(type, "@", false, false, false) + ")";

            List<IDataParameter> pks = new List<IDataParameter>();
            FillPKColumn(type, obj, pks, "","");

            List<string> pk = new List<string>();
            foreach (var sq in pks)
            {
                pk.Add(sq.ParameterName + "=" + sq.Value.ToString());
            }

            int ret = ExecuteNonQueryTransaction(sql, type, prms);
            InsertAudit(this, "", "Insert", null, obj, "");
            return ret;
        }

        public int Update(T obj)
        {
            FormatObject(obj);
            QueryTransaction<T> build = new QueryTransaction<T>();
            build.Builder.BeginTransaction();
            try
            {
                T oldObj = Get(obj);
                Type type = typeof(T);

                List<IDataParameter> prms = new List<IDataParameter>();
                
                if (MyReflection.GetValue<DateTime>(obj, "UpdateDate").Year <= 2000) MyReflection.SetValue(obj, "UpdateDate", DateTime.Now);
                if (string.IsNullOrEmpty(MyReflection.GetValue<string>(obj, "UpdateOfficer"))) MyReflection.SetValue(obj, "UpdateOfficer", "SYSTEM");

                string where = FillPKColumn(type, obj, prms, "", " AND ");
                string sql = "UPDATE [" + GetTableName(type) + "] SET " + FillColumn(type, obj, prms, false) + " WHERE " + FillPKColumn(type, obj, prms, "", " AND ");
                int ret = build.Builder.ExecuteNonQuery(sql, type, prms);

                InsertAudit(build.Builder, "", "Update", oldObj, obj, "");
                build.Builder.CommitTransaction();
                return ret;
            }
            catch (Exception)
            {
                build.Builder.RollbackTransaction();
                throw;
            }
        }

        public int UpdateTransaction(T obj)
        {
            FormatObject(obj);
            T oldObj = Get(obj);
            Type type = typeof(T);

            List<IDataParameter> prms = new List<IDataParameter>();

            if (MyReflection.GetValue<DateTime>(obj, "UpdateDate").Year <= 2000) MyReflection.SetValue(obj, "UpdateDate", DateTime.Now);
            if (string.IsNullOrEmpty(MyReflection.GetValue<string>(obj, "UpdateOfficer"))) MyReflection.SetValue(obj, "UpdateOfficer", "SYSTEM");

            string where = FillPKColumn(type, obj, prms, "", " AND ");
            string sql = "UPDATE [" + GetTableName(type) + "] SET " + FillColumn(type, obj, prms, false) + " WHERE " + FillPKColumn(type, obj, prms, "", " AND ");
            int ret = ExecuteNonQueryTransaction(sql, type, prms);

            InsertAudit(this, "", "Update", oldObj, obj, "");

            return ret;
        }

        public void InsertAudit(IQueryBuilder<T> qb, string sentence, string commandtype, object old, object newobj, string officername)
        {
			if (ConfigurationManager.AppSettings["UseAudit"] == "N") return;
            if (string.IsNullOrEmpty(officername)) officername = "SYSTEM";
        }
    }
}
