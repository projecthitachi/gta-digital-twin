﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Common
{
    public class HexOps
    {
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {

                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        /// <summary>
        /// Convert a string to hex value
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static string HexEncode(byte[] ba)
        {
            // SOLUTION 1
            //var c = new char[ba.Length * 2];
            //for (var i = 0; i < ba.Length; i++)
            //{
            //  var b = ba[i] >> 4;
            //  c[i * 2] = (char)(55 + b + (((b - 10) >> 31) & -7));
            //  b = ba[i] & 0xF;
            //  c[i * 2 + 1] = (char)(55 + b + (((b - 10) >> 31) & -7));
            //}
            //return new string(c);
            // SOLUTION 2
            var hex = new StringBuilder(ba.Length * 2);
            foreach (var b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        /// <summary>
        /// Converts a hex value to a string
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] HexDecode(string hexString)
        {
            if (hexString == null || (hexString.Length & 1) == 1) return null;
            // SOLUTION 1
            //hexString = hexString.ToUpper();
            //var hexStringLength = hexString.Length;
            //var b = new byte[hexStringLength / 2];
            //for (var i = 0; i < hexStringLength; i += 2)
            //{
            //  var topChar = (hexString[i] > 0x40 ? hexString[i] - 0x37 : hexString[i] - 0x30) << 4;
            //  var bottomChar = hexString[i + 1] > 0x40 ? hexString[i + 1] - 0x37 : hexString[i + 1] - 0x30;
            //  b[i / 2] = Convert.ToByte(topChar + bottomChar);
            //}
            // SOLUTION 2
            var numberChars = hexString.Length;
            var bytes = new byte[numberChars / 2];
            for (var i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
            return bytes;
        }
    }
}
