﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Reflection;

namespace Common
{
    /// <summary>
    /// Summary description for Tools.
    /// </summary>
    public class MyReflection
    {
        public static void SetValue(object obj, string propertyName, object value)
        {
            Type t = obj.GetType();

            PropertyInfo prop = t.GetProperty(propertyName);
            if (prop != null) prop.SetValue(obj, value);
            FieldInfo field = t.GetField(propertyName);
            if (field != null) field.SetValue(obj, value);
        }

        public static T GetValue<T>(object obj, string propertyName)
        {
            object value = null;
            Type t = obj.GetType();
            PropertyInfo prop = t.GetProperty(propertyName);
            if (prop != null) value = prop.GetValue(obj);
            FieldInfo field = t.GetField(propertyName);
            if (field != null) value = field.GetValue(obj);
            return (T) Convert.ChangeType(value, typeof(T));
        }

        public static Type GetType<U>(string propertyName)
        {
            Type t = typeof(U);
            PropertyInfo prop = t.GetProperty(propertyName);
            if (prop != null) return prop.PropertyType;
            FieldInfo field = t.GetField(propertyName);
            if (field != null) return field.FieldType;
            return null;
        }

    }
}
