﻿// has.iot.Components.GlobalFunction
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PluginContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Configuration;
using uPLibrary.Networking.M2Mqtt;

namespace VideoAnalytic.PeopleCounting.Models.System
{
    public class GlobalFunction
    {
        public static Dictionary<string, IPlugin> _Plugins = new Dictionary<string, IPlugin>();
        public static string mqttServer = null;

        public static MqttClient MqttClient = null;

        public static class Constants
        {
            public const string DateFormat = "MM-dd-yyyy";
            public const string DateFormatGrid = "{0:MM-dd-yyyy}";
            public static string Admin = "Administrator";
        }

        public static void TrimNull<T>(T poRecord)
        {
            IEnumerable<PropertyInfo> stringProperties = from p in poRecord.GetType().GetProperties()
                                                         where p.PropertyType == typeof(string)
                                                         select p;
            foreach (PropertyInfo stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(poRecord, null);
                if (currentValue == null)
                {
                    currentValue = "";
                }
                stringProperty.SetValue(poRecord, currentValue.Trim(), null);
            }
        }

        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || (strInput.StartsWith("[") && strInput.EndsWith("]")))
            {
                try
                {
                    JToken obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException)
                {
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public static void MqttListener()
        {
            mqttServer = WebConfigurationManager.AppSettings["server_mqtt"].ToString();
            MqttClient = new MqttClient(mqttServer);
        }
    }
}