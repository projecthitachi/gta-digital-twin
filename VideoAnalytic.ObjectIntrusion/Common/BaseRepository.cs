/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 08 Oct 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcPaging;

namespace Common
{
    public class BaseRepository<T> : IRepository<T> where T : class, new()
    {
        public IRepository<T> Repository = null;

        public string[] _PrimaryKeyColumns = null;

        public BaseRepository()
        {
            SetPrimaryKeyColumns();
        }

        public BaseRepository(IRepository<T> repo)
        {
            Repository = repo;
            SetPrimaryKeyColumns();
        }

        public void SetPrimaryKeyColumns()
        {
            QueryBuilderBase<T> builder = new QueryBuilderBase<T>();
            string[] cols = new string[] { };
            _PrimaryKeyColumns = builder.GetPKColumnsArray(typeof(T), "", true, false, false, out cols);
        }

        public string[] PrimaryKeyColumns
        {
            get { return _PrimaryKeyColumns; }
        }

        public object[] PrimaryKeyValues(T obj)
        {
            List<object> result = new List<object>();
            for (int i = 0; i < PrimaryKeyColumns.Length; i++)
            {
                object item = MyReflection.GetValue<object>(obj, PrimaryKeyColumns[i]);
                result.Add(item);
            }
            return result.ToArray();
        }

        public virtual long ClearDB()
        {
            if (Repository != null) return Repository.ClearDB();
            return 0;
        }

        public virtual List<T> GetAll()
        {
            if (Repository != null) return Repository.GetAll();
            return null;
        }

        public List<T> Aggregate(int id, string[] query, string sortcolumn, string ascdesc)
        {
            if (Repository != null) return Repository.Aggregate(id, query, sortcolumn, ascdesc);
            return null;
        }

        public virtual List<T> FindOr(string match, string[] column, object[] value, string order, string sortby)
        {
            if (Repository != null) return Repository.FindOr(match, column, value, order, sortby);
            return null;
        }

        public virtual List<T> FindOr(int id, string match, string[] column, object[] value, string order, string sortby)
        {
            if (Repository != null) return Repository.FindOr(id, match, column, value, order, sortby);
            return null;
        }

        public virtual List<T> FindAnd(string match, string[] column, object[] value, string order, string sortby)
        {
            if (Repository != null) return Repository.FindAnd(match, column, value, order, sortby);
            return null;
        }

        public virtual List<T> FindAnd(int id, string match, string[] column, object[] value, string order, string sortby)
        {
            if (Repository != null) return Repository.FindAnd(id, match, column, value, order, sortby);
            return null;
        }

        public virtual List<T> Find(int id, string match, string type, string[] columns, object[] values, string order = "", string sortby = "")
        {
            if (Repository != null) return Repository.Find(id, match, type, columns, values, order, sortby);
            return null;
        }

        public virtual long Count()
        {
            if (Repository != null) return Repository.Count();
            return 0;
        }

        public virtual PagedList<T> GetPage(int page)
        {
            if (Repository != null) return Repository.GetPage(page);
            return null;
        }

        public virtual long CountSearch(string searchText)
        {
            if (Repository != null) return Repository.CountSearch(searchText);
            return 0;
        }

        public virtual PagedList<T> GetPageSearch(string searchText, int page)
        {
            if (Repository != null) return Repository.GetPageSearch(searchText, page);
            return null;
        }

        public virtual T Get(string objectid)
        {
            if (Repository != null) return Repository.Get(objectid);
            return default(T);
        }

        public virtual T Get(object objectid)
        {
            if (Repository != null) return Repository.Get(objectid);
            return default(T);
        }

        public virtual long Insert(T d)
        {
            if (Repository != null) return Repository.Insert(d);
            return 0;
        }

        public virtual string InsertReturnID(T d)
        {
            if (Repository != null) return Repository.InsertReturnID(d);
            return string.Empty;
        }

        public virtual long Delete(T d)
        {
            if (Repository != null) return Repository.Delete(d);
            return 0;
        }

        public virtual long Delete(string objectid)
        {
            if (Repository != null) return Repository.Delete(objectid);
            return 0;
        }

        public virtual long Update(T d)
        {
            if (Repository != null) return Repository.Update(d);
            return 0;
        }

        public virtual T GetBy(string column, object value)
        {
            if (Repository != null) return Repository.GetBy(column, value);
            return default(T);
        }

        public virtual T GetByIn(string column, object value, string column2, object value2)
        {
            if (Repository != null) return Repository.GetByIn(column, value,column2, value2);
            return default(T);
        }

        public virtual T GetByMany(string[] column, object[] value)
        {
            if (Repository != null) return Repository.GetByMany(column, value);
            return default(T);
        }

        public virtual List<T> GetManyByMany(string[] column, object[] value)
        {
            if (Repository != null) return Repository.GetManyByMany(column, value);
            return null;
        }

        public virtual List<T> GetManyBy(string column, object value)
        {
            if (Repository != null) return Repository.GetManyBy(column, value);
            return null;
        }

        public virtual long DeleteBy(string column, object value)
        {
            if (Repository != null) return Repository.DeleteBy(column, value);
            return 0;
        }

        public virtual long DeleteByMany(string[] column, object[] value)
        {
            if (Repository != null) return Repository.DeleteByMany(column, value);
            return 0;
        }

        public long CountByMany(string[] column, object[] value)
        {
            return 0;
        }
    }
}
