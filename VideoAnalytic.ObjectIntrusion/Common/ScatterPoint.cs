/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 10 Jan 2017 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ScatterPoint
    {

        public double UTMX = 0;
        public double UTMY = 0;
        public string Timezone;

        public double Longitude = 0;
        public double Latitude = 0;

        public string KalmanStateLatitude = string.Empty;
        public string KalmanStateLongitude = string.Empty;
        public double CEPAccuracy = 0;
        public DateTime CreatedDate = DateTime.MinValue;

        public string UnitID = string.Empty;

        public int Cluster { get; set; }

        public ScatterPoint()
        {

        }

        public ScatterPoint(double x, double y, string timezone)
        {
            UTMX = x;
            UTMY = y;
            Timezone = timezone;
        }

        public bool MyEqual(ScatterPoint sp)
        {
            if (sp.UTMX == UTMX && sp.UTMY == UTMY) return true;
            return false;
        }
    }

    public static class ScatterPointExtend
    {
        public static bool MyContains(this List<ScatterPoint> list, ScatterPoint sp)
        {
            foreach(ScatterPoint l in list)
            {
                if (l.MyEqual(sp)) return true;
            }
            return false;
        }
    }
}
