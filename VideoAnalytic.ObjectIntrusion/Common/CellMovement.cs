﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class CellMovement
    {
        public double RelativeRightX = 0;
        public double RelativeRightY = 0;
        public double RelativeBelowX = 0;
        public double RelativeBelowY = 0;


        public double X00 = 0;
        public double Y00 = 0;

        public double OffsetRightX = 0;
        public double OffsetRightY = 0;
        public double OffsetBottomX = 0;
        public double OffsetBottomY = 0;

        public CellMovement(double relativeRightX, double relativeRightY, double relativeBelowX, double relativeBelowY, double x00, double y00, double offsetRightX, double offsetRightY, double offsetBottomX, double offsetBottomY)
        {
            RelativeRightX = relativeRightX;
            RelativeRightY = relativeRightY;
            RelativeBelowX = relativeBelowX;
            RelativeBelowY = relativeBelowY;

            X00 = x00;
            Y00 = y00;

            OffsetRightX = offsetRightX;
            OffsetRightY = offsetRightY;
            OffsetBottomX = offsetBottomX;
            OffsetBottomY = offsetBottomY;
        }

        public static CellMovement PixelMovement(int x, int y, int cellsizeX, int cellsizeY)
        {
            CellMovement movement = new CellMovement(cellsizeX / 2, 0, 0, cellsizeY / 2, 0, 0, cellsizeX * (x), 0, 0, cellsizeY * (y));
            return movement;
        }

        public CellMovement Copy()
        {
            CellMovement movement = new CellMovement(RelativeRightX, RelativeRightY, RelativeBelowX, RelativeBelowY, X00, Y00, OffsetRightX, OffsetRightY, OffsetBottomX, OffsetBottomY);
            return movement;
        }
    }
}
