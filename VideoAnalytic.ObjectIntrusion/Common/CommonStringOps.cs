/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 18 Dec 2016 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
//-----------------------------------------------------------------------------------------------------------------------------
// Author    : John Kenedy
// Email     : jokenjp@yahoo.com
// WebSite   : http://www.scrappermin.com
// 
// You are feel free to use this source code as you feel like but you need to retain the original ownership information which
// this source code is written originally by John Kenedy (jokenjp@yahoo.com) on every copy you distribute whether you have
// modified or not. You can add your contribution such as patches, new features, and adding your name as contributor without
// deleting or modifying original author.
//
// This source code is provided "AS IS" without warranty, the author shall not be held liable for any damage caused by this
// software whether material or immaterial.
//-----------------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;

using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Security.Cryptography;

namespace Common
{
    /// <summary>
    /// String operations class. This class provide methods to operate on string using Method Extension feature.
    /// </summary>
    public static class CommonStringOps
    {
        /// <summary>
        /// Random Generator
        /// </summary>
        public static Random RND = new Random();

        /// <summary>
        /// Sanitize a text 'asda' into asda
        /// </summary>
        /// <param name="str">The text</param>
        /// <returns>Return the Sanitize text</returns>
        public static string Sanitize(this string str)
        {
            str = str.Trim();
            if ((str.StartsWith("\'") && str.EndsWith("\'")))
            {
                if (str.Length == 1) return string.Empty;
                if (str.Length == 2) return string.Empty;
                return str.Substring(1, str.Length - 2);
            }
            return str;
        }

        /// <summary>
        /// Unsanitize a text asda into 'asda'
        /// </summary>
        /// <param name="str">The text</param>
        /// <returns>Return the Unsanitize text</returns>
        public static string UnSanitize(this string str)
        {
            str = str.Trim();
            return "'" + str + "'";
        }

        /// <summary>
        /// Get file extension
        /// </summary>
        /// <param name="file">File path</param>
        /// <returns>The file extension</returns>
        public static string GetFileExtension(string file)
        {
            return Path.GetExtension(file);
        }

        /// <summary>
        /// Get filename without extension
        /// </summary>
        /// <param name="file">File path</param>
        /// <returns>The file name withou extension</returns>
        public static string GetFilenameWithoutExtension(string file)
        {
            return Path.GetFileNameWithoutExtension(file);
        }

        /// <summary>
        /// Matchin a string with a Regex and return multiple result
        /// </summary>
        /// <param name="content">The string</param>
        /// <param name="pattern">The pattern</param>
        /// <returns>The multiple result</returns>
        public static string[] RegexMultiple(string content, string pattern)
        {
            if (string.IsNullOrEmpty(content)) return new string[] { };
            MatchCollection ms = Regex.Matches(content, pattern);

            List<string> result = new List<string>();
            foreach (Match m in ms)
            {
                if (m.Success) result.Add(m.Value);
            }
            return result.ToArray();
        }

        /// <summary>
        /// Check if file exist
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <returns>Return 1 if exist 0 if not exist</returns>
        public static string[] FileExist(string filename)
        {
            return System.IO.File.Exists(filename) ? new string[] { "1" } : new string[] { "0"};
        }

        /// <summary>
        /// Check if file exist
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <returns>Return 1 if exist 0 if not exist</returns>
        public static void FileDelete(string filename)
        {
            if (System.IO.File.Exists(filename))
                System.IO.File.Delete(filename);
        }
        
        /// <summary>
        /// Check if specified string is inside a file, check by matching each line
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <param name="text">The text to check</param>
        /// <returns>Return "1" if exist "0" if not exist</returns>
        public static string[] InsideFile(string filename, string text)
        {
            if (File.Exists(filename) == false) return new string[] { "0" };
            string[] texts = File.ReadAllLines(filename);
            foreach (string t in texts)
            {
                if (t.ToLower().Trim() == text.ToLower().Trim()) return new string[] { "1" };
            }
            return new string[] { "0" };
        }

        /// <summary>
        /// Read All Text
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <returns>Return "1" if exist "0" if not exist</returns>
        public static string[] ReadAllText(string filename)
        {
            if (File.Exists(filename) == false) return new string[] { "0" };
            string[] texts = new string[] { File.ReadAllText(filename) };
            return texts;
        }

        /// <summary>
        /// Load File to list of string
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <returns>Return list of string of file content</returns>
        public static string[] LoadFile(string filename)
        {
            if (File.Exists(filename) == false) return new string[0];
            string[] texts = File.ReadAllLines(filename);
            return texts;
        }

        /// <summary>
        /// Remove HTML tag from HTML string
        /// </summary>
        /// <param name="htmlString">The html string</param>
        /// <returns>The string without HTML</returns>
        public static string StripHTML(this string htmlString)
        {
            try
            {
                string pattern = @"<(.|\n)*?>";
                return Regex.Replace(htmlString, pattern, string.Empty);
            }
            catch
            {
                return htmlString;
            }
        }

        /// <summary>
        /// Substring from start of the startChar
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="startChar">The start char</param>
        /// <returns>The string start from last occurence of startChar</returns>
        public static string SubstringLastIndexOf(string str, string startChar)
        {
            if (str.IndexOf(startChar, StringComparison.CurrentCultureIgnoreCase) < 0) return str;
            int index = str.LastIndexOf(startChar, StringComparison.CurrentCultureIgnoreCase);
            if (str.Length <= index + 1) return string.Empty;
            return str.Substring(index + 1);
        }

        /// <summary>
        /// Substring from index 0 to start of the startChar
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="startChar">The start char</param>
        /// <returns>The string start from last occurence of startChar</returns>
        public static string SubstringLastIndexOfLast(string str, string startChar)
        {
            if (str.IndexOf(startChar, StringComparison.CurrentCultureIgnoreCase) < 0) return str;
            int index = str.LastIndexOf(startChar, StringComparison.CurrentCultureIgnoreCase);
            return str.Substring(0, index);
        }

        /// <summary>
        /// Substring from start of the startChar
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="startChar">The start char</param>
        /// <returns>The string start from first occurence of startChar</returns>
        public static string SubstringIndexOf(string str, string startChar)
        {
            if (str.IndexOf(startChar, StringComparison.CurrentCultureIgnoreCase) < 0) return str;
            int index = str.IndexOf(startChar, StringComparison.CurrentCultureIgnoreCase);
            if (str.Length <= index + 1) return string.Empty;
            return str.Substring(index + 1);
        }

        /// <summary>
        /// Substring from index 0 to start of the startChar
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="startChar">The start char</param>
        /// <returns>The string start from first occurence of startChar</returns>
        public static string SubstringIndexOfLast(string str, string startChar)
        {
            if (str.IndexOf(startChar, StringComparison.CurrentCultureIgnoreCase) < 0) return str;
            int index = str.IndexOf(startChar, StringComparison.CurrentCultureIgnoreCase);
            return str.Substring(0, index);
        }

        /// <summary>
        /// Date To String
        /// </summary>
        /// <param name="format">Format of the date</param>
        /// <returns>String in date format</returns>
        public static string CurrDate(string format)
        {
            try
            {
                return DateTime.Now.ToString(format);
            }
            catch
            {
            }
            return string.Empty;
        }

        /// <summary>
        /// Date To String
        /// </summary>
        /// <param name="format">Format of the date</param>
        /// <param name="day">Offset how many day</param>
        /// <returns>String in date format</returns>
        public static string CurrDateOffset(string format, string day)
        {
            try
            {
                DateTime dt = DateTime.Now;
                int d = 0;
                int.TryParse(day, out d);
                dt = dt.AddDays(d);
                return dt.ToString(format);
            }
            catch
            {
            }
            return string.Empty;
        }

        /// <summary>
        /// Date To String
        /// </summary>
        /// <param name="dd">Day</param>
        /// <param name="mm">Month</param>
        /// <param name="yyyy">Year</param>
        /// <param name="format">Format of the date</param>
        /// <returns>String in date format</returns>
        public static string OtherDate(string dd, string mm, string yyyy, string format)
        {
            try
            {
                int d = DateTime.Now.Day;
                int m = DateTime.Now.Month;
                int y = DateTime.Now.Year;
                int.TryParse(dd, out d);
                int.TryParse(mm, out m);
                int.TryParse(yyyy, out y);
                DateTime dt = new DateTime(y, m, d);
                return dt.ToString(format);
            }
            catch
            {
            }
            return string.Empty;
        }

        /// <summary>
        /// Date To String
        /// </summary>
        /// <param name="dd">Day</param>
        /// <param name="mm">Month</param>
        /// <param name="yyyy">Year</param>
        /// <param name="format">Format of the date</param>
        /// <returns>String in date format</returns>
        public static string OtherDateOffset(string dd, string mm, string yyyy, string format, string day)
        {
            try
            {
                int next = 0;
                int.TryParse(day, out next);
                int d = DateTime.Now.Day;
                int m = DateTime.Now.Month;
                int y = DateTime.Now.Year;
                int.TryParse(dd, out d);
                int.TryParse(mm, out m);
                int.TryParse(yyyy, out y);
                DateTime dt = new DateTime(y, m, d).AddDays(next);
                return dt.ToString(format);
            }
            catch
            {
            }
            return string.Empty;
        }

        /// <summary><![CDATA[
        /// Search for tag starting from in front
        /// Example, startTag='<div>', endTag='</div>'
        /// Data :
        /// <div>http://data1 Data 1</div><div id='unique'>more</div>
        /// <div>http://data2 Data 2</div><div id='unique'>more</div>
        /// ...
        /// Return : 'http://data1 Data 1', 'http://data2 Data 2', ... in IList ]]>
        /// </summary>
        /// <param name="webContent">The data that is being parsed</param>
        /// <param name="tagStart">The start of the data</param>
        /// <param name="tagEnd">The end of the data</param>
        /// <returns>Return all the tag matches</returns>
        public static string[] TagMatch(this string webContent, string tagStart, string tagEnd)
        {
            List<string> rs = new List<string>();
            string search = tagStart;
            int start = webContent.IndexOf(search);
            int end = 0;
            while (start != -1)
            {
                if (start == -1) break;
                if (start + search.Length >= webContent.Length) break;
                end = webContent.IndexOf(tagEnd, start + search.Length, StringComparison.CurrentCultureIgnoreCase);
                if (end == -1) break;
                
                string temp = webContent.Substring(start + search.Length, end - start - search.Length);
                rs.Add(temp);

                start = webContent.IndexOf(search, end + tagEnd.Length, StringComparison.CurrentCultureIgnoreCase);
            }
            return rs.ToArray();
        }

        /// <summary>
        /// Split a string into array of string with space but allows string with space by starting with quote
        /// </summary>
        /// <param name="str">string to be splitted by space</param>
        /// <returns>List of string</returns>
        public static List<string> GetArguments(this string str)
        {
            if (string.IsNullOrEmpty(str)) return new List<string>() { "" };

            List<string> result = new List<string>();
            bool quote = false;
            bool justadd = false;
            foreach (char c in str)
            {
                if (c == '"' && quote == false)
                {
                    quote = true;
                    if (justadd == false) result.Add(string.Empty);
                    else justadd = true;
                }
                else if (c == '"' && quote == true)
                {
                    quote = false;
                }
                else if (c == ' ' && quote == false)
                {
                    justadd = true;
                    result.Add(string.Empty);
                }
                else
                {
                    if (result.Count == 0) result.Add(string.Empty);
                    result[result.Count - 1] = result[result.Count - 1] + c;
                }
            }
            return result;
        }

        /// <summary>
        /// Get parameters from a filename
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static List<string> GetParameters(string filename)
        {
            List<string> result = new List<string>();
            string spamconfigfile = filename;
            if (File.Exists(spamconfigfile) == false) return new List<string>();
            string texts = File.ReadAllText(spamconfigfile);

            using (FileStream fs = new FileStream(spamconfigfile, FileMode.Open))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    string line = sr.ReadLine();
                    string comment = string.Empty;
                    if (string.IsNullOrEmpty(line) == false && line.ToLower() == "[param]")
                    {
                        do
                        {
                            line = sr.ReadLine();
                            if (line.ToLower() == "[param]")
                            {
                                result.Add(comment.Trim());
                                comment = string.Empty;
                                continue;
                            }
                            else
                            {
                                comment += line + "\r\n";
                            }
                        } while (sr.EndOfStream == false);
                        if (string.IsNullOrEmpty(comment.Trim()) == false) result.Add(comment.Trim());
                    }
                    sr.Close();
                    fs.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// Get ending of a SKip Tag Match
        /// </summary>
        /// <param name="text"></param>
        /// <param name="search"></param>
        /// <param name="endsearch"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="skipcount"></param>
        /// <returns></returns>
        public static int GetEnd(string text, string search, string endsearch, int start, int end)
        {
            string inbetween = text.Substring(start, end - start);
            string[] counts = inbetween.Split(new string[] { search }, StringSplitOptions.None);
            if ((counts != null && counts.Length > 1))
            {
                int skipcount = counts.Length - 1;
                for (int i = 0; i < skipcount; i++)
                {
                    start = end + endsearch.Length;
                    end = text.IndexOf(endsearch, start, StringComparison.CurrentCultureIgnoreCase);
                    if (end == -1) return -1;
                    end = GetEnd(text, search, endsearch, start, end);
                }
            }
            return end;
        }
        /// <summary><![CDATA[
        /// Search for tag starting from in front
        /// Example, startTag='<div>', endTag='</div>', skipStart='<div>'
        /// Data :
        /// <div>http://data1 <div>Data 1</div>asdasd</div>
        /// <div>http://data2 <div>Data 2</div>asdasd</div>
        /// ...
        /// Return : 'http://data1 <div>Data 1</div>', 'http://data2 <div>Data 2</div>', ... in string[] ]]>
        /// </summary>
        /// <param name="webContent">The data that is being parsed</param>
        /// <param name="tagStart">The start of the data</param>
        /// <param name="tagEnd">The end of the data</param>
        /// <param name="skipStart">The tag to skip through</param>
        /// <returns>Return all the tag matches</returns>
        public static string[] TagMatchSkip(this string webContent, string tagStart, string tagEnd, string skipStart)
        {
            List<string> rs = new List<string>();
            string search = tagStart;
            int start = webContent.IndexOf(search);
            int end = 0;
            int skip = 0;
            while (start != -1)
            {
                if (start == -1) break;
                if (start + search.Length >= webContent.Length) break;
                end = webContent.IndexOf(tagEnd, start + search.Length, StringComparison.CurrentCultureIgnoreCase);
                if (end == -1) break;

                skip = webContent.IndexOf(skipStart, start + skipStart.Length, StringComparison.CurrentCultureIgnoreCase);
                if (skip != -1)
                    end = GetEnd(webContent, skipStart, tagEnd, start + search.Length, end);

                string temp = string.Empty;
                if (end == -1)
                    temp = webContent.Substring(start + search.Length, webContent.Length);
                else if (end > skip)
                    temp = webContent.Substring(start + search.Length, end - start - search.Length);
                else if (skip == -1 || skip > end)
                    temp = webContent.Substring(start + search.Length, end - start - search.Length);
                else
                    temp = webContent.Substring(start + search.Length, end - start - search.Length);
                rs.Add(temp);

                if (end != -1) start = webContent.IndexOf(search, end + tagEnd.Length, StringComparison.CurrentCultureIgnoreCase);
                else start = -1;
            }
            return rs.ToArray();
        }

        /// <summary><![CDATA[
        /// Example, tagStart="<div>", tagEnd="</div><div id='unique'>more</div>"
        /// Data :
        /// <div>http://data1 Data 1</div><div id='unique'>more</div><div>http://data2 Data 2</div><div id='unique'>more</div>
        /// ...
        /// Return : "http://data1 Data 1", "http://data2 Data 2"
        /// Even the searching is from behind the result return is in start to end order ]]>
        /// </summary>
        /// <param name="webContent">The content being parsed</param>
        /// <param name="tagStart">The starting tag to look when the ending tag which is unique is found</param>
        /// <param name="tagEnd">The ending tag that is used for finding since it is unique at the end part, and then begin to search for the starting tag</param>
        /// <returns>Return all the tag matches</returns>
        public static string[] TagMatchFromBehind(this string webContent, string tagStart, string tagEnd)
        {
            //int endPos = -1;
            //endPos = webContent.IndexOf(tagEnd, StringComparison.CurrentCultureIgnoreCase);
            //IList<string> result = new List<string>();
            //while (endPos >= 0)
            //{
            //    string data = webContent.Substring(0, endPos);

            //    int startPos = data.LastIndexOf(tagStart);
            //    if (startPos == -1)
            //    {
            //        endPos = webContent.IndexOf(tagEnd, endPos + tagEnd.Length, StringComparison.CurrentCultureIgnoreCase);
            //        continue;
            //    }

            //    string dt = string.Empty;
            //    if (startPos + tagStart.Length + (endPos - (startPos + tagStart.Length)) <= data.Length)
            //    {
            //        dt = data.Substring(startPos + tagStart.Length, (endPos - (startPos + tagStart.Length)));
            //    }
            //    result.Add(dt);

            //    endPos = webContent.IndexOf(tagEnd, endPos + tagEnd.Length, StringComparison.CurrentCultureIgnoreCase);
            //}
            //return result.ToArray();
            int endPost = -1;
            endPost = webContent.LastIndexOf(tagEnd, StringComparison.CurrentCultureIgnoreCase);
            List<string> result = new List<string>();
            string data = webContent;
            while (endPost >= 0)
            {
                data = data.Substring(0, endPost);
                int startPos = data.LastIndexOf(tagStart);
                if (startPos == -1) break;

                string dt = string.Empty;
                if (startPos + tagStart.Length + (endPost - (startPos + tagStart.Length)) <= data.Length)
                {
                    dt = data.Substring(startPos + tagStart.Length, (endPost - (startPos + tagStart.Length)));
                    result.Insert(0, dt);
                    if (startPos > 0) data = data.Substring(0, startPos);
                    else break;
                }
                endPost = data.LastIndexOf(tagEnd, StringComparison.CurrentCultureIgnoreCase);
            }
            return result.ToArray();
        }

        /// <summary>
        /// Return first word and the rest of the words
        /// Example : !Run Command This
        /// Return string[0]=first word string[1]=the rest of the word
        /// </summary>
        /// <param name="eMessage">The sentence</param>
        /// <returns>Return first word and the rest of the words</returns>
        public static string[] FirstWord(this string eMessage)
        {
            string top = string.Empty;
            string bottom = string.Empty; ;
            int start = eMessage.IndexOf(" ");
            if (start != -1) top = eMessage.Substring(0, start);
            else top = eMessage;
            bottom = string.Empty;
            if (start != -1 && start != eMessage.Length - 1) bottom = eMessage.Substring(start + 1);
            if (string.IsNullOrEmpty(bottom)) return new string[] {top };
            else return new string[] { top, bottom };
        }

        /// <summary><![CDATA[
        /// Find the End Match of a Current Curl
        /// SAMPLE
        ///         Download(\"http://www.aaa.com\"), \"<div title='aa'>\", \"</div>\")
        ///     ignore this ^          ignore this ^                        find this ^ ]]>
        /// </summary>
        /// <param name="syntax">Syntax</param>
        /// <param name="startIndex">The start index</param>
        /// <param name="startMatch">The character match start</param>
        /// <param name="endMatch">The character match end</param>
        /// <returns>Return the index of the item matched as the end of the start</returns>
        public static int FindEndMatch(this string syntax, int startIndex, char startMatch, char endMatch)
        {
            int count = 0;
            for (int i = startIndex; i < syntax.Length; i++)
            {
                if (syntax[i] == endMatch && count == 0) return i;
                else if (syntax[i] == startMatch) count++;
                else if (syntax[i] == endMatch) count--;
            }
            return -1;
        }

        /// <summary>
        ///  When given an index, will find that the tag surrounded this index 'asdsada &lt; abc &gt;asdasd', 10, returns '&lt; abc &gt;'
        /// </summary>
        /// <param name="content">The content to be searched</param>
        /// <param name="index">The index</param>
        /// <returns>return the whole tag</returns>
        public static string GetTag(string content, int index)
        {
            if (index < 0) return string.Empty;
            int s = 0;
            int e = 0;
            string tag = string.Empty;
            for (int i = index; i >= 0; i--)
            {
                if (content[i] == '<')
                {
                    s = i;
                    break;
                }
            }
            for (int i = index; i < content.Length; i++)
            {
                if (content[i] == '>')
                {
                    e = i;
                    break;
                }
            }
            string str = content.Substring(s, (e - s) + 1);
            return str;
        }

        /// <summary>
        /// Single Tag Match starting from start
        /// </summary>
        /// <param name="content">Content</param>
        /// <param name="search">The search start string</param>
        /// <param name="endsearch">The search end string</param>
        /// <returns>The string between search and endsearch</returns>
        public static string SingleTagMatch(string content, string search, string endsearch)
        {
            int s = content.IndexOf(search);
            if (s == -1) return string.Empty;
            int e = content.IndexOf(endsearch, s + search.Length);
            if (e == -1) return string.Empty;
            return content.Substring(s + search.Length, e - s - search.Length);
        }

        /// <summary><![CDATA[
        /// Find an attribute content with an unique ID or Class no matter where the position of the attribute even after or before the attribute
        /// Data : <div id="test" value="123"></div>
        /// Data : <div value="123" id="test"></div>
        /// using TagMatchSingle(Data, "id=\"test\"", "value=\"", "\"") will return "123" ]]>
        /// </summary>
        /// <param name="content">The content</param>
        /// <param name="containing">The unique attribute</param>
        /// <param name="attributestart">The attribute to search the value for</param>
        /// <param name="attributeend">The attribute to end the search</param>
        /// <returns>The string between attributestart and attributeend</returns>
        public static string TagMatchSingle(this string content, string containing, string attributestart, string attributeend)
        {
            int s = content.IndexOf(containing);
            if (s == -1)
            {
                return string.Empty;
            }
            string tag = GetTag(content, s);
            string result = SingleTagMatch(tag, attributestart, attributeend);
            return result;
        }

        /// <summary>
        /// Copy from one stream to another scream
        /// </summary>
        /// <param name="src">Source stream</param>
        /// <param name="dest">Destination stream</param>
        public static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

        /// <summary>
        /// Zip a string and return the base 64 encoded
        /// </summary>
        /// <param name="str">The string to be zipped</param>
        /// <returns>The zipped string and encoded to base64</returns>
        public static string Zip(this string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    //msi.CopyTo(gs);
                    CopyTo(msi, gs);
                }

                return Convert.ToBase64String(mso.ToArray());
            }
        }

        /// <summary>
        /// Unzip a base 64 encoded and zipped string into the original string
        /// </summary>
        /// <param name="bytesstr">The zipped and base 64 encoded string</param>
        /// <returns>The original string</returns>
        public static string Unzip(this string bytesstr)
        {
            byte[] bytes = Convert.FromBase64String(bytesstr);
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }

        /// <summary>
        /// Compresses the string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>Return the compressed string</returns>
        public static string CompressString(this string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        /// <summary>
        /// Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns>Return the decompressed string</returns>
        public static string DecompressString(this string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        /// <summary>
        /// Return a string with trim and toupper applied
        /// </summary>
        /// <param name="str">The string to apply</param>
        /// <returns>The result after trim and toupper</returns>
        public static string TrimUp(this string str)
        {
            if (str == null) return null;
            return str.Trim().ToUpper();
        }

        /// <summary>
        /// Convert relative url to absolute
        /// </summary>
        /// <param name="relativeUrl">the relative url</param>
        /// <param name="baseurl">the base url</param>
        /// <returns>Absolute url</returns>
        public static string ToAbsoluteUrl(this string relativeUrl, string baseurl)
        {
            if (relativeUrl.StartsWith("http", StringComparison.CurrentCultureIgnoreCase)) return relativeUrl;
            return new System.Uri(new Uri(baseurl), relativeUrl).AbsoluteUri;
        }

        /// <summary>
        /// Make a sentence from word with capitalize first letter
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSentence(this string str)
        {
            string[] strs = str.Split(new char[]{'.'}, StringSplitOptions.RemoveEmptyEntries);
            string result = string.Empty;
            foreach (string s in strs)
            {
                string s2 = string.Empty;
                if (s.Length > 0) s2 = s[0].ToString().ToUpper();
                if (s.Length > 1) s2 = s2 + s.Substring(1);
                result += s2 + ".";
            }
            if (result.Length > 0) result = result.Substring(0, result.Length - 1);
            return result;
        }

        /// <summary>
        /// Generate Random String with char A-Z or 0-9
        /// </summary>
        /// <param name="len">The length of the string</param>
        /// <returns>Random string with length</returns>
        public static string RandomString(int len)
        {
            string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder sb = new StringBuilder();

            while ((len--) > 0)
                sb.Append(str[(int)(RND.NextDouble() * str.Length)]);

            return sb.ToString();
        }

        /// <summary>
        /// Generate Random String with char 0 to 9
        /// </summary>
        /// <param name="len">The length of the string</param>
        /// <returns>Random string with length</returns>
        public static string RandomInteger(int len)
        {
            string str = "0123456789";
            StringBuilder sb = new StringBuilder();

            while ((len--) > 0)
                sb.Append(str[(int)(RND.NextDouble() * str.Length)]);

            return sb.ToString();
        }

        /// <summary>
        /// Random 0 to n
        /// </summary>
        /// <param name="n">The max integer</param>
        /// <returns>Random the random integer</returns>
        public static string RandomNext(int n)
        {
            return RND.Next(n).ToString();
        }

        /// <summary>
        /// Get MD5 Hash from a string using OAuth Method
        /// </summary>
        /// <param name="input">The string to get MD5 from</param>
        /// <returns>The MD5 string result</returns>
        public static string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// Get the string representation of an integer using Radix
        /// </summary>
        /// <param name="digit">The integer to get string from</param>
        /// <param name="radix">The radix</param>
        /// <returns>The string representation of the integer</returns>
        public static char forDigit(int digit, int radix)
        {
            if (radix < 2 || radix > 36)
                throw new ArgumentOutOfRangeException("radix");

            if (digit < 0 || digit >= radix)
                throw new ArgumentOutOfRangeException("digit");

            if (digit < 10)
                return (char)(digit + (int)'0');
            return (char)(digit - 10 + (int)'a');
        }

        /// <summary>
        /// Get MD5 Hash from a string using OAuth Method
        /// </summary>
        /// <param name="input">The string to get MD5 from</param>
        /// <returns>The MD5 string result</returns>
        public static string GetMd5HashOAuth(string input)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            byte[] arrayOfByte = data;
            int i = arrayOfByte.Length;
            StringBuilder localStringBuilder = new StringBuilder(i << 1);
            for (int j = 0; j < i; j++)
            {
                localStringBuilder.Append(forDigit((0xF0 & arrayOfByte[j]) >> 4, 16));
                localStringBuilder.Append(forDigit(0xF & arrayOfByte[j], 16));
            }
            return localStringBuilder.ToString();
        }

        /// <summary>
        /// Check a string contains a text
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="substring">The text</param>
        /// <returns>Return the index of the string</returns>
        public static string IndexOf(string str, string substring)
        {
            int index = str.IndexOf(substring, StringComparison.CurrentCultureIgnoreCase);
            return index.ToString();
        }

        /// <summary>
        /// Check a string starts with a text
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="substring">The text</param>
        /// <returns>Return "1" if true, "0" if false</returns>
        public static string StartsWith(string str, string substring)
        {
            if (str.StartsWith(substring, StringComparison.CurrentCultureIgnoreCase)) return "1";
            return "0";
        }

        /// <summary>
        /// Check a string ends with a text
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="substring">The text</param>
        /// <returns>Return "1" if true, "0" if false</returns>
        public static string EndsWith(string str, string substring)
        {
            if (str.EndsWith(substring, StringComparison.CurrentCultureIgnoreCase)) return "1";
            return "0";
        }

        /// <summary>
        /// If two string equal
        /// </summary>
        /// <param name="str">String one</param>
        /// <param name="substring">String two</param>
        /// <returns>Return "1" if equal "0" if not</returns>
        public static string Equal(string str, string substring)
        {
            if (str.ToUpper() == substring.ToUpper()) return "1";
            return "0";
        }
        /// <summary>
        /// Get the filename only without path
        /// </summary>
        /// <param name="filename">The filename with path</param>
        /// <returns>The filaname without path</returns>
        public static string PathGetFilename(string filename)
        {
            return Path.GetFileName(filename);
        }

        /// <summary>
        /// Get file length
        /// </summary>
        /// <param name="filename">File name</param>
        /// <returns>File length in string</returns>
        public static string PathFileLength(string filename)
        {
            if (File.Exists(filename) == false) return "0";
            FileInfo fi = new FileInfo(filename);
            return fi.Length.ToString();
        }

        /// <summary>
        /// Convert to Base 64
        /// </summary>
        /// <param name="str">String to convert</param>
        /// <returns>Converted Base 64 string</returns>
        public static string ConvertToBase64(string str)
        {
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(str));
        }

        /// <summary>
        /// Convert from Base64
        /// </summary>
        /// <param name="str">Base64 string</param>
        /// <returns>The normal string</returns>
        public static string ConvertFromBase64(string str)
        {
            return Encoding.ASCII.GetString(Convert.FromBase64String(str));
        }

        /// <summary>
        /// Encode a url
        /// </summary>
        /// <param name="str">The unencoded url</param>
        /// <returns>The encoded url</returns>
        public static string UrlEncode(string str)
        {
            return HttpUtility.UrlEncode(str);
        }

        /// <summary>
        /// Decode a url
        /// </summary>
        /// <param name="str">The encoded url</param>
        /// <returns>The unencoded url</returns>
        public static string UrlDecode(string str)
        {
            return HttpUtility.UrlDecode(str);
        }

        /// <summary>
        /// Encode a html
        /// </summary>
        /// <param name="str">The unencoded html</param>
        /// <returns>The encoded html</returns>
        public static string HtmlEncode(string str)
        {
            return HttpUtility.HtmlEncode(str);
        }

        /// <summary>
        /// Decode a html
        /// </summary>
        /// <param name="str">The encoded html</param>
        /// <returns>The unencoded html</returns>
        public static string HtmlDecode(string str)
        {
            return HttpUtility.HtmlDecode(str);
        }

        /// <summary>
        /// Escape a data string
        /// </summary>
        /// <param name="str">A POST string</param>
        /// <returns>The escape result</returns>
        public static string EscapeDataString(string str)
        {
            return Uri.EscapeDataString(str);
        }

        /// <summary>
        /// Escape a uri string
        /// </summary>
        /// <param name="str">A uri string</param>
        /// <returns>The escape result</returns>
        public static string EscapeUriString(string str)
        {
            return Uri.EscapeUriString(str);
        }

        /// <summary>
        /// Fix invalid JSON that has " inside string without being escaped
        /// </summary>
        /// <param name="curl">The invalid json</param>
        /// <returns>The fixed string</returns>
        public static string FixJSONWithQuote(string curl)
        {
            string temp = string.Empty;
            bool isstring = false;
            bool startadding = false;
            string tempstring = string.Empty;
            for (int i = 0; i < curl.Length; i++)
            {
                if ((i - 1 <= 0 || curl[i - 1] != '\\') && curl[i] == '\"' && isstring == false)
                {
                    isstring = true;
                    tempstring += curl[i];
                }
                else if ((i - 1 <= 0 || curl[i - 1] != '\\') && curl[i] == '\"' && isstring == true)
                {
                    if ((i + 1 < curl.Length))
                    {
                        if (curl[i + 1] == '}' || curl[i + 1] == ',' || curl[i + 1] == ':')
                        {
                            tempstring += curl[i];
                            isstring = false;
                            startadding = true;
                        }
                    }
                    else
                    {
                        tempstring += curl[i];
                        isstring = false;
                        startadding = true;
                    }
                }
                else if (isstring == false)
                {
                    temp += curl[i];
                }
                else if (isstring == true)
                {
                    tempstring += curl[i];
                }

                if (startadding)
                {
                    startadding = false;
                    temp += tempstring;
                    tempstring = "";
                }
            }
            return temp;
        }

        /// <summary>
        /// Check if contains chinese character
        /// </summary>
        /// <param name="str">The string</param>
        /// <returns></returns>
        public static string IsContainChinese(string str)
        {
            for (int x = 0; x < str.Length; x++)
            {
                if (char.GetUnicodeCategory(str[x]) == System.Globalization.UnicodeCategory.OtherLetter)
                {
                    return "1";
                }
            }
            return "0";
        }

        /// <summary>
        /// Check if contains chinese character
        /// </summary>
        /// <param name="str">The string</param>
        /// <returns></returns>
        public static string RemoveChinese(string str)
        {
            string english = "ABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890-=[]\\;',./~!@#$%^&*()_+{}|:\"<>? \r\n\t";
            string result = string.Empty;
            for (int x = 0; x < str.Length; x++)
            {
                if (english.IndexOf(str[x].ToString().ToUpper()) >= 0)
                {
                    result += str[x];
                }
            }
            return result;
        }
    }
}
