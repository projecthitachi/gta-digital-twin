﻿// Bacnet.Raspberry.Models.RaspberryDataLog_REC
using System;
using System.ComponentModel.DataAnnotations;

namespace VideoAnalytic.PeopleCounting.Models
{
    public class VAPeopleCountingDataLog_REC
    {
        [Display(Name = "RecordID")]
        public long RecordID
        {
            get;
            set;
        }


        [Display(Name = "RecordTimestamp")]
        public DateTime RecordTimestamp
        {
            get;
            set;
        }

        [Display(Name = "Record Status")]
        public int RecordStatus
        {
            get;
            set;
        }

        [Display(Name = "Device ID")]
        public string DeviceID
        {
            get;
            set;
        }

        [Display(Name = "Object ID")]
        public string ObjectID
        {
            get;
            set;
        }

        [Display(Name = "Payload")]
        public string Payload
        {
            get;
            set;
        }
    }
}