/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 29 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Web;
using System.Threading;
using Common;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using Microsoft.AspNet.SignalR;

namespace VideoAnalytic.ObjectIntrusion
{
    public class VLCVideoRecord
    {
        private static object InstanceLock = new object();
        private static VLCVideoRecord _Instance;


        public static VLCVideoRecord Instance
        {
            get
            {
                if (_Instance != null) return _Instance;

                lock(InstanceLock)
                {
                    lock(InstanceLock)
                    {
                        _Instance = new VLCVideoRecord();
                    }
                }
                return _Instance;
            }
        }

        private VLCVideoRecord()
        {
        }


        public Dictionary<string, string> StartRecordZone = new Dictionary<string, string>();
        public Dictionary<string, DateTime> StopRecordTime = new Dictionary<string, DateTime>();
        public Dictionary<string, bool> IsStartRecord = new Dictionary<string, bool>();
        public Dictionary<string, string> LastVideoPath = new Dictionary<string, string>();
        public Dictionary<string, Vlc.DotNet.Core.VlcMediaPlayer> MediaPlayer = new Dictionary<string, Vlc.DotNet.Core.VlcMediaPlayer>();

        public void StartRecord(string CameraRtsp, string zoneid)
        {
            if (IsStartRecord.ContainsKey(CameraRtsp) && IsStartRecord[CameraRtsp])
            {
                //if (StopRecordTime.ContainsKey(CameraRtsp))
                //    StopRecordTime[CameraRtsp] = DateTime.Now.AddSeconds(10);
                //else
                //    StopRecordTime.Add(CameraRtsp, DateTime.Now.AddSeconds(10));
                return;
            }
            if (StopRecordTime.ContainsKey(CameraRtsp))
                StopRecordTime[CameraRtsp] = DateTime.Now.AddSeconds(10);
            else
                StopRecordTime.Add(CameraRtsp, DateTime.Now.AddSeconds(10));

            if (StartRecordZone.ContainsKey(CameraRtsp))
                StartRecordZone[CameraRtsp] = zoneid;
            else
                StartRecordZone.Add(CameraRtsp, zoneid);

            if (IsStartRecord.ContainsKey(CameraRtsp) == false)
                IsStartRecord.Add(CameraRtsp, true);
            else
                IsStartRecord[CameraRtsp] = true;
            StartRecordReal(CameraRtsp, zoneid);
        }

        public void StopRecord(string CameraRtsp, string zoneid)
        {
            if (StopRecordTime.ContainsKey(CameraRtsp) == false) return;
            DateTime t = StopRecordTime[CameraRtsp];
            TimeSpan ts = DateTime.Now - t;

            if (ts.TotalSeconds > 0)
            {
                StopRecordReal(CameraRtsp, zoneid);

                if (IsStartRecord.ContainsKey(CameraRtsp) == false)
                    IsStartRecord.Add(CameraRtsp, false);
                else
                    IsStartRecord[CameraRtsp] = false;
            }
        }

        public void StartRecordReal(string CameraRtsp, string zoneid)
        {
            try
            {
                CVCoreLib.CVCoreConfig _Config = new CVCoreLib.CVCoreConfig();

                var libDirectory = new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", "libvlc", IntPtr.Size == 4 ? "win-x86" : "win-x64"));
                var destination = Path.Combine(_Config.GetRecordPath(), Common.MyRandom.RandomString(10) + ".mp4");

                if (LastVideoPath.ContainsKey(CameraRtsp)) LastVideoPath[CameraRtsp] = destination;
                else LastVideoPath.Add(CameraRtsp, destination);

                var x = new Vlc.DotNet.Core.VlcMediaPlayer(libDirectory);
                MediaPlayer.Add(CameraRtsp, x);

                var mediaOptions = new[]
                {
                ":sout=#file{dst=" + destination + "}",
                ":sout-keep"
            };

                x.SetMedia(new Uri(CameraRtsp), mediaOptions);
                x.Play();
            }
            catch (Exception ex)
            {
                LibraryTools.WriteName("StartRecordReal", ex);
            }
        }

        public void StopRecordReal(string CameraRtsp, string zoneid)
        {
            if (MediaPlayer.ContainsKey(CameraRtsp) == false) return;

            try
            {
                var x = MediaPlayer[CameraRtsp];
                x.Stop();
                x.Dispose();
            }
            catch { }

            MediaPlayer.Remove(CameraRtsp);

            SqlRFID_ALERTDAL dal = new SqlRFID_ALERTDAL();
            SqlVA_ALERTDAL dal2 = new SqlVA_ALERTDAL();
            if (LastVideoPath.ContainsKey(CameraRtsp))
            {
                dal.UpdateImage(zoneid, LastVideoPath[CameraRtsp]);
                dal2.UpdateImage(zoneid, LastVideoPath[CameraRtsp]);
            }
        }

        public bool IsRunning = false;
        public Thread RunningThread = null;
        public void Start()
        {
            if (IsRunning) return;
            IsRunning = true;
            ThreadStart ts = new ThreadStart(StartReal);
            RunningThread = new Thread(ts);
            RunningThread.Start();
        }

        public void Stop()
        {
            IsRunning = false;
            try
            {
                if (RunningThread != null)
                {
                    RunningThread.Abort();
                    RunningThread = null;
                }
            }
            catch { }
        }

        public void StartReal()
        {
            do
            {
                try
                {
                    List<string> removeKeys = new List<string>();
                    foreach(var key in StopRecordTime.Keys)
                    {
                        DateTime t = StopRecordTime[key];
                        TimeSpan ts = DateTime.Now - t;
                        if (ts.TotalSeconds > 0)
                        {
                            removeKeys.Add(key);
                            if (StartRecordZone.ContainsKey(key))
                                StopRecord(key, StartRecordZone[key]);
                            else
                                StopRecord(key, "area 0");

                            if (IsStartRecord.ContainsKey(key))
                                IsStartRecord[key] = false;
                            else
                                IsStartRecord.Add(key, false);
                        }
                    }

                    foreach(var key in removeKeys)
                    {
                        try
                        {
                            StopRecordTime.Remove(key);
                        }
                        catch { }
                    }
                }
                catch (Exception ex)
                {
                    LibraryTools.WriteName("VLCVideoRecord_RunAll", ex);
                }
                Thread.Sleep(2000);
            } while (IsRunning);
        }
    }

}