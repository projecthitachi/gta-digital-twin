﻿// has.app.Components.RealTimeHub
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using CVCoreLib;
using System;
using Common;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using VideoAnalytic.PeopleCounting;
using System.Threading;
using System.Web.Mvc;
using System.Web;
using System.Net.WebSockets;
using Newtonsoft.Json;
using WebSocketSharp;
using WebSocketSharp.Server;
using System.Configuration;

namespace VideoAnalytic.ObjectIntrusion
{
    public class VABehavior : WebSocketBehavior
    {
        protected override void OnClose(CloseEventArgs e)
        {

            if (VAObjectHub.Instance.PlaybackClients.ContainsKey(this)) VAObjectHub.Instance.PlaybackClients.Remove(this);
        }

        protected override void OnError(WebSocketSharp.ErrorEventArgs e)
        {
            if (VAObjectHub.Instance.PlaybackClients.ContainsKey(this)) VAObjectHub.Instance.PlaybackClients.Remove(this);
        }

        protected override void OnOpen()
        {
            if (VAObjectHub.Instance.PlaybackClients.ContainsKey(this) == false) VAObjectHub.Instance.PlaybackClients.Add(this, "");
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (e.Data.StartsWith("ID:"))
            {
                if (VAObjectHub.Instance.PlaybackClients.ContainsKey(this)) VAObjectHub.Instance.PlaybackClients[this] = e.Data.Substring(3);
                else VAObjectHub.Instance.PlaybackClients.Add(this, e.Data.Substring(3));
            }

            if (e.Data.StartsWith("MAXMIN:"))
            {
                string str = e.Data.Substring(7);
                string[] ss = str.Split(new char[] { '_' });
                if (ss.Length == 2)
                {
                    VAObjectHub.Instance.Receive(ss[0], ss[1]);
                }
            }

            if (e.Data.StartsWith("CAMERA:"))
            {
                string str = e.Data.Substring(7);
                int cam = 0;
                int.TryParse(str, out cam);
                if (VAObjectHub.Instance.CameraConn.ContainsKey(this) == false) VAObjectHub.Instance.CameraConn.Add(this, cam);
                else VAObjectHub.Instance.CameraConn[this] = cam;
            }
        }
    }

    public class VAObjectHub
    {
        public static object InstanceLock = new object();
        public static VAObjectHub _Instance;
        public static VAObjectHub Instance
        {
            get
            {
                if (_Instance != null) return _Instance;
                lock(InstanceLock)
                {
                    lock (InstanceLock)
                    {
                        _Instance = new VAObjectHub();
                    }
                }
                return _Instance;
            }
        }
        public Action<object, bool> Broadcast;

        public VAHubModel MyHubObject = new VAHubModel();
        public CVCoreLibrary CVCoreLibrary = CVCoreLibrary.Instance;
        public string LastBackgroundID = string.Empty;

        public Dictionary<WebSocketBehavior, int> CameraConn = new Dictionary<WebSocketBehavior, int>();
        public Dictionary<WebSocketBehavior, int> BackgroundIndex = new Dictionary<WebSocketBehavior, int>();
        public Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL>> Background = new Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL>>();
        public Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL2>> Thumb = new Dictionary<WebSocketBehavior, List<VA_ALERTDETAIL2>>();
        public Dictionary<WebSocketBehavior, string> PlaybackClients = new Dictionary<WebSocketBehavior, string>();
        public WebSocketServer wss;
        public bool IsRunning = false;
        public void InitializeCVCore()
        {
            if (IsRunning) return;
            IsRunning = true;
            ThreadStart ts = new ThreadStart(InitializeCVCoreReal);
            Thread t = new Thread(ts);
            t.Start();
        }

        public void InitializeCVCoreReal()
        {
            try
            {
                wss = new WebSocketServer("ws://localhost:8181");
                wss.AddWebSocketService<VABehavior>("/service");
                wss.Start();
            }
            catch (Exception ex) { LibraryTools.WriteName("InitializeCVCoreReal", ex); }

            CallbackProcessor.Instance.OnImageReceived += Instance_OnImageReceived;
            CallbackProcessor.Instance.StartAll(DateTime.MinValue);
            VideoAnalytic.ObjectIntrusion.VLCVideoRecord.Instance.Start();

            SqlVA_ALERTDAL dal = new SqlVA_ALERTDAL();

            while(CallbackProcessor.Instance.IsProcess)
            {
                try
                {
                    string obj = string.Empty;
                    foreach (WebSocketBehavior con in PlaybackClients.Keys)
                    {
                        MyHubObject.AlertList = dal.GetAlertId();
                        MyHubObject.AlertListObject = dal.GetAlertList();

                        if (PlaybackClients[con] == "")
                        {
                            obj = JsonConvert.SerializeObject(MyHubObject);
                            con.Context.WebSocket.Send(obj);
                            Thread.Sleep(100);
                            continue;
                        }

                        SqlVA_ALERTDETAILDAL dal1 = new SqlVA_ALERTDETAILDAL();
                        var back = dal1.GetManyBy("ParentId", PlaybackClients[con]);
                        if (Background.ContainsKey(con) == false) Background.Add(con, back);
                        else Background[con] = back;

                        if (Background.ContainsKey(con) && Background[con].Count > 0)
                        {
                            List<string> image = new List<string>();

                            VA_ALERT va = dal.GetBy("Id", PlaybackClients[con]);
                            if (BackgroundIndex.ContainsKey(con) == false) BackgroundIndex.Add(con, 0);
                            BackgroundIndex[con] = BackgroundIndex[con] + 1;

                            if (BackgroundIndex[con] > Background[con].Count) BackgroundIndex[con] = 0;
                            if (Background[con].Count > 0 && BackgroundIndex[con] >= 0 && BackgroundIndex[con] < Background[con].Count)
                            {
                                try
                                {
                                    MyHubObject.CVCoreImage = Background[con][BackgroundIndex[con]].Image;
                                }
                                catch { }
                            }

                            if (image.Count > 0) MyHubObject.DetectedImage = image;
                        }

                        obj = JsonConvert.SerializeObject(MyHubObject);
                        con.Context.WebSocket.Send(obj);
                    }

                    Thread.Sleep(100);
                }
                catch(Exception ex)
                {
                    LibraryTools.WriteName("VAHub_InitializeCVCoreReal", ex);
                }
            }
        }

        private void Instance_OnImageReceived(HVAAlert alert)
        {
            try
            {
                SqlVA_ALERTDAL dal = new SqlVA_ALERTDAL();
                VA_ALERT al = new VA_ALERT();
                al.AlertDate = alert.FrameTime;
                al.AssetName = (alert.InstanceType.IndexOf("Object") >= 0 ? "Object" : "Human");
                al.CameraID = alert.SubName;
                al.CreateDate = DateTime.Now;
                al.CREATED_DT = al.CreateDate;
                al.CreateOfficer = "SYSTEM";
                al.Description = alert.InstanceType + " detected on " + alert.InstanceName;
                al.Epc = al.AssetName;
                al.FrameUrl = alert.DataID;
                al.IsMax = false;
                al.ImageList = "";
                al.MaxPerson = 0;
                al.MinPerson = 0;
                al.PeopleCount = 0;
                al.RecordStart = new DateTime(2001, 1, 1);
                al.RecordStop = new DateTime(2001, 1, 1);
                al.UpdateDate = al.CreateDate;
                al.UpdateOfficer = al.CreateOfficer;
                al.VideoPath = "";
                al.ViewDate = DateTime.MinValue;
                al.ZoneID = alert.SubName;
                al.ZoneName = alert.SubName;

                dal.Insert(al);

                SqlVA_ALERTDETAILDAL dalDetail = new SqlVA_ALERTDETAILDAL();
                VA_ALERTDETAIL det = new VA_ALERTDETAIL();
                det.ParentId = al.Id;
                det.CreateDate = al.CreateDate;
                det.CreateOfficer = al.CreateOfficer;
                det.FrameUrl = al.FrameUrl;

                string fileContent =  Convert.ToBase64String(File.ReadAllBytes( MapPath(alert.Image)));
                string str = "data:image/jpeg;base64," + fileContent;
                det.Image = str;
                det.UpdateDate = al.UpdateDate;
                det.UpdateOfficer = al.UpdateOfficer;

                dalDetail.Insert(det);

                VLCVideoRecord.Instance.StartRecord(ConfigurationManager.AppSettings["CameraRtsp"], alert.SubName);
            }
            catch(Exception ex)
            {
                LibraryTools.WriteName("VAObjectHub_OnImageReceived", ex);
            }
        }
        
        public string MapPath(string str)
        {
            str = str.Replace("/", "\\");
            str = str.TrimStart('\\');
            string curr = AppDomain.CurrentDomain.BaseDirectory;
            return Path.Combine(curr, str);
        }

        public string Receive(string maxo, string mino)
        {
            int max = 0;
            int min = 0;
            int.TryParse(maxo, out max);
            int.TryParse(mino, out min);
            VAObjectHub.Instance.CVCoreLibrary.MaxPerson = max;
            VAObjectHub.Instance.CVCoreLibrary.MinPerson = min;
            MyHubObject.MaxPerson = max;
            MyHubObject.MinPerson = min;

            return "success";
        }
    }
}