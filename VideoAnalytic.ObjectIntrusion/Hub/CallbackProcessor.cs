﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Common;
using System.Data;
using System.Data.SqlClient;
using uPLibrary.Networking.M2Mqtt;

namespace VideoAnalytic.ObjectIntrusion
{
    public class CallbackProcessor
    {
        private static MqttClient mqttClient;

        private static object InstanceLock = new object();
        private static CallbackProcessor _Instance;

        SqlQueryBuilder<HVAAlert> alertDal = new SqlQueryBuilder<HVAAlert>();
        SqlQueryBuilder<Callback> callbackDal = new SqlQueryBuilder<Callback>();
        public object UnprocessedLock = new object();
        public object ProcessingLock = new object();

        public bool IsAdding = false;
        public bool IsProcess = false;
        public Thread AddingThread = null;
        public Thread ProcessThread = null;
        public List<Callback> Unprocessed = new List<Callback>();
        public List<Callback> Processing = new List<Callback>();
        public DateTime StartAt;
        DateTime LastProcess = DateTime.Now;

        public delegate void ImageReceived(HVAAlert alert);
        public event ImageReceived OnImageReceived;

        private CallbackProcessor()
        {

        }

        public static CallbackProcessor Instance
        {
            get
            {
                lock (InstanceLock)
                {
                    lock (InstanceLock)
                    {
                        if (_Instance == null)
                            _Instance = new CallbackProcessor();
                        return _Instance;
                    }
                }
            }
        }

        public void StartAdding()
        {
            IsAdding = true;
            AddingThread = new Thread(StartAddingReal);
            AddingThread.Start();
        }

        public void StartProcess(DateTime dt)
        {
            if (dt != DateTime.MinValue) StartAt = dt;

            IsProcess = true;
            ProcessThread = new Thread(StartProcessReal);
            ProcessThread.Start();
        }

        public void StartAll(DateTime dt)
        {
            StartAdding();
            StartProcess(dt);
        }

        public void StopAll()
        {
            StopAdding();
            StopProcess();
            CVCoreLib.CVCoreConfig _Config = new CVCoreLib.CVCoreConfig();
            _Config.SetSetting("LastProcessDate", LastProcess.ToString("yyyy MMM dd HH:mm:ss"));
        }

        public void StopThread(Thread t)
        {
            if (t != null)
            {
                try
                {
                    t.Abort();
                }
                catch { }
            }
        }

        public void StopAdding()
        {
            IsAdding = false;
            StopThread(AddingThread);
            AddingThread = null;
        }

        public void StopProcess()
        {
            IsProcess = false;
            StopThread(ProcessThread);
            ProcessThread = null;
        }

        public bool IsExist(Callback cb)
        {
            HVAAlert al = alertDal.GetByMany(" AND ", new string[] { "DataID" }, new object[] { cb.DataID });
            if (al == null)
            {
                if (Unprocessed != null)
                {
                    Callback first = Unprocessed.Where(m => m.Id == cb.Id).FirstOrDefault();
                    if (first == null) return false;
                    return true;
                }
                if (Processing != null)
                {
                    Callback first = Processing.Where(m => m.Id == cb.Id).FirstOrDefault();
                    if (first == null) return false;
                    return true;
                }
            }
            return true;
        }

        public void StartAddingReal()
        {
            
            Callback max = callbackDal.ExecuteSingle("SELECT MAX(UpdateDate) AS UpdateDate FROM Callback", typeof(Callback), new List<System.Data.IDataParameter>());

            if (max != null) LastProcess = max.UpdateDate;
            CVCoreLib.CVCoreConfig _Config = new CVCoreLib.CVCoreConfig();
            DateTime d = _Config.GetLastProcessDate();
            if (d != DateTime.MinValue) LastProcess = d;
            if (StartAt != DateTime.MinValue) LastProcess = StartAt;

            while (IsAdding)
            {
                try
                {
                    List<IDataParameter> prms = new List<IDataParameter>();
                    prms.Add(new SqlParameter("@UpdateDate", LastProcess));

                    List<Callback> maxes = callbackDal.ExecuteList("SELECT * FROM Callback WHERE UpdateDate>@UpdateDate", typeof(Callback), prms, true);
                    if (maxes != null && maxes.Count > 0)
                    {
                        maxes = maxes.OrderByDescending(m => m.UpdateDate).ToList(); ;
                        LastProcess = maxes[0].UpdateDate;

                        foreach (Callback cb in maxes)
                        {
                            if (IsExist(cb) == false)
                            {
                                lock (UnprocessedLock)
                                {
                                    Unprocessed.Add(cb);
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    LibraryTools.WriteName("CallbackProcessor_StartAddingReal", ex);
                }
                Thread.Sleep(500);
            }
        }

        public T GetData<T>(string str, string start, string end)
        {
            if (typeof(T) == typeof(DateTime))
            {
                string[] strs = CommonStringOps.TagMatch(str, start, end);
                if (strs == null || strs.Length == 0) return (T)Convert.ChangeType(DateTime.MinValue, typeof(T));

                string strx = strs[0];
                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(strx, out dt);
                return (T)Convert.ChangeType(dt, typeof(T));
            }

            if (typeof(T) == typeof(string))
            {
                string[] strs = CommonStringOps.TagMatch(str, start, end);
                if (strs == null || strs.Length == 0) return (T)Convert.ChangeType("", typeof(T));
                return (T)Convert.ChangeType(strs[0], typeof(T));
            }
            return default(T);
        }

        public void StartProcessReal()
        {
            while (IsProcess)
            {
                try
                {
                    CVCoreLib.CVCoreConfig _Config = new CVCoreLib.CVCoreConfig();
                    mqttClient = new MqttClient(_Config.GetMqttServer());
                    mqttClient.Subscribe(new string[1] { "VideoAnalytic/HVAAlert" }, new byte[1] { 2 });
                    mqttClient.Connect(Guid.NewGuid().ToString());
                    LibraryTools.WriteName("CallbackProcessor_StartProcessReal_MQTT", "Successfully connecteed to MQTT Server " + _Config.GetMqttServer());
                    break;
                }
                catch (Exception ex)
                {
                    LibraryTools.WriteName("CallbackProcessor_StartProcessReal_MQTT", "Unable to start processing until MQTT is connected", ex);
                    CVCoreLib.CVCoreConfig _Config = new CVCoreLib.CVCoreConfig();
                    if (_Config.GetIgnoreMqtt())
                    {
                        LibraryTools.WriteName("CallbackProcessor_StartProcessReal_MQTT", "Starting processing HVAAlert without publishing to MQTT");
                        break;
                    }
                }
                Thread.Sleep(500);
            }

            while(IsProcess)
            {
                try
                {
                    Callback cb = null;
                    if (Unprocessed != null && Unprocessed.Count > 0)
                    {
                        lock (UnprocessedLock)
                        {
                            cb = Unprocessed[0];
                            Unprocessed.RemoveAt(0);
                        }
                    }
                    
                    if (cb != null && cb.Filename.ToLower().EndsWith(".jpg"))
                    {
                        
                        HVAAlert al = alertDal.GetByMany(" AND ", new string[] { "DataID" }, new object[] { cb.DataID });
                        if (al == null)
                        {
                            lock (UnprocessedLock)
                                Unprocessed.Add(cb);
                            continue;
                        }

                        try
                        {
                            SqlRFID_ALERTDAL rfidAlertDal = new SqlRFID_ALERTDAL();
                            RFID_ALERT rfid = rfidAlertDal.GetByMany(new string[] { "DataID" }, new object[] { al.DataID });
                            if (rfid == null)
                            {
                                lock (UnprocessedLock)
                                    Unprocessed.Add(cb);
                                continue;
                            }
                            if (rfid != null)
                            {
                                rfid.Image = cb.Base64orData;
                                rfidAlertDal.Update(rfid);
                            }
                        }
                        catch { }

                        al.Image = cb.Base64orData;
                        alertDal.Update(al);

                        if (OnImageReceived != null)
                        {
                            try
                            {
                                OnImageReceived(al);
                            }
                            catch { }
                        }

                       
                    }
                    else if (cb != null)
                    {
                        HVAAlert al = new HVAAlert();
                        al.DataID = cb.DataID;
                        al.CreateOfficer = "SYSTEM";
                        al.UpdateOfficer = al.CreateOfficer;
                        al.CreateDate = DateTime.Now;
                        al.UpdateDate = al.CreateDate;

                        al.FrameTime = GetData<DateTime>(cb.Base64orData, "\"FrameTime\"    : \"", "\"");
                        al.InstanceType = GetData<string>(cb.Base64orData, "\"InstanceType\" : \"", "\"");
                        al.InstanceName = GetData<string>(cb.Base64orData, "\"InstanceName\" : \"", "\"");
                        al.SubName = GetData<string>(cb.Base64orData, "\"SubName\"      : \"", "\"");

                        if (al.InstanceType == "") continue;

                        alertDal.Insert(al);

                        RFID_ALERT rfid = new RFID_ALERT();
                        rfid.AssetName = al.InstanceType;
                        rfid.AlertDate = DateTime.Now;
                        rfid.CREATED_DT = al.FrameTime;
                        rfid.Description = al.InstanceType + " on type " + al.InstanceName;
                        rfid.Epc = "";
                        rfid.UpdateDate = al.UpdateDate;
                        rfid.UpdateOfficer = al.UpdateOfficer;
                        rfid.CreateDate = al.CreateDate;
                        rfid.CreateOfficer = al.CreateOfficer;
                        rfid.ZoneID = al.SubName;
                        rfid.ZoneName = al.SubName;
                        rfid.DataID = al.DataID;

                        SqlRFID_ALERTDAL rfidAlertDal = new SqlRFID_ALERTDAL();
                        rfidAlertDal.Insert(rfid);

                        AlertEvent ae = new AlertEvent();
                        ae.Alert = al;
                        ae.Callbacks = callbackDal.GetManyByMany(" AND ", new string[] { "DataID" }, new object[] { al.DataID });

                        if (mqttClient != null && mqttClient.IsConnected)
                        {
                            try
                            {
                                string msg = Newtonsoft.Json.JsonConvert.SerializeObject(ae);
                                byte[] aeb = System.Text.Encoding.ASCII.GetBytes(msg);
                                mqttClient.Publish("VideoAnalytic/HVAAlert", aeb);
                            }
                            catch(Exception ex)
                            {
                                LibraryTools.WriteName("CallbackProcessor_StartProcessReal_MQTT", "MQTT get disconnected", ex);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    LibraryTools.WriteName("CallbackProcessor_StartProcessReal", ex);
                }
                Thread.Sleep(100);
            }
        }
    }
}
