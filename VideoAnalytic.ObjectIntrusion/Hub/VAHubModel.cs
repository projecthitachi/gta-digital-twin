﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace VideoAnalytic.ObjectIntrusion
{
    public class VAHubModel
    {
        public List<string> AlertList { get; set; }
        public List<VA_ALERT> AlertListObject { get; set; }

        public int AlertCount { get; set; }
        public List<string> DetectedImage { get; set; }
        public string CVCoreImage { get; set; }
        public int PeopleCount { get; set; }
        public int MaxPerson { get; set; }
        public int MinPerson { get; set; }
        public string LastEventID { get; set; }
    }
}
