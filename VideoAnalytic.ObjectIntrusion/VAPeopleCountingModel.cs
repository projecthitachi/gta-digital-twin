﻿// Bacnet.Raspberry.Models.RaspberryModel
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using VideoAnalytic.PeopleCounting.Models;
using VideoAnalytic.PeopleCounting.Models.System;

namespace VideoAnalytic.PeopleCounting.Models
{
    public class VAPeopleCountingModel
    {
        private string t70310fieldselect = "\r\n            t70310r001 as RecordID,\r\n            t70310r002 as RecordTimestamp,\r\n            t70310r003 as RecordStatus,\r\n            t70310f001 as DeviceID,\r\n            t70310f002 as ObjectID,\r\n            t70310f003 as Payload";

        private string t70211fieldselect = "\r\n            t70211r001 as RecordID,\r\n            t70211r002 as RecordTimestamp,\r\n            t70211r003 as RecordStatus,\r\n            t70211f001 as DeviceID,\r\n            t70211f002 as ObjectName,\r\n            t70211f003 as Instance";


        public int ErrorNo
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }

        public VAPeopleCountingModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }

        public DbRawSqlQuery<VAPeopleCountingDataLog_REC> GetListDataLog(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70310fieldselect + " FROM t70310 ORDER BY t70310r001 DESC;";
            if (DeviceID != null)
            {
                sSQL = "SELECT " + t70310fieldselect + " FROM t70310 WHERE t70310f001 = '" + DeviceID + "' ORDER BY t70310r001 DESC;";
            }
            return oRemoteDB.Database.SqlQuery<VAPeopleCountingDataLog_REC>(sSQL, Array.Empty<object>());
        }

        public DbRawSqlQuery<VAPeopleCountingDataLog_REC> GetListDataLog()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70310fieldselect + " FROM t70310 ORDER BY t70310r001 DESC;";
            return oRemoteDB.Database.SqlQuery<VAPeopleCountingDataLog_REC>(sSQL, Array.Empty<object>());
        }

        public bool DataLogInsert(DatabaseContext oRemoteDB, VAPeopleCountingDataLog_REC poRecord)
        {
            bool bReturn = true;
            try
            {
                poRecord.RecordID = 0L;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                string sSQL = "INSERT INTO t70310       (       t70310r002, t70310r003,       t70310f001, t70310f002, t70310f003)       VALUES       (      @pt70310r002, @pt70310r003,       @pt70310f001, @pt70310f002, @pt70310f003      );SELECT @pt70310r001 = SCOPE_IDENTITY(); ";
                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter
                {
                    ParameterName = "@pt70310r001",
                    SqlDbType = SqlDbType.BigInt,
                    Direction = ParameterDirection.Output
                });
                oParameters.Add(new SqlParameter("@pt70310r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70310r003", poRecord.RecordStatus));
                oParameters.Add(new SqlParameter("@pt70310f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70310f002", poRecord.ObjectID));
                oParameters.Add(new SqlParameter("@pt70310f003", poRecord.Payload));
                SqlParameter[] vSqlParameter = oParameters.ToArray();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            return bReturn;
        }

        public DbRawSqlQuery<VAPeopleCounting_REC> GetListDataObject(string DeviceID)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = "SELECT " + t70211fieldselect + " FROM t70211 WHERE t70211f001 = '" + DeviceID + "' ORDER BY t70211r001 DESC;";
            return oRemoteDB.Database.SqlQuery<VAPeopleCounting_REC>(sSQL, Array.Empty<object>());
        }

        public bool ObjectInsert(DatabaseContext oRemoteDB, VAPeopleCountingObject_REC poRecord)
        {
            bool bReturn = true;
            try
            {
                poRecord.RecordID = 0L;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                string sSQL = "INSERT INTO t70211       (       t70211r002, t70211r003,       t70211f001, t70211f002, t70211f003)       VALUES       (      @pt70211r002, @pt70211r003,       @pt70211f001, @pt70211f002, @pt70211f003      );SELECT @pt70211r001 = SCOPE_IDENTITY(); ";
                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter
                {
                    ParameterName = "@pt70211r001",
                    SqlDbType = SqlDbType.BigInt,
                    Direction = ParameterDirection.Output
                });
                oParameters.Add(new SqlParameter("@pt70211r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70211r003", poRecord.RecordStatus));
                oParameters.Add(new SqlParameter("@pt70211f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70211f002", poRecord.ObjectName));
                oParameters.Add(new SqlParameter("@pt70211f003", poRecord.Instance));
                SqlParameter[] vSqlParameter = oParameters.ToArray();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            return bReturn;
        }

        public bool ObjectDelete(DatabaseContext oRemoteDB, VAPeopleCountingObject_REC poRecord)
        {
            bool bReturn = true;
            string sSQL = "DELETE FROM t70211    WHERE (t70211f001 = @pt70211f001) ;";
            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70211f001", poRecord.DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn != 1)
            {
                ErrorMessage = "Failed to delete record!<br>Record has been deleted by another Logging.";
                bReturn = false;
            }
            return bReturn;
        }

        public DbRawSqlQuery<VAPeopleCounting_REC> GetSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT tb.t70200f005 AS DeviceID FROM t70100 ta JOIN t70200 tb ON ta.t70100f001 = tb.t70200f001 WHERE ta.t70100f001 = 'Bacnet';";
            return oRemoteDB.Database.SqlQuery<VAPeopleCounting_REC>(sSQL, Array.Empty<object>());
        }

        public DbRawSqlQuery<VAPeopleCounting_REC> GetLastSerialNo()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT TOP 1 t70210f001 AS DeviceID FROM t70210 ORDER BY t70210r001 DESC;";
            return oRemoteDB.Database.SqlQuery<VAPeopleCounting_REC>(sSQL, Array.Empty<object>());
        }

        public DbRawSqlQuery<VAPeopleCounting_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT t70210r001 AS RecordID, t70210r002 AS RecordTimestamp, t70210r003 AS RecordStatus, t70210f001 AS DeviceID, t70210f002 AS DeviceName, t70210f003 AS CategoryID, t70210f004 AS IPAddress, t70210f005 AS MacAddress, t70210f006 AS SSID, t70210f007 AS Password, t70210f008 AS MQTTHost, t70210f009 AS MQTTUsername, t70210f010 AS MQTTPassword, t70210f011 AS StatusDevice, t70210f012 AS StatusSwitch, t70210f013 AS Live FROM dbo.t70210 ORDER BY t70210r001 DESC;";
            return oRemoteDB.Database.SqlQuery<VAPeopleCounting_REC>(sSQL, Array.Empty<object>());
        }

        public bool Insert(DatabaseContext oRemoteDB, VAPeopleCounting_REC poRecord)
        {
            bool bReturn = true;
            try
            {
                poRecord.RecordID = 0L;
                poRecord.RecordTimestamp = DateTime.Now;
                poRecord.RecordStatus = 0;
                if (GetLastSerialNo().ToList().Count == 0)
                {
                    poRecord.DeviceID = GetSerialNo().SingleOrDefault().DeviceID + ".0000.0001";
                }
                else
                {
                    string[] serialNo = GetLastSerialNo().SingleOrDefault().DeviceID.Split('.');
                    int lastNo = Convert.ToInt32(serialNo[3]) + 1;
                    poRecord.DeviceID = serialNo[0] + "." + serialNo[1] + "." + serialNo[2] + "." + lastNo.ToString("D4");
                }
                GlobalFunction.TrimNull(poRecord);
                string sSQL = "INSERT INTO t70210       (       t70210r002, t70210r003,       t70210f001, t70210f002, t70210f003, t70210f004, t70210f005, t70210f006, t70210f007, t70210f008, t70210f009, t70210f010, t70210f011, t70210f012, t70210f013       )       VALUES       (      @pt70210r002, @pt70210r003,       @pt70210f001, @pt70210f002, @pt70210f003, @pt70210f004, @pt70210f005, @pt70210f006, @pt70210f007, @pt70210f008, @pt70210f009, @pt70210f010, @pt70210f011, @pt70210f012, @pt70210f013       );SELECT @pt70210r001 = SCOPE_IDENTITY(); ";
                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter
                {
                    ParameterName = "@pt70210r001",
                    SqlDbType = SqlDbType.BigInt,
                    Direction = ParameterDirection.Output
                });
                oParameters.Add(new SqlParameter("@pt70210r002", poRecord.RecordTimestamp));
                oParameters.Add(new SqlParameter("@pt70210r003", poRecord.RecordStatus));
                oParameters.Add(new SqlParameter("@pt70210f001", poRecord.DeviceID));
                oParameters.Add(new SqlParameter("@pt70210f002", poRecord.DeviceName));
                oParameters.Add(new SqlParameter("@pt70210f003", poRecord.CategoryID));
                oParameters.Add(new SqlParameter("@pt70210f004", poRecord.IPAddress));
                oParameters.Add(new SqlParameter("@pt70210f005", poRecord.MacAddress));
                oParameters.Add(new SqlParameter("@pt70210f006", poRecord.SSID));
                oParameters.Add(new SqlParameter("@pt70210f007", poRecord.Password));
                oParameters.Add(new SqlParameter("@pt70210f008", poRecord.MQTTHost));
                oParameters.Add(new SqlParameter("@pt70210f009", poRecord.MQTTUsername));
                oParameters.Add(new SqlParameter("@pt70210f010", poRecord.MQTTPassword));
                oParameters.Add(new SqlParameter("@pt70210f011", poRecord.StatusDevice));
                oParameters.Add(new SqlParameter("@pt70210f012", poRecord.StatusSwitch));
                oParameters.Add(new SqlParameter("@pt70210f013", poRecord.Live));
                SqlParameter[] vSqlParameter = oParameters.ToArray();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
                if (nReturn == 1)
                {
                    poRecord.RecordID = Convert.ToInt64(vSqlParameter[0].Value);
                }
                else
                {
                    ErrorMessage = "Failed to insert record!";
                    bReturn = false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                bReturn = false;
            }
            return bReturn;
        }

        public bool Update(DatabaseContext oRemoteDB, VAPeopleCounting_REC poRecord)
        {
            bool bReturn2 = true;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "UPDATE t70210    SET       t70210f002 = @pt70210f002,       t70210f003 = @pt70210f003,       t70210f004 = @pt70210f004,       t70210f005 = @pt70210f005,       t70210f006 = @pt70210f006,       t70210f007 = @pt70210f007,       t70210f008 = @pt70210f008,       t70210f009 = @pt70210f009,       t70210f010 = @pt70210f010,       t70210f011 = @pt70210f011,       t70210f012 = @pt70210f012,       t70210f013 = @pt70210f013    WHERE (t70210f001 = @pt70210f001) ;";
            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70210f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70210f002", poRecord.DeviceName));
            oParameters.Add(new SqlParameter("@pt70210f003", poRecord.CategoryID));
            oParameters.Add(new SqlParameter("@pt70210f004", poRecord.IPAddress));
            oParameters.Add(new SqlParameter("@pt70210f005", poRecord.MacAddress));
            oParameters.Add(new SqlParameter("@pt70210f006", poRecord.SSID));
            oParameters.Add(new SqlParameter("@pt70210f007", poRecord.Password));
            oParameters.Add(new SqlParameter("@pt70210f008", poRecord.MQTTHost));
            oParameters.Add(new SqlParameter("@pt70210f009", poRecord.MQTTUsername));
            oParameters.Add(new SqlParameter("@pt70210f010", poRecord.MQTTPassword));
            oParameters.Add(new SqlParameter("@pt70210f011", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70210f012", poRecord.StatusSwitch));
            oParameters.Add(new SqlParameter("@pt70210f013", poRecord.Live));
            SqlParameter[] vSqlParameter = oParameters.ToArray();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn != 1)
            {
                ErrorMessage = "Failed to update record!<br>Record has been updated or deleted by another User.";
                bReturn2 = false;
                throw new Exception(ErrorMessage);
            }
            return bReturn2;
        }

        public bool UpdateStatus(DatabaseContext oRemoteDB, VAPeopleCounting_REC poRecord)
        {
            bool bReturn2 = true;
            poRecord.RecordTimestamp = DateTime.Now;
            poRecord.RecordStatus = 0;
            GlobalFunction.TrimNull(poRecord);
            string sSQL = "UPDATE t70210    SET       t70210f011 = @pt70210f011,       t70210f012 = @pt70210f012    WHERE (t70210f001 = @pt70210f001) ;";
            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70210f001", poRecord.DeviceID));
            oParameters.Add(new SqlParameter("@pt70210f011", poRecord.StatusDevice));
            oParameters.Add(new SqlParameter("@pt70210f012", poRecord.StatusSwitch));
            SqlParameter[] vSqlParameter = oParameters.ToArray();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            if (nReturn != 1)
            {
                ErrorMessage = "Failed to update record!<br>Record has been updated or deleted by another User.";
                bReturn2 = false;
                throw new Exception(ErrorMessage);
            }
            return bReturn2;
        }

        public bool Delete(DatabaseContext oRemoteDB, VAPeopleCounting_REC poRecord)
        {
            bool bReturn = true;
            try
            {
                string sSQL = "DELETE FROM t70210    WHERE (t70210f001 = @pt70210f001) ;";
                List<SqlParameter> oParameters = new List<SqlParameter>();
                oParameters.Add(new SqlParameter("@pt70210f001", poRecord.DeviceID));
                SqlParameter[] vSqlParameter = oParameters.ToArray();
                int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            }
            catch (Exception)
            {
                bReturn = false;
            }
            return bReturn;
        }

        public bool updateStatusObj(DatabaseContext oRemoteDB, string RecordID, string DeviceID)
        {
            bool bReturn = true;
            string sSQL = " UPDATE t70211 SET t70211r003 = 0 WHERE t70211f001 = @pt70210r001; ";
            if (RecordID != null)
            {
                sSQL = sSQL + " UPDATE t70211 SET t70211r003 = 1 WHERE t70211f001 = @pt70210r001 AND t70211r001 IN(" + RecordID + ");";
            }
            List<SqlParameter> oParameters = new List<SqlParameter>();
            oParameters.Add(new SqlParameter("@pt70210r001", DeviceID));
            SqlParameter[] vSqlParameter = oParameters.ToArray();
            int nReturn = oRemoteDB.Database.ExecuteSqlCommand(sSQL, vSqlParameter);
            return bReturn;
        }
    }
}