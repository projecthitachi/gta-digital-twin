﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using has.app.Components;
using has.app.Models;
using Microsoft.AspNet.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Web.Hosting;

namespace has.app.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult ResetInit()
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            Constants.init = true;
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetmenuActive(string menu)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            Constants.modulActive = menu;
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DummyData(int startM, int endM)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            //Dictionary<int, int> dictionary = new Dictionary<int,int> { { 1, 31 }, { 2, 28 }, { 3, 31 }, { 4, 30 }, { 5, 31 }, { 6, 30 }, { 7, 31 }, { 8, 31 }, { 9, 30 }, { 10, 31 }, { 11, 30 }, { 12, 31 } };

            //var database = Constants.client.GetDatabase("IoT");
            //var DataLogs = database.GetCollection<BacnetNew_REC>("LightingDataLogs");

            //for (int m = startM; m <= endM; m++)
            //{
            //    int dl = dictionary[m];
            //    if (m == 8) { dl = 23; }
            //    for (int d = 1; d <= dl; d++)
            //    {
            //        for (int h = 7; h <= 19; h++)
            //        {
            //            string objName = "DB-T2-7-1-P-Energy-Out";
            //            string Instance = "25";
            //            BacnetNew_REC voBacnet = new BacnetNew_REC();
            //            string iString = "2019-" + m.ToString("D2") + "-" + d.ToString("D2") + " "+ h.ToString("D2") + ":00:02";
            //            DateTime oDate = DateTime.ParseExact(iString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            //            Random random = new Random();
            //            int PresentValue = random.Next(20, 100);
            //            if (oDate.ToString("dddd") == "Sunday" || oDate.ToString("dddd") == "Saturday") {
            //                PresentValue = random.Next(20, 100) * 30 / 100;
            //            } 

            //            voBacnet.RecordTimestamp = oDate;
            //            voBacnet.DeviceID = "200007";
            //            voBacnet.DeviceName = "MACH-Pro";
            //            voBacnet.ObjectName = objName;
            //            voBacnet.PresentValue = PresentValue;
            //            voBacnet.InstanceID = Instance;
            //            voBacnet.Units = "Kwh";

            //            DataLogs.InsertOne(voBacnet);
            //            Thread.Sleep(100);
            //        }
            //    }
            //}

            //ResultData.Add("msg", "Success");
            //ResultData.Add("errorcode", "0");
            //ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getSmartDevices()
        {
            var database = Constants.client.GetDatabase("IoT");
            var CollectDevice = database.GetCollection<SmartDevices_REC>("SmartDevices");

            var res = CollectDevice.Find(_ => true).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getPowerMesh()
        {
            var database = Constants.client.GetDatabase("IoT");
            var CollectDevice = database.GetCollection<PowerMesh_REC>("PowerMesh");

            var res = CollectDevice.Find(_ => true).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getVAV()
        {
            var database = Constants.client.GetDatabase("IoT");
            var CollectDevice = database.GetCollection<AirconDataVAV_REC>("AirCondDataVAV");

            var res = CollectDevice.Find(_ => true).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getDataDummy(string StartDate, string EndDate )
        {
            //var database = Constants.client.GetDatabase("IoT");
            //var collection = database.GetCollection<DPC_Chart_REC>("LightingDataLogs");

            //var pipline = "{ $group: { _id: { day: { $dayOfMonth: \"$RecordTimestamp\"}, month: { $month: \"$RecordTimestamp\"}, year: { $year: \"$RecordTimestamp\" } },  Current: { $sum: \"$PresentValue\" }} }";
            //var pipeline = collection.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            //var result = pipeline.ToList().Where(w => w._id.month == 8).OrderBy(o => o.Day);
            var result = new Dictionary<string, string>();

            //string iString = "2019-08-23 07:00:02";
            //string iString2 = "2019-08-23 19:00:02";
            //DateTime GteDate = DateTime.ParseExact(StartDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            //DateTime LTDate = DateTime.ParseExact(EndDate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            //var builder = Builders<BacnetNewView_REC>.Filter;
            //var filter = builder.And(builder.Gte<DateTime>("RecordTimestamp", GteDate), builder.Lt<DateTime>("RecordTimestamp", LTDate));
            //List<BacnetNewView_REC> result = collection.Find(filter).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MqttTest()
        {
            return View();
        }
        public ActionResult Component()
        {
            return View();
        }
        public ActionResult PublishToBroker(string topic,string msg)
        {
            //LogPublish publishToBroker("Curtain/Bacnet/Write", "{'deviceID':'127001','ObjectType':'OBJECT_BINARY_VALUE:8193','value':'0'}");
            Dictionary<string, string> ResultData = new Dictionary<string, string>();

            if (topic == "Curtain/Bacnet/Write")
            {

                var database = Constants.client.GetDatabase("IoT");
                var DataLogs = database.GetCollection<CurtainLogService>("CurtainLogService");
                JObject voobj = JObject.Parse(msg);

                int DeviceID = Convert.ToInt32(voobj["deviceID"].ToString());
                string[] poObj = voobj["ObjectType"].ToString().Split(':');

                CurtainLogService voLog = new CurtainLogService();
                voLog.RecordTimestamp = DateTime.Now;
                voLog.DeviceID = DeviceID.ToString();
                voLog.ObjectName = poObj[0].ToString();
                voLog.PresentValue =voobj["value"].ToString();
                voLog.InstanceID = poObj[1];
                voLog.Status = "Before Publish";
                voLog.ObjectTypeID = Convert.ToInt32(poObj[1].ToString());
                voLog.ObjectType = poObj[0];
                DataLogs.InsertOne(voLog);

                string strValue = Convert.ToString(msg);
                Constants.MqttClient.Publish(topic, Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                voLog.RecordTimestamp = DateTime.Now;
                voLog.Status = "After Publish";
                DataLogs.InsertOne(voLog);
            }
            else
            {
                string strValue = Convert.ToString(msg);
                Constants.MqttClient.Publish(topic, Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }

            

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetLocation()
        {
            LocationModel oModel = new LocationModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<Location_REC> oTable = oModel.GetListOrderByType(oRemoteDB).ToList();

            var jsonResult = Json(oTable);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GetAntennaLocation()
        {
            LocationModel oModel = new LocationModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<Location_REC> oTable = oModel.GetAntennaLocation(oRemoteDB).ToList();

            var jsonResult = Json(oTable);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        #region Handler
        public static void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string[] topic = e.Topic.Split('/');
            string Category = topic[0];
            if (topic.Length > 3)
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                if (Constants.IsValidJson(message))
                {
                    switch (Category)
                    {
                        case "Elevator":
                            //ElevatorHandler(message, topic);
                            var Elevator = new Thread(() => ElevatorHandler(message, topic));
                            Elevator.Start();
                            break;
                        case "M2L":
                            //ElevatorHandlerLive(message, topic);
                            var M2L = new Thread(() => ElevatorHandler(message, topic));
                            M2L.Start();
                            break;
                        case "L2M":
                            //ElevatorHandlerLive(message, topic);
                            var L2M = new Thread(() => ElevatorHandlerLive(message, topic));
                            L2M.Start();
                            break;
                        case "RFID":
                            //RFIDHandler(message, topic);
                            var RFID = new Thread(() => RFIDHandler(message, topic));
                            RFID.Start();
                            break;
                        case "mobileapp":
                            //MobileAppHandler(message, topic);
                            var MB = new Thread(() => MobileAppHandler(message, topic));
                            MB.Start();
                            break;
                        case "Aircond":
                            var AC = new Thread(() => AirconHandler(message, topic));
                            AC.Start();
                            break;
                        default:
                            //Console.WriteLine("Default case");
                            break;
                    }
                }
            }
            else if (topic.Length == 2)
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                if (Constants.IsValidJson(message))
                {
                    switch (Category)
                    {
                        case "WidgetRealtime":
                            //WidgetRealtime(message, topic);
                            var Wr = new Thread(() => WidgetRealtime(message, topic));
                            Wr.Start();
                            break;
                        default:

                            break;
                    }
                }
            }
        }
        public static void MobileAppHandler(string message, string[] topic)
        {
            switch (topic[2])
            {
                case "alert":
                    Constants.RT.Clients.All.Alert("WidgetAlert", message);
                    break;
                default:

                    break;
            }
        }
        public static void WidgetRealtime(string message, string[] topic)
        {
            switch (topic[1])
            {
                case "Smartlight":
                    if (Constants.modulActive == "SmartLight")
                    {
                        Constants.RT.Clients.All.Smartlight("WidgetRealtime", message);
                    }
                    break;
                case "SmartlightDevice":
                    if (Constants.modulActive == "SmartLight")
                    {
                        Constants.RT.Clients.All.SmartlightDevice("WidgetRealtime", message);
                    }
                    break;
                case "SmartlightSwitch":
                    if (Constants.modulActive == "SmartLight")
                    {
                        Constants.RT.Clients.All.SmartlightSwitch("WidgetRealtime", message);
                    }
                    break;
                case "Smartplug":
                    if (Constants.modulActive == "SmartPlug")
                    {
                        Constants.RT.Clients.All.Smartplug("WidgetRealtime", message);
                    }
                    break;
                case "SmartplugDevice":
                    if (Constants.modulActive == "SmartPlug")
                    {
                        Constants.RT.Clients.All.SmartplugDevice("WidgetRealtime", message);
                    }
                    break;
                case "SmartplugSwitch":
                    if (Constants.modulActive == "SmartPlug")
                    {
                        Constants.RT.Clients.All.SmartplugSwitch("WidgetRealtime", message);
                    }
                    break;
                case "Home":
                    if (Constants.modulActive == "Home")
                    {
                        Constants.RT.Clients.All.TempAndHumidity("HomeWidgetRealtime", message);
                    }
                    break;
                case "Power":
                    if (Constants.modulActive == "Power")
                    {
                        Constants.RT.Clients.All.TempAndHumidity("PowerWidgetRealtime", message);
                    }
                    break;
                case "Aircond":
                    if (Constants.modulActive == "AirCond")
                    {
                        Constants.RT.Clients.All.AircondWidget("WidgetRealtime", message);
                    }
                    break;
                case "AirCondStatus":
                    if (Constants.modulActive == "AirCond")
                    {
                        Constants.RT.Clients.All.AirCondStatus("AirCondStatus", message);
                    }
                    break;
                case "SensorWidget":
                    if (Constants.modulActive == "Sensor")
                    {
                        Constants.RT.Clients.All.SensorWidget("SensorWidget", message);
                    }
                    break;
                case "SensortempData":
                        Constants.RT.Clients.All.TempAndHumidity("tempData", message);
                    break;
                case "SensorhumidityData":
                        Constants.RT.Clients.All.TempAndHumidity("humidityData", message);
                    break;
                case "HumanPresent":
                    if (Constants.modulActive == "HumanPresence" || Constants.modulActive == "Home")
                    {
                        Constants.RT.Clients.All.HumanPresence("persondata", message);
                    }
                    break;
                default:

                    break;
            }
            
        }
        public static void AirconHandler(string message, string[] topic)
        {
            switch (topic[3])
            {
                case "WriteDatas":
                    object result = JsonConvert.DeserializeObject(message);
                    JObject voobj = JObject.Parse(result.ToString());
                    Constants.RT.Clients.All.AirconWrite("WriteDatas", voobj["value"].ToString());
                    break;
                default:

                    break;
            }
        }
        #endregion

        public static int IntervalCommand = Convert.ToInt32(WebConfigurationManager.AppSettings["interval_command"].ToString());
        public static int IntervalCommandLoad = Convert.ToInt32(WebConfigurationManager.AppSettings["interval_command_load"].ToString());
        static string logfilePath = HostingEnvironment.MapPath("~/Content/assets/json/log.txt");
        public static void ElevatorHandler(string message, string[] topic)
        {
            object result = JsonConvert.DeserializeObject(message);
            if (topic.Contains("AppDatas"))
            {
                JArray voobj = JArray.Parse(result.ToString());
                List<JObject> objL1 = new List<JObject>();
                List<JObject> objL2 = new List<JObject>();
                List<JObject> objL3 = new List<JObject>();
                List<JObject> objL4 = new List<JObject>();
                foreach (JObject obj in voobj)
                {
                    string current_lift = "";
                    if(obj["command"].ToString() == "up" || obj["command"].ToString() == "down")
                        current_lift = obj["value"][0].ToString();
                    else
                        current_lift = obj["value"].ToString();

                    if (current_lift == "1")
                        objL1.Add(obj);
                    if (current_lift == "2")
                        objL2.Add(obj);
                    if (current_lift == "3")
                        objL3.Add(obj);
                    if (current_lift == "4")
                        objL4.Add(obj);
                }
                if (objL1.Count > 0)
                {
                    Thread lift1 = new Thread(() => lift1Handler(objL1));
                    lift1.Start();
                }
                if (objL2.Count > 0)
                {
                    Thread lift2 = new Thread(() => lift2Handler(objL2));
                    lift2.Start();
                }
                if (objL3.Count > 0)
                {
                    Thread lift3 = new Thread(() => lift3Handler(objL3));
                    lift3.Start();
                }
                if (objL4.Count > 0)
                {
                    Thread lift4 = new Thread(() => lift4Handler(objL4));
                    lift4.Start();
                }
            }
        }
        public static void ElevatorHandlerLive(string message, string[] topic)
        {
            IMongoDatabase database = Constants.client.GetDatabase("IoT");
            IMongoCollection<ElevatorDataLog_REC> ElevatorDataLog = database.GetCollection<ElevatorDataLog_REC>("ElevatorDataLog");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            ElevatorQuery oQuery = new ElevatorQuery();

            string Category = topic[0]; //L2M = Lift to Mqtt, M2L = Mqtt to Lift
            if (Constants.IsValidJson(message))
            {
                var RT = GlobalHost.ConnectionManager.GetHubContext<AppHub>();
                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());

                string country = topic[1];
                string project_name = topic[2];
                string Table = topic[3];
                if (Category == "M2L")
                {
                    var rootresult = ElevatorDataObject.Find(new BsonDocument()).ToList();
                    if (rootresult.Count > 0)
                    {
                        if (voobj["command"] != null)
                        {
                            JArray voArray = JArray.Parse(voobj["command"].ToString());
                            string timestamp = voobj["timeStamp"].ToString();
                            for (int i = 0; i < voArray.Count; i++)
                            {
                                JObject poobj = JObject.Parse(voArray[i].ToString());
                                string a = poobj["a"].ToString();
                                int d = Convert.ToInt32(poobj["d"].ToString());

                                int intValueA = Convert.ToInt32(a);
                                string hexValueA = intValueA.ToString("X");
                                if (hexValueA.Length == 1) { hexValueA = hexValueA.PadLeft(2, '0'); }
                                string BinValueD = Constants.IntToBinaryString(d);

                                if (a == "510" && d == 255)//M2L
                                {

                                }
                                else if (a == "511" && d == 255)
                                {

                                }
                                else
                                {
                                    ElevatorObject_REC dataObj = rootresult.FindAll(f => f.Table == Table && f.Address == hexValueA).SingleOrDefault();
                                    if (dataObj.Type == "K")
                                    {
                                        BinValueD = BinValueD.PadLeft(8, '0');
                                        char[] array = BinValueD.ToCharArray();
                                        List<int> poFloor = new List<int>();
                                        int ta = 7;

                                        for (int t = 0; t < array.Length; t++)
                                        {
                                            int volift = t + 1;
                                            Elevator_REC voRec = oQuery.GetOne(volift.ToString()).SingleOrDefault();
                                            if (voRec != null)
                                            {
                                                int letter = Convert.ToInt32(array[ta].ToString());
                                                if (dataObj.Address == "20")
                                                {
                                                    string data_send = "{\"command\":\"ParkingOperation\",\"value\":[" + volift + "," + letter + "]}";
                                                    RT.Clients.All.TempAndHumidity("ElevatorDataLive", data_send);
                                                }
                                            }
                                            ta--;
                                        }
                                    } else if(dataObj.Type == "B")
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
                else if (Category == "L2M")//Subscribe
                {
                    var rootresult = ElevatorDataObject.Find(_ => true).ToList();
                    if (rootresult.Count > 0)
                    {
                        if (voobj["status"] != null)
                        {
                            JArray voArray = JArray.Parse(voobj["status"].ToString());
                            string timestamp = voobj["timeStamp"].ToString();
                            DateTime ts = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToDouble(timestamp));
                            DateTime ct = DateTime.Now.AddSeconds(Constants.ElevatorLimitTime * -1);
                            //Constants.writelog("=============================================");
                            //Constants.writelog(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " ts: " + ts.ToString("yyyy-MM-dd HH:mm:ss.fff") + " ct: " + ct.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            //Constants.writelog(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + message);
                            //Constants.writelog(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " (" + (ts > ct) + ") ");

                            //if (ts > ct)
                            //{
                                for (int i = 0; i < voArray.Count; i++)
                                {
                                    JObject poobj = JObject.Parse(voArray[i].ToString());
                                    string a = poobj["a"].ToString();
                                    int d = Convert.ToInt32(poobj["d"].ToString());
                                    if (a == "510" && d == 255)//M2L
                                    {

                                    }
                                    else if (a == "511" && d == 255)
                                    {

                                    }
                                    else
                                    {
                                        int intValueA = Convert.ToInt32(a);
                                        string hexValueA = intValueA.ToString("X");
                                        if (hexValueA.Length == 1) { hexValueA = hexValueA.PadLeft(2, '0'); }
                                        string BinValueD = Constants.IntToBinaryString(d);

                                        ElevatorObject_REC dataObj = rootresult.FindAll(f => f.Table == Table && f.Address == hexValueA).SingleOrDefault();
                                        if (dataObj != null)
                                        {
                                            if (dataObj.Type == "B")
                                            {
                                                List<string> CarPositionAddress = new List<string>() { "08", "09", "0A", "0B", "0C", "0D", "0E", "0F" };
                                                if (CarPositionAddress.Contains(dataObj.Address))
                                                {
                                                    int lift = CarPositionAddress.IndexOf(dataObj.Address) + 1;
                                                    string data_send = "{\"command\":\"CarPosition\",\"value\":[" + lift + "," + d + "]}";
                                                    RT.Clients.All.TempAndHumidity("ElevatorDataLive", data_send);
                                                }
                                            }
                                            else if (dataObj.Type == "K")
                                            {
                                                BinValueD = BinValueD.PadLeft(8, '0');
                                                char[] array = BinValueD.ToCharArray();
                                                List<int> poFloor = new List<int>();
                                                int ta = 7;

                                                for (int t = 0; t < array.Length; t++)
                                                {
                                                    int volift = t + 1;
                                                    Elevator_REC voRec = oQuery.GetOne(volift.ToString()).SingleOrDefault();
                                                    if (voRec != null)
                                                    {
                                                        int letter = Convert.ToInt32(array[ta].ToString());
                                                        if (dataObj.Address == "10")
                                                        {
                                                            string data_send = "{\"command\":\"Up\",\"value\":[" + volift + "," + letter + "]}";
                                                            RT.Clients.All.TempAndHumidity("ElevatorDataLive", data_send);
                                                        }
                                                        else if (dataObj.Address == "11")
                                                        {
                                                            string data_send = "{\"command\":\"Down\",\"value\":[" + volift + "," + letter + "]}";
                                                            RT.Clients.All.TempAndHumidity("ElevatorDataLive", data_send);
                                                        }
                                                        else if (dataObj.Address == "1C")
                                                        {
                                                            string data_send = "{\"command\":\"ElevatorRunning\",\"value\":[" + volift + "," + letter + "]}";
                                                            RT.Clients.All.TempAndHumidity("ElevatorDataLive", data_send);
                                                        }
                                                    }
                                                    ta--;
                                                }
                                            }
                                            else if (dataObj.Type == "F")
                                            {

                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                //}
                            }
                        }
                    }
                }
            }

        }
        public static void RFIDHandler(string message, string[] topic)
        {
            object result = JsonConvert.DeserializeObject(message);
            if (topic.Contains("AppDatas"))
            {
                if (Constants.modulActive == "HumanPresence")
                {
                    Constants.RT.Clients.All.TempAndHumidity("RFIDPresenceLocation", result);
                }
            }
        }
        public static void lift1Handler(List<JObject> objL1)
        {
            foreach (JObject obj in objL1)
            {
                Constants.RT.Clients.All.TempAndHumidity("ElevatorData", JsonConvert.SerializeObject(obj));
                int interval_status = obj["command"].ToString().Contains("load") ? IntervalCommandLoad : IntervalCommand;
                Thread.Sleep(interval_status);
            }
        }
        public static void lift2Handler(List<JObject> objL2)
        {
            foreach (JObject obj in objL2)
            {
                Constants.RT.Clients.All.TempAndHumidity("ElevatorData", JsonConvert.SerializeObject(obj));
                int interval_status = obj["command"].ToString().Contains("load") ? IntervalCommandLoad : IntervalCommand;
                Thread.Sleep(interval_status);
            }
        }
        public static void lift3Handler(List<JObject> objL3)
        {
            foreach (JObject obj in objL3)
            {
                Constants.RT.Clients.All.TempAndHumidity("ElevatorData", JsonConvert.SerializeObject(obj));
                int interval_status = obj["command"].ToString().Contains("load") ? IntervalCommandLoad : IntervalCommand;
                Thread.Sleep(interval_status);
            }
        }
        public static void lift4Handler(List<JObject> objL4)
        {
            foreach (JObject obj in objL4)
            {
                Constants.RT.Clients.All.TempAndHumidity("ElevatorData", JsonConvert.SerializeObject(obj));
                int interval_status = obj["command"].ToString().Contains("load") ? IntervalCommandLoad : IntervalCommand;
                Thread.Sleep(interval_status);
            }
        }

        public ActionResult DeviceCamera()
        {
            CameraModel oModel = new CameraModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<Camera_REC> oTable = oModel.GetListDeviceCamera(oRemoteDB).ToList();

            var jsonResult = Json(oTable);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult HumanPresenceDummy()
        {
            LocationModel oModel = new LocationModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<Location_REC> dTable = oModel.GetAntennaLocation(oRemoteDB).ToList();

            List<Object> oTable = new List<Object>();
            Random rnd = new Random();
            foreach (Location_REC temp in dTable)
            {
                int count = 0;
                string object_id = "";
                string idx = temp.LocationID.Substring((temp.LocationID.Length - 2), 2);
                if (temp.LocationID.StartsWith("001003"))
                {
                    count = rnd.Next(0, 6);
                    object_id = "f03_o-persong" + idx;
                }
                else if (temp.LocationID.StartsWith("001007"))
                {
                    if (temp.LocationID != "001007001")
                    {
                        count = rnd.Next(0, 6);
                        object_id = "f07_o-persong" + (Convert.ToInt32(idx) - 1).ToString("00");
                    }
                    else
                    {
                        count = rnd.Next(10, 120);
                        object_id = "f07_o-person" + idx;
                    }
                }

                oTable.Add(
                    new { ObjectID = object_id, LocationID = temp.LocationID, Count = count }
                );
            }

            var jsonResult = Json(oTable, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GenerateHuman(int startM, int endM)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            Dictionary<int, int> monthinYear = new Dictionary<int, int> { { 1, 31 }, { 2, 28 }, { 3, 31 }, { 4, 30 }, { 5, 31 }, { 6, 30 }, { 7, 31 }, { 8, 31 }, { 9, 30 }, { 10, 31 }, { 11, 30 }, { 12, 31 } };
            var listFl = new List<string> { "3rd Floor", "7th Floor"};
            var listThermal = new List<int> { 0, -1};
            var database = Constants.client.GetDatabase("IoT");
            var DataLogs = database.GetCollection<HumanResence_REC>("HumanPresenceLogs");

            for (int m = startM; m <= endM; m++)
            {
                int dl = monthinYear[m];
                if (m == 10) { dl = 22; }
                for (int d = 1; d <= dl; d++)
                {
                    for (int h = 1; h <= 23; h++)
                    {
                        for (int s = 1; s <= 59; s++)
                        {
                            HumanResence_REC voData = new HumanResence_REC();
                            string iString = "2019-" + m.ToString("D2") + "-" + d.ToString("D2") + " " + h.ToString("D2") + ":" + s.ToString("D2") + ":02";
                            DateTime oDate = DateTime.ParseExact(iString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                            Random random = new Random();
                            int PresentValue = random.Next(100, 150);
                            if (oDate.ToString("dddd") == "Sunday" || oDate.ToString("dddd") == "Saturday" || h < 8 || h > 19)
                            {
                                PresentValue = random.Next(100, 150) * 10 / 100;
                            }
                            int indexFl = random.Next(listFl.Count);
                            int indexTH = random.Next(listThermal.Count);
                            int indexTH2 = random.Next(listThermal.Count);

                            voData.recordTimestamp = oDate;
                            voData.uuid = "200007";
                            voData.location = listFl[indexFl];
                            voData.thermalSensation = listThermal[indexTH];
                            voData.thermalComfortSatisfaction = listThermal[indexTH2];
                            voData.totalPerson = PresentValue;

                            DataLogs.InsertOne(voData);
                            Thread.Sleep(100);
                        }
                    }
                }
            }

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Generate()
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            var database = Constants.client.GetDatabase("IoT");
            var DataLogs = database.GetCollection<HumanResenceHourly_REC>("HumanPresenceMonthlySum");

            for (int m = 1; m <= 12; m++)
            {
                Random random = new Random();
                int PresentValue = random.Next(3000, 4500);
                HumanResenceHourly_REC voData = new HumanResenceHourly_REC();
                voData.recordTimestamp = DateTime.Now;
                voData.category = m.ToString();
                voData.value = PresentValue;
                DataLogs.InsertOne(voData);
                Thread.Sleep(1000);
            }

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RunHuman(string start, string interval)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            bool opp = true;
            if (start == "0") { opp = false; }
            Constants.start = opp;
            if (opp) {
                int inv = Convert.ToInt32(interval);
                var runHuman = new Thread(() => runHumanService(inv));
                runHuman.Start();
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        private static void runHumanService(int interval)
        {
            while (Constants.start)
            {
                Random random = new Random();
                int PresentValue = random.Next(100, 150);
                Dictionary<string, int> ResultData = new Dictionary<string, int>();
                ResultData.Add("OccupantNumber", PresentValue);
                ResultData.Add("Hours", Convert.ToInt32(DateTime.Now.ToString("HH")));
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResultData));
                Constants.RT.Clients.All.OccupantNumber(strValue);

                var database = Constants.client.GetDatabase("IoT");
                var collecCount = database.GetCollection<HumanResenceHourly_REC>("HumanPresenceRealtime");
                int jam = Convert.ToInt32(DateTime.Now.ToString("HH"));
                HumanResenceHourly_REC res = collecCount.Find(x => x.category == jam.ToString()).SingleOrDefault();
                if (res != null)
                {
                    if (res.value == 0)
                    {
                        var builder = Builders<HumanResenceHourly_REC>.Filter;
                        var filter = builder.Eq<string>("category", res.category);
                        var update = Builders<HumanResenceHourly_REC>.Update.Set("value", PresentValue);
                        collecCount.UpdateOne(filter, update);
                        Constants.RT.Clients.All.UpdateHumanPresentRealtime(strValue);
                    }
                }
                Thread.Sleep(interval);
            }
        }
        public ActionResult ElevatorGetLastPosition()
        {
            var database = Constants.client.GetDatabase("IoT");
            var elevatorCollection = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            var filter_mongo = new BsonDocument()
            {
                {"Table", "G0053349"},
                {"a", new BsonDocument() {
                    { "$gte", 8 },
                    { "$lte", 11 }
                }},
                {"Type", "B" }
            };
            List<ElevatorObject_REC> elvList = elevatorCollection.Find(filter_mongo).ToList();
            return Json(elvList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCameraControl()
        {
            string jsonFilePath = HostingEnvironment.MapPath("~/Content/assets/digitaltwin/json/camera_control.json"); ;
            string json = System.IO.File.ReadAllText(jsonFilePath);
            return Content(json);
        }
        public class cameraControl
        {
            public virtual string name { get; set; }
            public virtual string camera { get; set; }
            public virtual string orbitControls { get; set; }
        }
        public ActionResult SetCameraControl(cameraControl voData)
        {
            string jsonFilePath = HostingEnvironment.MapPath("~/Content/assets/digitaltwin/json/camera_control.json"); ;
            string json = System.IO.File.ReadAllText(jsonFilePath);
            List<cameraControl> currentData = JsonConvert.DeserializeObject<List<cameraControl>>(json);
            int idx = currentData.FindIndex(w => w.name == voData.name);
            if(idx > -1)
            {
                currentData[idx].camera = voData.camera;
                currentData[idx].orbitControls = voData.orbitControls;
                System.IO.File.WriteAllText(jsonFilePath, JsonConvert.SerializeObject(currentData));
            }

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Commands(string cmd, string DeviceID, string ObjType, string value)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            string strValue = "{'deviceID':'" + DeviceID + "','ObjectType':'" + ObjType + "','value':'" + value + "'}";
            Constants.MqttClient.Publish("Aircond/Raspberry/" + cmd, Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
    }
}