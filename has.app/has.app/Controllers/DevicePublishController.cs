﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using has.app.Components;
using has.app.Models;
using Microsoft.AspNet.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Web.Hosting;

namespace has.app.Controllers
{
    public class DevicePublishController : Controller
    {
        public ActionResult SetSwitch(string PluginID, string DeviceID, string id)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            if (PluginID == "Smartlight")
            {
                string strValue = Convert.ToString(id);
                Constants.MqttClient.Publish("Smartlight/Espurna/" + DeviceID + "/relay/0/set", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                string strValue = Convert.ToString(id);
                if (strValue == "1") { strValue = "ON"; } else { strValue = "OFF"; }
                Constants.MqttClient.Publish("Smartplug/Tasmota/"+DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkRelay(string PluginID, string DeviceID)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            if (PluginID == "SmartLight")
            {
                string strValue = Convert.ToString("query");
                Constants.MqttClient.Publish("Smartlight/Espurna/" + DeviceID + "/relay/0/set", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            else
            {
                string strValue = Convert.ToString("");
                Constants.MqttClient.Publish("Smartplug/Tasmota/" + DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
    }
}