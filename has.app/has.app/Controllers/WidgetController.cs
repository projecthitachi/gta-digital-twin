﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using has.app.Components;
using has.app.Models;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using uPLibrary.Networking.M2Mqtt.Messages;


namespace has.app.Controllers
{
    public class WidgetController : Controller
    {
        #region Home
        public ActionResult Home()
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HomeWidgetView_REC>("PowerDataWidget");
            var WidgetView = database.GetCollection<WidgetFlowRateView_REC>("WidgetFlowRate");
            var personLocation = database.GetCollection<PersonLocation>("PersonLocation");

            var builder = Builders<HomeWidgetView_REC>.Filter;
            var flt = builder.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00")));
            var resData = collecCount.Find(flt).SingleOrDefault();
            if (resData != null)
            {
                int fr7 = resData.Flowrate7f / 2;
                ResultData.Add("Flowrate", resData.Flowrate.ToString());
                ResultData.Add("Flowrate3f", resData.Flowrate3f.ToString());
                ResultData.Add("Flowrate3fA", resData.Flowrate3f.ToString());
                ResultData.Add("Flowrate7f", resData.Flowrate7f.ToString());
                ResultData.Add("Flowrate7fA", fr7.ToString());
                ResultData.Add("Flowrate7fB", fr7.ToString());
                ResultData.Add("Light3", resData.Light3.ToString());
                ResultData.Add("Light3A", resData.Light3A.ToString());
                ResultData.Add("Light7", resData.Light7.ToString());
                ResultData.Add("Light7A", resData.Light7A.ToString());
                ResultData.Add("Light7B", resData.Light7B.ToString());
                ResultData.Add("Light7C", resData.Light7C.ToString());
                ResultData.Add("Light7D", resData.Light7D.ToString());
                ResultData.Add("Power3", resData.Power3.ToString());
                ResultData.Add("Power3A", resData.Power3A.ToString());
                ResultData.Add("Power7", resData.Power7.ToString());
                ResultData.Add("Power7A", resData.Power7A.ToString());
                ResultData.Add("Power7B", resData.Power7B.ToString());
                ResultData.Add("Power7C", resData.Power7C.ToString());
                ResultData.Add("Power7D", resData.Power7D.ToString());
                ResultData.Add("Air3", resData.Air3.ToString());
                ResultData.Add("Air3A", resData.Air3A.ToString());
                ResultData.Add("Air3B", resData.Air3B.ToString());
                ResultData.Add("Air7", resData.Air7.ToString());
                ResultData.Add("Air7A", resData.Air7A.ToString());
                ResultData.Add("Air7B", resData.Air7B.ToString());
                ResultData.Add("Elv1", resData.Elv1.ToString());
                ResultData.Add("Elv2", resData.Elv2.ToString());
                ResultData.Add("Elv3", resData.Elv3.ToString());
                ResultData.Add("Elv4", resData.Elv4.ToString());
                ResultData.Add("Lighting", resData.Lighting.ToString());
                ResultData.Add("PowerPlug", resData.PowerPlug.ToString());
                ResultData.Add("Aircond", resData.Aircond.ToString());
                ResultData.Add("Elevator", resData.Elevator.ToString());
                ResultData.Add("Total", resData.Total.ToString());
            }
            else
            {
                ResultData.Add("Flowrate", "0");
                ResultData.Add("Flowrate3f", "0");
                ResultData.Add("Flowrate3fA", "0");
                ResultData.Add("Flowrate7f", "0");
                ResultData.Add("Flowrate7fA", "0");
                ResultData.Add("Flowrate7fB", "0");
                ResultData.Add("Light3", "0");
                ResultData.Add("Light3A", "0");
                ResultData.Add("Light7", "0");
                ResultData.Add("Light7A", "0");
                ResultData.Add("Light7B", "0");
                ResultData.Add("Light7C", "0");
                ResultData.Add("Light7D", "0");
                ResultData.Add("Power3", "0");
                ResultData.Add("Power3A", "0");
                ResultData.Add("Power7", "0");
                ResultData.Add("Power7A", "0");
                ResultData.Add("Power7B", "0");
                ResultData.Add("Power7C", "0");
                ResultData.Add("Power7D", "0");
                ResultData.Add("Air3", "0");
                ResultData.Add("Air3A", "0");
                ResultData.Add("Air3B", "0");
                ResultData.Add("Air7", "0");
                ResultData.Add("Air7A", "0");
                ResultData.Add("Air7B", "0");
                ResultData.Add("Elv1", "0");
                ResultData.Add("Elv2", "0");
                ResultData.Add("Elv3", "0");
                ResultData.Add("Elv4", "0");
                ResultData.Add("Lighting", "0");
                ResultData.Add("PowerPlug", "0");
                ResultData.Add("Aircond", "0");
                ResultData.Add("Elevator", "0");
                ResultData.Add("Total", "0");
            }
            List<PersonLocation> res = personLocation.Find(_ => true).ToList();
            ResultData.Add("OccupantNumber", res.Count.ToString());
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HomeAnalytic()
        {
            DateTime today = DateTime.Today;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var dates = Enumerable.Range(0, 7).Select(days => sunday.AddDays(days)).ToList();

            List<HomeWidgetAnalytic_REC> Result = new List<HomeWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HomeWidgetView_REC>("PowerDataWidget");

            int no = 1;
            Dictionary<string, int> dictionary = new Dictionary<string, int> { { "Mon", 1 }, { "Tue", 2 }, { "Wed", 3 }, { "Thu", 4 }, { "Fri", 5 }, { "Sat", 6 }, { "Sun", 7 } };
            foreach (DateTime date in dates)
            {
                HomeWidgetAnalytic_REC ResultData = new HomeWidgetAnalytic_REC();
                //var builder = Builders<HomeWidgetView_REC>.Filter;
                //var flt = builder.Eq<DateTime>("RecordTimestamp", DateTime.Parse(date.ToString("yyyy-MM-dd 00:00:00")));
                //var resData = collecCount.Find(flt).SingleOrDefault();

                // get data
                var filter = new BsonDocument
                {
                    {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
                    {"$lte", DateTime.Now}
                };
                string match = "{$match: {RecordTimestamp : " + filter + " }}";

                var pipeline = collecCount.Aggregate().AppendStage<HomeWidgetView_REC>(match);
                var res = pipeline.ToList();
                
                if (res.Count > 0)
                {
                    int i = 0;
                    ResultData.day = date.AddDays(0).DayOfWeek.ToString().Substring(0, 3);
                    ResultData.no = dictionary[ResultData.day];
                    ResultData.unit = "Kwh";
                    int bs = 0;
                    int cr = 0;
                    foreach (var resData in res)
                    {
                        if (resData.RecordTimestamp.AddDays(0).DayOfWeek.ToString().Substring(0, 3) == date.AddDays(0).DayOfWeek.ToString().Substring(0, 3))
                        {
                            int bl = (resData.Total * 25 / 100) + resData.Total;
                            bs += bl;
                            cr += resData.Total;
                            i++;
                        }
                    }
                    if (i > 0)
                    {
                        ResultData.Baseline = bs / i;
                        ResultData.Current = cr / i;
                    }
                    else
                    {
                        ResultData.Baseline = 0;
                        ResultData.Current = 0; 
                    }
                }
                else
                {
                    ResultData.day = date.AddDays(0).DayOfWeek.ToString().Substring(0, 3);
                    ResultData.no = dictionary[ResultData.day];
                    ResultData.unit = "Kwh";
                    ResultData.Baseline = 0;
                    ResultData.Current = 0;
                }

                Result.Add(ResultData);
                no++;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HomeAnalyticEUI()
        {

            List<HomeWidgetAnalyticEUI_REC> Result = new List<HomeWidgetAnalyticEUI_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HomeWidgetView_REC>("PowerDataWidget");

            // get data
            var filter = new BsonDocument
            {
                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
                {"$lte", DateTime.Now}
            };
            string match = "{$match: {RecordTimestamp : " + filter + " }}";

            var pipeline = collecCount.Aggregate().AppendStage<HomeWidgetView_REC>(match);
            var res = pipeline.ToList();

            int no = 1;
            int Total = 0;
            foreach (HomeWidgetView_REC data in res)
            {
                Total += data.Total;
                no++;
            }
            HomeWidgetAnalyticEUI_REC ResultData = new HomeWidgetAnalyticEUI_REC();
            Total = Total / 1500;
            int bl = (Total * 25 / 100) + Total;
            ResultData.no = 1;
            ResultData.unit = "Kwh/m2/year";
            ResultData.Value = bl;
            ResultData.Category = "Baseline";
            ResultData.color = "#525252";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticEUI_REC();
            ResultData.no = 2;
            ResultData.unit = "Kwh/m2/year";
            ResultData.Value = Total;
            ResultData.Category = "Current";
            ResultData.color = "#fff";
            Result.Add(ResultData);

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Power
        public ActionResult PowerDailyEnergy(bool Flowrate, bool Aircond, bool Lighting, bool PowerPlug, bool Elevator)
        {
            DateTime today = DateTime.Today;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var dates = Enumerable.Range(0, 7).Select(days => sunday.AddDays(days)).ToList();

            List<PowerWidgetAnalytic_REC> Result = new List<PowerWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HomeWidgetView_REC>("PowerDataWidget");

            int no = 1;
            Dictionary<string, int> dictionary = new Dictionary<string, int> { { "Mon", 1 }, { "Tue", 2 }, { "Wed", 3 }, { "Thu", 4 }, { "Fri", 5 }, { "Sat", 6 }, { "Sun", 7 } };
            List<string> dicType  = new List<string> { "Flowrate", "Aircond", "Lighting", "PowerPlug", "Elevator" };
            if (!Flowrate) { dicType.RemoveAt(dicType.IndexOf("Flowrate")); }  if (!Aircond) { dicType.RemoveAt(dicType.IndexOf("Aircond")); }
             if (!Lighting) { dicType.RemoveAt(dicType.IndexOf("Lighting")); }  if (!PowerPlug) { dicType.RemoveAt(dicType.IndexOf("PowerPlug")); }  if (!Elevator) { dicType.RemoveAt(dicType.IndexOf("Elevator")); }
            Dictionary<string, string> lblDic = new Dictionary<string, string> { { "Flowrate", "1-Flowrate" }, { "Aircond", "2-Aircon" }, { "Lighting", "3-Lighting" }, { "PowerPlug", "4-PowerPlug" }, { "Elevator", "5-Elevator" } };
            foreach (string voType in dicType) {
                string labelType = lblDic[voType];
                foreach (DateTime date in dates)
                {
                    PowerWidgetAnalytic_REC ResultData = new PowerWidgetAnalytic_REC();

                    // get data
                    var filter = new BsonDocument
                    {
                        {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
                        {"$lte", DateTime.Now}
                    };
                    string addfield = "{$addFields:{presentVal: \"$" + voType + "\"}}";
                    string match = "{$match: {RecordTimestamp : " + filter + " }}";

                    var pipeline = collecCount.Aggregate().AppendStage<PowerWidgetView_REC>(addfield).AppendStage<PowerWidgetView_REC>(match);
                    var res = pipeline.ToList();

                    if (res.Count > 0)
                    {
                        int i = 0;
                        ResultData.Days = date.AddDays(0).DayOfWeek.ToString().Substring(0, 3);
                        ResultData.no = dictionary[ResultData.Days];
                        ResultData.Type = labelType;
                        int bs = 0;
                        int cr = 0;
                        foreach (var resData in res)
                        {
                            if (resData.RecordTimestamp.AddDays(0).DayOfWeek.ToString().Substring(0, 3) == date.AddDays(0).DayOfWeek.ToString().Substring(0, 3))
                            {
                                int bl = (resData.presentVal * 25 / 100) + resData.presentVal;
                                bs += bl;
                                cr += resData.presentVal;
                                i++;
                            }
                        }
                        if (i > 0)
                        {
                            ResultData.baseline = bs / i;
                            ResultData.current = cr / i;
                        }
                        else
                        {
                            ResultData.baseline = 0;
                            ResultData.current = 0;
                        }
                    }
                    else
                    {
                        ResultData.Days = date.AddDays(0).DayOfWeek.ToString().Substring(0, 3);
                        ResultData.no = dictionary[ResultData.Days];
                        ResultData.Type = labelType;
                        ResultData.baseline = 0;
                        ResultData.current = 0;
                    }

                    Result.Add(ResultData);
                    no++;
                }

            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PowerMonthlyEnergy(string filterMonth, bool filter3f, bool filter7f, bool filterElv)
        {
            DateTime today = DateTime.Today;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var dates = Enumerable.Range(0, 7).Select(days => sunday.AddDays(days)).ToList();

            List<PowerWidgetAnalytic_REC> Result = new List<PowerWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HomeWidgetView_REC>("PowerDataWidget");

            int no = 1;
            int gm = 0;
            int lm = 0;
            Dictionary<string, int> dictionary = new Dictionary<string, int> { { "Flowrate", 1 }, { "Aircond", 2 }, { "Lighting", 3 }, { "PowerPlug", 4 }, { "Elevator", 5 } };
            List<string> dicType = new List<string> { "Flowrate", "Aircond", "Lighting", "PowerPlug", "Elevator" };
            //if (filterMonth == "CM")
            //{
            //    gm = 0;
            //    lm = 1;
            //}
            //else if (filterMonth == "LM")
            //{
            //    gm = -1;
            //    lm = 0;
            //}
            foreach (string voType in dicType)
            {
                string labelType = (voType == "Aircond") ? "Aircon" : voType;
                PowerWidgetAnalytic_REC ResultData = new PowerWidgetAnalytic_REC();
                string field3f = "";
                string field7f = "";
                if(voType == "PowerPlug")
                {
                    field3f = "Power3";
                    field7f = "Power7";
                }
                else if (voType == "Aircond")
                {
                    field3f = "Air3";
                    field7f = "Air7";
                }
                else if (voType == "Lighting")
                {
                    field3f = "Light3";
                    field7f = "Light7";
                }
                else if (voType == "Flowrate")
                {
                    field3f = "Flowrate3f";
                    field7f = "Flowrate7f";
                }
                else if (voType == "Elevator")
                {
                    field3f = "Elevator";
                    field7f = "Elevator";
                }
                // CM/
                var filter = new BsonDocument
                {
                    {"$gte", DateTime.Parse(DateTime.Now.AddMonths(0).ToString("yyyy-MM-01 00:00:00"))},
                    {"$lt", DateTime.Parse(DateTime.Now.AddMonths(1).ToString("yyyy-MM-01 00:00:00"))}
                };
                string addfield = "{$addFields:{presentVal3f: \"$" + field3f + "\", presentVal7f: \"$" + field7f + "\"}}";
                string match = "{$match: {RecordTimestamp : " + filter + " }}";

                var pipeline = collecCount.Aggregate().AppendStage<PowerWidgetView2_REC>(addfield).AppendStage<PowerWidgetView2_REC>(match);
                var res = pipeline.ToList();
                // lM
                var filter2 = new BsonDocument
                {
                    {"$gte", DateTime.Parse(DateTime.Now.AddMonths(-1).ToString("yyyy-MM-01 00:00:00"))},
                    {"$lt", DateTime.Parse(DateTime.Now.AddMonths(0).ToString("yyyy-MM-01 00:00:00"))}
                };
                string addfield2 = "{$addFields:{presentVal3f: \"$" + field3f + "\", presentVal7f: \"$" + field7f + "\"}}";
                string match2 = "{$match: {RecordTimestamp : " + filter2 + " }}";

                var pipeline2 = collecCount.Aggregate().AppendStage<PowerWidgetView2_REC>(addfield2).AppendStage<PowerWidgetView2_REC>(match2);
                var res2 = pipeline2.ToList();

                // CM
                if (res.Count > 0)
                {
                    int i = 0;
                    ResultData.no = dictionary[voType];
                    ResultData.Type = labelType;
                    int cr = 0;
                    foreach (var resData in res)
                    {
                        int presentVal = 0;
                        if (voType == "Elevator")
                        {
                            if (filterElv)
                            {
                                presentVal = resData.presentVal3f;
                            }
                        }
                        else
                        {
                            
                            if (filter3f && !filter7f)
                            {
                                presentVal = resData.presentVal3f;
                            }
                            else if (!filter3f && filter7f)
                            {
                                presentVal = resData.presentVal7f;
                            }
                            else if (filter3f && filter7f)
                            {
                                presentVal = resData.presentVal3f + resData.presentVal7f;
                            }
                        }
                        cr += presentVal;
                        i++;
                    }

                    if (i > 0)
                    {
                        ResultData.current = cr ;
                        ResultData.baseline = (cr * 25 / 100) + cr;
                    }
                    else
                    {
                        ResultData.current = 0;
                        ResultData.baseline = 0;
                    }
                }
                else
                {
                    ResultData.no = dictionary[voType];
                    ResultData.Type = labelType;
                    ResultData.current = 0;
                    ResultData.baseline = 0;
                }

                //LM
                //if (res2.Count > 0)
                //{
                //    int i2 = 0;
                //    int bs = 0;
                //    foreach (var resData in res2)
                //    {
                //        int presentVal = 0;
                //        if (voType == "Elevator")
                //        {
                //            if (filterElv)
                //            {
                //                presentVal = resData.presentVal3f;
                //            }
                //        }
                //        else
                //        {

                //            if (filter3f && !filter7f)
                //            {
                //                presentVal = resData.presentVal3f;
                //            }
                //            else if (!filter3f && filter7f)
                //            {
                //                presentVal = resData.presentVal7f;
                //            }
                //            else if (filter3f && filter7f)
                //            {
                //                presentVal = resData.presentVal3f + resData.presentVal7f;
                //            }
                //        }
                //        bs += presentVal;
                //        i2++;
                //    }
                //    if (i2 > 0)
                //    {
                //        ResultData.baseline = bs;
                //    }
                //    else
                //    {
                //        ResultData.baseline = 0;
                //    }
                //}
                //else
                //{
                //    ResultData.no = dictionary[voType];
                //    ResultData.Type = labelType;
                //    ResultData.baseline = 0;
                //}

                Result.Add(ResultData);
                no++;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PowerAnalyticECD()
        {

            List<HomeWidgetAnalyticECD_REC> Result = new List<HomeWidgetAnalyticECD_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HomeWidgetView_REC>("PowerDataWidget");

            // get data
            var filter = new BsonDocument
            {
                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
                {"$lte", DateTime.Now}
            };
            string match = "{$match: {RecordTimestamp : " + filter + " }}";

            var pipeline = collecCount.Aggregate().AppendStage<HomeWidgetView_REC>(match);
            var res = pipeline.ToList();

            int no = 1;
            int Aircond = 0;
            int Elevator = 0;
            int Lighting = 0;
            int PowerPlug = 0;
            int Flowrate = 0;
            int Total = 0;
            foreach (HomeWidgetView_REC data in res)
            {
                Aircond += data.Aircond;
                Elevator += data.Elevator;
                Lighting += data.Lighting;
                PowerPlug += data.PowerPlug;
                Flowrate += data.Flowrate;
                Total += data.Total;
                no++;
            }
            Total = Total + Flowrate;
            HomeWidgetAnalyticECD_REC ResultData = new HomeWidgetAnalyticECD_REC();
            double val = Convert.ToDouble(Aircond) / Convert.ToDouble(Total) * 100;
            ResultData.no = 4;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Air-con";
            ResultData.color = "#ffffff90";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticECD_REC();
            val = Convert.ToDouble(Flowrate) / Convert.ToDouble(Total) * 100;
            ResultData.no = 3;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Flowrate";
            ResultData.color = "#ffffff10";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticECD_REC();
            val = Convert.ToDouble(Elevator) / Convert.ToDouble(Total) * 100;
            ResultData.no = 2;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Elevator";
            ResultData.color = "#ffffff60";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticECD_REC();
            val = Convert.ToDouble(Lighting) / Convert.ToDouble(Total) * 100;
            ResultData.no = 1;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Lighting";
            ResultData.color = "#ffffff30";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticECD_REC();
            val = Convert.ToDouble(PowerPlug) / Convert.ToDouble(Total) * 100;
            ResultData.no = 3;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Plug Load";
            ResultData.color = "#ffffff20";
            Result.Add(ResultData);

            

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PowerDataChart(string type)
        {

            List<HomeWidgetAnalytic_REC> Result = new List<HomeWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<PowerChart_REC>("PowerDataChart");

            if (type.Substring(0, 3) == "Air")
            {
                type = type.Substring(0, 3) + type.Substring(6, 2);
            }else if(type.Length > 8 && type.Substring(0, 8) == "Flowrate")
            {
                if(type.Substring(9, 1) == "1")
                {
                    type = "Flowrate3f";
                }
                else
                {
                    type = "Flowrate7f";
                }
            }

            int no = 1;
            for (int time = 0; time <= 23; time++)
            {
                HomeWidgetAnalytic_REC ResultData = new HomeWidgetAnalytic_REC();

                // get data
                var filter = new BsonDocument
                {
                    {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))},
                    {"$lte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 23:00:00"))}
                };
                string match = "{$match: {RecordTimestamp : " + filter + " }}";

                var pipeline = collecCount.Aggregate().AppendStage<PowerChart_REC>(match);
                var res = pipeline.ToList();

                if (res.Count > 0)
                {
                    int i = 0;
                    ResultData.day = time.ToString();
                    ResultData.no = time;
                    ResultData.unit = "Kwh";
                    int bs = 0;
                    int cr = 0;
                    foreach (var resData in res)
                    {
                        if (resData.RecordTimestamp.Hour == time)
                        {
                            var poVal = resData.GetType().GetProperty(type).GetValue(resData, null);
                            int voVal = Convert.ToInt32(poVal);
                            int bl = (voVal * 25 / 100) + voVal;
                            bs += bl;
                            cr += voVal;
                            i++;
                        }
                    }
                    if (i > 0)
                    {
                        ResultData.Baseline = bs / i;
                        ResultData.Current = cr / i;
                    }
                    else
                    {
                        ResultData.Baseline = 0;
                        ResultData.Current = 0;
                    }
                }
                else
                {
                    ResultData.day = time.ToString();
                    ResultData.no = time;
                    ResultData.unit = "Kwh";
                    ResultData.Baseline = 0;
                    ResultData.Current = 0;
                }

                Result.Add(ResultData);
                no++;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Feedback
        public ActionResult FeedbackAge()
        {
            Dictionary<string, int> ResultData = new Dictionary<string, int>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<FeedbackAge_REC>("UserFeedback");

            string grp = "{ $group: {_id: \"\", below : {$sum : { $cond: [{$and:[ {$gte:[{$toInt : \"$age\"},1]}, {$lt:[{$toInt : \"$age\"}, 30]}]}, 1, 0]}}, 'young' : {$sum : { $cond: [{$and:[ {$gte:[{$toInt : \"$age\"},30]}, {$lte:[{$toInt : \"$age\"}, 40]}]}, 1, 0]}}, 'old' : {$sum : { $cond: [{$and:[ {$gte:[{$toInt : \"$age\"},41]}, {$lte:[{$toInt : \"$age\"}, 50]}]}, 1, 0]}}, 'above' : {$sum : { $cond: [{$gte:[{$toInt : \"$age\"},50]}, 1, 0]}} }}";
            var pipeline = collecCount.Aggregate().AppendStage<FeedbackAge_REC>(grp);
            var resData = pipeline.SingleOrDefault();
            List<FeedbackChart_REC> res = new List<FeedbackChart_REC>();
            if (resData != null)
            {
                FeedbackChart_REC Age = new FeedbackChart_REC();
                Age.color = "#f17b79";
                Age.Category = "30 and Below";
                Age.Value = resData.below;
                res.Add(Age);
                Age = new FeedbackChart_REC();
                Age.color = "#f17b79";
                Age.Category = "31-40";
                Age.Value = resData.young;
                res.Add(Age);
                Age = new FeedbackChart_REC();
                Age.color = "#f17b79";
                Age.Category = "41-50";
                Age.Value = resData.old;
                res.Add(Age);
                Age = new FeedbackChart_REC();
                Age.color = "#f17b79";
                Age.Category = "50 and Above";
                Age.Value = resData.above;
                res.Add(Age);

            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeedbackTCS()
        {
            Dictionary<string, int> ResultData = new Dictionary<string, int>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<FeedbackTCS_REC>("UserFeedback");

            string grp = "{$group: {_id: '', 'DA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalSensationID\"}, -3] }, 1, 0] } }, 'CA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalSensationID\"}, -2] }, 1, 0] } }, 'BA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalSensationID\"}, -1] }, 1, 0] } }, 'AA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalSensationID\"}, 0] }, 1, 0] } }, 'AB': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalSensationID\"}, 1] }, 1, 0] } }, 'AC': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalSensationID\"}, 2] }, 1, 0] } }, 'AD': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalSensationID\"}, 3] }, 1, 0] } } } }";
            var pipeline = collecCount.Aggregate().AppendStage<FeedbackTCS_REC>(grp);
            var resData = pipeline.SingleOrDefault();
            List<FeedbackChart_REC> res = new List<FeedbackChart_REC>();
            if (resData != null)
            {
                FeedbackChart_REC TCS = new FeedbackChart_REC();
                TCS.color = "#00ff00";
                TCS.Category = "3";
                TCS.Value = resData.DA;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#50f250";
                TCS.Category = "2";
                TCS.Value = resData.CA;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#99e599";
                TCS.Category = "1";
                TCS.Value = resData.BA;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#d9d9d9";
                TCS.Category = "0";
                TCS.Value = resData.AA;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#c88686";
                TCS.Category = "-1";
                TCS.Value = resData.AB;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#b93d3d";
                TCS.Category = "-2";
                TCS.Value = resData.AC;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#a70000";
                TCS.Category = "-3";
                TCS.Value = resData.AD;
                res.Add(TCS);

            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeedbackTS()
        {
            Dictionary<string, int> ResultData = new Dictionary<string, int>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<FeedbackTCS_REC>("UserFeedback");

            string grp = "{$group: {_id: '', 'DA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalComfortSatisfactionID\"}, -3] }, 1, 0] } }, 'CA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalComfortSatisfactionID\"}, -2] }, 1, 0] } }, 'BA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalComfortSatisfactionID\"}, -1] }, 1, 0] } }, 'AA': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalComfortSatisfactionID\"}, 0] }, 1, 0] } }, 'AB': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalComfortSatisfactionID\"}, 1] }, 1, 0] } }, 'AC': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalComfortSatisfactionID\"}, 2] }, 1, 0] } }, 'AD': {$sum: {$cond: [{$eq: [{$toInt: \"$thermalComfortSatisfactionID\"}, 3] }, 1, 0] } } } }";
            var pipeline = collecCount.Aggregate().AppendStage<FeedbackTCS_REC>(grp);
            var resData = pipeline.SingleOrDefault();
            List<FeedbackChart_REC> res = new List<FeedbackChart_REC>();
            if (resData != null)
            {
                FeedbackChart_REC TCS = new FeedbackChart_REC();
                TCS = new FeedbackChart_REC();
                TCS.color = "#0000ff";
                TCS.Category = "3";
                TCS.Value = resData.AD;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#5050f2";
                TCS.Category = "2";
                TCS.Value = resData.AC;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#c88686";
                TCS.Category = "1";
                TCS.Value = resData.AB;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#9898e5";
                TCS.Category = "0";
                TCS.Value = resData.AA;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#c98686";
                TCS.Category = "-1";
                TCS.Value = resData.BA;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#ba3e3e";
                TCS.Category = "-2";
                TCS.Value = resData.CA;
                res.Add(TCS);
                TCS.color = "#a70000";
                TCS.Category = "-3";
                TCS.Value = resData.DA;
                res.Add(TCS);

            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeedbackTDT()
        {
            Dictionary<string, int> ResultData = new Dictionary<string, int>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<FeedbackTDT_REC>("UserFeedback");

            string grp = "{$group: { _id:\"\",before: {$sum: { $cond: [{$and:[ {$gte:[{$hour: \"$recordTimestamp\"},0]}, {$lt:[{$hour: \"$recordTimestamp\"}, 11]}]}, 1, 0]} }, afternon: {$sum: { $cond: [{$and:[ {$gte:[{$hour: \"$recordTimestamp\"},11]}, {$lt:[{$hour: \"$recordTimestamp\"}, 14]}]}, 1, 0]} }, evening: {$sum: { $cond: [{$and:[ {$gte:[{$hour: \"$recordTimestamp\"},14]}, {$lt:[{$hour: \"$recordTimestamp\"}, 17]}]}, 1, 0]} }, night: {$sum: { $cond: [{$and:[ {$gte:[{$hour: \"$recordTimestamp\"},17]}, {$lte:[{$hour: \"$recordTimestamp\"}, 23]}]}, 1, 0]} } }}";
            var pipeline = collecCount.Aggregate().AppendStage<FeedbackTDT_REC>(grp);
            var resData = pipeline.SingleOrDefault();
            List<FeedbackChart_REC> res = new List<FeedbackChart_REC>();
            if (resData != null)
            {
                FeedbackChart_REC TCS = new FeedbackChart_REC();
                TCS.color = "#4e79a7";
                TCS.Category = "Before 11AM";
                TCS.Value = resData.before;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#4e79a7";
                TCS.Category = "11AM-2PM";
                TCS.Value = resData.afternon;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#4e79a7";
                TCS.Category = "2PM-5PM";
                TCS.Value = resData.evening;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#4e79a7";
                TCS.Category = "After 5PM";
                TCS.Value = resData.night;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#4e79a7";
                TCS.Category = "No Perticular time";
                TCS.Value = 9;
                res.Add(TCS);
                TCS = new FeedbackChart_REC();
                TCS.color = "#4e79a7";
                TCS.Category = "Alway";
                TCS.Value = 5;
                res.Add(TCS);

            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeedbackGender()
        {
            Dictionary<string, int> ResultData = new Dictionary<string, int>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<FeedbackChart_REC>("UserFeedback");

            string grp = "{$group:{_id:\"$gender\",Value:{ $sum: 1}}}";
            var pipeline = collecCount.Aggregate().AppendStage<FeedbackChart_REC>(grp);
            var resData = pipeline.ToList();
            List<FeedbackChart_REC> res = new List<FeedbackChart_REC>();
            if (resData.Count > 0)
            {
                FeedbackChart_REC Female = resData.Where(x => x._id == "Female").SingleOrDefault();
                if (Female != null)
                {
                    Female.color = "#f17b79";
                    Female.Category = "F";
                    res.Add(Female);
                }
                else
                {
                    FeedbackChart_REC nData = new FeedbackChart_REC();
                    nData.Value = 0;
                    nData.color = "#f17b79";
                    nData.Category = "F";
                    res.Add(nData);
                }
                FeedbackChart_REC Male = resData.Where(x => x._id == "Male").SingleOrDefault();
                if (Male != null)
                {
                    Male.color = "#f17b79";
                    Male.Category = "M";
                    res.Add(Male);
                }
                else
                {
                    FeedbackChart_REC nData = new FeedbackChart_REC();
                    nData.Value = 0;
                    nData.color = "#f17b79";
                    nData.Category = "M";
                    res.Add(nData);
                }
                
                
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeedbackRace()
        {
            Dictionary<string, int> ResultData = new Dictionary<string, int>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<FeedbackChart_REC>("UserFeedback");

            string grp = "{$group:{_id:\"$race\",Value:{ $sum: 1}}}";
            var pipeline = collecCount.Aggregate().AppendStage<FeedbackChart_REC>(grp);
            var resData = pipeline.ToList();
            List<FeedbackChart_REC> res = new List<FeedbackChart_REC>();
            if (resData.Count > 0)
            {
                FeedbackChart_REC Chinese = resData.Where(x => x._id == "Chinese").SingleOrDefault();
                if (Chinese != null)
                {
                    Chinese.color = "#f17b79";
                    Chinese.Category = "Chinese";
                    res.Add(Chinese);
                }
                else
                {
                    FeedbackChart_REC nData = new FeedbackChart_REC();
                    nData.Value = 0;
                    nData.color = "#f17b79";
                    nData.Category = "Chinese";
                    res.Add(nData);
                }
                FeedbackChart_REC Japanese = resData.Where(x => x._id == "Japanese").SingleOrDefault();
                if (Japanese != null)
                {
                    Japanese.color = "#f17b79";
                    Japanese.Category = "Japanese";
                    res.Add(Japanese);
                }
                else
                {
                    FeedbackChart_REC nData = new FeedbackChart_REC();
                    nData.Value = 0;
                    nData.color = "#f17b79";
                    nData.Category = "Japanese";
                    res.Add(nData);
                }
                FeedbackChart_REC Malaysian = resData.Where(x => x._id == "Malaysian").SingleOrDefault();
                if (Malaysian != null)
                {
                    Malaysian.color = "#f17b79";
                    Malaysian.Category = "Malaysian";
                    res.Add(Malaysian);
                }
                else
                {
                    FeedbackChart_REC nData = new FeedbackChart_REC();
                    nData.Value = 0;
                    nData.color = "#f17b79";
                    nData.Category = "Malaysian";
                    res.Add(nData);
                }
                FeedbackChart_REC Indian = resData.Where(x => x._id == "Indian").SingleOrDefault();
                if (Indian != null)
                {
                    Indian.color = "#f17b79";
                    Indian.Category = "Indian";
                    res.Add(Indian);
                }
                else
                {
                    FeedbackChart_REC nData = new FeedbackChart_REC();
                    nData.Value = 0;
                    nData.color = "#f17b79";
                    nData.Category = "Indian";
                    res.Add(nData);
                }
                FeedbackChart_REC Others = resData.Where(x => x._id == "Others").SingleOrDefault();
                if (Others != null)
                {
                    Others.color = "#f17b79";
                    Others.Category = "Others";
                    res.Add(Others);
                }
                else
                {
                    FeedbackChart_REC nData = new FeedbackChart_REC();
                    nData.Value = 0;
                    nData.color = "#f17b79";
                    nData.Category = "Others";
                    res.Add(nData);
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Human
        public ActionResult HumanPresentHourlyAvg()
        {
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HumanResenceHourly_REC>("HumanPresenceHourlyAvg");

            List<HumanResenceHourly_REC> res = collecCount.Find(_ => true).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HumanPresentDailyAvg()
        {
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HumanResenceHourly_REC>("HumanPresenceDayilyAvg");

            List<HumanResenceHourly_REC> res = collecCount.Find(_ => true).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HumanPresentMonthlySum()
        {
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HumanResenceHourly_REC>("HumanPresenceMonthlySum");

            List<HumanResenceHourly_REC> res = collecCount.Find(_ => true).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HumanPresentRealtime()
        {
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HumanResenceHourly_REC>("HumanPresenceRealtime");

            List<HumanResenceHourly_REC> res = collecCount.Find(_ => true).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HumanPresentRealtimeGetCounting()
        {
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<PersonLocation>("PersonLocation");
            List<PersonLocation> res = collecCount.Find(_ => true).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HumanPieChart(int total)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            List<Dictionary<string, string>> Res = new List<Dictionary<string, string>>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<Users_REC>("Users");

            List<Users_REC> res = collecCount.Find(x => x.recordStatus == 0).ToList();
            int dissati = res.Count;
            int satis = total - dissati;
            ResultData.Add("value", satis.ToString());
            ResultData.Add("category", "Satisfied");
            ResultData.Add("color", "#ffffff90");
            Res.Add(ResultData);

            ResultData = new Dictionary<string, string>();
            ResultData.Add("value", dissati.ToString());
            ResultData.Add("category", "Dissatified");
            ResultData.Add("color", "#ffffff30");
            Res.Add(ResultData);
            return Json(Res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HomeAnalyticECD()
        {

            List<HomeWidgetAnalyticECD_REC> Result = new List<HomeWidgetAnalyticECD_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<HomeWidgetView_REC>("PowerDataWidget");

            // get data
            var filter = new BsonDocument
            {
                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
                {"$lte", DateTime.Now}
            };
            string match = "{$match: {RecordTimestamp : " + filter + " }}";

            var pipeline = collecCount.Aggregate().AppendStage<HomeWidgetView_REC>(match);
            var res = pipeline.ToList();

            int no = 1;
            int Aircond = 0;
            int Elevator = 0;
            int Lighting = 0;
            int PowerPlug = 0;
            int Total = 0;
            foreach (HomeWidgetView_REC data in res)
            {
                Aircond += data.Aircond;
                Elevator += data.Elevator;
                Lighting += data.Lighting;
                PowerPlug += data.PowerPlug;
                Total += data.Total;
                no++;
            }
            HomeWidgetAnalyticECD_REC ResultData = new HomeWidgetAnalyticECD_REC();
            double val = Convert.ToDouble(Aircond) / Convert.ToDouble(Total) * 100;
            ResultData.no = 4;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Air-con";
            ResultData.color = "#ffffff90";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticECD_REC();
            val = Convert.ToDouble(Elevator) / Convert.ToDouble(Total) * 100;
            ResultData.no = 2;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Elevator";
            ResultData.color = "#ffffff60";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticECD_REC();
            val = Convert.ToDouble(Lighting) / Convert.ToDouble(Total) * 100;
            ResultData.no = 1;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Lighting";
            ResultData.color = "#ffffff30";
            Result.Add(ResultData);

            ResultData = new HomeWidgetAnalyticECD_REC();
            val = Convert.ToDouble(PowerPlug) / Convert.ToDouble(Total) * 100;
            ResultData.no = 3;
            ResultData.value = Convert.ToDouble(val.ToString("0.#"));
            ResultData.category = "Plug Load";
            ResultData.color = "#ffffff20";
            Result.Add(ResultData);

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Smart Device
        public ActionResult SmartPlugLevelUsage(string IP)
        {
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartPlugLevelUseage_REC>("SmartPlugLevelUsageNew");

            List<SmartPlugLevelUseage_REC> res = collecCount.Find(x => x.ip == IP).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SmartPlugWeekdayConsumtionAvg(string IP)
        {
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartPlugWeekdayConsumtionAvg_REC>("SmartplugWeekdayConsumtionAvg");

            List<SmartPlugWeekdayConsumtionAvg_REC> res = collecCount.Find(x => x.ip == IP).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Smartlight()
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            var database = Constants.client.GetDatabase("IoT");
            var collection = database.GetCollection<SmartWidget_REC>("SmartlightDataLogs");
            var CollectDevice = database.GetCollection<SmartDeviceView_REC>("SmartlightDevices");

            int TotalDevice = 0;
            int DeviceOn = 0;
            int DeviceOff = 0;
            foreach (SmartDeviceView_REC voData in CollectDevice.Find(_ => true).ToList())
            {
                TotalDevice += 1;
                if (voData.Status == 1) { DeviceOn += 1; } else { DeviceOff += 1; }
            }

            // get data
            var filter = new BsonDocument
            {
                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))},
                {"$lte", DateTime.Now}
            };
            string match = "{$match: {RecordTimestamp : " + filter + " }}";
            string grp = "{$group:{_id:{DeviceID:\"$DeviceID\"},ApparentPower:{$sum:{$toDecimal:\"$ActivePower\"}}}}";

            var pipeline = collection.Aggregate().AppendStage<SmartWidget_REC>(match).AppendStage<SmartWidget_REC>(grp);
            var res = pipeline.ToList();
            
            if (res != null)
            {
                decimal Total = 0;
                foreach (SmartWidget_REC voData in res)
                {
                    Total += voData.ApparentPower;
                }
                double hours = (DateTime.Now - DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))).TotalHours;
                //decimal voTotal = (Convert.ToDecimal(Total) * Convert.ToInt32(hours)) / 1000;
                decimal voTotal = Convert.ToDecimal(Total) / Convert.ToDecimal(hours) / 1000;
                
                ResultData.Add("Total", voTotal.ToString("0.##"));
                ResultData.Add("TotalDevice", TotalDevice.ToString());
                ResultData.Add("DeviceOn", DeviceOn.ToString());
                ResultData.Add("DeviceOff", DeviceOff.ToString());
            }
            else
            {
                ResultData.Add("Total", "0");
                ResultData.Add("TotalDevice", "0");
                ResultData.Add("DeviceOn", "0");
                ResultData.Add("DeviceOff", "0");
            }

            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Smartplug()
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            var database = Constants.client.GetDatabase("IoT");
            var collection = database.GetCollection<SmartWidget_REC>("SmartplugDataLogs");
            var CollectDevice = database.GetCollection<SmartDeviceView_REC>("SmartplugDevices");

            int TotalDevice = 0;
            int DeviceOn = 0;
            int DeviceOff = 0;
            foreach (SmartDeviceView_REC voData in CollectDevice.Find(_ => true).ToList())
            {
                TotalDevice += 1;
                if (voData.Status == 1) { DeviceOn += 1; } else { DeviceOff += 1; }
            }

            // get data
            var filter = new BsonDocument
            {
                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))},
                {"$lte", DateTime.Now}
            };
            string match = "{$match: {RecordTimestamp : " + filter + " }}";
            string grp = "{$group:{_id:{DeviceID:\"$DeviceID\"},ApparentPower:{$sum:{$toDecimal:\"$ActivePower\"}}}}";

            var pipeline = collection.Aggregate().AppendStage<SmartWidget_REC>(match).AppendStage<SmartWidget_REC>(grp);
            var res = pipeline.ToList();

            if (res != null)
            {
                decimal Total = 0;
                foreach (SmartWidget_REC voData in res)
                {
                    Total += voData.ApparentPower;
                }
                double hours = (DateTime.Now - DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))).TotalHours;
                //decimal voTotal = (Convert.ToDecimal(Total) * Convert.ToInt32(hours)) / 1000;
                decimal voTotal = Convert.ToDecimal(Total) / 1000;

                ResultData.Add("Total", voTotal.ToString("0.##"));
                ResultData.Add("TotalDevice", TotalDevice.ToString());
                ResultData.Add("DeviceOn", DeviceOn.ToString());
                ResultData.Add("DeviceOff", DeviceOff.ToString());
            }
            else
            {
                ResultData.Add("Total", "0");
                ResultData.Add("TotalDevice", "0");
                ResultData.Add("DeviceOn", "0");
                ResultData.Add("DeviceOff", "0");
            }

            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SmartplugDevices()
        {
            var database = Constants.client.GetDatabase("IoT");
            var CollectDevice = database.GetCollection<SmartDevices_REC>("SmartDevices");
            List<SmartDevices_REC> ResultData = CollectDevice.Find(f=>f.Category == "Smartplug").ToList();
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Power Plug
        public ActionResult generateData()
        {
            List<SmartPlugWidgetAnalyticZone_REC> Result = new List<SmartPlugWidgetAnalyticZone_REC>();

            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartplugWidget_REC>("SmartplugWidgetRealtime");

            for (int a = 0; a < 24; a++)
            {
                string H = a.ToString("D2");
                SmartplugWidget_REC newData = new SmartplugWidget_REC();
                newData.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd "+H+":00:00"));
                newData.DeviceID = "0002.0001.0000.0016";
                newData.DeviceName = "SP0016";
                newData.IPAddress = "10.0.1.56";
                newData.Location = "F07Z01";
                newData.Consumption = 0;
                collecCount.InsertOne(newData);
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PowerplugWeeklyConsumption(string zone, string ofilter)
        {
            DateTime today = DateTime.Today;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var dates = Enumerable.Range(0, 7).Select(days => sunday.AddDays(days)).ToList();

            List<SmartPlugWidgetAnalytic_REC> Result = new List<SmartPlugWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartplugWidget_REC>("SmartplugWidget");

            int no = 1;
            Dictionary<string, int> dictionary = new Dictionary<string, int> { { "Mon", 1 }, { "Tue", 2 }, { "Wed", 3 }, { "Thu", 4 }, { "Fri", 5 }, { "Sat", 6 }, { "Sun", 7 } };
            foreach (DateTime date in dates)
            {
                SmartPlugWidgetAnalytic_REC ResultData = new SmartPlugWidgetAnalytic_REC();

                // get data
                var filter = new BsonDocument
                {
                    {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
                    {"$lte", DateTime.Now}
                };
                string match = "{$match: { RecordTimestamp : " + filter + " }}";
                if (zone != "All" && zone != null && zone != "")
                {
                    if (ofilter == "floor")
                    {
                        match = "{$match: {  Location: {$regex : \".*" + zone + ".*\" }, RecordTimestamp : " + filter + " }}";
                    }
                    else
                    {
                        match = "{$match: {  Location: {$in: [\"" + zone + "\"] }, RecordTimestamp : " + filter + " }}";
                    }
                    
                }

                var pipeline = collecCount.Aggregate().AppendStage<SmartplugWidget_REC>(match);
                var res = pipeline.ToList();

                if (res.Count > 0)
                {
                    int i = 0;
                    ResultData.day = date.AddDays(0).DayOfWeek.ToString().Substring(0, 3);
                    ResultData.no = dictionary[ResultData.day];
                    ResultData.unit = "Kwh";
                    double bs = 0;
                    double cr = 0;
                    foreach (var resData in res)
                    {
                        if (resData.RecordTimestamp.AddDays(0).DayOfWeek.ToString().Substring(0, 3) == date.AddDays(0).DayOfWeek.ToString().Substring(0, 3))
                        {
                            double bl = (resData.Consumption * 25 / 100) + resData.Consumption;
                            bs += bl;
                            cr += resData.Consumption;
                            i++;
                        }
                    }
                    if (i > 0)
                    {
                        ResultData.Baseline = bs / i;
                        ResultData.Current = cr / i;
                        ResultData.Current = Convert.ToDouble(ResultData.Current.ToString("0.##"));
                        ResultData.Baseline = ResultData.Current;
                    }
                    else
                    {
                        ResultData.Baseline = 0;
                        ResultData.Current = 0;
                    }
                }
                else
                {
                    ResultData.day = date.AddDays(0).DayOfWeek.ToString().Substring(0, 3);
                    ResultData.no = dictionary[ResultData.day];
                    ResultData.unit = "Kwh";
                    ResultData.Baseline = 0;
                    ResultData.Current = 0;
                }

                Result.Add(ResultData);
                no++;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PowerplugSwithedOff(string zone, string ofilter)
        {
            List<HomeWidgetAnalytic_REC> Result = new List<HomeWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartplugSwitchedOff_REC>("SmartplugSwitchedOff");
            int no = 0;
            for (int a = 0; a < 24; a++)
            {
                HomeWidgetAnalytic_REC ResultData = new HomeWidgetAnalytic_REC();
                // get data
                var filter = new BsonDocument
                {
                    {"$gte", DateTime.Now.AddDays(-7)},
                    {"$lte", DateTime.Now}
                };
                string match = "{$match: { RecordTimestamp : " + filter + " }}";
                if (zone != "All" && zone != null && zone != "")
                {
                    if (ofilter == "floor")
                    {
                        match = "{$match: {  Location: {$regex : \".*" + zone + ".*\" }, RecordTimestamp : " + filter + " }}";
                    }
                    else
                    {
                        match = "{$match: {  Location: {$in: [\"" + zone + "\"] }, RecordTimestamp : " + filter + " }}";
                    }
                }

                var pipeline = collecCount.Aggregate().AppendStage<SmartplugSwitchedOff_REC>(match);
                var res = pipeline.ToList();

                if (res.Count > 0)
                {
                    int i = 0;
                    ResultData.day = no.ToString();
                    ResultData.no = no;
                    ResultData.unit = "off";
                    int cr = 0;
                    foreach (var resData in res)
                    {
                        int currentH = resData.RecordTimestamp.AddDays(0).Hour;
                        if (currentH == no)
                        {
                            int poOff = resData.Off - resData.On;
                            cr += poOff;
                            i++;
                        }
                    }
                    if (i > 0)
                    {
                        ResultData.Baseline = cr / i;
                        ResultData.Current = cr / i;
                    }
                    else
                    {
                        ResultData.Baseline = 0;
                        ResultData.Current = 0;
                    }
                }
                else
                {
                    ResultData.day = no.ToString();
                    ResultData.no = no;
                    ResultData.unit = "off";
                    ResultData.Baseline = 0;
                    ResultData.Current = 0;
                }

                Result.Add(ResultData);
                no++;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PowerPlugZone(string zone, string ofilter)
        {

            List<SmartPlugWidgetAnalyticZone_REC> Result = new List<SmartPlugWidgetAnalyticZone_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartplugWidget_REC>("SmartplugWidget");

            // get data
            var filter = new BsonDocument
            {
                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
                {"$lte", DateTime.Now}
            };
            string group = "{$group:{ _id:\"$Location\",avgConsumption: { $avg:\"$Consumption\"}}}";
            string match = "{$match: {RecordTimestamp : " + filter + " }}";

            if (zone != "All" && zone != null && zone != "")
            {
                if (ofilter == "floor")
                {
                    match = "{$match: {  Location: {$regex : \".*" + zone + ".*\" }, RecordTimestamp : " + filter + " }}";
                }
                else
                {
                    group = "{$group:{ _id:\"$DeviceName\",avgConsumption: { $avg:\"$Consumption\"}}}";
                    match = "{$match: {  Location: {$in: [\"" + zone + "\"] }, RecordTimestamp : " + filter + " }}";
                }
            }

            var pipeline = collecCount.Aggregate().AppendStage<SmartplugZone_REC>(match).AppendStage<SmartplugZone_REC>(group);
            var res = pipeline.SortByDescending(s => s.avgConsumption).Limit(5).ToList();

            int no = 1;
            double Total = 0;
            SmartPlugWidgetAnalyticZone_REC ResultData = new SmartPlugWidgetAnalyticZone_REC();

            LocationModel oModel = new LocationModel();
            DatabaseContext oRemoteDB = new DatabaseContext();
            IList<Location_REC> oTable = oModel.GetListOrderByType(oRemoteDB).ToList();

            foreach (SmartplugZone_REC data in res)
            {
                ResultData = new SmartPlugWidgetAnalyticZone_REC();
                ResultData.no = no;
                ResultData.unit = "Kwh";
                ResultData.Value = data.avgConsumption;
                ResultData.Value = Convert.ToDouble(ResultData.Value.ToString("0.##"));
                ResultData.Category = data._id;
                ResultData.zone = data._id;
                if (ofilter == "floor")
                {
                    ResultData.Category = oTable.Where(x => x.Zone == data._id).SingleOrDefault().LocationName;
                }
                ResultData.color = "#fff";
                Result.Add(ResultData);

                Total += data.avgConsumption;
                no++;
            }

            ResultData = new SmartPlugWidgetAnalyticZone_REC();
            ResultData.no = no+1;
            ResultData.unit = "Kwh";
            ResultData.Value = Total / no;
            ResultData.Value = Convert.ToDouble(ResultData.Value.ToString("0.##"));
            ResultData.Category = "Average";
            ResultData.color = "#525252";
            Result.Add(ResultData);

            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PowerplugConsumptionRealtime(string zone, string ofilter)
        {
            List<SmartPlugWidgetAnalytic_REC> Result = new List<SmartPlugWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartplugWidget_REC>("SmartplugWidgetRealtime");
            int no = 0;
            for (int a = 0; a < 24; a++)
            {
                SmartPlugWidgetAnalytic_REC ResultData = new SmartPlugWidgetAnalytic_REC();
                // get data
                var filter = new BsonDocument
                {
                    {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))},
                    {"$lte", DateTime.Now}
                };
                string match = "{$match: { RecordTimestamp : " + filter + " }}";
                if (zone != "All" && zone != null && zone != "")
                {
                    if (ofilter == "floor")
                    {
                        match = "{$match: {  Location: {$regex : \".*" + zone + ".*\" }, RecordTimestamp : " + filter + " }}";
                    }
                    else
                    {
                        match = "{$match: {  Location: {$in: [\"" + zone + "\"] }, RecordTimestamp : " + filter + " }}";
                    }
                }

                var pipeline = collecCount.Aggregate().AppendStage<SmartplugWidget_REC>(match);
                var res = pipeline.ToList();

                if (res.Count > 0)
                {
                    int i = 0;
                    ResultData.day = no.ToString();
                    ResultData.no = no;
                    ResultData.unit = "kwh";
                    double bs = 0;
                    double cr = 0;
                    foreach (var resData in res)
                    {
                        int currentH = resData.RecordTimestamp.AddDays(0).Hour;
                        if (currentH == no)
                        {
                            double bl = (resData.Consumption * 25 / 100) + resData.Consumption;
                            bs += bl;
                            cr += resData.Consumption;
                            i++;
                        }
                    }
                    if (i > 0)
                    {
                        ResultData.Baseline = bs;
                        ResultData.Current = cr;
                    }
                    else
                    {
                        ResultData.Baseline = 0;
                        ResultData.Current = 0;
                    }
                }
                else
                {
                    ResultData.day = no.ToString();
                    ResultData.no = no;
                    ResultData.unit = "kwh";
                    ResultData.Baseline = 0;
                    ResultData.Current = 0;
                }

                Result.Add(ResultData);
                no++;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SwithedOffRealtime(string zone, string ofilter)
        {
            List<HomeWidgetAnalytic_REC> Result = new List<HomeWidgetAnalytic_REC>();
            var database = Constants.client.GetDatabase("IoT");
            var collecCount = database.GetCollection<SmartplugSwitchedOff_REC>("SmartplugSwitchedOff");
            int no = 0;
            for (int a = 0; a < 24; a++)
            {
                HomeWidgetAnalytic_REC ResultData = new HomeWidgetAnalytic_REC();
                // get data
                var filter = new BsonDocument
                {
                    {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))},
                    {"$lte", DateTime.Now}
                };
                string match = "{$match: { RecordTimestamp : " + filter + " }}";
                if (zone != "All" && zone != null && zone != "")
                {
                    if (ofilter == "floor")
                    {
                        match = "{$match: {  Location: {$regex : \".*" + zone + ".*\" }, RecordTimestamp : " + filter + " }}";
                    }
                    else
                    {
                        match = "{$match: {  Location: {$in: [\"" + zone + "\"] }, RecordTimestamp : " + filter + " }}";
                    }
                }

                var pipeline = collecCount.Aggregate().AppendStage<SmartplugSwitchedOff_REC>(match);
                var res = pipeline.ToList();

                if (res.Count > 0)
                {
                    int i = 0;
                    ResultData.day = no.ToString();
                    ResultData.no = no;
                    ResultData.unit = "off";
                    int cr = 0;
                    foreach (var resData in res)
                    {
                        int currentH = resData.RecordTimestamp.AddDays(0).Hour;
                        if (currentH == no)
                        {
                            int poOff = resData.Off - resData.On;
                            cr += poOff;
                            i++;
                        }
                    }
                    if (i > 0)
                    {
                        ResultData.Baseline = cr;
                        ResultData.Current = cr;
                    }
                    else
                    {
                        ResultData.Baseline = 0;
                        ResultData.Current = 0;
                    }
                }
                else
                {
                    ResultData.day = no.ToString();
                    ResultData.no = no;
                    ResultData.unit = "off";
                    ResultData.Baseline = 0;
                    ResultData.Current = 0;
                }

                Result.Add(ResultData);
                no++;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "Sensor"
        public ActionResult GetSensorDeviceByZone(string zone)
        {
            var database = Constants.client.GetDatabase("IoT");
            var collection = database.GetCollection<Sensor_REC>("SensorDataDevice");
            var filter = new BsonDocument(){{"Zone", zone}};
            List<Sensor_REC> res = collection.Find(filter).SortBy(s=>s.RecordStatus).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSensorTemperature()
        {
            var database = Constants.client.GetDatabase("IoT");
            var collection = database.GetCollection<Sensor_REC>("SensorDataDevice");
            var filter = new BsonDocument() { { "DeviceName", "Temperature" } };
            List<Sensor_REC> res = collection.Find(filter).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Elevator"
        public ActionResult GetElevatorList()
        {
            ElevatorQuery oQuery = new ElevatorQuery();
            List<Elevator_REC> oModel = oQuery.GetList().ToList();
            return Json(oModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ObjectPeakOperation()
        {
            IMongoDatabase database = Constants.client.GetDatabase("IoT");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            var filter = new BsonDocument();

            filter.AddRange(new Dictionary<string, object>() { { "Table", "G0053350" } });
            filter.AddRange(new BsonDocument() {
                {
                    "Address", new BsonDocument(){
                        { "$in", new BsonArray() { "81","82" } }
                    }
                }
            });

            var sort = new BsonDocument();
            sort.AddRange(new BsonDocument() { { "Address", 1 } });

            IList<ElevatorObject_REC> vResult = ElevatorDataObject.Find(filter).Sort(sort).ToList();

            return Json(vResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ObjectHeartbeat()
        {
            IMongoDatabase database = Constants.client.GetDatabase("IoT");
            IMongoCollection<ElevatorObject_REC> ElevatorDataObject = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            var filter = new BsonDocument();

            filter.AddRange(new BsonDocument() {
                {
                    "Address", new BsonDocument(){
                        { "$in", new BsonArray() { "1FE","1FF" } }
                    }
                }
            });

            var sort = new BsonDocument();
            sort.AddRange(new BsonDocument() { { "Address", 1 } });

            IList<ElevatorObject_REC> vResult = ElevatorDataObject.Find(filter).Sort(sort).ToList();

            return Json(vResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ElevatorCommands(List<string> aList, List<string> dList, DateTime? RunOn, string Operation, string cmdDescription)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                cmdDevice voResData = new cmdDevice();
                List<cmds> ListcmdData = new List<cmds>();

                for (int i = 0; i < aList.Count; i++)
                {
                    cmds cmdData = new cmds();
                    int a = int.Parse(aList[i], System.Globalization.NumberStyles.HexNumber);
                    ushort d = Convert.ToUInt16(dList[i], 2);
                    cmdData.a = a;
                    cmdData.d = d;

                    ListcmdData.Add(cmdData);
                }

                Double now = (Double)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                voResData.timeStamp = now.ToString("0");
                voResData.command = ListcmdData;

                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                Constants.MqttClient.Publish("M2L/sg/hitachiasia/G0053350", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                ResultData.Add("msg", "Send Command Success");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Success");
            }
            catch (Exception ex)
            {
                ResultData.Add("msg", ex.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Error");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ElevatorGetLastCommand()
        {
            IMongoDatabase database = Constants.client.GetDatabase("IoT");
            IMongoCollection<ElevatorObject_REC> Collections = database.GetCollection<ElevatorObject_REC>("ElevatorDataObject");
            object oData = new { };

            string[] AddressParking = { "20" };

            string[] AddressUpPeak = { "81", "89", "91", "99" };
            string[] AddressDownPeak = { "82", "8A", "92", "9A" };

            string[] AddressStandbyFloor1 = { "41", "49", "51", "59" };
            string[] AddressStandbyFloor2 = { "43", "4B", "53", "5B" };
            string[] AddressStandbyFloor3 = { "45", "4D", "55", "5D" };

            string[] AddressStandbyElevator1 = { "42", "4A", "52", "5A" };
            string[] AddressStandbyElevator2 = { "44", "4C", "54", "5C" };
            string[] AddressStandbyElevator3 = { "46", "4E", "56", "5E" };

            string[] joinAddress = AddressParking
                .Union(AddressUpPeak).ToArray()
                .Union(AddressDownPeak).ToArray()
                .Union(AddressStandbyFloor1).ToArray()
                .Union(AddressStandbyFloor2).ToArray()
                .Union(AddressStandbyFloor3).ToArray()
                .Union(AddressStandbyElevator1).ToArray()
                .Union(AddressStandbyElevator2).ToArray()
                .Union(AddressStandbyElevator3).ToArray();

            var filter_mongo = new BsonDocument();
            Dictionary<string, object> obj_filter = new Dictionary<string, object>();
            obj_filter.Add("Table", "G0053350");
            obj_filter.Add("Address", new BsonDocument("$in", new BsonArray(joinAddress)));
            filter_mongo.AddRange(obj_filter);

            List<ElevatorObject_REC> voList = Collections.Find(filter_mongo).ToList();
            string parkingData = "";
            object peakOperationData = new { command = "", floor = "" };

            string e1 = ""; string f1 = ""; string e2 = ""; string f2 = ""; string e3 = ""; string f3 = "";
            foreach (ElevatorObject_REC voTemp in voList)
            {
                if (AddressParking.Contains(voTemp.Address))
                    parkingData = intToBinary(voTemp.d);

                if (AddressUpPeak.Contains(voTemp.Address) && voTemp.d > 0)
                    peakOperationData = new { command = "up", floor = voTemp.d };

                if (AddressDownPeak.Contains(voTemp.Address) && voTemp.d > 0)
                    peakOperationData = new { command = "down", floor = voTemp.d };

                if (AddressStandbyFloor1.Contains(voTemp.Address))
                    f1 = voTemp.d.ToString();
                if (AddressStandbyFloor2.Contains(voTemp.Address))
                    f2 = voTemp.d.ToString();
                if (AddressStandbyFloor3.Contains(voTemp.Address))
                    f3 = voTemp.d.ToString();

                if (AddressStandbyElevator1.Contains(voTemp.Address))
                    e1 = voTemp.d.ToString();
                if (AddressStandbyElevator2.Contains(voTemp.Address))
                    e2 = voTemp.d.ToString();
                if (AddressStandbyElevator3.Contains(voTemp.Address))
                    e3 = voTemp.d.ToString();
            }
            object s1Data = new { elevator = e1, floor = f1 };
            object s2Data = new { elevator = e2, floor = f2 };
            object s3Data = new { elevator = e3, floor = f3 };

            oData = new {
                parking = parkingData,
                peakOperation = peakOperationData,
                standby = new
                {
                    standby1 = s1Data,
                    standby2 = s2Data,
                    standby3 = s3Data,
                }
            };

            var jsonResult = Json(oData);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public string intToBinary(int val)
        {
            string binary = Convert.ToString(val, 2);
            string pad4 = Convert.ToInt32(binary).ToString("0000");
            return pad4;
        }
        public ActionResult ElevatorSchedulerGetList([DataSourceRequest]DataSourceRequest poRequest, string Status)
        {
            IMongoDatabase database = Constants.client.GetDatabase("IoT");
            IMongoCollection<ElevatorScheduler_REC> Collections = database.GetCollection<ElevatorScheduler_REC>("ElevatorScheduler");

            int skip = (poRequest.Page * poRequest.PageSize) - poRequest.PageSize;
            int limit = poRequest.PageSize;
            var ds = new List<ElevatorScheduler_REC>().ToDataSourceResult(poRequest);
            var filter_mongo = new BsonDocument();

            //if (filter_date.StartTime != DateTime.MinValue && filter_date.EndTime != DateTime.MinValue)
            //{
            //    Dictionary<string, object> obj_filter = new Dictionary<string, object>();
            //    obj_filter.Add("RecordTimestamp", new BsonDocument() {
            //        { "$gte", DateTime.Parse(filter_date.StartTime.ToString("yyyy-MM-dd HH:mm:00")) },
            //        { "$lte", DateTime.Parse(filter_date.EndTime.ToString("yyyy-MM-dd HH:mm:00")) }
            //    });
            //    filter_mongo.AddRange(obj_filter);
            //}

            if(Status != "" && Status != "All")
            {
                Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                obj_filter.Add("Status", Status);
                filter_mongo.AddRange(obj_filter);
            }

            var sort_mongo = new BsonDocument();
            if (poRequest.Filters.Count > 0)
            {
                foreach (var filter in poRequest.Filters)
                {
                    if (filter is FilterDescriptor)
                    {
                        FilterDescriptor filt = (FilterDescriptor)filter;
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                    else
                    {
                        FilterDescriptor filt = Constants.KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        Dictionary<string, object> obj_filter = new Dictionary<string, object>();
                        obj_filter.Add(filt.Member, BsonRegularExpression.Create(new Regex(filt.Value.ToString(), RegexOptions.IgnoreCase)));
                        filter_mongo.AddRange(obj_filter);
                    }
                }
            }

            if (poRequest.Sorts.Count > 0)
            {
                foreach (SortDescriptor sort in poRequest.Sorts)
                {
                    Dictionary<string, object> obj_sort = new Dictionary<string, object>();
                    if (sort.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        obj_sort.Add(sort.Member, 1);
                    else
                        obj_sort.Add(sort.Member, -1);

                    sort_mongo.AddRange(obj_sort);
                }
            }

            List<ElevatorScheduler_REC> oTable = Collections.Find(filter_mongo).Sort(sort_mongo).Skip(skip).Limit(limit).ToList();
            ds.Data = oTable;
            ds.Total = Convert.ToInt32(Collections.Find(filter_mongo).CountDocuments());

            var jsonResult = Json(ds);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult ElevatorSchedulerSet(List<string> aList, List<string> dList, DateTime RunOn, string Operation, string cmdDescription)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constants.client.GetDatabase("IoT");
                IMongoCollection<ElevatorScheduler_REC> collection = database.GetCollection<ElevatorScheduler_REC>("ElevatorScheduler");

                cmdDevice voResData = new cmdDevice();
                List<cmds> ListcmdData = new List<cmds>();

                for (int i = 0; i < aList.Count; i++)
                {
                    cmds cmdData = new cmds();
                    int a = int.Parse(aList[i], System.Globalization.NumberStyles.HexNumber);
                    ushort d = Convert.ToUInt16(dList[i], 2);
                    cmdData.a = a;
                    cmdData.d = d;

                    ListcmdData.Add(cmdData);
                }

                Double now = (Double)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                voResData.timeStamp = now.ToString("0");
                voResData.command = ListcmdData;
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));

                ElevatorScheduler_REC voRec = new ElevatorScheduler_REC();
                voRec.RecordTimestamp = DateTime.Now;
                voRec.Command = strValue;
                voRec.CommandDescription = cmdDescription;
                voRec.Operation = Operation;
                voRec.RunOn = RunOn;
                voRec.Status = "Upcoming";
                collection.InsertOne(voRec);

                ResultData.Add("msg", "Insert Scheduler Success");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Success");
            }
            catch (Exception ex)
            {
                ResultData.Add("msg", ex.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Error");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ElevatorSchedulerDelete(string _id)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            try
            {
                IMongoDatabase database = Constants.client.GetDatabase("IoT");
                IMongoCollection<ElevatorScheduler_REC> collection = database.GetCollection<ElevatorScheduler_REC>("ElevatorScheduler");

                FilterDefinitionBuilder<ElevatorScheduler_REC> whereclause_builder = Builders<ElevatorScheduler_REC>.Filter;
                FilterDefinition<ElevatorScheduler_REC> where_clause = whereclause_builder.Eq<ObjectId>("_id", ObjectId.Parse(_id));

                collection.DeleteOne(where_clause);

                ResultData.Add("msg", "Delete Scheduler Success");
                ResultData.Add("errorcode", "0");
                ResultData.Add("title", "Success");
            }
            catch (Exception ex)
            {
                ResultData.Add("msg", ex.Message);
                ResultData.Add("errorcode", "100");
                ResultData.Add("title", "Error");
            }
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }
        
        #endregion

        public ActionResult CustomerExperienceCenter(string zone)
        {
            var database = Constants.client.GetDatabase("IoT");
            var collection = database.GetCollection<Sensor_REC>("SensorDataDevice");
            var filter = new BsonDocument() { { "Zone", zone } };
            List<Sensor_REC> res = collection.Find(f=>f.Zone == zone).SortBy(s => s.RecordStatus).ToList();
            return Json(new { sensor = res }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerExperienceCenterPerson(string avatarId)
        {
            IMongoDatabase database = Constants.client.GetDatabase("IoT");

            IMongoCollection<Person> collectionPerson = database.GetCollection<Person>("Person");
            IMongoCollection<SmartWatchLog_REC> collectionLog = database.GetCollection<SmartWatchLog_REC>("SmartwatchDataLogs"); 
            var SensorData = database.GetCollection<Sensor_REC>("SensorDataDevice");

            Person voPerson = collectionPerson.Find(f => f.avatarID == avatarId).SortByDescending(s=>s.avatarID).Limit(1).SingleOrDefault();
            SmartWatchLog_REC voLog = collectionLog.Find(f => f.person == voPerson.personID && f.location != null).SortByDescending(s=>s._id).Limit(1).SingleOrDefault();


            var builder = Builders<Sensor_REC>.Filter;
            var filter = builder.Eq<string>("Zone", voLog.location);
            var resData = SensorData.Find(filter).ToList();

            var co2 = "0";
            var airVelocity = "0";
            var outdoorTemperature = "0";
            var humidity = "0";
            if (resData.Count > 0)
            {
                co2 = resData.Where(x => x.DeviceName == "CO2").SingleOrDefault().PresentValue;
                airVelocity = resData.Where(x => x.DeviceName == "Air Velocity").SingleOrDefault().PresentValue;
                outdoorTemperature = resData.Where(x => x.DeviceName == "Temperature").SingleOrDefault().PresentValue;
                humidity = resData.Where(x => x.DeviceName == "Relative Humidity").SingleOrDefault().PresentValue;
            }

            voLog.personName = voPerson.personName;
            voLog.co2 = co2;
            voLog.airVelocity = airVelocity;
            voLog.outdoorTemperature = outdoorTemperature;
            voLog.humidity = humidity;


            return Json(voLog);
        }

        /*public ActionResult CustomerExperienceCenterLightStatus(string statusID)
        {
            String ID = "0";
            IMongoDatabase database = Constants.client.GetDatabase("IoT");
            IMongoCollection<CurtainLogService> collectionCurtainStatus = database.GetCollection<CurtainLogService>("CurtainLogService");

            *//*Light ID
             * 539 = CoveLight
             * 527 = Surrounding Light
             * 8193 = Door Status
             *//*
            switch (statusID)
            {
                case "539":
                    ID = "28";
                    break;
                case "527":
                    ID = "1799";
                    break;
                case "8193":
                    ID = "8193";
                    break;
            }

            CurtainLogService voLogStatus = collectionCurtainStatus.Find(f => f.InstanceID == ID && f.Status == "").SortByDescending(s => s.RecordTimestamp).Limit(1).SingleOrDefault();
            return Json(voLogStatus);
        }*/

        public ActionResult RealtimeAlert()
        {
            IMongoDatabase database = Constants.client.GetDatabase("IoT");
            IMongoCollection<Alert_REC> collection = database.GetCollection<Alert_REC>("Alert");

            //BsonDocument[] stages = new BsonDocument[] {
            //    new BsonDocument{
            //        {
            //            "$match",
            //                new BsonDocument{
            //                    {"recordTimestamp",new BsonDocument
            //                        {
            //                            {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00")) },
            //                            {"$lte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 23:59:59")) }
            //                        }
            //                    }
            //                }
            //        }
            //    },
            //    new BsonDocument{
            //        {
            //            "$lookup",
            //            new BsonDocument {
            //                {"from", "Person"},
            //                {"localField", "uuid"},
            //                {"foreignField", "personID"},
            //                {"as", "personData"},
            //            }
            //        }
            //    },
            //    new BsonDocument
            //    {
            //        {
            //            "$sort",
            //            new BsonDocument {
            //                { "recordTimestamp", -1 }
            //            }
            //        }
            //    }
            //};

            var filter = new BsonDocument
            {
                {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00")) },
                {"$lte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 23:59:59")) }
            };
            var lookup = new BsonDocument
            {
                {"from", "Person"},
                {"localField", "uuid"},
                {"foreignField", "personID"},
                {"as", "personData"},
            };
            var sort = new BsonDocument
            {
                { "recordTimestamp", -1 }
            };
            string[] stage = new[] {
                "{ $match: {recordTimestamp : " + filter + " }}",
                "{ $lookup: " + lookup + " }",
                "{ $sort: " + sort + " }"
            };
            var oTable = collection.Aggregate()
                .AppendStage<Alert_REC>("{ $match: {recordTimestamp : " + filter + " }}")
            .AppendStage<Alert_REC>(stage[1])
            .AppendStage<Alert_REC>(stage[2]);
            var oResult = oTable.ToList();



            //var filter = new BsonDocument
            //    {
            //        {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 00:00:00"))},
            //        {"$lte", DateTime.Now}
            //    };
            //string match = "{$match: {RecordTimestamp : " + filter + " }}";

            //var pipeline = collecCount.Aggregate().AppendStage<HomeWidgetView_REC>(match);
            //var res = pipeline.ToList();


            var jsonResult = Json(oResult);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}