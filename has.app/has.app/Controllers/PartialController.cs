﻿using has.app.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace has.app.Controllers
{
    public class PartialController : Controller
    {
        public ActionResult _MainMenu()
        {
            return PartialView("Partials/_MainMenu");
        }
        public ActionResult _WidgetLocation()
        {
            return PartialView("Partials/_WidgetLocation");
        }
        public ActionResult _WidgetAnalytic()
        {
            return PartialView("Partials/_WidgetAnalytic");
        }
        public ActionResult _WidgetRealtime()
        {
            return PartialView("Partials/_WidgetRealtime");
        }
    }
}