/******************************************************************************************************************************/
/*** AUTHOR NAME : JOHN KENEDY                                                                                              ***/
/*** EMAIL       : sorainnosia@gmail.com                                                                                    ***/
/*** History     : 28 Nov 2018 - JOHN KENEDY - Initial Development                                                          ***/
/***             :                                                                                                          ***/
/******************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace has.app.Controllers
{
    public class ListenerController : Controller
    {
        public ListenerController()
        {

        }

        [HttpPost]
        [BasicAuthentication("ryadel", "12345", BasicRealm = "Ryadel")]
        public ActionResult Callback(string eid)
        {
            try
            {
                List<Callback> db = new List<Callback>();
                CVCoreLib.CVCoreConfig _Config = new CVCoreLib.CVCoreConfig();

                string rnd = MyRandom.RandomString(9);
                using (var ms = new MemoryStream(1024 * 10 * 5))
                {
                    //Request.Body.CopyTo(ms);

                    //string str = System.Text.Encoding.UTF8.GetString(ms.ToArray());

                    //if (string.IsNullOrEmpty(str) == false)
                    //{
                    //    LibraryTools.WriteName("HVAListener_Callback_" + eid + "_" + rnd, str);

                    //    Callback cb = new Callback();
                    //    cb.Base64orData = str;
                    //    cb.CreateOfficer = "SYSTEM";
                    //    cb.UpdateOfficer = cb.CreateOfficer;
                    //    cb.CreateDate = DateTime.Now;
                    //    cb.UpdateDate = cb.CreateDate;
                    //    cb.DataID = rnd;
                    //    cb.Filename = "";
                    //    db.Add(cb);
                    //}

                    int count = 0;
                    try
                    {
                        if (Request.Files != null && Request.Files.Count != 0)
                           count = Request.Files.Count;
                    }
                    catch { }

                    if (count == 1 && db.Count == 0 && Request.Files[0].FileName.ToLower().EndsWith(".jpg") == false)
                        return Content("Fail");

                    
                    if (count > 0)
                    {
                        foreach (var file in Request.Files)
                        {
                            string content = string.Empty;
                            try
                            {
                                using (var stream = new BinaryReader(((HttpPostedFileBase)Request.Files[file.ToString()]).InputStream))
                                {
                                    var filex = ((HttpPostedFileBase)Request.Files[file.ToString()]);
                                    byte[] by = null;
                                    using (MemoryStream ms2 = new MemoryStream())
                                    {
                                        stream.BaseStream.CopyTo(ms2);
                                        by = ms2.ToArray();
                                        content = System.Text.Encoding.ASCII.GetString(by);
                                    }

                                    string path = _Config.GetRecordPath();
                                    if (filex.FileName.ToLower().EndsWith(".jpg"))
                                    {
                                        if (Directory.Exists(path) == false) Directory.CreateDirectory(path);
                                        path = Path.Combine(path, rnd + ".jpg");
                                        System.IO.File.WriteAllBytes(path, by);
                                    }

                                    Callback cb = new Callback();
                                    if (filex.FileName.ToLower().EndsWith(".jpg"))
                                        cb.Base64orData = "/Content/Record/" + rnd + ".jpg";
                                    else
                                        cb.Base64orData = content;

                                    cb.CreateOfficer = "SYSTEM";
                                    cb.UpdateOfficer = cb.CreateOfficer;
                                    cb.CreateDate = DateTime.Now;
                                    cb.UpdateDate = cb.CreateDate;
                                    cb.DataID = rnd;
                                    cb.Filename = filex.FileName;
                                    db.Add(cb);

                                    LibraryTools.WriteName("HVAListener_Callback_" + eid + "_" + rnd, filex.FileName + "->" + content);
                                }
                            }
                            catch (Exception ex)
                            {
                                LibraryTools.WriteName("HVAListener_Callback_Error", "Read file fail : ", ex);
                            }
                        }
                    }

                    if (db.Count > 0 && _Config.GetCallbackDB())
                    {
                        SqlQueryBuilder<Callback> dal = new SqlQueryBuilder<Callback>();
                        foreach (Callback c in db)
                            dal.Insert(c);
                    }
                    Response.StatusCode = 200;
                    return Content("success");
                }
            }
            catch(Exception ex)
            {
                LibraryTools.WriteName("HVAListener_Callback_Error", ex);
                Response.StatusCode = 500;
                return Content("fail");
            }
        }
    }
}