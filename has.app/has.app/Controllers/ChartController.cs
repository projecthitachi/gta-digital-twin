﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using has.app.Components;
using has.app.Models;
using Microsoft.AspNet.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using uPLibrary.Networking.M2Mqtt.Messages;


namespace has.app.Controllers
{
    public class ChartController : Controller
    {
        public ActionResult GenerateData()
        {
            return View();
        }
        public ActionResult GenerateDummyData(paramGenerate atr)
        {
            Dictionary<string, string> ResultData = new Dictionary<string, string>();
            Dictionary<int, int> dictionary = new Dictionary<int,int> { { 1, 31 }, { 2, 28 }, { 3, 31 }, { 4, 30 }, { 5, 31 }, { 6, 30 }, { 7, 31 }, { 8, 31 }, { 9, 30 }, { 10, 31 }, { 11, 30 }, { 12, 31 } };

            var database = Constants.client.GetDatabase("IoT");
            var DataLogs = database.GetCollection<BacnetNew_REC>(atr.collection);

            for (int m = atr.StartM; m <= atr.EndM; m++)
            {
                int dl = dictionary[m];
                if (m == 8) { dl = 23; }
                for (int d = 1; d <= dl; d++)
                {
                    for (int h = 8; h <= 19; h++)
                    {
                        BacnetNew_REC voBacnet = new BacnetNew_REC();
                        string iString = "2019-" + m.ToString("D2") + "-" + d.ToString("D2") + " "+ h.ToString("D2") + ":00:02";
                        DateTime oDate = DateTime.ParseExact(iString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                        Random random = new Random();
                        int PresentValue = random.Next(atr.Min, atr.Max);
                        if (oDate.ToString("dddd") == "Sunday" || oDate.ToString("dddd") == "Saturday") {
                            PresentValue = random.Next(atr.Min, atr.Max) * 30 / 100;
                        }

                        voBacnet.RecordTimestamp = oDate;
                        voBacnet.DeviceID = atr.DeviceID;
                        voBacnet.DeviceName = atr.DeviceName;
                        voBacnet.ObjectName = atr.objName;
                        voBacnet.PresentValue = PresentValue;
                        voBacnet.InstanceID = atr.Instance;
                        voBacnet.Units = "Kwh";

                        DataLogs.InsertOne(voBacnet);
                        Thread.Sleep(100);
                    }
                }
            }

            ResultData.Add("msg", "Success");
            ResultData.Add("errorcode", "0");
            ResultData.Add("title", "Success");
            return Json(ResultData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConsDeviceHome()
        {
            var database = Constants.client.GetDatabase("IoT");
            var Aircond = database.GetCollection<DPC_Chart_REC>("AircondDataLogs");
            var Lighting = database.GetCollection<DPC_Chart_REC>("LightingDataLogs");
            var Elevator = database.GetCollection<DPC_Chart_REC>("ElevatorDataLogs");
            var Plugload = database.GetCollection<DPC_Chart_REC>("PlugloadDataLogs");

            var pipline = "{ $group: { _id: { day: { $dayOfMonth: \"$RecordTimestamp\"}, month: { $month: \"$RecordTimestamp\"}, year: { $year: \"$RecordTimestamp\" } },  Current: { $sum: \"$PresentValue\" }} }";
            var BuilderA = Aircond.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            var BuilderL = Lighting.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            var BuilderE = Elevator.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            var BuilderP = Plugload.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            Dictionary<string, List<DPC_Chart_REC>> Result = new Dictionary<string, List<DPC_Chart_REC>>();
            List<DPC_Chart_REC> Devices = new List<DPC_Chart_REC>();
            var resA = BuilderA.ToList().Where(w => w.DateTime >= DateTime.Now).OrderBy(o => o.Day);
            var resL = BuilderL.ToList().Where(w => w.DateTime >= DateTime.Now).OrderBy(o => o.Day);
            var resE = BuilderE.ToList().Where(w => w.DateTime >= DateTime.Now).OrderBy(o => o.Day);
            var resP = BuilderP.ToList().Where(w => w.DateTime >= DateTime.Now).OrderBy(o => o.Day);

            if (resA.Count() > 0)
            {
                Devices.AddRange(resA);
                Result.Add("AirCond", Devices);
            }
            else
            {
                DPC_Chart_REC nullD = new DPC_Chart_REC();
                nullD.Current = 0;
                Devices.Add(nullD);
                Result.Add("AirCond", Devices);
            }
            if (resA.Count() > 0)
            {
                Devices = new List<DPC_Chart_REC>();
                Devices.AddRange(resL);
                Result.Add("Lighting", Devices);
            }
            else
            {
                DPC_Chart_REC nullD = new DPC_Chart_REC();
                nullD.Current = 0;
                Devices.Add(nullD);
                Result.Add("Lighting", Devices);
            }
            if (resA.Count() > 0)
            {
                Devices = new List<DPC_Chart_REC>();
                Devices.AddRange(resE);
                Result.Add("Elevator", Devices);
            }
            else
            {
                DPC_Chart_REC nullD = new DPC_Chart_REC();
                nullD.Current = 0;
                Devices.Add(nullD);
                Result.Add("Elevator", Devices);
            }
            if (resA.Count() > 0)
            {
                Devices = new List<DPC_Chart_REC>();
                Devices.AddRange(resP);
                Result.Add("Plugload", Devices);
            }
            else
            {
                DPC_Chart_REC nullD = new DPC_Chart_REC();
                nullD.Current = 0;
                Devices.Add(nullD);
                Result.Add("Plugload", Devices);
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DayliPowerConsumtion()
        {
            var database = Constants.client.GetDatabase("IoT");
            var Aircond = database.GetCollection<DPC_Chart_REC>("AircondDataLogs");
            var Lighting = database.GetCollection<DPC_Chart_REC>("LightingDataLogs");
            var Elevator = database.GetCollection<DPC_Chart_REC>("ElevatorDataLogs");
            var Plugload = database.GetCollection<DPC_Chart_REC>("PlugloadDataLogs");

            var pipline = "{ $group: { _id: { day: { $dayOfMonth: \"$RecordTimestamp\"}, month: { $month: \"$RecordTimestamp\"}, year: { $year: \"$RecordTimestamp\" } },  Current: { $sum: \"$PresentValue\" }} }";
            var BuilderA = Aircond.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            var BuilderL = Lighting.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            var BuilderE = Elevator.Aggregate().AppendStage<DPC_Chart_REC>(pipline);
            var BuilderP = Plugload.Aggregate().AppendStage<DPC_Chart_REC>(pipline);

            List<DPC_Chart_REC> Result = new List<DPC_Chart_REC>();
            var resA = BuilderA.ToList().Where(w => w.DateTime >= DateTime.Now.AddDays(-7)).OrderBy(o => o.Day);
            var resL = BuilderL.ToList().Where(w => w.DateTime >= DateTime.Now.AddDays(-7)).OrderBy(o => o.Day);
            var resE = BuilderE.ToList().Where(w => w.DateTime >= DateTime.Now.AddDays(-7)).OrderBy(o => o.Day);
            var resP = BuilderP.ToList().Where(w => w.DateTime >= DateTime.Now.AddDays(-7)).OrderBy(o => o.Day);
            Result.AddRange(resA);
            Result.AddRange(resL);
            Result.AddRange(resE);
            Result.AddRange(resP);

            return Json(Result.GroupBy(p => p.DayName).Select(g => new { DayName = g.Key, Current = g.Sum(s => s.Current), BaseLine = g.Sum(s => s.BaseLine) }), JsonRequestBehavior.AllowGet);
        }
        
    }
}