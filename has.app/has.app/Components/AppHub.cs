﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace has.app.Components
{
    public class AppHub : Hub
    {
        public AppHub()
        {
            //if (VAHub.Broadcast == null) VAHub.Broadcast = new Action<object, bool>(Broadcast);
        }

        public string SetPlayback(string id)
        {

            //string conId = base.Context.ConnectionId;
            //dynamic d = base.Clients.Client(conId);
            //return VAHub.SetPlayback(d, conId, id);
            return string.Empty;
        }

        public string Receive(string maxo, string mino)
        {
            //return VAHub.Receive(maxo, mino);
            return string.Empty;
        }

        public void Broadcast(object obj, bool all)
        {
            if (all)
            {
                try
                {
                    base.Clients.All.Receive(obj);
                }
                catch (Exception ex)
                {
                    Common.LibraryTools.WriteName("BroadcastVA", ex);
                }
                return;
            }

            //if (VAHub.PlaybackClients == null || VAHub.PlaybackClients.Count == 0) return;

            //foreach (dynamic o in VAHub.PlaybackClients.Values)
            //{
            //    try
            //    {
            //        o.Receive(obj);
            //    }
            //    catch (Exception ex)
            //    {
            //        Common.LibraryTools.WriteName("BroadcastVA", ex);
            //    }
            //}
        }
        public void Smartlight(string Type, string Datas)
        {
            Clients.All.Smartlight(Type, Datas);
        }
        public void Smartplug(string Type, string Datas)
        {
            Clients.All.Smartplug(Type, Datas);
        }
        public void Send(string name, string massage, string type)
        {
            switch (type)
            {
                case "Dashboard":
                    Clients.All.Dashboard(name, massage);
                    break;
                case "TempAndHumidity":
                    Clients.All.TempAndHumidity(name, massage);
                    break;
                default:
                    Clients.All.broadcastMessage(name, massage);
                    break;
            }
        }
    }
}