﻿using Kendo.Mvc;
using Microsoft.AspNet.SignalR;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using uPLibrary.Networking.M2Mqtt;

namespace has.app.Components
{
    public class Constants
    {
        public static MqttClient MqttClient = null;

        public static IHubContext RT = null;
        public static bool init = true;
        public static bool start = true;
        public static string modulActive = "Home";
        public static MongoClient client = new MongoClient(WebConfigurationManager.AppSettings["mongodb_server"].ToString());
        public static int ElevatorLimitTime = Convert.ToInt32(WebConfigurationManager.AppSettings["elevator_limit_time"].ToString());
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public static string AddOrdinal(int num)
        {
            if (num <= 0) return "";

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return "th";
            }

            switch (num % 10)
            {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }

        }
        public static string IntToBinaryString(int number)
        {
            const int mask = 1;
            var binary = string.Empty;
            while (number > 0)
            {
                // Logical AND the number and prepend it to the result string
                binary = (number & mask) + binary;
                number = number >> 1;
            }

            return binary;
        }
        public static void writelog(string log)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(log + Environment.NewLine);
            // flush every 20 seconds as you do it
            string path = HttpRuntime.AppDomainAppPath + "Logs";
            File.AppendAllText(path + "/ElevatorLogs_"+DateTime.Now.ToString("yyyyMMdd")+".txt", sb.ToString());
            sb.Clear();
        }
        public static string digitalTwinScenes()
        {
            string jsonFilePath = HostingEnvironment.MapPath("~/Content/assets/digitaltwin/json/default_scenes.json"); ;
            string json = File.ReadAllText(jsonFilePath);
            return json;
        }
        public static FilterDescriptor KendoChangeComposite(IEnumerable<IFilterDescriptor> filters)
        {
            FilterDescriptor filt = new FilterDescriptor();
            foreach (var filter in filters)
            {
                if (filter is CompositeFilterDescriptor)
                    KendoChangeComposite(((CompositeFilterDescriptor)filter).FilterDescriptors);
                else
                    filt = ((FilterDescriptor)filter);
            }
            return filt;
        }
    }

}