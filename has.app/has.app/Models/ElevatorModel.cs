﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace has.app.Models
{
    public class ElevatorDataLog_REC//t70320
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [Display(Name = "RecordID")]
        public long RecordID { get; set; }

        [Display(Name = "RecordTimestamp")]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Record Status")]
        public Int32 RecordStatus { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "Elevator")]
        public string Elevator { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Payload")]
        public string Payload { get; set; }

        [Display(Name = "Command Type")]
        public string CommandType { get; set; }
    }
    public class ElevatorObject_REC//t70221
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [Display(Name = "RecordID")]
        public long RecordID { get; set; }

        [Display(Name = "RecordTimestamp")]
        public DateTime RecordTimestamp { get; set; }

        [Display(Name = "Record Status")]
        public Int32 RecordStatus { get; set; }

        [Display(Name = "Table")]
        public string Table { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "DeviceID")]
        public string DeviceID { get; set; }

        [Display(Name = "Elevator")]
        public int Elevator { get; set; }

        [Display(Name = "Floor")]
        public List<int> Floor { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        public int a { get; set; }
        public int d { get; set; }

        [UIHint("ClientCategory")]
        public CategoryView_REC Category
        {
            get;
            set;
        }
    }
    public class ElevatorScheduler_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [Display(Name = "Record Timestamp")]
        public DateTime RecordTimestamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [Display(Name = "Run On")]
        public DateTime RunOn { get; set; }
        [Display(Name = "Operation")]
        public string Operation { get; set; }
        [Display(Name = "Command")]
        public string Command { get; set; }
        [Display(Name = "Command")]
        public string CommandDescription { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
    public class CategoryView_REC
    {
        public string CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
    public class Elevator_REC//t70220
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Elevator { get; set; }
        public string StatusDevice { get; set; }
        public int Live { get; set; }
    }
    public class cmdDevice
    {
        public string timeStamp { get; set; }
        public List<cmds> command { get; set; }
    }
    public class cmds
    {
        public int a { get; set; }
        public int d { get; set; }
    }
    public class ElevatorQuery
    {
        #region "t70220fieldselect"
        private string t70220fieldselect = @"
            t70220r001 as RecordID,
            t70220r002 as RecordTimestamp,
            t70220r003 as RecordStatus,
            t70220f001 as DeviceID,
            t70220f002 as DeviceName,
            t70220f003 as StatusDevice,
            t70220f004 as Live,
            t70220f005 as Elevator";
        #endregion
        public DbRawSqlQuery<Elevator_REC> GetList()
        {
            DatabaseContext oRemoteDB = new DatabaseContext();

            string sSQL = " SELECT " + t70220fieldselect + " FROM dbo.t70220 " +
                "ORDER BY t70220f005 ASC;";

            var vQuery = oRemoteDB.Database.SqlQuery<Elevator_REC>(sSQL);

            return vQuery;
        }
        public DbRawSqlQuery<Elevator_REC> GetOne(string Elevator)
        {
            DatabaseContext oRemoteDB = new DatabaseContext();
            string sSQL = " SELECT " + t70220fieldselect + " FROM dbo.t70220 WHERE t70220f005 = '" + Elevator + "' ;";
            var vQuery = oRemoteDB.Database.SqlQuery<Elevator_REC>(sSQL);
            return vQuery;
        }
    }
}