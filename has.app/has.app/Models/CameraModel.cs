﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace has.app.Models
{
    public class Camera_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------ 
        public string Category { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public string UrlRtsp { get; set; }
        public string UrlService { get; set; }
    }
    public class CameraModel
    {
        public DbRawSqlQuery<Camera_REC> GetListDeviceCamera(DatabaseContext oRemoteDB)
        {
            string sSQL = @"
                SELECT
                t70203r001 as RecordID,
                t70203f003 as Category,
                t70203f001 as DeviceID,
                t70203f002 as DeviceName,
                t70203f004 as IPAddress,
                t70203f006 as LocationID,
                t8030f002 as LocationName,
                '' as UrlRtsp,
                t70103f005 as UrlService
                from t70203
                LEFT JOIN t8030 on t8030f001 = t70203f006
                LEFT JOIN t70103 on t70103f001 = 'CameraThermal'
                UNION SELECT
                t70204r001 as RecordID,
                t70204f003 as Category,
                t70204f001 as DeviceID,
                t70204f002 as DeviceName,
                t70204f004 as IPAddress,
                t70204f009 as LocationID,
                t8030f002 as LocationName,
                t70204f008 as UrlRtsp,
                t70104f003 as UrlService
                from t70204
                LEFT JOIN t8030 on t8030f001 = t70204f009
                LEFT JOIN t70104 on t70104f001 = 'CameraCCTV'
                ";
            var vQuery = oRemoteDB.Database.SqlQuery<Camera_REC>(sSQL);
            return vQuery;
        }
    }
}