﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace has.app.Models
{
    #region Home
    public class DeviceHomeConsum_REC
    {
        public int AirCond { get; set; }
        public int Lighting { get; set; }
        public int PlugLoad { get; set; }
        public int Elevator { get; set; }
    }
    public class HomeWidget_REC
    {
        public int RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int Lighting { get; set; }
        public int PowerPlug { get; set; }
        public int Aircond { get; set; }
        public int Elevator { get; set; }
        public int Total { get; set; }
        public int Light3 { get; set; }
        public int Light7 { get; set; }
        public int Power3 { get; set; }
        public int Power7 { get; set; }
        public int Air3 { get; set; }
        public int Air7 { get; set; }
        public int Elv1 { get; set; }
        public int Elv2 { get; set; }
        public int Elv3 { get; set; }
        public int Elv4 { get; set; }
        public int Flowrate { get; set; }
        public int Flowrate3f { get; set; }
        public int Flowrate7f { get; set; }
        public int Light3A { get; set; }
        public int Light7A { get; set; }
        public int Light7B { get; set; }
        public int Light7C { get; set; }
        public int Light7D { get; set; }
        public int Power3A { get; set; }
        public int Power7A { get; set; }
        public int Power7B { get; set; }
        public int Power7C { get; set; }
        public int Power7D { get; set; }
        public int Air3A { get; set; }
        public int Air3B { get; set; }
        public int Air7A { get; set; }
        public int Air7B { get; set; }
    }
    public class HomeWidgetView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int Lighting { get; set; }
        public int PowerPlug { get; set; }
        public int Aircond { get; set; }
        public int Elevator { get; set; }
        public int Total { get; set; }
        public int Light3 { get; set; }
        public int Light7 { get; set; }
        public int Power3 { get; set; }
        public int Power7 { get; set; }
        public int Air3 { get; set; }
        public int Air7 { get; set; }
        public int Elv1 { get; set; }
        public int Elv2 { get; set; }
        public int Elv3 { get; set; }
        public int Elv4 { get; set; }
        public int Flowrate { get; set; }
        public int Flowrate3f { get; set; }
        public int Flowrate7f { get; set; }
        public int Light3A { get; set; }
        public int Light7A { get; set; }
        public int Light7B { get; set; }
        public int Light7C { get; set; }
        public int Light7D { get; set; }
        public int Power3A { get; set; }
        public int Power7A { get; set; }
        public int Power7B { get; set; }
        public int Power7C { get; set; }
        public int Power7D { get; set; }
        public int Air3A { get; set; }
        public int Air3B { get; set; }
        public int Air7A { get; set; }
        public int Air7B { get; set; }
    }
    public class HomeWidgetAnalytic_REC
    {
        public int no { get; set; }
        public string day { get; set; }
        public string unit { get; set; }
        public int Baseline { get; set; }
        public int Current { get; set; }
    }
    public class HomeWidgetAnalyticEUI_REC
    {
        public int no { get; set; }
        public string Category { get; set; }
        public string unit { get; set; }
        public int Value { get; set; }
        public string color { get; set; }
    }
    public class HomeWidgetAnalyticECD_REC
    {
        public int no { get; set; }
        public string category { get; set; }
        public double value { get; set; }
        public string color { get; set; }
    }
    #endregion

    #region Power
    public class FlowRateDataLog_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public double PresentValue { get; set; }
        public string floor { get; set; }
    }
    public class WidgetFlowRateView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long Timestamp { get; set; }
        public decimal CHWSTemp { get; set; }
        public decimal CHWRTemp { get; set; }
        public decimal CHWSFlow { get; set; }
        public decimal KW { get; set; }
        public decimal KWh { get; set; }
        public decimal CHWSTemp3f { get; set; }
        public decimal CHWRTemp3f { get; set; }
        public decimal CHWSFlow3f { get; set; }
        public decimal KW3f { get; set; }
        public decimal KWh3f { get; set; }
        public decimal CHWSTemp7f { get; set; }
        public decimal CHWRTemp7f { get; set; }
        public decimal CHWSFlow7f { get; set; }
        public decimal KW7f { get; set; }
        public decimal KWh7f { get; set; }
    }
    public class WidgetFlowRate_REC
    {
        public long Timestamp { get; set; }
        public decimal CHWSTemp { get; set; }
        public decimal CHWRTemp { get; set; }
        public decimal CHWSFlow { get; set; }
        public decimal KW { get; set; }
        public decimal KWh { get; set; }
        public decimal CHWSTemp3f { get; set; }
        public decimal CHWRTemp3f { get; set; }
        public decimal CHWSFlow3f { get; set; }
        public decimal KW3f { get; set; }
        public decimal KWh3f { get; set; }
        public decimal CHWSTemp7f { get; set; }
        public decimal CHWRTemp7f { get; set; }
        public decimal CHWSFlow7f { get; set; }
        public decimal KW7f { get; set; }
        public decimal KWh7f { get; set; }
    }
    public class FlowRate_REC
    {
        public long Timestamp { get; set; }
        public decimal CHWSTemp { get; set; }
        public decimal CHWRTemp { get; set; }
        public decimal CHWSFlow { get; set; }
        public decimal KW { get; set; }
        public decimal KWh { get; set; }
        public int Floor { get; set; }
    }

    public class PowerChart_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int Elv1 { get; set; }
        public int Elv2 { get; set; }
        public int Elv3 { get; set; }
        public int Elv4 { get; set; }
        public int Flowrate { get; set; }
        public int Flowrate3f { get; set; }
        public int Flowrate7f { get; set; }
        public int Light3A { get; set; }
        public int Light7A { get; set; }
        public int Light7B { get; set; }
        public int Light7C { get; set; }
        public int Light7D { get; set; }
        public int Power3A { get; set; }
        public int Power7A { get; set; }
        public int Power7B { get; set; }
        public int Power7C { get; set; }
        public int Power7D { get; set; }
        public int Air3A { get; set; }
        public int Air3B { get; set; }
        public int Air7A { get; set; }
        public int Air7B { get; set; }
    }
    #endregion

    #region Feedback
    public class FeedbackAge_REC
    {
        public string _id { get; set; }
        public int below { get; set; }
        public int young { get; set; }
        public int old { get; set; }
        public int above { get; set; }
    }
    public class FeedbackTDT_REC
    {
        public string _id { get; set; }
        public int before { get; set; }
        public int afternon { get; set; }
        public int evening { get; set; }
        public int night { get; set; }
    }
    public class FeedbackTCS_REC
    {
        public string _id { get; set; }
        public int AA { get; set; }
        public int AB { get; set; }
        public int AC { get; set; }
        public int AD { get; set; }
        public int BA { get; set; }
        public int CA { get; set; }
        public int DA { get; set; }
    }
    public class FeedbackChart_REC
    {
        public string _id { get; set; }
        public int no { get; set; }
        public string Category { get; set; }
        public string unit { get; set; }
        public int Value { get; set; }
        public string color { get; set; }
    }
    #endregion

    #region Smart Device
    public class SmartLog_REC
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
    }
    public class looging_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string type { get; set; }
        public string msg { get; set; }
    }
    public class SmartDevice_REC
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string IPAddress { get; set; }
        public int Relay { get; set; }
        public int Status { get; set; }
        public int TotalUsage { get; set; }
    }
    public class SmartDeviceView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string IPAddress { get; set; }
        public int Relay { get; set; }
        public int Status { get; set; }
        public int TotalUsage { get; set; }
    }
    public class SmartDevices_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public double Consumption { get; set; }
    }
    public class PowerMesh_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
    }
    public class SmartWidget_REC
    {
        public smart_id _id { get; set; }
        public decimal ApparentPower { get; set; }
    }
    public class smart_id
    {
        public string DeviceID { get; set; }
    }
    public class SmartPlugLevelUseage_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int recordID { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        public string category { get; set; }
        public int min { get; set; }
        public int max { get; set; }
        public int med { get; set; }
        public string titleMin { get; set; }
        public string titleMed { get; set; }
        public string titleMax { get; set; }
        public string ip { get; set; }
    }
    public class SmartPlugWeekdayConsumtionAvg_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }
        public string day { get; set; }
        public string unit { get; set; }
        public int Baseline { get; set; }
        public int Current { get; set; }
        public string ip { get; set; }
    }
    #endregion

    public class Bacnet_REC
    {
        public long RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public string PresentValue { get; set; }
    }

    public class BacnetNew_REC
    {
        public long RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public int PresentValue { get; set; }
        public string Units { get; set; }
    }

    public class BacnetNewView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public int PresentValue { get; set; }
        public string Units { get; set; }
    }

    public class ExampleGroup
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string SomeStringField { get; set; }
        public int SomeNumberField { get; set; }
    }

    public class Sensor_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string Node { get; set; }
        public string Chanel { get; set; }
        public string PresentValue { get; set; }
        public string Units { get; set; }
        public string Zone { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
    }

    public class DPC_Chart_REC
    {
        Random random = new Random();
        public o_date _id { get; set; }
        public DateTime DateTime {
            get {
                if(_id != null)
                {
                    return Convert.ToDateTime(string.Concat(_id.month, "/", _id.day, "/", _id.year));
                }
                else
                {
                    return DateTime.Now;
                }
            }
            set {
                Convert.ToDateTime(String.Concat(_id.month, "/", _id.day, "/", _id.year));
            }
        }
        public string Date {
            get {
                if(_id != null)
                {
                    return String.Concat(_id.month, "/", _id.day, "/", _id.year);
                }
                else
                {
                    return "";
                }
            }
            set {
                String.Concat(_id.month, "/", _id.day, "/", _id.year);
            }
        }
        public int Day {
            get {
                if (_id != null)
                {
                    return _id.day;
                }
                else
                {
                    return 0;
                }
            }
            set { _id.day.ToString(); }
        }
        public int Month {
            get {
                if (_id != null)
                {
                    return _id.month;
                }
                else
                {
                    return 0;
                }
            } set { _id.month.ToString(); } }
        public int Year {
            get {
                if (_id != null)
                {
                    return _id.year;
                }
                else
                {
                    return 0;
                }
            } set {  _id.year.ToString(); }
        }
        public string DayName {
            get { return (DateTime != null) ? DateTime.ToString("ddd") : ""; }
            set { DateTime.ToString("ddd"); }
        }
        public string MonthName { get { return (DateTime != null) ? DateTime.ToString("MMM") : ""; } set { DateTime.ToString("MMM"); } }
        public int Current { get; set; }
        public int BaseLine {
            get {
                int max = Current + (Current * 20 / 100);
                return random.Next(Current, max);
            }
            set {
                int max = Current + (Current * 20 / 100);
                random.Next(Current, max).ToString();
            }
        }
    }

    public class Power_REC
    {
        Random random = new Random();
        public o_date _id { get; set; }
        public DateTime DateTime
        {
            get
            {
                if (_id != null)
                {
                    return Convert.ToDateTime(string.Concat(_id.month, "/", _id.day, "/", _id.year));
                }
                else
                {
                    return DateTime.Now;
                }
            }
            set
            {
                Convert.ToDateTime(String.Concat(_id.month, "/", _id.day, "/", _id.year));
            }
        }
        public string Date
        {
            get
            {
                if (_id != null)
                {
                    return String.Concat(_id.month, "/", _id.day, "/", _id.year);
                }
                else
                {
                    return "";
                }
            }
            set
            {
                String.Concat(_id.month, "/", _id.day, "/", _id.year);
            }
        }
        public int Day
        {
            get
            {
                if (_id != null)
                {
                    return _id.day;
                }
                else
                {
                    return 0;
                }
            }
            set { _id.day.ToString(); }
        }
        public int Month
        {
            get
            {
                if (_id != null)
                {
                    return _id.month;
                }
                else
                {
                    return 0;
                }
            }
            set { _id.month.ToString(); }
        }
        public int Year
        {
            get
            {
                if (_id != null)
                {
                    return _id.year;
                }
                else
                {
                    return 0;
                }
            }
            set { _id.year.ToString(); }
        }
        public string DayName
        {
            get { return (DateTime != null) ? DateTime.ToString("ddd") : ""; }
            set { DateTime.ToString("ddd"); }
        }
        public string MonthName { get { return (DateTime != null) ? DateTime.ToString("MMM") : ""; } set { DateTime.ToString("MMM"); } }
        public string ObjectName {
            get { return (_id != null) ? _id.ObjectName : ""; }
            set { _id.ObjectName.ToString(); }
        }
        public int PresentValue {
            get { return (_id != null) ? Convert.ToInt32(_id.PresentValue) : 0; }
            set { Convert.ToInt32(_id.PresentValue); }
        }
    }

    public class o_date
    {
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string ObjectName { get; set; }
        public string PresentValue { get; set; }
    }

    public class paramGenerate
    {
        public int StartD { get; set; }
        public int EndD { get; set; }
        public int StartM { get; set; }
        public int EndM { get; set; }
        public int Max { get; set; }
        public int Min { get; set; }
        public string collection { get; set; }
        public string objName { get; set; }
        public string Instance { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
    }

    public class Alert_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }

        public int recordStatus { get; set; }
        
        public string uuid { get; set; }
        public string personName { get; set; }
        public string location { get; set; }
        public string locationID { get; set; }
        public string thermalComfortSatisfaction { get; set; }
        public string thermalSensation { get; set; }

        public string temperature { get; set; }
        public string humidity { get; set; }
        public string co2 { get; set; }
        public string airVelocity { get; set; }
        public string skinTemperature { get; set; }
        public string heartRate { get; set; }
        public string step { get; set; }

        public List<Person> personData { get; set; }
    }
    public class Person
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string personID { get; set; }
        public virtual string personName { get; set; }
        public virtual string defaultZone { get; set; }
        public virtual string avatarID { get; set; }
        public virtual string uuid { get; set; }
    }
    public class HumanResence_REC
    {
        public long recordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string uuid { get; set; }
        public string location { get; set; }
        public int thermalSensation { get; set; }
        public int thermalComfortSatisfaction { get; set; }
        public int totalPerson { get; set; }
    }

    public class HumanResenceHourly_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string category { get; set; }
        public int value { get; set; }
    }

    public class Users_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long recordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        //------------------------------
        [Key]
        public string uuid { get; set; }
        public string password { get; set; }
        public DateTime? birthDate { get; set; }
        public int age { get; set; }
        public string gender { get; set; }
        public string race { get; set; }
        public string raceSpecify { get; set; }
        public decimal height { get; set; }
        public decimal weight { get; set; }

    }
    public class PersonLocation
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public virtual string gatewayID { get; set; }
        public virtual string type { get; set; }
        public virtual string tagID { get; set; }
        public virtual string rssi { get; set; }
        public virtual string personID { get; set; }
        public virtual string location { get; set; }
        public virtual string defaultZone { get; set; }
        public virtual string avatarID { get; set; }
        public virtual string personName { get; set; }
    }
    public class PowerWidgetView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int Lighting { get; set; }
        public int PowerPlug { get; set; }
        public int Aircond { get; set; }
        public int Elevator { get; set; }
        public int Total { get; set; }
        public int Light3 { get; set; }
        public int Light7 { get; set; }
        public int Power3 { get; set; }
        public int Power7 { get; set; }
        public int Air3 { get; set; }
        public int Air7 { get; set; }
        public int Elv1 { get; set; }
        public int Elv2 { get; set; }
        public int Elv3 { get; set; }
        public int Elv4 { get; set; }
        public int Flowrate { get; set; }
        public int Flowrate3f { get; set; }
        public int Flowrate7f { get; set; }
        public int Light3A { get; set; }
        public int Light7A { get; set; }
        public int Light7B { get; set; }
        public int Light7C { get; set; }
        public int Light7D { get; set; }
        public int Power3A { get; set; }
        public int Power7A { get; set; }
        public int Power7B { get; set; }
        public int Power7C { get; set; }
        public int Power7D { get; set; }
        public int Air3A { get; set; }
        public int Air3B { get; set; }
        public int Air7A { get; set; }
        public int Air7B { get; set; }
        public int presentVal { get; set; }
    }
    public class PowerWidgetView2_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int RecordID { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int Lighting { get; set; }
        public int PowerPlug { get; set; }
        public int Aircond { get; set; }
        public int Elevator { get; set; }
        public int Total { get; set; }
        public int Light3 { get; set; }
        public int Light7 { get; set; }
        public int Power3 { get; set; }
        public int Power7 { get; set; }
        public int Air3 { get; set; }
        public int Air7 { get; set; }
        public int Elv1 { get; set; }
        public int Elv2 { get; set; }
        public int Elv3 { get; set; }
        public int Elv4 { get; set; }
        public int Flowrate { get; set; }
        public int Flowrate3f { get; set; }
        public int Flowrate7f { get; set; }
        public int Light3A { get; set; }
        public int Light7A { get; set; }
        public int Light7B { get; set; }
        public int Light7C { get; set; }
        public int Light7D { get; set; }
        public int Power3A { get; set; }
        public int Power7A { get; set; }
        public int Power7B { get; set; }
        public int Power7C { get; set; }
        public int Power7D { get; set; }
        public int Air3A { get; set; }
        public int Air3B { get; set; }
        public int Air7A { get; set; }
        public int Air7B { get; set; }
        public int presentVal3f { get; set; }
        public int presentVal7f { get; set; }
    }
    public class PowerWidgetAnalytic_REC
    {
        public int no { get; set; }
        public string Type { get; set; }
        public string Days { get; set; }
        public int baseline { get; set; }
        public int current { get; set; }
    }

    public class AirconDataVAV_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public int DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int InstanceID { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public string GroupVAV { get; set; }
        public string Title { get; set; }
        public string Unit { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public double PresentValue { get; set; }

    }

    public class SmartPlugWidgetAnalytic_REC
    {
        public int no { get; set; }
        public string day { get; set; }
        public string unit { get; set; }
        public double Baseline { get; set; }
        public double Current { get; set; }
    }

    public class SmartPlugWidgetAnalyticZone_REC
    {
        public int no { get; set; }
        public string Category { get; set; }
        public string zone { get; set; }
        public string unit { get; set; }
        public double Value { get; set; }
        public string color { get; set; }
    }
    public class SmartplugWidget_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string Location { get; set; }
        public double Consumption { get; set; }
    }

    public class SmartplugSwitchedOff_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string Location { get; set; }
        public int On { get; set; }
        public int Off { get; set; }
    }

    public class SmartplugZone_REC
    {
        [BsonId]
        public string _id { get; set; }
        public double avgConsumption { get; set; }
        public string zone { get; set; }
    }

    public class SmartWatchLog_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public virtual ObjectId _id { get; set; }
        public virtual string mac { get; set; }
        public virtual string person { get; set; }
        public virtual string location { get; set; }
        public virtual string type { get; set; }
        public virtual int battery { get; set; }
        public virtual int heartrate { get; set; }
        public virtual int step { get; set; }
        public virtual int sleep { get; set; }
        public virtual int calories { get; set; }
        public virtual int blood { get; set; }
        public virtual decimal temperature { get; set; }
        //public virtual int sos { get; set; }
        //public virtual int unwear { get; set; }
        public virtual bool sos { get; set; }
        public virtual bool unwear { get; set; }
        public virtual bool falling { get; set; }
        public virtual int nearby { get; set; }
        public virtual bool battery_update { get; set; }
        public virtual bool blood_update { get; set; }
        public virtual bool temperature_update { get; set; }
        //additional field
        public virtual string personName { get; set; }
        public virtual string co2 { get; set; }
        public virtual string airVelocity { get; set; }
        public virtual string outdoorTemperature { get; set; }
        public virtual string humidity { get; set; }

    }

    public class Curtain_CEC
    {
        public ObjectId _id { get; set; }
        public string LuxValue { get; set; }
        public string DimmingValue { get; set; }
        public string OccupantNumber { get; set; }
        public string CurtainStatusLeft { get; set; }
        public string CurtainStatusRight { get; set; }
    }

    public class CurtainLogService
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public string PresentValue { get; set; }
        public string Status { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectType { get; set; }
        public string Plugin { get; set; }
    }
}