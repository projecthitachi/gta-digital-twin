﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace has.app.Models
{
    public class Location_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public string Zone { get; set; }

        public long ParentMenuID { get; set; }
        public string LocationType { get; set; }
        public string CameraPosition { get; set; }
        public string ControlTarget { get; set; }
        public string ObjectName { get; set; }

    }

    public class LocationDetail_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        //------------------------------
        [Key]
        public long ParentID { get; set; }
        public string ReaderID { get; set; }
        public int AntennaNo { get; set; }
        public decimal MaxRSSI { get; set; }

        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public string CameraPosition { get; set; }
        public string ControlTarget { get; set; }
        public string ObjectName { get; set; }
    }

    public class LocationModel
    {
        public int ErrorNo { get; set; }
        public string ErrorMessage { get; set; }

        public LocationModel()
        {
            ErrorNo = 0;
            ErrorMessage = "";
        }
        #region "t8030fieldselect"
        private string t8030fieldselect = @"
            t8030r001 as RecordID,
            t8030r002 as RecordTimestamp,
            t8030r003 as RecordStatus,
            t8030f001 as LocationID,
            t8030f002 as LocationName,
            t8030f003 as ParentMenuID,
            t8030f004 as LocationType,
            t8030f005 as CameraPosition,
            t8030f006 as ControlTarget,
            t8030f007 as ObjectName,
            t8030f008 as Zone
        ";
        #endregion
        #region "t8031fieldselect"
        private string t8031fieldselect = @"
            t8031r001 as RecordID,
            t8031r002 as RecordTimestamp,
            t8031r003 as RecordStatus,
            t8031f001 as ParentID,
            t8031f002 as ReaderID,
            t8031f003 as AntennaNo,
            t8031f004 as MaxRSSI
        ";
        #endregion
        public DbRawSqlQuery<Location_REC> GetListOrderByType(DatabaseContext oRemoteDB)
        {
            string sSQL = "select " + t8030fieldselect + " from t8030 order by t8030f004, t8030f001 asc";
            var vQuery = oRemoteDB.Database.SqlQuery<Location_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<LocationDetail_REC> GetListDetail(DatabaseContext oRemoteDB, long ParentRecordID)
        {
            string sSQL = "select " + t8031fieldselect + ", t8030f001 as LocationID, t8030f002 as LocationName from t8031 LEFT JOIN t8030 on t8030r001 = t8031f001 where t8031f001 = " + ParentRecordID;
            var vQuery = oRemoteDB.Database.SqlQuery<LocationDetail_REC>(sSQL);
            return vQuery;
        }
        public DbRawSqlQuery<Location_REC> GetAntennaLocation(DatabaseContext oRemoteDB)
        {
            //string sSQL = "select t8030f001 as LocationID,t8030f002 as LocationName from t70206 JOIN t8030 on t70206f006 = t8030f001 GROUP BY t8030f001,t8030f002";
            string sSQL = "select t8030f001 as LocationID,t8030f002 as LocationName from t8030 where (t8030f001 like '001003%' or t8030f001 like '001007%') and t8030f004 = 'ROOM' GROUP BY t8030f001,t8030f002";
            var vQuery = oRemoteDB.Database.SqlQuery<Location_REC>(sSQL);
            return vQuery;
        }
    }
}