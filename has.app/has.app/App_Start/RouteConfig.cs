﻿using has.app.Components;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.app
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            Constants.MqttClient = new MqttClient(WebConfigurationManager.AppSettings["server_mqtt"].ToString());
            Constants.MqttClient.Subscribe(new string[] { "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            Constants.MqttClient.MqttMsgPublishReceived += has.app.Controllers.HomeController.Client_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            Constants.MqttClient.Connect(clientId);

            Constants.RT = GlobalHost.ConnectionManager.GetHubContext<AppHub>();
           
        }
    }
}
